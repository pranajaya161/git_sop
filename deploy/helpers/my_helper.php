<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function getModul($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT namaModul FROM tblmodul WHERE idModul = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->namaModul;
	return $nama;
}

function getSatuan($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT namaSatuan FROM tblsatuan WHERE idSatuan = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->namaSatuan;
	return $nama;
}
function getKategori($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT keterangan FROM tblkategori WHERE idKategori = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->keterangan;
	return $nama;
}
function getStyle($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT namaStyle FROM tblstyle WHERE idStyle = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->namaStyle;
	return $nama;
}
function getStylePict($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT pict FROM tblstyle WHERE idStyle = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->pict;
	return $nama;
}
function getBahan($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT namaBahan FROM tblbahan WHERE idBahan = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->namaBahan;
	return $nama;
}
//=== Table Client
function getClient($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT namaClient FROM tblclient WHERE idClient = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->namaClient;
	return $nama;
}
function getClient_init($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT initClient FROM tblclient WHERE idClient = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->initClient;
	return $nama;
}
//=== End Table Client
function getInformasi($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT keterangan FROM tblinformasi WHERE idInformasi = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->keterangan;
	return $nama;
}
function getPoRefno($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT refNo FROM mkt_po_sm WHERE idPo = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->refNo;
	return $nama;
}  
function getPoNoPo($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT noPo FROM mkt_po_sm WHERE idPo = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->noPo;
	return $nama;
}
//=== RefNo
function getRefNoProspecting($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT refNo FROM mkt_pros_sm WHERE idProspecting = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->refNo;
	return $nama;
}

function getRefNo($kode, $init, $bulan, $tahun){
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select max(idProspecting) as idProspecting from mkt_pros_sm where month(tgl_proses) and year(tgl_proses)")->row();
	$MaksID 	= $noUrut->idProspecting + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$init.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
}
function getRefNoPo($kode, $init, $bulan, $tahun){
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select count(*) as nom from mkt_po_sm where month(tanggal) and year(tanggal)")->row();
	$MaksID 	= $noUrut->nom + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$init.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
}
function getRefnoSmp($kode, $bulan, $tahun){
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select max(idSample) as nom from mkt_pros_sample where month(tgl_proses) and year(tgl_proses)")->row();
	$MaksID 	= $noUrut->nom + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
}
