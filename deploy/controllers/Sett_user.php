<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sett_user extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_setting');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'sett_user';
		$data['title']		= 'Setting --> User';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_setting->show();
		$this->template->load('role','isi','setting/v_sett_user',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'sett_user';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->input->post('userId'));
		$cek = $this->db->query("select count(*) as nom from users where userId='$userId'")->row();
		if($cek->nom > 0)
		{
			$this->session->set_flashdata('pesan','Error, UserId sudah ada...');
			redirect($aplikasi,'refresh');
		}
		else 
		{
			$this->db->trans_begin();
				$data = array(
					'idUser'=>0,
					'userId'=>addslashes($this->input->post('userId')),
					'nama'=>addslashes($this->input->post('nama')),
					'password'=>get_hash($this->input->post('password')),
					'email'=>addslashes($this->input->post('email')),
					'idModul '=>addslashes($this->input->post('idModul')),
					'isActive '=>1,
				);
				$this->m_setting->add_user($data);
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
				redirect($aplikasi,'refresh');
			} else {
				$this->db->trans_commit();
				$this->session->set_flashdata('pesan','Data berhasil di tambah');
				redirect($aplikasi,'refresh');
			}
		}
	}
	public function edit()
	{
		$aplikasi 			= 'sett_user';
		$data['aplikasi'] 	= $aplikasi;
		$idUser				= addslashes($this->input->post('idUser'));
		$this->db->trans_begin();
			$data = array(
				'nama'=>addslashes($this->input->post('nama')),
				'password'=>get_hash($this->input->post('password')),
				'email'=>addslashes($this->input->post('email')),
				'idModul '=>addslashes($this->input->post('idModul')),
				'isActive '=>addslashes($this->input->post('isActive')),
			);
			$this->m_setting->edit_user($idUser, $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Ubah');
			redirect($aplikasi,'refresh');
		}	
	}
	
}