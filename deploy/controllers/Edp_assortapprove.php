<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edp_assortapprove extends CI_Controller {
	private $db2;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_edp');
		$this->db2 = $this->load->database('mega', TRUE);
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'edp_assortapprove';
		$data['title']		= 'Edp : Assortment Transfer Approve';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_edp->show_edp_assort_apv();
		$this->template->load('role','isi','edp/v_edp_assort_apv', $data);
	}
	

}