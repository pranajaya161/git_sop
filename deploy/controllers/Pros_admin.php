<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pros_admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_prospecting');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'pros_admin';
		$data['title']		= 'Marketing --> Admin Activity';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_prospecting->show_admin_act();
		$this->template->load('role','isi','prospecting/v_pros_admin',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'pros_admin';
		$data['aplikasi'] 	= $aplikasi;
		$kode				= 'MKT-PROS';
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$status 			= 'MKT_ADM';
		$clientInit			= getClient_init(addslashes($this->input->post('idClient')));
		$this->db->trans_begin();
			$data = array(
				'idProspecting'=>0,
				'tanggal'=>addslashes($this->input->post('tanggal')),
				'refNo'=>getRefno($kode, $clientInit, $bulan, $tahun),
				'idClient'=>addslashes($this->input->post('idClient')),
				'idInformasi'=>addslashes($this->input->post('idInformasi')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>$status,
				'userId'=>$userId,
				'isActive'=>1,
			);
			$this->m_prospecting->tambah_admin_act($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$aplikasi 			= 'pros_admin';
		$data['aplikasi'] 	= $aplikasi;
		$kode				= 'MKT-PROS';
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$status 			= 'MKT_ADM';
		$clientInit			= getClient_init(addslashes($this->input->post('idClient')));
		$this->db->trans_begin();
			$data = array(
				'idClient'=>addslashes($this->input->post('idClient')),
				'idInformasi'=>addslashes($this->input->post('idInformasi')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>$status,
				'userId'=>$userId,
				'isActive'=>addslashes($this->input->post('isActive'))
			);
			$this->m_prospecting->edit_admin_act(addslashes($this->input->post('idProspecting')) , $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Update');
			redirect($aplikasi,'refresh');
		}
	}
	public function add_detail()
	{
		$aplikasi 			= 'pros_admin/add_detail/'.$this->uri->segment(3);
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$idProspecting		= addslashes($this->uri->segment(3));
		$data['data']		= $this->m_prospecting->show_admin_head($idProspecting);
		$data['detail']		= $this->m_prospecting->show_admin_det($idProspecting);
		$this->template->load('role','isi','prospecting/v_pros_admin_add',$data);
	}
	public function tambah_det()
	{
		$aplikasi 			= 'pros_admin/add_detail/'.addslashes($this->input->post('idProspecting'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$idProspecting		= addslashes($this->input->post('idProspecting'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$nourut = $this->db->query("select max(nourut) as nom from mkt_pros_dt where idProspecting='$idProspecting'")->row();
			$data = array(
				'idProspecting'=>addslashes($this->input->post('idProspecting')),
				'nourut'=>($nourut->nom + 1),
				'status'=>addslashes($this->input->post('status')),
				'tgl_target'=>addslashes($this->input->post('tgl_target')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'userId'=>$userId,
				'isActive'=>1
			);
			$this->m_prospecting->tambah_admin_act_det($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus()
	{
		$aplikasi 			= 'pros_admin/add_detail/'.addslashes($this->input->post('idProspecting'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idProspecting		= addslashes($this->input->post('idProspecting'));
		$nourut				= addslashes($this->input->post('nourut'));
		$this->db->trans_begin();
			$this->db->query("delete from mkt_pros_dt where idProspecting='$idProspecting' and  nourut ='$nourut'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di hapus');
			redirect($aplikasi,'refresh');
		}
	}
	public function cari_prospecting()
	{
		$data['title']	= 'Data Prospecting';
		$data['data']	= $this->m_prospecting->show_admin_act();
		$this->load->view('prospecting/v_prospecting_cari', $data);
	}
}