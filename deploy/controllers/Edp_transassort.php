<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edp_transassort extends CI_Controller {
	private $db2;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_edp');
		$this->db2 = $this->load->database('mega', TRUE);
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'edp_transassort';
		$data['title']		= 'Edp : Assortment Transfer';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_edp->show_edp_assort_trans();
		$this->template->load('role','isi','edp/v_edp_assort_trans', $data);
	}
	public function add()
	{
		$aplikasi 			= 'edp_transassort';
		$data['title']		= 'Edp : Assortment Transfer';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_edp->show_edp_assort_trans_id($this->uri->segment(3));
		$this->template->load('role','isi','edp/v_edp_assort_trans_add', $data);
	}
	public function add_detail()
	{
		$aplikasi 			= 'edp_transassort/add/'.addslashes($this->input->post('import_id'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$nama_pakaian		= addslashes($this->input->post('nama_pakaian'));
		$import_id			= addslashes($this->input->post('import_id'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$status = addslashes($this->input->post('status'));
		$this->db->trans_begin();
			$cekdata=$this->db->query("select count(*) as unit from tblstyle where namaStyle='$nama_pakaian'")->row();
			if($cekdata->unit==0){
				$data=array(
					'idStyle'=>0,
					'namaStyle'=>$nama_pakaian,
					'isActive'=>1,
					'pict'=>'foto.jpg',
					'status'=>'style',
					'userId'=>$userId
				);
				$this->db->insert('tblstyle',$data);
			}
			$style	= $this->db->query("select * from tblstyle where namaStyle='$nama_pakaian'")->row();	
			$idStyle = $style->idStyle;
			$query=$this->db->query("select * from edp_assortment_dt a where a.import_id='$import_id'  and a.nama_pakaian='$nama_pakaian' and artikel='' order by nik, code_ukuran, size, ukuran_f1, ukuran_f2")->result();
			foreach($query as $b)
			{
				$num =1;
				$qty=$b->qty;
				$idPo=$b->idPo;
				for($i=0;$i<$qty;$i++){
					$num++;
					$data=array(
					'id'=>0,
					'import_id'=>$b->import_id,
					'nourut'=>$num,
					'nama'=>$b->nama,
					'nik'=>$b->nik,
					'nama_pakaian'=>$b->nama_pakaian,
					'lokasi'=>$b->lokasi,
					'jabatan'=>$b->jabatan,
					'keterangan'=>$b->keterangan,
					'info_tambahan'=>'EDP',
					'size'=>$b->size,
					'nama_ukuran_f1'=>$b->nama_ukuran_f1,
					'alias_ukuran_f1'=>$b->alias_ukuran_f1, 
					'ukuran_f1'=>$b->ukuran_f1,
					'nama_ukuran_f2'=>$b->nama_ukuran_f2,
					'alias_ukuran_f2'=>$b->alias_ukuran_f2, 
					'ukuran_f2'=>$b->ukuran_f2,
					'nama_ukuran_f3'=>$b->nama_ukuran_f3,
					'alias_ukuran_f3'=>$b->alias_ukuran_f3, 
					'ukuran_f3'=>$b->ukuran_f3,
					'nama_ukuran_f4'=>$b->nama_ukuran_f4,
					'alias_ukuran_f4'=>$b->alias_ukuran_f4, 
					'ukuran_f4'=>$b->ukuran_f4,
					'nama_ukuran_f5'=>$b->nama_ukuran_f5,
					'alias_ukuran_f5'=>$b->alias_ukuran_f5, 
					'ukuran_f5'=>$b->ukuran_f5,
					'nama_ukuran_f6'=>$b->nama_ukuran_f6,
					'alias_ukuran_f6'=>$b->alias_ukuran_f6, 
					'ukuran_f6'=>$b->ukuran_f6,
					'nama_ukuran_f7'=>$b->nama_ukuran_f7,
					'alias_ukuran_f7'=>$b->alias_ukuran_f7, 
					'ukuran_f7'=>$b->ukuran_f7,
					'nama_ukuran_f8'=>$b->nama_ukuran_f8,
					'alias_ukuran_f8'=>$b->alias_ukuran_f8, 
					'ukuran_f8'=>$b->ukuran_f8,
					'nama_ukuran_f9'=>$b->nama_ukuran_f9,
					'alias_ukuran_f9'=>$b->alias_ukuran_f9, 
					'ukuran_f9'=>$b->ukuran_f9,
					'nama_ukuran_f10'=>$b->nama_ukuran_f10,
					'alias_ukuran_f10'=>$b->alias_ukuran_f10, 
					'ukuran_f10'=>$b->ukuran_f10,
					'idStyle'=>$idStyle,
					'artikel'=>'',
					'idPo'=>$b->idPo,
					'idDept'=>'5',
					'status' =>'OPEN',
					'userId' =>$userId,
					'isActive' =>0
					);
					$this->db->insert('edp_assortment_pros',$data);
					$this->db->query("update edp_assortment_dt set artikel=1 where import_id='$import_id'  and nama_pakaian='$nama_pakaian' and artikel=''");
				}
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Kirim');
			redirect($aplikasi,'refresh');
		}
	}

}