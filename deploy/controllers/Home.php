<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$data['title']='<title> danaIN -> Home</title>';
		$this->template->load('role','isi','v_home',$data);
		//$this->load->view('v_home');
	}
}