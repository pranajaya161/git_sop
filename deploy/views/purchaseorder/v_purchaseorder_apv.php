<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><b><?php echo $title?></b>   <a href="<?php echo base_URL()?><?php echo $aplikasi?>/add" class="btn btn-xs btn-info"  style="float: right">+ Tambah</a></p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th>No</th>
								<th>Tgl.Po</th>
								<th>Tgl.Kirim</th>
								<th>RefNo</th>
								<th>Buyer</th>
								<th>No.PO</th>
								<th>No.OR</th>
								<th>Jenis</th>
								<th>Keterangan</th>
								<th>Qty</th>
								<th>Nominal</th>
								<th>Satuan</th>
								<th>Unit</th>
								<th>Tgl.Proses</th>
								<th>UserId</th>
								<th style="text-align: center">Approve</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach ($data as $a): ?>
							<tr class="odd gradeX">
								<td><?php echo $i++;?></td>
								<td><?php echo $a->tanggal;?></td>
								<td><?php echo $a->tglKirim;?></td>
								<td><a href="#" data-toggle="modal" data-target="#modal_view<?php echo $a->idPo?>"><?php echo $a->refNo;?></a></td>
								<td><?php echo getClient($a->idClient);?></td>
								<td><?php echo $a->noPo;?></td>
								<td><?php echo $a->noOr;?></td>
								<td><?php echo $a->jenis;?></td>
								<td><?php echo $a->keterangan;?></td>
								<td><?php echo $a->qty;?></td>
								<td><?php echo $a->amount;?></td>
								<td><?php echo $a->satuan;?></td>
								<td><?php echo $a->pcs;?></td>
								<td><?php echo $a->tgl_proses;?></td>
								<td><?php echo $a->userId;?></td>
								<td style="text-align: center">
									<a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_approve<?php echo $a->idPo?>"><i class="fa fa-check"></i></a>
								</td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<?php foreach($data as $b): $idPo=$b->idPo; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_approve<?php echo $b->idPo?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> Approve <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<form name="cabang" action="<?php echo base_URL()?>mkt_purchaseorder_apv/Approval" method="post" accept-charset="utf-8" enctype="multipart/form-data">
							<input type="hidden" name="idPo" value="<?php echo $idPo?>">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<label for="exampleInputEmail1">Tanggal</label>
										<input type="text" name="tanggal" value="<?php echo date('Y-m-d')?>" class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
									</div>	
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label for="exampleInputEmail1">Status</label>
										<select name="status" class="form-control">
											<option value="1">Approve</option>
											<option value="0">Reject</option>
										</select>
									</div>	
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label for="exampleInputEmail1">Keterangan</label>
										<input type="text" name="keterangan"  class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" requied>
									</div>	
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<button type="submit" class="btn btn-success">Approve</button>
										<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
									</div>	
								</div>	
							</div>
						</form>	
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End View -->
<?php foreach($data as $b): $idProspecting=$b->idProspecting; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_view<?php echo $b->idProspecting?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Status</th>
									<th>Tgl.Input</th>
									<th>Tgl.Target</th>
									<th>Keterangan</th>
									<th>User</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$lihat  = $this->db->query("select * from mkt_pros_dt where idProspecting='$idProspecting' order by tgl_proses")->result();
							foreach($lihat as $x):
							?>	
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo $x->status?></td>
									<td><?php echo $x->tgl_input?></td>
									<td><?php echo $x->tgl_target?></td>
									<td><?php echo $x->keterangan?></td>
									<td><?php echo $x->userId?></td>
							<?php endforeach;?>
							</tbody>
						</table>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End View -->
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
