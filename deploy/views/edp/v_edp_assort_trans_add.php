<head>
    meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
foreach ($data as $a):
  	$import_id			= $a->import_id;
  	$project_no 		= $a->project_no;
  	$lot_number			= $a->lot_number;
  	$filename 			= $a->filename;
  	$nama_perusahaan 	= $a->nama_perusahaan;
   	$total_records 		= $a->total_records;
    $import_datetime 	= $a->import_datetime;
endforeach;
$mode		= $this->uri->segment(4);

?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b>Project.No : <?php echo $project_no?> | Lot.No : <?php echo $lot_number?> | Buyer : <?php echo $nama_perusahaan?> </b>
                </div>
                <div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
						<form name="cabang" action="<?php echo base_URL()?>edp_transassort/add/<?php echo $import_id?>/generate" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
							<input type="hidden" name="import_id" value="<?php echo $import_id?>">
							<div class="col-lg-4">
								<div class="form-group">
									<label for="exampleInputEmail1">Nama Style</label>
									<select name="nama_pakaian" class="form-control">
									<?php
									$lihat=$this->db->query("select nama_pakaian from edp_assortment_dt where import_id='$import_id' and artikel='' group by nama_pakaian")->result();
									foreach($lihat as $a):
									?>
									<option value="<?php echo $a->nama_pakaian?>"><?php echo $a->nama_pakaian?></option>
								<?php endforeach;?>
									</select>
								</div>
							</div>	
							<div class="col-lg-4">
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-primary">Generate</button>
									<a href="<?php echo base_URL()?>edp_assort" class="btn btn-warning">Kembali</a>
								</div>
							</div>	
                       </form>
					</div>
					<?php if(!empty($mode)){
						$nama_pakaian = addslashes($this->input->post('nama_pakaian'));
						$cek=$this->db->query("select nama_ukuran_f1, nama_ukuran_f2, nama_ukuran_f3, nama_ukuran_f4, nama_ukuran_f5, nama_ukuran_f6, nama_ukuran_f7, nama_ukuran_f8,nama_ukuran_f9, nama_ukuran_f10  from edp_assortment_dt a where a.import_id='$import_id' and a.nama_pakaian='$nama_pakaian' group by nama_ukuran_f1, nama_ukuran_f2, nama_ukuran_f3, nama_ukuran_f4, nama_ukuran_f5, nama_ukuran_f6, nama_ukuran_f7, nama_ukuran_f8,nama_ukuran_f9, nama_ukuran_f10")->row();
						?>
						<div class="row mt">
				        	<div class="col-lg-12">
								<div class="form-panel">
									<p><?php echo $nama_pakaian?></p>
									<form action="<?php echo base_URL()?>edp_transassort/add_detail" method="post" class="form-horizontal" accept-charset="utf-8" enctype="multipart/form-data">
									<table class="table table-bordered table-striped table-condensed">
										<input type="hidden" name="import_id" value="<?php echo $import_id?>" />
										<input type="hidden" name="nama_pakaian" value="<?php echo $nama_pakaian?>" />
										<tr>
											<th style="text-align:left">Size</th>	
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f1?></th>							
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f2?></th>
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f3?></th>							
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f4?></th>
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f5?></th>							
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f6?></th>
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f7?></th>							
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f8?></th>
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f9?></th>							
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f10?></th>
											<th style="text-align:right">Qty</th>
											<th style="text-align:left">Numbering</th>									
										</tr>
										<?php
										$no=1;
										$query=$this->db->query("select code_ukuran, size, ukuran_f1, ukuran_f2, ukuran_f3, ukuran_f4, ukuran_f5, ukuran_f6, ukuran_f7, ukuran_f8, ukuran_f9, ukuran_f10, sum(qty) as tqty from edp_assortment_dt a where a.import_id='$import_id' and a.nama_pakaian='$nama_pakaian' and ukuran=0 group by code_ukuran order by code_ukuran, size, ukuran_f1, ukuran_f2, ukuran_f3, ukuran_f4, ukuran_f5, ukuran_f6, ukuran_f7, ukuran_f8, ukuran_f9, ukuran_f10")->result();
										foreach($query as $b){
											$code_ukuran=$b->code_ukuran;
											$tqty=$tqty+$b->tqty;
										?>
										<tr>
											<td ><?php echo $b->size;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f1;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f2;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f3;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f4;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f5;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f6;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f7;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f8;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f9;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f10;?></td>
											<td style="text-align:right"><?php echo number_format($b->tqty);?></td>
											<?php if($code_ukuran==1){ ?>
												<td><?php echo $code_ukuran?> Of <?php echo $tqty?>  </td>
											<?php } else { ?>
												<td><?php echo ($tqty + 1)-$b->tqty?> Of <?php echo $tqty?>  </td>
											<?php } ?>
										</tr>
										<?php $no++;
											$txqty=$txqty+$b->tqty;
										} ?>
										<tr class="tabhead">
											<th colspan="11">Total</th>
											<th style="text-align:right"><?php echo number_format($txqty)?></th>
											<th ></th>
										</tr>
									</table>	
									<p><input name="submit" type="submit" class="button" value="Proses" onClick="return confirm('Data akan di Proses...?');">&nbsp;&nbsp;</p>
								</form>
								</div>
							</div>
						</div>
					<?php } ?>	
					<div class="row">
						<div class="panel-body">
							<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>No.</th>
										<th style="text-align:left">Style</th>
										<th style="text-align:left">Lokasi</th>
										<th style="text-align:left">Jabatan</th>
										<th style="text-align:right">Record</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no=1;
									$detail = $this->db->query("select idStyle, lokasi, jabatan, count(*) as nom from edp_assortment_pros where import_id='$import_id' and status ='OPEN' group by idStyle, lokasi, jabatan order by idStyle")->result();
									foreach($detail as $a){
									?>
										<tr>
											<td style="text-align:right"><?php echo $no?></td>
											<td><?php echo getStyle($a->idStyle)?></td>
											<td><?php echo $a->lokasi?></td>
											<td><?php echo $a->jabatan?></td>
											<td style="text-align:right"><?php echo number_format($a->nom)?></td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchStyle(){
	openWindow('<?php echo base_URL()?>desg_request/cari','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedsStyle(idStyle, namaStyle, pict){   
    document.getElementById('idStyle').value = idStyle;
    document.getElementById('namaStyle').value = namaStyle;
	document.getElementById('pict').value = pict;
}
</script>