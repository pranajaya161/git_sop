<head>
    meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
foreach ($data as $a):
  $idProspecting	= $a->idProspecting;
  $tanggal 			= $a->tanggal;
  $refNo			= $a->refNo;
  $keterangan 		= $a->keterangan;
endforeach;
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<legend class="page-header"><?php echo $title?></legend>
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
					<form name="cabang" action="<?php echo base_URL()?><?php echo $aplikasi?>/tambah" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
							<div class="col-lg-4">
								<input type="hidden" name="idSample" value="0" />
								<div class="form-group">
									<label>Tgl Target</label>
									<input type="text" class="form-control" name="tgl_target" value="<?php echo date('Y-m-d')?>" id="input1" placeholder="Tgl.Target">
	                            </div>
	                            <div class="form-group">
									<label class="col-sm-12 control-label">Prospecting&nbsp;&nbsp;&nbsp;<a href="#" onclick="searchProspecting()"><span><i class="fa fa-search"></i></span></a></label>
									<input type="hidden" name="idProspecting" id="idProspecting" class="tfield" readonly />
									<input type="text" class="form-control" name="refNo" id="refNo" readonly>
	                            </div>
	                            <div class="form-group">
									<label>Keterangan</label>
									<input type="text" class="form-control" name="keterangan"  placeholder="Keterangan" required>
	                            </div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="exampleInputEmail1">Buyer</label>
									<input class="form-control" name="idClient" type="hidden" id="idClient"  readonly>
									<input class="form-control" name="namaClient" type="text" id="namaClient"  readonly>
								</div>
								<div class="form-group">
									<label>Item</label>
									<input type="text" class="form-control" name="item"  placeholder="Item" required>
	                            </div>
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-primary">Simpan</button>
									<a href="<?php echo base_URL()?>pros_admin" class="btn btn-warning">Kembali</a>
								</div>
							</div>	
                       </form>
					</div>
					<hr>
					<div class="row">
						<div class="panel-body">
							<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>No</th>
										<th>Ref.No</th>
										<th>Tgl.Target</th>
										<th>Item</th>
										<th>Keterangan</th>
										<th>User Id</th>
										<th>Tgl.Proses</th>
										<th style="text-align: center">Upload</th>
										<th style="text-align: center">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($data as $b): ?>
							  		<tr>
							  			<td><?php echo $i++;?></td>
							  			<td><?php echo getRefNoProspecting($b->idProspecting);?></td>
							  			<td><?php echo $b->tgl_target;?></td>
							  			<td><?php echo $b->item;?></td>
							  			<td><?php echo $b->keterangan;?></td>
							  			<td><?php echo $b->userId;?></td>
							  			<td><?php echo $b->tgl_proses;?></td>
							  			<td style="text-align: center">
											<form name="cabang" action="<?php echo base_URL()?><?php echo $aplikasi?>/sample_bahan/<?php echo $b->idSample?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
												<input type="hidden" name="idSample" value="<?php echo $b->idSample?>" />
												<button type="submit" class="btn btn-info"><i class="fa fa-upload"></i></button>
											</form>	
										</td>
							  			<td style="text-align: center">
											<form name="cabang" action="<?php echo base_URL()?><?php echo $aplikasi?>/hapus" method="post" accept-charset="utf-8" enctype="multipart/form-data">
												<input type="hidden" name="idSample" value="<?php echo $b->idSample?>" />
												<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
											</form>	
										</td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchProspecting(){
	openWindow('<?php echo base_URL()?>pros_admin/cari_prospecting','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedProspecting(idProspecting, refNo, idClient, namaClient){   
    document.getElementById('idProspecting').value = idProspecting;
    document.getElementById('refNo').value = refNo;
	document.getElementById('idClient').value = idClient;
	document.getElementById('namaClient').value = namaClient;
}
</script>