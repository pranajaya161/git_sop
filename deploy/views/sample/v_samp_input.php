<head>
    meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
foreach ($data as $a):
  $idProspecting	= $a->idProspecting;
  $tanggal 			= $a->tanggal;
  $refNo			= $a->refNo;
  $keterangan 		= $a->keterangan;
endforeach;
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<legend class="page-header"><?php echo $title?></legend>
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
					</div>
					<hr>
					<div class="row">
						<div class="panel-body">
							<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>No</th>
										<th>Ref.No</th>
										<th>Tgl.Target</th>
										<th>Item</th>
										<th>Keterangan</th>
										<th>User Id</th>
										<th>Tgl.Proses</th>
										<th style="text-align: center">Upload</th>
										<th style="text-align: center">Kirim</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($data as $b): ?>
							  		<tr>
							  			<td><?php echo $i++;?></td>
							  			<td><?php echo getRefNoProspecting($b->idProspecting);?></td>
							  			<td><?php echo $b->tgl_target;?></td>
							  			<td><?php echo $b->item;?></td>
							  			<td><?php echo $b->keterangan;?></td>
							  			<td><?php echo $b->userId;?></td>
							  			<td><?php echo $b->tgl_proses;?></td>
							  			<?php if($b->status > 0) { ?>
							  				<td></td>
							  				<td></td>
							  			<?php } else { ?>	
							  			<td style="text-align: center">
											<form name="cabang" action="<?php echo base_URL()?><?php echo $aplikasi?>/sample_style/<?php echo $b->idSample?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
												<input type="hidden" name="idSample" value="<?php echo $b->idSample?>" />
												<button type="submit" class="btn btn-warning"><i class="fa fa-upload"></i></button>
											</form>	
										</td>
							  			<td style="text-align: center">
											<form name="cabang" action="<?php echo base_URL()?><?php echo $aplikasi?>/kirim" method="post" accept-charset="utf-8" enctype="multipart/form-data">
												<input type="hidden" name="idSample" value="<?php echo $b->idSample?>" />
												<button type="submit" class="btn btn-info"><i class="fa fa-send"></i></button>
											</form>	
										</td>
									<?php } ?>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchProspecting(){
	openWindow('<?php echo base_URL()?>pros_admin/cari_prospecting','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedProspecting(idProspecting, refNo, idClient, namaClient){   
    document.getElementById('idProspecting').value = idProspecting;
    document.getElementById('refNo').value = refNo;
	document.getElementById('idClient').value = idClient;
	document.getElementById('namaClient').value = namaClient;
}
</script>