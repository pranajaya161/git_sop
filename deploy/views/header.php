<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>SOP-Megacorp</title>
		<link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
		<link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
		<link href="<?php echo base_URL()?>vendor/morrisjs/morris.css" rel="stylesheet">
		<link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	</head>
	<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_URL()?>home">IT-danaIN</a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="<?php echo base_URL()?>home"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
						<li>
                            <a href="#"><i class="fa fa-cogs fa-fw"></i> Setting<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_URL()?>sett_user">User Aplikasi</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_URL()?>sett_menu">Menu</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_URL()?>sett_modul">Modul</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cog fa-fw"></i> Parameter<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_URL()?>param_bahan">Params Bahan</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_URL()?>param_design">Params Design</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_URL()?>param_sample">Params Sample</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_URL()?>param_style">Params Style</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_URL()?>param_buyer">Params Buyer</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Marketing<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Prospecting<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>pros_admin">&nbsp;&nbsp;Admin Activity</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>pros_admin">&nbsp;&nbsp;Approve Cust Design</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li><a href="<?php echo base_URL()?>pros_apv_cust">Approve Cust Design</a>
                                </li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Cost Calculation<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>pros_upl_cost">&nbsp;&nbsp;Upload Cost Calculation</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>pros_apv_cost">&nbsp;&nbsp;Approve Cost Calculation</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Purchase Order<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>mkt_purchaseorder">&nbsp;&nbsp;Input Purchase Order</a>
                                        </li>
                                   </ul>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>mkt_purchaseorder_apv">&nbsp;&nbsp;Approve Purchase Order</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <!-- Design -->
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Design<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Design Request<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>desg_request">&nbsp;&nbsp;Upload Design </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>desg_approve">&nbsp;&nbsp;Approve Design </a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Sample Request<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>samp_request">&nbsp;&nbsp;Request </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>samp_approve">&nbsp;&nbsp;Approve Request </a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <!--- Sample -->
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Sample<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Sample Request<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>smp_input">&nbsp;&nbsp;Upload Sample </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>samp_approve_input">&nbsp;&nbsp;Approve Sample </a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Edp<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Assortment<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>edp_assort">&nbsp;&nbsp;EDP1 </a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="<?php echo base_URL()?>edp_transassort">Transfer Data Assortment</a></li>
                                <li><a href="<?php echo base_URL()?>edp_assortapprove">Approve Assortment</a></li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>