-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 14, 2019 at 10:53 AM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.15-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sop`
--

-- --------------------------------------------------------

--
-- Table structure for table `edp_assortment`
--

CREATE TABLE `edp_assortment` (
  `import_id` int(11) NOT NULL,
  `project_no` varchar(70) NOT NULL DEFAULT '',
  `lot_number` int(11) NOT NULL DEFAULT '1',
  `filename` varchar(70) NOT NULL,
  `nama_perusahaan` varchar(70) NOT NULL,
  `is_data_completed` varchar(1) NOT NULL DEFAULT 'N',
  `total_records` int(11) NOT NULL DEFAULT '0',
  `import_datetime` datetime NOT NULL,
  `pc` varchar(15) NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL,
  `isActive` int(11) NOT NULL,
  `idPo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `edp_assortment`
--

INSERT INTO `edp_assortment` (`import_id`, `project_no`, `lot_number`, `filename`, `nama_perusahaan`, `is_data_completed`, `total_records`, `import_datetime`, `pc`, `tgl_proses`, `userId`, `isActive`, `idPo`) VALUES
(8, 'ANGKASA PURA 2', 3, 'IMPORT MD LOT3.xlsx', 'AP02 SP', 'Y', 188, '2017-06-02 09:58:37', 'EDP01', '2019-02-09 07:21:34', 'Guest', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `edp_assortment_dt`
--

CREATE TABLE `edp_assortment_dt` (
  `import_id` int(11) NOT NULL DEFAULT '0',
  `nama` varchar(50) NOT NULL DEFAULT '',
  `nik` varchar(20) NOT NULL DEFAULT '',
  `nama_pakaian` varchar(50) NOT NULL DEFAULT '',
  `lokasi` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `info_tambahan` varchar(50) DEFAULT NULL,
  `qty` int(11) DEFAULT '1',
  `size` varchar(11) DEFAULT '',
  `nama_ukuran_f1` varchar(50) DEFAULT NULL,
  `alias_ukuran_f1` varchar(50) DEFAULT NULL,
  `ukuran_f1` float DEFAULT '0',
  `nama_ukuran_f2` varchar(50) DEFAULT NULL,
  `alias_ukuran_f2` varchar(50) DEFAULT NULL,
  `ukuran_f2` float DEFAULT '0',
  `nama_ukuran_f3` varchar(50) DEFAULT NULL,
  `alias_ukuran_f3` varchar(50) DEFAULT NULL,
  `ukuran_f3` float DEFAULT '0',
  `nama_ukuran_f4` varchar(50) DEFAULT NULL,
  `alias_ukuran_f4` varchar(50) DEFAULT NULL,
  `ukuran_f4` float DEFAULT '0',
  `nama_ukuran_f5` varchar(50) DEFAULT NULL,
  `alias_ukuran_f5` varchar(50) DEFAULT NULL,
  `ukuran_f5` float DEFAULT '0',
  `nama_ukuran_f6` varchar(50) DEFAULT NULL,
  `alias_ukuran_f6` varchar(50) DEFAULT NULL,
  `ukuran_f6` float DEFAULT '0',
  `nama_ukuran_f7` varchar(50) DEFAULT NULL,
  `alias_ukuran_f7` varchar(50) DEFAULT NULL,
  `ukuran_f7` float DEFAULT '0',
  `nama_ukuran_f8` varchar(50) DEFAULT NULL,
  `alias_ukuran_f8` varchar(50) DEFAULT NULL,
  `ukuran_f8` float DEFAULT '0',
  `nama_ukuran_f9` varchar(50) DEFAULT NULL,
  `alias_ukuran_f9` varchar(50) DEFAULT NULL,
  `ukuran_f9` float DEFAULT '0',
  `nama_ukuran_f10` varchar(50) DEFAULT NULL,
  `alias_ukuran_f10` varchar(50) DEFAULT NULL,
  `ukuran_f10` float DEFAULT '0',
  `datetime_uploaded` datetime DEFAULT NULL,
  `ukuran` varchar(50) DEFAULT '',
  `code_ukuran` int(50) DEFAULT '0',
  `no_karton` int(11) NOT NULL DEFAULT '0',
  `karton_id` int(11) NOT NULL DEFAULT '0',
  `artikel` varchar(35) NOT NULL,
  `idPo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `edp_assortment_dt`
--

INSERT INTO `edp_assortment_dt` (`import_id`, `nama`, `nik`, `nama_pakaian`, `lokasi`, `jabatan`, `keterangan`, `info_tambahan`, `qty`, `size`, `nama_ukuran_f1`, `alias_ukuran_f1`, `ukuran_f1`, `nama_ukuran_f2`, `alias_ukuran_f2`, `ukuran_f2`, `nama_ukuran_f3`, `alias_ukuran_f3`, `ukuran_f3`, `nama_ukuran_f4`, `alias_ukuran_f4`, `ukuran_f4`, `nama_ukuran_f5`, `alias_ukuran_f5`, `ukuran_f5`, `nama_ukuran_f6`, `alias_ukuran_f6`, `ukuran_f6`, `nama_ukuran_f7`, `alias_ukuran_f7`, `ukuran_f7`, `nama_ukuran_f8`, `alias_ukuran_f8`, `ukuran_f8`, `nama_ukuran_f9`, `alias_ukuran_f9`, `ukuran_f9`, `nama_ukuran_f10`, `alias_ukuran_f10`, `ukuran_f10`, `datetime_uploaded`, `ukuran`, `code_ukuran`, `no_karton`, `karton_id`, `artikel`, `idPo`) VALUES
(8, 'ABDUL SABAR', '20003501', 'KEMEJA L/S TEKNISI PRIA', 'PADANG', 'TEKNISI PRIA', 'PJ BJ 30 \"', 'EDP01', 3, 'SP/15,5', 'pj.tgn', 'Panjang Tangan', 33, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 3, 0, 0, '', 1),
(8, 'ACHMAD SUHARYANTO', '20003977', 'KEMEJA S/S AVSEC PRIA', 'PEKANBARU', 'AVSEC PRIA', 'CUTI TITIP. UK VIA VICTOR NAINGGOLAN (BAWA BAJU LA', 'EDP01', 3, 'SP/14,5', 'pj.tgn', 'Panjang Tangan', 18, 'lg.leher', 'Lingkar Leher', 16, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:13', '', 2, 0, 0, '', 1),
(8, 'ACHMAD SULAIMAN HARAHAP', '20005645', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'SILANGIT', 'TEKNISI PRIA', '', 'EDP01', 3, 'SP/36', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 20, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 7, 0, 0, '', 1),
(8, 'ADDINU RAHMATULLAH', '20002702', 'CELANA PANJANG AVSEC PRIA', 'PADANG', 'AVSEC PRIA', 'PB, MINT U LM', 'EDP01', 3, 'SP/43', 'pj.cln', 'Tinggi Celana panjang', 100, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 123, 'lgr.pesak', 'Vesak/ Pesak', 70, 'lg.paha', 'Lingkar paha', 80, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:14', '', 8, 0, 0, '', 1),
(8, 'ADE ADINEGARA ARMY', '20002398', 'KEMEJA S/S RFFS PRIA', 'PADANG', 'RFFS PRIA', '', 'EDP01', 3, 'SP/17', 'pj.tgn', 'Panjang Tangan', 21, 'lg.leher', 'Lingkar Leher', 19, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:34', '', 2, 0, 0, '', 1),
(8, 'ADEK FEBRI', '20002225', 'CELANA PANJANG AVSEC PRIA', 'PONTIANAK', 'AVSEC PRIA', '', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 102, 'lg.pgg', 'Lingkar Pinggang', 44, 'lg.pggl', 'Lingkar pinggul', 126, 'lgr.pesak', 'Vesak/ Pesak', 78, 'lg.paha', 'Lingkar paha', 82, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:14', '', 13, 0, 0, '', 1),
(8, 'AFFAN PERDANA HARAHAP', '20005646', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'SILANGIT', 'TEKNISI PRIA', '', 'EDP01', 3, 'SP/37', 'pj.cln', 'Tinggi Celana panjang', 88, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 18, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 8, 0, 0, '', 1),
(8, 'AGUS GUNAWAN', '20003991', 'KEMEJA S/S AVSEC PRIA', 'BANDA ACEH', 'AVSEC PRIA', '', 'EDP01', 3, 'SP/15,5', 'pj.tgn', 'Panjang Tangan', 19, 'lg.leher', 'Lingkar Leher', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:13', '', 7, 0, 0, '', 1),
(8, 'AINI FIRDAUSY', '20003190', 'CELANA PJG TNP KARET AVSEC', 'PEKANBARU', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/2L', 'pj.cln', 'Tinggi Celana panjang', 100, 'lg.pgg', 'Lingkar Pinggang', 90, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:16', '', 16, 0, 0, '', 1),
(8, 'ALZOG PENDRA BUDHI', '20001475', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'PADANG', 'PEJABAT AVSEC PRIA', '', 'EDP01', 1, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 70, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:27', '', 6, 0, 0, '', 1),
(8, 'ALZOG PENDRA BUDHI', '20001475', 'CELANA PANJANG NAVY', 'PADANG', 'PEJABAT AVSEC PRIA', '', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 70, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:27', '', 17, 0, 0, '', 1),
(8, 'ALZOG PENDRA BUDHI', '20001475', 'KEMEJA L/S', 'PADANG', 'PEJABAT AVSEC PRIA', '', 'EDP01', 1, 'SP/15,5', 'pj.tgn', 'Panjang Tangan', 33, 'lg.leher', 'Lingkar Leher', 16.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:26', '', 5, 0, 0, '', 1),
(8, 'ALZOG PENDRA BUDHI', '20001475', 'KEMEJA S/S', 'PADANG', 'PEJABAT AVSEC PRIA', '', 'EDP01', 2, 'SP/15,5', 'pj.tgn', 'Panjang Tangan', 19, 'lg.leher', 'Lingkar Leher', 16.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:26', '', 5, 0, 0, '', 1),
(8, 'AMALIUN FRIDOLIN SITUMORANG', '20005643', 'CELANA PANJANG NAVY', 'SILANGIT', 'BACK OFFICE PRIA', 'T.U P. Daniel', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 64, 'lg.paha', 'Lingkar paha', 64, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 11, 0, 0, '', 1),
(8, 'ANANDA HARAPAN T', '20004684', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'BANDA ACEH', 'TEKNISI PRIA', 'UK LAMA INFO PAK HARIONO', 'EDP01', 3, 'SP/42', 'pj.cln', 'Tinggi Celana panjang', 104, 'lg.pgg', 'Lingkar Pinggang', 42, 'lg.pggl', 'Lingkar pinggul', 124, 'lgr.pesak', 'Vesak/ Pesak', 78, 'lg.paha', 'Lingkar paha', 82, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 10, 0, 0, '', 1),
(8, 'ANGGUN WANNITA PERTIWI', '20006475', 'CELANA PJG TNP KARET AVSEC', 'PADANG', 'AVSEC WANITA JILBAB', 'LINGKAR DADA 98 JADI', 'EDP01', 3, 'SP/L', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 88, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:15', '', 10, 0, 0, '', 1),
(8, 'ANGGUN WANNITA PERTIWI', '20006475', 'KEMEJA L/S AVSEC WANITA', 'PADANG', 'AVSEC WANITA JILBAB', 'LINGKAR DADA 98 JADI', 'EDP01', 3, 'SP/14', 'pj.baju', 'Panjang Baju', 68, 'pj.tgn', 'Panjang Tangan', 58, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:15', '', 1, 0, 0, '', 1),
(8, 'ANIF NURUL SETYORINI', '20003196', 'CELANA PJG TNP KARET AVSEC', 'PEKANBARU', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/S', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 76, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:16', '', 1, 0, 0, '', 1),
(8, 'ANNA FARHANNA', '20004022', 'CELANA PJG KARET AVSEC', 'BANDA ACEH', 'AVSEC WANITA JILBAB', 'LUB.KAKI 20,5', 'EDP01', 3, 'SP/2L', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 20.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:15', '', 1, 0, 0, '', 1),
(8, 'ARFIAN FERNICKO', '20002620', 'CELANA PANJANG AVSEC PRIA', 'PONTIANAK', 'AVSEC PRIA', 'TU', 'EDP01', 3, 'SP/40', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 74, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:14', '', 7, 0, 0, '', 1),
(8, 'ARIO FUAD HARMA', '20002324', 'CELANA PANJANG NAVY', 'PONTIANAK', 'PEJABAT PRIA', '', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 41, 'lg.pggl', 'Lingkar pinggul', 124, 'lgr.pesak', 'Vesak/ Pesak', 70, 'lg.paha', 'Lingkar paha', 76, 'lg.bawah', 'Dada Bawah', 40, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:30', '', 29, 0, 0, '', 1),
(8, 'ARRIHSON BINTANG', '20006479', 'KEMEJA S/S AVSEC PRIA', 'PONTIANAK', 'AVSEC PRIA', 'BISEP 44 CM', 'EDP01', 3, 'SP/15', 'pj.tgn', 'Panjang Tangan', 19, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:13', '', 5, 0, 0, '', 1),
(8, 'BARIR BAIHAQI', '20002241', 'CELANA PANJANG AVSEC PRIA', 'PONTIANAK', 'AVSEC PRIA', 'TU/ UK LAMA', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 42, 'lg.pggl', 'Lingkar pinggul', 122, 'lgr.pesak', 'Vesak/ Pesak', 72, 'lg.paha', 'Lingkar paha', 76, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:14', '', 10, 0, 0, '', 1),
(8, 'BAYUH ISWANTORO', '20001499', 'CELANA PANJANG AVSEC PRIA', 'PONTIANAK', 'PEJABAT  GM', '', 'EDP01', 1, 'SP/34', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 19, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:25', '', 2, 0, 0, '', 1),
(8, 'BAYUH ISWANTORO', '20001499', 'CELANA PANJANG NAVY', 'PONTIANAK', 'PEJABAT  GM', '', 'EDP01', 3, 'SP/34', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 19, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:25', '', 10, 0, 0, '', 1),
(8, 'BENIATI', '20001500', 'CELANA PJG NAVY KARET', 'PEKANBARU', 'BACK OFFICE WANITA JILBAB', 'PAHA +2CM', 'EDP01', 2, 'SP/L', 'pj.cln', 'Tinggi Celana panjang', 90, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 2, 0, 0, '', 1),
(8, 'BENNI AFRIZAL', '20001501', 'CELANA PANJANG NAVY', 'PADANG', 'BACK OFFICE PRIA', 'JD, ATASAN BWU JD DAN DIUKUR ULANG N', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 77, 'lg.paha', 'Lingkar paha', 66, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 14, 0, 0, '', 1),
(8, 'BENY FITRA', '20002859', 'CELANA PANJANG NAVY', 'PADANG', 'BACK OFFICE PRIA', '', 'EDP01', 3, 'SP/40', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 76, 'lg.paha', 'Lingkar paha', 80, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 23, 0, 0, '', 1),
(8, 'BUDI SANTOSO', '20003575', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'PONTIANAK', 'TEKNISI PRIA', '', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 62, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 5, 0, 0, '', 1),
(8, 'CUT PUTRI NIDA ARISTA', '20003586', 'KEMEJA L/S TEKNISI WANITA', 'BANDA ACEH', 'TEKNISI WANITA JILBAB', 'BISEP 20 CM ( JD )', 'EDP01', 3, 'SP/14', 'pj.baju', 'Panjang Baju', 66, 'pj.tgn', 'Panjang Tangan', 60, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:36', '', 1, 0, 0, '', 1),
(8, 'DANIEL SIDABUTAR', '20005640', 'CELANA PANJANG NAVY', 'SILANGIT', 'BACK OFFICE PRIA', '', 'EDP01', 3, 'SP/30', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 60, 'lg.paha', 'Lingkar paha', 58, 'lg.bawah', 'Dada Bawah', 18, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 1, 0, 0, '', 1),
(8, 'DARMAI SYURYA NINGSIH', '20001508', 'CELANA PJG NAVY TNP KARET', 'PADANG', 'BACK OFFICE WANITA JILBAB', 'TNP KARET', 'EDP01', 2, 'SP/XL', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 81, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 2, 0, 0, '', 1),
(8, 'DEDDY JUMARI LUMBANTORUAN', '20002042', 'CELANA PANJANG NAVY', 'BANDA ACEH', 'PEJABAT PRIA', 'PJ BJ = 34\"', 'EDP01', 3, 'SP/45', 'pj.cln', 'Tinggi Celana panjang', 102, 'lg.pgg', 'Lingkar Pinggang', 45, 'lg.pggl', 'Lingkar pinggul', 132, 'lgr.pesak', 'Vesak/ Pesak', 84, 'lg.paha', 'Lingkar paha', 87, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 24, 0, 0, '', 1),
(8, 'DEDDY JUMARI LUMBANTORUAN', '20002042', 'KEMEJA L/S', 'BANDA ACEH', 'PEJABAT PRIA', 'PJ BJ = 34\"', 'EDP01', 1, 'SP/18', 'pj.tgn', 'Panjang Tangan', 35, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 9, 0, 0, '', 1),
(8, 'DEDDY JUMARI LUMBANTORUAN', '20002042', 'KEMEJA S/S', 'BANDA ACEH', 'PEJABAT PRIA', 'PJ BJ = 34\"', 'EDP01', 2, 'SP/18', 'pj.tgn', 'Panjang Tangan', 21, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 10, 0, 0, '', 1),
(8, 'DEDE WIRATAMA BRATA', '20005259', 'CELANA PANJANG AVSEC PRIA', 'PADANG', 'AVSEC PRIA', '', 'EDP01', 3, 'SP/30', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 31, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:14', '', 1, 0, 0, '', 1),
(8, 'DEFRIDA', '20001519', 'BLOUSE KRAH BIRU', 'PADANG', 'PEJABAT WANITA JILBAB', 'BLS +2 DR DPEC', 'EDP01', 2, 'SP/18', 'pj.baju', 'Panjang Baju', 64, 'pj.tgn', 'Panjang Tangan', 52, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:32', '', 2, 0, 0, '', 1),
(8, 'DEFRIDA', '20001519', 'BLOUSE KRAH ORANGE', 'PADANG', 'PEJABAT WANITA JILBAB', 'BLS +2 DR DPEC', 'EDP01', 1, 'SP/18', 'pj.baju', 'Panjang Baju', 64, 'pj.tgn', 'Panjang Tangan', 52, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:32', '', 2, 0, 0, '', 1),
(8, 'DIAN FADHILLA', '20006178', 'CELANA PJG TNP KARET AVSEC', 'PEKANBARU', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/L', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 86, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:16', '', 9, 0, 0, '', 1),
(8, 'DIDI HERDIANSYAH', '20002248', 'CELANA PANJANG AVSEC PRIA', 'PONTIANAK', 'AVSEC PRIA', '', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 43, 'lg.pggl', 'Lingkar pinggul', 124, 'lgr.pesak', 'Vesak/ Pesak', 70, 'lg.paha', 'Lingkar paha', 78, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:14', '', 11, 0, 0, '', 1),
(8, 'DIMAS PRASETYO', '20005003', 'CELANA PANJANG RFFS PRIA', 'PONTIANAK', 'RFFS PRIA', '', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 104, 'lg.pgg', 'Lingkar Pinggang', 43, 'lg.pggl', 'Lingkar pinggul', 128, 'lgr.pesak', 'Vesak/ Pesak', 80, 'lg.paha', 'Lingkar paha', 80, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:34', '', 10, 0, 0, '', 1),
(8, 'DITA WAHYUNING RATRI', '20004827', 'ROK PJG NAVY KARET', 'PADANG', 'BACK OFFICE WANITA JILBAB', '', 'EDP01', 2, 'SP/4L', 'pj.rok', 'Panjang Rok', 110, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 112, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 3, 0, 0, '', 1),
(8, 'DODI SAGITA', '20002250', 'CELANA PANJANG RFFS PRIA', 'PADANG', 'RFFS PRIA', '', 'EDP01', 3, 'SP/31', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 32, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:34', '', 1, 0, 0, '', 1),
(8, 'Dra. WAHYU WIDAYATI', '20000290', 'BLAZER NAVY', 'BANDA ACEH', 'PEJABAT WANITA JILBAB', 'BISEP 18,5,PINGGANG ROK 79,PINGGUL 110,BOTTOM 70', 'EDP01', 2, 'SP/16', 'pj.baju', 'Panjang Baju', 66, 'pj.tgn', 'Panjang Tangan', 50, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:32', '', 4, 0, 0, '1', 1),
(8, 'Dra. WAHYU WIDAYATI', '20000290', 'ROK PJG NAVY KARET', 'BANDA ACEH', 'PEJABAT WANITA JILBAB', 'BISEP 18,5,PINGGANG ROK 79,PINGGUL 110,BOTTOM 70', 'EDP01', 2, 'SP', 'pj.rok', 'Panjang Rok', 86, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:32', '', 5, 0, 0, '', 1),
(8, 'DWI RATIH OCTARIA', '20006490', 'CELANA PJG TNP KARET AVSEC', 'PADANG', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/M', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 82, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:15', '', 3, 0, 0, '', 1),
(8, 'EDWIN EKA PUTRA', '20002252', 'CELANA PANJANG NAVY', 'PADANG', 'BACK OFFICE PRIA', '', 'EDP01', 3, 'SP/37', 'pj.cln', 'Tinggi Celana panjang', 100, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 67, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 20, 0, 0, '', 1),
(8, 'EFRINALDI', '20001533', 'CELANA PANJANG NAVY', 'PADANG', 'PEJABAT PRIA', '', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 62, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 15, 0, 0, '', 1),
(8, 'EFRINALDI', '20001533', 'KEMEJA L/S', 'PADANG', 'PEJABAT PRIA', '', 'EDP01', 1, 'SP/15', 'pj.tgn', 'Panjang Tangan', 30, 'lg.leher', 'Lingkar Leher', 16.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 1, 0, 0, '', 1),
(8, 'EFRINALDI', '20001533', 'KEMEJA S/S', 'PADANG', 'PEJABAT PRIA', '', 'EDP01', 2, 'SP/15', 'pj.tgn', 'Panjang Tangan', 18, 'lg.leher', 'Lingkar Leher', 16.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 2, 0, 0, '', 1),
(8, 'ELFIAN', '20001536', 'KEMEJA S/S AVSEC PRIA', 'PEKANBARU', 'AVSEC PRIA', '', 'EDP01', 3, 'SP//15,5', 'pj.tgn', 'Panjang Tangan', 20, 'lg.leher', 'Lingkar Leher', 17.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:13', '', 1, 0, 0, '', 1),
(8, 'ELIZA MAHDALENA', '20001538', 'BLAZER NAVY', 'PADANG', 'BACK OFFICE WANITA JILBAB', 'UK LM', 'EDP01', 2, 'SP', 'pj.baju', 'Panjang Baju', 0, 'pj.tgn', 'Panjang Tangan', 0, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:19', '', 5, 0, 0, '1', 1),
(8, 'ELIZA MAHDALENA', '20001538', 'BLOUSE KRAH BIRU', 'PADANG', 'BACK OFFICE WANITA JILBAB', 'UK LM', 'EDP01', 2, 'SP', 'pj.baju', 'Panjang Baju', 0, 'pj.tgn', 'Panjang Tangan', 0, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 4, 0, 0, '', 1),
(8, 'ELIZA MAHDALENA', '20001538', 'BLOUSE KRAH ORANGE', 'PADANG', 'BACK OFFICE WANITA JILBAB', 'UK LM', 'EDP01', 1, 'SP', 'pj.baju', 'Panjang Baju', 0, 'pj.tgn', 'Panjang Tangan', 0, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:19', '', 4, 0, 0, '', 1),
(8, 'ELIZA MAHDALENA', '20001538', 'ROK PJG NAVY KARET', 'PADANG', 'BACK OFFICE WANITA JILBAB', 'UK LM', 'EDP01', 2, 'SP', 'pj.rok', 'Panjang Rok', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 4, 0, 0, '', 1),
(8, 'ENGRACIA', '20003651', 'ROK PJG NAVY KARET', 'PADANG', 'BACK OFFICE WANITA JILBAB', '', 'EDP01', 2, 'SP/XL', 'pj.rok', 'Panjang Rok', 96, 'lg.pgg', 'Lingkar Pinggang', 78, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 1, 0, 0, '', 1),
(8, 'ERIC ALJAZULY', '20005828', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'PONTIANAK', 'TEKNISI PRIA', '', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 90, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 62, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 2, 0, 0, '', 1),
(8, 'ERLINDA WISMAI', '20001546', 'BLAZER NAVY', 'PADANG', 'PEJABAT WANITA JILBAB', 'UK LM', 'EDP01', 2, 'SP', 'pj.baju', 'Panjang Baju', 0, 'pj.tgn', 'Panjang Tangan', 0, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:32', '', 5, 0, 0, '1', 1),
(8, 'ERLINDA WISMAI', '20001546', 'BLOUSE KRAH BIRU', 'PADANG', 'PEJABAT WANITA JILBAB', 'UK LM', 'EDP01', 2, 'SP', 'pj.baju', 'Panjang Baju', 0, 'pj.tgn', 'Panjang Tangan', 0, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:32', '', 4, 0, 0, '', 1),
(8, 'ERLINDA WISMAI', '20001546', 'BLOUSE KRAH ORANGE', 'PADANG', 'PEJABAT WANITA JILBAB', 'UK LM', 'EDP01', 1, 'SP', 'pj.baju', 'Panjang Baju', 0, 'pj.tgn', 'Panjang Tangan', 0, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:32', '', 4, 0, 0, '', 1),
(8, 'ERLINDA WISMAI', '20001546', 'CELANA PJG NAVY KARET', 'PADANG', 'PEJABAT WANITA JILBAB', 'UK LM', 'EDP01', 2, 'SP', 'pj.cln', 'Tinggi Celana panjang', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:32', '', 11, 0, 0, '', 1),
(8, 'ERLISNA FITRA', '20001547', 'BLOUSE KRAH BIRU', 'PADANG', 'BACK OFFICE WANITA JILBAB', 'UK LM TTP UKURAN', 'EDP01', 2, 'SP', 'pj.baju', 'Panjang Baju', 70, 'pj.tgn', 'Panjang Tangan', 62, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 5, 0, 0, '', 1),
(8, 'ERLISNA FITRA', '20001547', 'BLOUSE KRAH ORANGE', 'PADANG', 'BACK OFFICE WANITA JILBAB', 'UK LM TTP UKURAN', 'EDP01', 1, 'SP', 'pj.baju', 'Panjang Baju', 70, 'pj.tgn', 'Panjang Tangan', 62, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 5, 0, 0, '', 1),
(8, 'ERLISNA FITRA', '20001547', 'CELANA PJG NAVY KARET', 'PADANG', 'BACK OFFICE WANITA JILBAB', 'UK LM TTP UKURAN', 'EDP01', 2, 'SP', 'pj.cln', 'Tinggi Celana panjang', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 11, 0, 0, '', 1),
(8, 'ESTON HASUDUNGAN SIANTURI', '20001551', 'KEMEJA L/S', 'PEKANBARU', 'PEJABAT PRIA', '', 'EDP01', 1, 'SP/15', 'pj.tgn', 'Panjang Tangan', 33, 'lg.leher', 'Lingkar Leher', 16.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 3, 0, 0, '', 1),
(8, 'ESTON HASUDUNGAN SIANTURI', '20001551', 'KEMEJA S/S', 'PEKANBARU', 'PEJABAT PRIA', '', 'EDP01', 2, 'SP/15', 'pj.tgn', 'Panjang Tangan', 19, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 3, 0, 0, '', 1),
(8, 'EWIN SAPUTRA', '20002685', 'CELANA PANJANG RFFS PRIA', 'BANDA ACEH', 'RFFS PRIA', '', 'EDP01', 3, 'SP/38', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 70, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:34', '', 2, 0, 0, '', 1),
(8, 'FAHMI REZKI', '20004367', 'CELANA PANJANG AVSEC PRIA', 'PEKANBARU', 'AVSEC PRIA', '', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 100, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 66, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:14', '', 5, 0, 0, '', 1),
(8, 'FAHMI REZKI', '20004367', 'KEMEJA S/S AVSEC PRIA', 'PEKANBARU', 'AVSEC PRIA', '', 'EDP01', 3, 'SP/14,5', 'pj.tgn', 'Panjang Tangan', 19, 'lg.leher', 'Lingkar Leher', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:13', '', 4, 0, 0, '', 1),
(8, 'FARIANTI LADIA', '20003121', 'BLAZER NAVY', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'PUNDAK BLZ 40,BOTTOM 53', 'EDP01', 2, 'SP/14', 'pj.baju', 'Panjang Baju', 66, 'pj.tgn', 'Panjang Tangan', 52, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:19', '', 3, 0, 0, '1', 1),
(8, 'FITRA MAULANA', '20002417', 'CELANA PANJANG NAVY', 'PADANG', 'PEJABAT PRIA', '', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 100, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 63, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:30', '', 18, 0, 0, '', 1),
(8, 'FITRI YULIANI', '20006180', 'CELANA PJG TNP KARET AVSEC', 'PEKANBARU', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/M', 'pj.cln', 'Tinggi Celana panjang', 100, 'lg.pgg', 'Lingkar Pinggang', 78, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:16', '', 7, 0, 0, '', 1),
(8, 'FITRIA ZAUFA LAILI', '20002871', 'BLAZER NAVY', 'BANDA ACEH', 'PEJABAT WANITA JILBAB', 'PINGGUL BLAZER 57', 'EDP01', 2, 'SP/12', 'pj.baju', 'Panjang Baju', 64, 'pj.tgn', 'Panjang Tangan', 54, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:32', '', 2, 0, 0, '1', 1),
(8, 'GARY PRASS SAGA', '20006182', 'KEMEJA S/S AVSEC PRIA', 'PEKANBARU', 'AVSEC PRIA', 'UK. LAMA (T.U, KE IBU TIKA)', 'EDP01', 3, 'SP/14,5', 'pj.tgn', 'Panjang Tangan', 19, 'lg.leher', 'Lingkar Leher', 16, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:13', '', 3, 0, 0, '', 1),
(8, 'GEBBIE FAUYENRA', '20006051', 'CELANA PJG NAVY KARET', 'PEKANBARU', 'FRONT LINER WANITA JILBAB', 'PAHA, LUTUT DAN KAKI -2CM LUTUT 46', 'EDP01', 2, 'SP/L', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 62, 'lg.bawah', 'Dada Bawah', 38, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:23', '', 4, 0, 0, '', 1),
(8, 'GUSTI RANDA GUNA', '20004380', 'CELANA PANJANG RFFS PRIA', 'PONTIANAK', 'RFFS PRIA', '', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 100, 'lg.pgg', 'Lingkar Pinggang', 47, 'lg.pggl', 'Lingkar pinggul', 136, 'lgr.pesak', 'Vesak/ Pesak', 80, 'lg.paha', 'Lingkar paha', 84, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:34', '', 8, 0, 0, '', 1),
(8, 'HANDOKO', '20003074', 'KEMEJA L/S', 'PADANG', 'PEJABAT PRIA', 'TGN L/S SPEC 16', 'EDP01', 1, 'SP/15,5', 'pj.tgn', 'Panjang Tangan', 32, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 4, 0, 0, '', 1),
(8, 'HANDOKO', '20003074', 'KEMEJA S/S', 'PADANG', 'PEJABAT PRIA', 'TGN L/S SPEC 16', 'EDP01', 2, '', 'pj.tgn', 'Panjang Tangan', 0, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 1, 0, 0, '', 1),
(8, 'HARDIANSYAH', '20002114', 'CELANA PANJANG RFFS PRIA', 'PONTIANAK', 'RFFS PRIA', '', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 102, 'lg.pgg', 'Lingkar Pinggang', 42, 'lg.pggl', 'Lingkar pinggul', 122, 'lgr.pesak', 'Vesak/ Pesak', 76, 'lg.paha', 'Lingkar paha', 76, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:34', '', 9, 0, 0, '', 1),
(8, 'HARRY GLENARDIE', '20002530', 'CELANA PANJANG NAVY', 'PEKANBARU', 'PEJABAT PRIA', 'LUTUT 56', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 45, 'lg.pggl', 'Lingkar pinggul', 130, 'lgr.pesak', 'Vesak/ Pesak', 78, 'lg.paha', 'Lingkar paha', 84, 'lg.bawah', 'Dada Bawah', 46, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:30', '', 30, 0, 0, '', 1),
(8, 'HASTURMAN YUNUS', '20000609', 'KEMEJA L/S', 'PEKANBARU', 'PEJABAT AVSEC PRIA', '', 'EDP01', 1, 'SP/15', 'pj.tgn', 'Panjang Tangan', 32, 'lg.leher', 'Lingkar Leher', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:26', '', 2, 0, 0, '', 1),
(8, 'HASTURMAN YUNUS', '20000609', 'KEMEJA S/S', 'PEKANBARU', 'PEJABAT AVSEC PRIA', '', 'EDP01', 2, 'SP/15', 'pj.tgn', 'Panjang Tangan', 19, 'lg.leher', 'Lingkar Leher', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:26', '', 4, 0, 0, '', 1),
(8, 'HASTURMAN YUNUS', '20000609', 'KEMEJA S/S AVSEC PRIA', 'PEKANBARU', 'PEJABAT AVSEC PRIA', '', 'EDP01', 1, 'SP/15', 'pj.tgn', 'Panjang Tangan', 19, 'lg.leher', 'Lingkar Leher', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:27', '', 6, 0, 0, '', 1),
(8, 'HERMAWAN', '20003703', 'CELANA PANJANG NAVY', 'BANDA ACEH', 'BACK OFFICE PRIA', '', 'EDP01', 3, 'SP/32', 'pj.cln', 'Tinggi Celana panjang', 100, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 64, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 6, 0, 0, '', 1),
(8, 'HERYANTO', '20000871', 'CELANA PANJANG RFFS PRIA', 'PONTIANAK', 'RFFS PRIA', 'TU/UK LAMA', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 43, 'lg.pggl', 'Lingkar pinggul', 116, 'lgr.pesak', 'Vesak/ Pesak', 86, 'lg.paha', 'Lingkar paha', 86, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:34', '', 6, 0, 0, '', 1),
(8, 'HESRON', '20001578', 'KEMEJA L/S', 'PEKANBARU', 'PEJABAT AVSEC PRIA', '', 'EDP01', 1, 'SP', 'pj.tgn', 'Panjang Tangan', 0, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:26', '', 10, 0, 0, '', 1),
(8, 'HESRON', '20001578', 'KEMEJA S/S', 'PEKANBARU', 'PEJABAT AVSEC PRIA', '', 'EDP01', 2, 'SP', 'pj.tgn', 'Panjang Tangan', 0, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:26', '', 11, 0, 0, '', 1),
(8, 'HESRON', '20001578', 'KEMEJA S/S AVSEC PRIA', 'PEKANBARU', 'PEJABAT AVSEC PRIA', '', 'EDP01', 1, 'SP', 'pj.tgn', 'Panjang Tangan', 0, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:27', '', 10, 0, 0, '', 1),
(8, 'HESTI RAMADHANTI', '20006495', 'CELANA PJG TNP KARET AVSEC', 'PONTIANAK', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/M', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 84, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:16', '', 4, 0, 0, '', 1),
(8, 'HESTIN MEILINDA', '20004139', 'CELANA PJG TNP KARET AVSEC', 'PONTIANAK', 'AVSEC WANITA JILBAB', 'TU/ UK LAMA', 'EDP01', 3, 'SP/M', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 77, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:16', '', 2, 0, 0, '', 1),
(8, 'HOTASI MANALU', '20000377', 'CELANA PANJANG AVSEC PRIA', 'SILANGIT', 'PEJABAT  GM', '', 'EDP01', 1, 'SP', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 36.5, 'lg.pggl', 'Lingkar pinggul', 108, 'lgr.pesak', 'Vesak/ Pesak', 66, 'lg.paha', 'Lingkar paha', 66, 'lg.bawah', 'Dada Bawah', 38, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:25', '', 9, 0, 0, '', 1),
(8, 'HOTASI MANALU', '20000377', 'CELANA PANJANG NAVY', 'SILANGIT', 'PEJABAT  GM', '', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 36.5, 'lg.pggl', 'Lingkar pinggul', 108, 'lgr.pesak', 'Vesak/ Pesak', 66, 'lg.paha', 'Lingkar paha', 66, 'lg.bawah', 'Dada Bawah', 38, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:25', '', 26, 0, 0, '', 1),
(8, 'ILHAM FAJRIADI', '20004144', 'CELANA PANJANG AVSEC PRIA', 'BANDA ACEH', 'AVSEC PRIA', 'BOTTON 19 JD', 'EDP01', 3, 'SP/34', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 19, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:14', '', 3, 0, 0, '', 1),
(8, 'INDRA GUNAWAN', '20002878', 'CELANA PANJANG NAVY', 'PADANG', 'BACK OFFICE PRIA', 'L/P PS ', 'EDP01', 3, 'SP/32', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 34, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 5, 0, 0, '', 1),
(8, 'ISRA JUMAIDY', '20002372', 'CELANA PANJANG RFFS PRIA', 'BANDA ACEH', 'RFFS PRIA', '', 'EDP01', 3, 'SP/41', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 41, 'lg.pggl', 'Lingkar pinggul', 122, 'lgr.pesak', 'Vesak/ Pesak', 74, 'lg.paha', 'Lingkar paha', 76, 'lg.bawah', 'Dada Bawah', 26, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:34', '', 4, 0, 0, '', 1),
(8, 'JAYA TAHOMA SIRAIT', '20000388', 'KEMEJA L/S', 'PEKANBARU', 'PEJABAT  GM', '', 'EDP01', 1, 'SP/16,5', 'pj.tgn', 'Panjang Tangan', 34, 'lg.leher', 'Lingkar Leher', 17.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:25', '', 8, 0, 0, '', 1),
(8, 'JAYA TAHOMA SIRAIT', '20000388', 'KEMEJA S/S', 'PEKANBARU', 'PEJABAT  GM', '', 'EDP01', 2, 'SP/16,5', 'pj.tgn', 'Panjang Tangan', 20, 'lg.leher', 'Lingkar Leher', 17.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:25', '', 8, 0, 0, '', 1),
(8, 'JOKO KASTOPO', '20001607', 'CELANA PANJANG NAVY', 'PEKANBARU', 'BACK OFFICE PRIA', 'LUTUT 68', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 106, 'lg.pgg', 'Lingkar Pinggang', 45, 'lg.pggl', 'Lingkar pinggul', 132, 'lgr.pesak', 'Vesak/ Pesak', 84, 'lg.paha', 'Lingkar paha', 88, 'lg.bawah', 'Dada Bawah', 50, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 32, 0, 0, '', 1),
(8, 'JOKO RETNO HARWOKO', '20002882', 'CELANA PANJANG NAVY', 'PADANG', 'PEJABAT PRIA', 'TTP UK VIA AZWIR, UK LAMA', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:30', '', 25, 0, 0, '', 1),
(8, 'KHADAFI', '20002375', 'CELANA PANJANG RFFS PRIA', 'PONTIANAK', 'RFFS PRIA', 'TU/UK LAMA', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 100, 'lg.pgg', 'Lingkar Pinggang', 42, 'lg.pggl', 'Lingkar pinggul', 124, 'lgr.pesak', 'Vesak/ Pesak', 76, 'lg.paha', 'Lingkar paha', 80, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:34', '', 7, 0, 0, '', 1),
(8, 'LAURENTIUS BRYANDICY RAJA WATOR', '20004640', 'KEMEJA L/S TEKNISI PRIA', 'PONTIANAK', 'TEKNISI PRIA', 'PJ BAJU 28', 'EDP01', 3, 'SP/15', 'pj.tgn', 'Panjang Tangan', 33, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 2, 0, 0, '', 1),
(8, 'LEO RAHMAD SIREGAR', '20005642', 'CELANA PANJANG NAVY', 'SILANGIT', 'BACK OFFICE PRIA', 'T.U tudak hadir', 'EDP01', 3, 'SP/31', 'pj.cln', 'Tinggi Celana panjang', 102, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 62, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 4, 0, 0, '', 1),
(8, 'LIDIA RAMADANI', '20005748', 'CELANA PJG NAVY KARET', 'PEKANBARU', 'BACK OFFICE WANITA JILBAB', 'PAHA , LUTUT DAN KAKI -2CM', 'EDP01', 2, 'SP/L', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 3, 0, 0, '', 1),
(8, 'LIDYA SEPTIANI', '20003315', 'CELANA PJG TNP KARET AVSEC', 'PADANG', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/L', 'pj.cln', 'Tinggi Celana panjang', 102, 'lg.pgg', 'Lingkar Pinggang', 74, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:15', '', 11, 0, 0, '', 1),
(8, 'LISDA YUNITA SIMATUPANG', '20003748', 'BLAZER NAVY', 'SILANGIT', 'PEJABAT WANITA NON JILBAB', 'JD', 'EDP01', 2, 'SP', 'pj.baju', 'Panjang Baju', 60, 'pj.tgn', 'Panjang Tangan', 55, 'lg.dada', 'Panjang Dada', 53, 'lg.pgg', 'Lingkar Pinggang', 43, 'lg.pggl', 'Lingkar pinggul', 54, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:33', '', 6, 0, 0, '1', 1),
(8, 'LISDA YUNITA SIMATUPANG', '20003748', 'CELANA PJG NAVY KARET', 'SILANGIT', 'PEJABAT WANITA NON JILBAB', 'JD', 'EDP01', 2, 'SP/XL', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 80, 'lg.pggl', 'Lingkar pinggul', 106, 'lgr.pesak', 'Vesak/ Pesak', 68, 'lg.paha', 'Lingkar paha', 66, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:33', '', 10, 0, 0, '', 1),
(8, 'LITA BUDIWAN', '20004182', 'CELANA PJG TNP KARET AVSEC', 'PADANG', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/M', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 79, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:15', '', 6, 0, 0, '', 1),
(8, 'LUKMANUL HAKIM RAMBE', '20005761', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'PEKANBARU', 'TEKNISI PRIA', '', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 68, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 4, 0, 0, '', 1),
(8, 'LUSITA TITIANDRI', '20001620', 'BLAZER NAVY', 'PADANG', 'BACK OFFICE WANITA JILBAB', '', 'EDP01', 2, 'SP/12', 'pj.baju', 'Panjang Baju', 62, 'pj.tgn', 'Panjang Tangan', 50, 'lg.dada', 'Panjang Dada', 46, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:19', '', 1, 0, 0, '1', 1),
(8, 'LUSITA TITIANDRI', '20001620', 'BLOUSE KRAH BIRU', 'PADANG', 'BACK OFFICE WANITA JILBAB', '', 'EDP01', 2, 'SP/12', 'pj.baju', 'Panjang Baju', 60, 'pj.tgn', 'Panjang Tangan', 52, 'lg.dada', 'Panjang Dada', 46, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 1, 0, 0, '', 1),
(8, 'LUSITA TITIANDRI', '20001620', 'BLOUSE KRAH ORANGE', 'PADANG', 'BACK OFFICE WANITA JILBAB', '', 'EDP01', 1, 'SP/12', 'pj.baju', 'Panjang Baju', 60, 'pj.tgn', 'Panjang Tangan', 52, 'lg.dada', 'Panjang Dada', 46, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:19', '', 1, 0, 0, '', 1),
(8, 'MAHESA', '20002422', 'CELANA PANJANG NAVY', 'PEKANBARU', 'PEJABAT PRIA', '', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 64, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:30', '', 16, 0, 0, '', 1),
(8, 'MARANIS', '20001627', 'CELANA PJG TNP KARET AVSEC', 'PADANG', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/L', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 92, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:15', '', 8, 0, 0, '', 1),
(8, 'MARYADI', '20005648', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'SILANGIT', 'TEKNISI PRIA', '', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 60, 'lg.paha', 'Lingkar paha', 66, 'lg.bawah', 'Dada Bawah', 18, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 3, 0, 0, '', 1),
(8, 'MARZUKI', '20004196', 'CELANA PANJANG AVSEC PRIA', 'BANDA ACEH', 'AVSEC PRIA', '', 'EDP01', 3, 'SP/39', 'pj.cln', 'Tinggi Celana panjang', 104, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 72, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:14', '', 6, 0, 0, '', 1),
(8, 'MELARINI PAKPAHAN', '20005164', 'BLAZER NAVY', 'SILANGIT', 'BACK OFFICE WANITA NON JILBAB', 'JD', 'EDP01', 2, 'SP', 'pj.baju', 'Panjang Baju', 62, 'pj.tgn', 'Panjang Tangan', 54, 'lg.dada', 'Panjang Dada', 92, 'lg.pgg', 'Lingkar Pinggang', 78, 'lg.pggl', 'Lingkar pinggul', 102, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:21', '', 7, 0, 0, '1', 1),
(8, 'MELARINI PAKPAHAN', '20005164', 'CELANA PJG NAVY KARET', 'SILANGIT', 'BACK OFFICE WANITA NON JILBAB', 'JD', 'EDP01', 2, 'SP/M', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 60, 'lg.paha', 'Lingkar paha', 60, 'lg.bawah', 'Dada Bawah', 18, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:21', '', 1, 0, 0, '', 1),
(8, 'MEUTIA SARI', '20003770', 'CELANA PJG KARET TEKNISI', 'BANDA ACEH', 'TEKNISI WANITA JILBAB', 'UK LAMA', 'EDP01', 3, 'SP/M', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 30, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:36', '', 1, 0, 0, '', 1),
(8, 'MIA FEBRINA', '20003771', 'CELANA PJG NAVY KARET', 'PEKANBARU', 'BACK OFFICE WANITA NON JILBAB', '', 'EDP01', 2, 'SP/XL', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:21', '', 6, 0, 0, '', 1),
(8, 'MOCH. NUR ALI ROSJIDI', '20001636', 'CELANA PANJANG NAVY', 'PADANG', 'PEJABAT PRIA', 'DIKLAT,UK LM  INFO BY PAK AJAY', 'EDP01', 3, 'SP/33', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 34, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:30', '', 7, 0, 0, '', 1),
(8, 'MOHAMMAD ARIFIN', '20002425', 'CELANA PANJANG NAVY', 'BANDA ACEH', 'PEJABAT PRIA', '', 'EDP01', 3, 'SP/39', 'pj.cln', 'Tinggi Celana panjang', 102, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 70, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 22, 0, 0, '', 1),
(8, 'MUHAMMAD RIDHO', '20004649', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'PEKANBARU', 'TEKNISI PRIA', 'SPEC B', 'EDP01', 3, 'SP/32', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 1, 0, 0, '', 1),
(8, 'MURNIATI', '20001644', 'ROK PJG NAVY KARET', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'PINGGANG ROK 88,PINGGUL 104', 'EDP01', 2, 'SP', 'pj.rok', 'Panjang Rok', 98, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 7, 0, 0, '', 1),
(8, 'MURNIATI', '20001777', 'CELANA PJG NAVY KARET', 'PEKANBARU', 'PEJABAT WANITA NON JILBAB', 'PESAK +2CM', 'EDP01', 2, 'SP/L', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 68, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:33', '', 5, 0, 0, '', 1),
(8, 'MURSIDAH ILFA', '20003789', 'ROK PJG NAVY KARET', 'PADANG', 'BACK OFFICE WANITA JILBAB', 'UK MNT MINTA DMSKAN DENGAN DITA TTP UK VIA MBA RIC', 'EDP01', 2, 'SP/4L', 'pj.rok', 'Panjang Rok', 110, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 112, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 3, 0, 0, '', 1),
(8, 'NAZAL RAUF', '20003355', 'CELANA PANJANG AVSEC PRIA', 'PEKANBARU', 'AVSEC PRIA', 'TITIP UK. KE IBU TIKA', 'EDP01', 3, 'SP/34', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 33, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:14', '', 4, 0, 0, '', 1),
(8, 'NINING NURHAYATI', '20006189', 'CELANA PJG TNP KARET AVSEC', 'PEKANBARU', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/M', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 88, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:16', '', 5, 0, 0, '', 1),
(8, 'NOERY INDAH SAYEKTI', '20004234', 'CELANA PJG TNP KARET AVSEC', 'PONTIANAK', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/XL', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 91, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:16', '', 15, 0, 0, '', 1),
(8, 'NOVI INDRAWAN', '20002693', 'CELANA PANJANG RFFS PRIA', 'PONTIANAK', 'RFFS PRIA', '', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 46, 'lg.pggl', 'Lingkar pinggul', 140, 'lgr.pesak', 'Vesak/ Pesak', 80, 'lg.paha', 'Lingkar paha', 88, 'lg.bawah', 'Dada Bawah', 48, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:34', '', 5, 0, 0, '', 1);
INSERT INTO `edp_assortment_dt` (`import_id`, `nama`, `nik`, `nama_pakaian`, `lokasi`, `jabatan`, `keterangan`, `info_tambahan`, `qty`, `size`, `nama_ukuran_f1`, `alias_ukuran_f1`, `ukuran_f1`, `nama_ukuran_f2`, `alias_ukuran_f2`, `ukuran_f2`, `nama_ukuran_f3`, `alias_ukuran_f3`, `ukuran_f3`, `nama_ukuran_f4`, `alias_ukuran_f4`, `ukuran_f4`, `nama_ukuran_f5`, `alias_ukuran_f5`, `ukuran_f5`, `nama_ukuran_f6`, `alias_ukuran_f6`, `ukuran_f6`, `nama_ukuran_f7`, `alias_ukuran_f7`, `ukuran_f7`, `nama_ukuran_f8`, `alias_ukuran_f8`, `ukuran_f8`, `nama_ukuran_f9`, `alias_ukuran_f9`, `ukuran_f9`, `nama_ukuran_f10`, `alias_ukuran_f10`, `ukuran_f10`, `datetime_uploaded`, `ukuran`, `code_ukuran`, `no_karton`, `karton_id`, `artikel`, `idPo`) VALUES
(8, 'NUR ESHIE RAMADYA', '20004871', 'CELANA PJG NAVY TNP KARET', 'PONTIANAK', 'BACK OFFICE WANITA JILBAB', 'LUTUT 52', 'EDP01', 2, 'SP', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 98, 'lg.pggl', 'Lingkar pinggul', 120, 'lgr.pesak', 'Vesak/ Pesak', 72, 'lg.paha', 'Lingkar paha', 39, 'lg.bawah', 'Dada Bawah', 21, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 3, 0, 0, '', 1),
(8, 'NUR SANDI JIHAD', '20004245', 'CELANA PANJANG AVSEC PRIA', 'PONTIANAK', 'AVSEC PRIA', '', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 44, 'lg.pggl', 'Lingkar pinggul', 126, 'lgr.pesak', 'Vesak/ Pesak', 74, 'lg.paha', 'Lingkar paha', 80, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:14', '', 12, 0, 0, '', 1),
(8, 'NUR SANDI JIHAD', '20004245', 'KEMEJA S/S AVSEC PRIA', 'PONTIANAK', 'AVSEC PRIA', '', 'EDP01', 3, 'SP/16,5', 'pj.tgn', 'Panjang Tangan', 20, 'lg.leher', 'Lingkar Leher', 18, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:14', '', 9, 0, 0, '', 1),
(8, 'NURHANI', '20001654', 'CELANA PJG NAVY KARET', 'PEKANBARU', 'FRONT LINER WANITA JILBAB', 'PAHA +2CM', 'EDP01', 2, 'SP/XL', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 68, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:23', '', 9, 0, 0, '', 1),
(8, 'NURIL HUDA', '20002850', 'KEMEJA L/S', 'PONTIANAK', 'PEJABAT PRIA', '', 'EDP01', 1, 'SP/16', 'pj.tgn', 'Panjang Tangan', 34, 'lg.leher', 'Lingkar Leher', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 6, 0, 0, '', 1),
(8, 'NURIL HUDA', '20002850', 'KEMEJA S/S', 'PONTIANAK', 'PEJABAT PRIA', '', 'EDP01', 2, 'SP/16', 'pj.tgn', 'Panjang Tangan', 20, 'lg.leher', 'Lingkar Leher', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 7, 0, 0, '', 1),
(8, 'NURLAILA', '20001656', 'ROK PJG NAVY KARET', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'PINGGANG ROK 96', 'EDP01', 2, 'SP', 'pj.rok', 'Panjang Rok', 100, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 8, 0, 0, '', 1),
(8, 'PAIMAN', '20000925', 'CELANA PANJANG NAVY', 'PONTIANAK', 'BACK OFFICE PRIA', '', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 38, 'lg.pggl', 'Lingkar pinggul', 122, 'lgr.pesak', 'Vesak/ Pesak', 72, 'lg.paha', 'Lingkar paha', 76, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 27, 0, 0, '', 1),
(8, 'PEPEN JULANTRI MANURUNG', '20005637', 'CELANA PANJANG NAVY', 'SILANGIT', 'BACK OFFICE PRIA', '', 'EDP01', 3, 'SP/33', 'pj.cln', 'Tinggi Celana panjang', 102, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 64, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 8, 0, 0, '', 1),
(8, 'PONIRAN', '20000932', 'CELANA PANJANG NAVY', 'PONTIANAK', 'PEJABAT RFFS PRIA', '', 'EDP01', 3, 'SP/39', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 78, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:31', '', 21, 0, 0, '', 1),
(8, 'PONIRAN', '20000932', 'CELANA PANJANG RFFS PRIA', 'PONTIANAK', 'PEJABAT RFFS PRIA', '', 'EDP01', 1, 'SP/39', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 78, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:31', '', 3, 0, 0, '', 1),
(8, 'PONIRAN', '20000932', 'KEMEJA L/S', 'PONTIANAK', 'PEJABAT RFFS PRIA', '', 'EDP01', 1, 'SP/16,5', 'pj.tgn', 'Panjang Tangan', 33, 'lg.leher', 'Lingkar Leher', 17.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:31', '', 7, 0, 0, '', 1),
(8, 'PONIRAN', '20000932', 'KEMEJA S/S', 'PONTIANAK', 'PEJABAT RFFS PRIA', '', 'EDP01', 2, 'SP/16,5', 'pj.tgn', 'Panjang Tangan', 20, 'lg.leher', 'Lingkar Leher', 17.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:31', '', 8, 0, 0, '', 1),
(8, 'PONIRAN', '20000932', 'KEMEJA S/S RFFS PRIA', 'PONTIANAK', 'PEJABAT RFFS PRIA', '', 'EDP01', 1, 'SP/16,5', 'pj.tgn', 'Panjang Tangan', 20, 'lg.leher', 'Lingkar Leher', 17.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:31', '', 1, 0, 0, '', 1),
(8, 'RACHMAD SYUKRAN', '20002896', 'CELANA PANJANG NAVY', 'BANDA ACEH', 'PEJABAT PRIA', 'TU VIA RAHMADIN,UK LAMA ( KMJ TURUN SIZE )', 'EDP01', 3, 'SP/36', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 66, 'lg.paha', 'Lingkar paha', 66, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 19, 0, 0, '', 1),
(8, 'RAHMADDIN', '20003841', 'CELANA PANJANG NAVY', 'BANDA ACEH', 'BACK OFFICE PRIA', '', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 19, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 12, 0, 0, '', 1),
(8, 'RAHMI SUSANTI', '20003843', 'KEMEJA L/S TEKNISI WANITA', 'BANDA ACEH', 'TEKNISI WANITA JILBAB', 'HAMIL BISEP 18,5 CM ( JD )', 'EDP01', 3, 'SP/16', 'pj.baju', 'Panjang Baju', 64, 'pj.tgn', 'Panjang Tangan', 54, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:36', '', 2, 0, 0, '', 1),
(8, 'RENDRA STEVEN', '20003854', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'SILANGIT', 'TEKNISI PRIA', 'Bisep kerja + 4, tidak hadir karena istri melahirk', 'EDP01', 3, 'SP/43', 'pj.cln', 'Tinggi Celana panjang', 100, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 124, 'lgr.pesak', 'Vesak/ Pesak', 70, 'lg.paha', 'Lingkar paha', 74, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 11, 0, 0, '', 1),
(8, 'RHABELA RIZAL', '20006188', 'CELANA PJG TNP KARET AVSEC', 'PEKANBARU', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/L', 'pj.cln', 'Tinggi Celana panjang', 104, 'lg.pgg', 'Lingkar Pinggang', 74, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:16', '', 12, 0, 0, '', 1),
(8, 'RINALDY OCTAVIANDHY', '20003871', 'KEMEJA L/S', 'PADANG', 'PEJABAT PRIA', 'TTP UK VIA AZWIR, UK LAMA', 'EDP01', 1, 'SP/16', 'pj.tgn', 'Panjang Tangan', 34, 'lg.leher', 'Lingkar Leher', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 6, 0, 0, '', 1),
(8, 'RINALDY OCTAVIANDHY', '20003871', 'KEMEJA S/S', 'PADANG', 'PEJABAT PRIA', 'TTP UK VIA AZWIR, UK LAMA', 'EDP01', 2, 'SP/16', 'pj.tgn', 'Panjang Tangan', 20, 'lg.leher', 'Lingkar Leher', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:29', '', 7, 0, 0, '', 1),
(8, 'RISA FISTIA', '20005725', 'CELANA PJG NAVY KARET', 'PONTIANAK', 'BACK OFFICE WANITA JILBAB', '', 'EDP01', 2, 'SP/XL', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 66, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 7, 0, 0, '', 1),
(8, 'RIZKI ARTHA AYU GUMILAR', '20005843', 'CELANA PJG TNP KARET AVSEC', 'PONTIANAK', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/XL', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 88, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:16', '', 14, 0, 0, '', 1),
(8, 'ROLAN SIMANJUNTAK', '20005627', 'KEMEJA S/S AVSEC PRIA', 'SILANGIT', 'AVSEC PRIA', 'kerung leher 17\"', 'EDP01', 3, 'SP/15,5', 'pj.tgn', 'Panjang Tangan', 19, 'lg.leher', 'Lingkar Leher', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:13', '', 7, 0, 0, '', 1),
(8, 'ROY MANGARA DONGAN LUMBANTOBING', '20005650', 'CELANA PANJANG NAVY', 'SILANGIT', 'BACK OFFICE PRIA', '', 'EDP01', 3, 'SP/35', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 66, 'lg.bawah', 'Dada Bawah', 18, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 13, 0, 0, '', 1),
(8, 'RYAN AMRIZ BATU BARA', '20006052', 'JAS', 'PEKANBARU', 'FRONT LINER PRIA', 'BODY JAS TAHUN LALU TERLALU BESAR/LEBAR. LINGKAR 6', 'EDP01', 1, 'SP/M', 'pj.baju', 'Panjang Baju', 70, 'pj.tgn', 'Panjang Tangan', 60, 'lbr.pundak', '', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:22', '', 1, 0, 0, '', 1),
(8, 'SALMAH', '20001685', 'ROK PJG NAVY KARET', 'PEKANBARU', 'BACK OFFICE WANITA JILBAB', 'MINTA A LINE, LEBAR BAWAH 78', 'EDP01', 2, 'SP/2L', 'pj.rok', 'Panjang Rok', 90, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 2, 0, 0, '', 1),
(8, 'SAMSURI', '20000954', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'PONTIANAK', 'TEKNISI PRIA', 'TU/ UK LAMA', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 100, 'lg.pgg', 'Lingkar Pinggang', 46, 'lg.pggl', 'Lingkar pinggul', 128, 'lgr.pesak', 'Vesak/ Pesak', 80, 'lg.paha', 'Lingkar paha', 86, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 13, 0, 0, '', 1),
(8, 'SAMUEL HORO SEVEN PASARIBU', '20005638', 'CELANA PANJANG NAVY', 'SILANGIT', 'BACK OFFICE PRIA', '', 'EDP01', 3, 'SP/34', 'pj.cln', 'Tinggi Celana panjang', 92, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 64, 'lg.paha', 'Lingkar paha', 62, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 9, 0, 0, '', 1),
(8, 'SANDITA ADIJAYA SURBAKTI', '20005700', 'CELANA PANJANG NAVY', 'PONTIANAK', 'BACK OFFICE PRIA', '', 'EDP01', 3, 'SP/31', 'pj.cln', 'Tinggi Celana panjang', 100, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 68, 'lg.paha', 'Lingkar paha', 68, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 3, 0, 0, '', 1),
(8, 'SITI AMINATUL JARIAH', '20004300', 'CELANA PJG TNP KARET AVSEC', 'PONTIANAK', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/XL', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 87, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:16', '', 13, 0, 0, '', 1),
(8, 'SOBALI', '20001693', 'KEMEJA S/S AVSEC PRIA', 'PADANG', 'AVSEC PRIA', '', 'EDP01', 3, 'SP/15,5', 'pj.tgn', 'Panjang Tangan', 19, 'lg.leher', 'Lingkar Leher', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:13', '', 7, 0, 0, '', 1),
(8, 'SUPARLAN', '20002568', 'KEMEJA L/S', 'PADANG', 'PEJABAT  GM', '', 'EDP01', 1, 'SP/16,5', 'pj.tgn', 'Panjang Tangan', 34, 'lg.leher', 'Lingkar Leher', 17.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:25', '', 8, 0, 0, '', 1),
(8, 'SUPARLAN', '20002568', 'KEMEJA S/S', 'PADANG', 'PEJABAT  GM', '', 'EDP01', 2, 'SP/16,5', 'pj.tgn', 'Panjang Tangan', 21, 'lg.leher', 'Lingkar Leher', 17.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:25', '', 9, 0, 0, '', 1),
(8, 'SUPRIYADI', '20002698', 'CELANA PANJANG RFFS PRIA', 'PONTIANAK', 'RFFS PRIA', 'CLN PS', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 104, 'lg.pgg', 'Lingkar Pinggang', 44, 'lg.pggl', 'Lingkar pinggul', 134, 'lgr.pesak', 'Vesak/ Pesak', 78, 'lg.paha', 'Lingkar paha', 86, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:34', '', 11, 0, 0, '', 1),
(8, 'SURASI', '20001003', 'CELANA PANJANG NAVY', 'PONTIANAK', 'BACK OFFICE PRIA', 'TU/UK LAMA', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 31, 'lg.pggl', 'Lingkar pinggul', 102, 'lgr.pesak', 'Vesak/ Pesak', 70, 'lg.paha', 'Lingkar paha', 68, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 28, 0, 0, '', 1),
(8, 'SYAFRIANA', '20001724', 'CELANA PJG TNP KARET AVSEC', 'PEKANBARU', 'AVSEC WANITA JILBAB', '', 'EDP01', 3, 'SP/4L', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 92, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:16', '', 17, 0, 0, '', 1),
(8, 'TIKA ERLANDA', '20003925', 'CELANA PJG NAVY KARET', 'PEKANBARU', 'BACK OFFICE WANITA JILBAB', '', 'EDP01', 2, 'SP/XL', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 18, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 8, 0, 0, '', 1),
(8, 'WAHYUDI', '20003936', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'PADANG', 'TEKNISI PRIA', 'JD', 'EDP01', 3, 'SP/40', 'pj.cln', 'Tinggi Celana panjang', 94, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 74, 'lg.paha', 'Lingkar paha', 77, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 9, 0, 0, '', 1),
(8, 'YANDI SAPUTRA', '20003447', 'KEMEJA S/S AVSEC PRIA', 'PADANG', 'AVSEC PRIA', '', 'EDP01', 3, 'SP/15,5', 'pj.tgn', 'Panjang Tangan', 19, 'lg.leher', 'Lingkar Leher', 17, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:13', '', 7, 0, 0, '', 1),
(8, 'YOS SUWAGIYONO', '20001043', 'KEMEJA L/S', 'BANDA ACEH', 'PEJABAT  GM', 'TIDAK UKUR,UKURAN LAMA,INFO ADILA,', 'EDP01', 1, 'SP/15,5', 'pj.tgn', 'Panjang Tangan', 33, 'lg.leher', 'Lingkar Leher', 16.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:25', '', 5, 0, 0, '', 1),
(8, 'YOS SUWAGIYONO', '20001043', 'KEMEJA S/S', 'BANDA ACEH', 'PEJABAT  GM', 'TIDAK UKUR,UKURAN LAMA,INFO ADILA,', 'EDP01', 2, 'SP/15,5', 'pj.tgn', 'Panjang Tangan', 20, 'lg.leher', 'Lingkar Leher', 16.5, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:25', '', 6, 0, 0, '', 1),
(8, 'YOS SUWAGIYONO', '20001043', 'KEMEJA S/S AVSEC PRIA', 'BANDA ACEH', 'PEJABAT  GM', 'TIDAK UKUR,UKURAN LAMA,INFO ADILA,', 'EDP01', 1, 'SP/15,5', 'pj.tgn', 'Panjang Tangan', 20, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:25', '', 8, 0, 0, '', 1),
(8, 'YULIA PANGASTUTI', '20002558', 'CELANA PJG KARET TEKNISI', 'PADANG', 'TEKNISI WANITA JILBAB', 'UK LAMA', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:36', '', 2, 0, 0, '', 1),
(8, 'YULIA PANGASTUTI', '20002558', 'KEMEJA L/S TEKNISI WANITA', 'PADANG', 'TEKNISI WANITA JILBAB', 'UK LAMA', 'EDP01', 3, 'SP', 'pj.baju', 'Panjang Baju', 0, 'pj.tgn', 'Panjang Tangan', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:36', '', 3, 0, 0, '', 1),
(8, 'YUNDA YANI', '20002559', 'BLOUSE KRAH BIRU', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'BISEP BLOUSE 18,5', 'EDP01', 2, 'SP/20', 'pj.baju', 'Panjang Baju', 70, 'pj.tgn', 'Panjang Tangan', 60, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 3, 0, 0, '', 1),
(8, 'YUNDA YANI', '20002559', 'BLOUSE KRAH ORANGE', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'BISEP BLOUSE 18,5', 'EDP01', 1, 'SP/20', 'pj.baju', 'Panjang Baju', 70, 'pj.tgn', 'Panjang Tangan', 60, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 3, 0, 0, '', 1),
(8, 'YURNIDA', '20001754', 'CELANA PJG NAVY TNP KARET', 'PADANG', 'FRONT LINER WANITA JILBAB', '', 'EDP01', 2, 'SP/M', 'pj.cln', 'Tinggi Celana panjang', 96, 'lg.pgg', 'Lingkar Pinggang', 80, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 0, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:23', '', 1, 0, 0, '', 1),
(8, 'YUSTIANA', '20001757', 'BLAZER NAVY', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'UK BAJU JADI', 'EDP01', 2, 'SP', 'pj.baju', 'Panjang Baju', 74, 'pj.tgn', 'Panjang Tangan', 54, 'lg.dada', 'Panjang Dada', 61, 'lg.pgg', 'Lingkar Pinggang', 56, 'lg.pggl', 'Lingkar pinggul', 73, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:19', '', 8, 0, 0, '1', 1),
(8, 'YUSTIANA', '20001757', 'BLOUSE KRAH BIRU', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'UK BAJU JADI', 'EDP01', 2, 'SP', 'pj.baju', 'Panjang Baju', 72, 'pj.tgn', 'Panjang Tangan', 56, 'lg.dada', 'Panjang Dada', 61, 'lg.pgg', 'Lingkar Pinggang', 59, 'lg.pggl', 'Lingkar pinggul', 69, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 6, 0, 0, '', 1),
(8, 'YUSTIANA', '20001757', 'BLOUSE KRAH ORANGE', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'UK BAJU JADI', 'EDP01', 1, 'SP', 'pj.baju', 'Panjang Baju', 72, 'pj.tgn', 'Panjang Tangan', 56, 'lg.dada', 'Panjang Dada', 61, 'lg.pgg', 'Lingkar Pinggang', 59, 'lg.pggl', 'Lingkar pinggul', 69, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:19', '', 6, 0, 0, '', 1),
(8, 'YUSTIANA', '20001757', 'ROK PJG NAVY KARET', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'UK BAJU JADI', 'EDP01', 2, 'SP', 'pj.rok', 'Panjang Rok', 92, 'lg.pgg', 'Lingkar Pinggang', 98, 'lg.pggl', 'Lingkar pinggul', 128, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:20', '', 6, 0, 0, '', 1),
(8, 'ZULBRITO RADIKAR', '20001374', 'CELANA PANJANG AVSEC & TEKNISI PRIA', 'PONTIANAK', 'PEJABAT AVSEC PRIA', '', 'EDP01', 1, 'SP', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 54, 'lg.pggl', 'Lingkar pinggul', 144, 'lgr.pesak', 'Vesak/ Pesak', 90, 'lg.paha', 'Lingkar paha', 94, 'lg.bawah', 'Dada Bawah', 48, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:27', '', 12, 0, 0, '', 1),
(8, 'ZULBRITO RADIKAR', '20001374', 'CELANA PANJANG NAVY', 'PONTIANAK', 'PEJABAT AVSEC PRIA', '', 'EDP01', 3, 'SP', 'pj.cln', 'Tinggi Celana panjang', 98, 'lg.pgg', 'Lingkar Pinggang', 54, 'lg.pggl', 'Lingkar pinggul', 144, 'lgr.pesak', 'Vesak/ Pesak', 90, 'lg.paha', 'Lingkar paha', 94, 'lg.bawah', 'Dada Bawah', 48, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:27', '', 31, 0, 0, '', 1),
(8, 'ZULBRITO RADIKAR', '20001374', 'KEMEJA L/S', 'PONTIANAK', 'PEJABAT AVSEC PRIA', '', 'EDP01', 1, 'SP', 'pj.tgn', 'Panjang Tangan', 33, 'lg.leher', 'Lingkar Leher', 20, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:26', '', 11, 0, 0, '', 1),
(8, 'ZULBRITO RADIKAR', '20001374', 'KEMEJA S/S', 'PONTIANAK', 'PEJABAT AVSEC PRIA', '', 'EDP01', 2, 'SP', 'pj.tgn', 'Panjang Tangan', 21, 'lg.leher', 'Lingkar Leher', 20, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:26', '', 12, 0, 0, '', 1),
(8, 'ZULBRITO RADIKAR', '20001374', 'KEMEJA S/S AVSEC PRIA', 'PONTIANAK', 'PEJABAT AVSEC PRIA', '', 'EDP01', 1, 'SP', 'pj.tgn', 'Panjang Tangan', 21, 'lg.leher', 'Lingkar Leher', 20, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:27', '', 11, 0, 0, '', 1),
(8, 'ZULFAHMI', '20003967', 'CELANA PANJANG NAVY', 'PADANG', 'BACK OFFICE PRIA', '2 KALI CLN G KEPKE', 'EDP01', 3, 'SP/31', 'pj.cln', 'Tinggi Celana panjang', 90, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, 'lgr.pesak', 'Vesak/ Pesak', 55, 'lg.paha', 'Lingkar paha', 0, 'lg.bawah', 'Dada Bawah', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:18', '', 2, 0, 0, '', 1),
(8, 'ZULPIADI', '20001049', 'KEMEJA L/S TEKNISI PRIA', 'PONTIANAK', 'TEKNISI PRIA', 'PJ BAJU 26', 'EDP01', 3, 'SP/15', 'pj.tgn', 'Panjang Tangan', 31, 'lg.leher', 'Lingkar Leher', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '2017-06-02 09:58:35', '', 1, 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `edp_assortment_pros`
--

CREATE TABLE `edp_assortment_pros` (
  `id` int(11) NOT NULL,
  `import_id` int(11) NOT NULL DEFAULT '0',
  `nourut` int(11) DEFAULT NULL,
  `nama` varchar(50) NOT NULL DEFAULT '',
  `nik` varchar(20) NOT NULL DEFAULT '',
  `nama_pakaian` varchar(50) NOT NULL DEFAULT '',
  `lokasi` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `info_tambahan` varchar(50) DEFAULT NULL,
  `size` varchar(11) DEFAULT '',
  `nama_ukuran_f1` varchar(50) DEFAULT NULL,
  `alias_ukuran_f1` varchar(50) DEFAULT NULL,
  `ukuran_f1` float DEFAULT '0',
  `nama_ukuran_f2` varchar(50) DEFAULT NULL,
  `alias_ukuran_f2` varchar(50) DEFAULT NULL,
  `ukuran_f2` float DEFAULT '0',
  `nama_ukuran_f3` varchar(50) DEFAULT NULL,
  `alias_ukuran_f3` varchar(50) DEFAULT NULL,
  `ukuran_f3` float DEFAULT '0',
  `nama_ukuran_f4` varchar(50) DEFAULT NULL,
  `alias_ukuran_f4` varchar(50) DEFAULT NULL,
  `ukuran_f4` float DEFAULT '0',
  `nama_ukuran_f5` varchar(50) DEFAULT NULL,
  `alias_ukuran_f5` varchar(50) DEFAULT NULL,
  `ukuran_f5` float DEFAULT '0',
  `nama_ukuran_f6` varchar(50) DEFAULT NULL,
  `alias_ukuran_f6` varchar(50) DEFAULT NULL,
  `ukuran_f6` float DEFAULT '0',
  `nama_ukuran_f7` varchar(50) DEFAULT NULL,
  `alias_ukuran_f7` varchar(50) DEFAULT NULL,
  `ukuran_f7` float DEFAULT '0',
  `nama_ukuran_f8` varchar(50) DEFAULT NULL,
  `alias_ukuran_f8` varchar(50) DEFAULT NULL,
  `ukuran_f8` float DEFAULT '0',
  `nama_ukuran_f9` varchar(50) DEFAULT NULL,
  `alias_ukuran_f9` varchar(50) DEFAULT NULL,
  `ukuran_f9` float DEFAULT '0',
  `nama_ukuran_f10` varchar(50) DEFAULT NULL,
  `alias_ukuran_f10` varchar(50) DEFAULT NULL,
  `ukuran_f10` float DEFAULT '0',
  `idStyle` int(11) NOT NULL,
  `artikel` varchar(35) NOT NULL,
  `idPo` int(11) NOT NULL,
  `idDept` int(11) NOT NULL,
  `status` varchar(25) NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL DEFAULT '0',
  `isActive` int(11) NOT NULL,
  `isMateriallist` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `edp_assortment_pros`
--

INSERT INTO `edp_assortment_pros` (`id`, `import_id`, `nourut`, `nama`, `nik`, `nama_pakaian`, `lokasi`, `jabatan`, `keterangan`, `info_tambahan`, `size`, `nama_ukuran_f1`, `alias_ukuran_f1`, `ukuran_f1`, `nama_ukuran_f2`, `alias_ukuran_f2`, `ukuran_f2`, `nama_ukuran_f3`, `alias_ukuran_f3`, `ukuran_f3`, `nama_ukuran_f4`, `alias_ukuran_f4`, `ukuran_f4`, `nama_ukuran_f5`, `alias_ukuran_f5`, `ukuran_f5`, `nama_ukuran_f6`, `alias_ukuran_f6`, `ukuran_f6`, `nama_ukuran_f7`, `alias_ukuran_f7`, `ukuran_f7`, `nama_ukuran_f8`, `alias_ukuran_f8`, `ukuran_f8`, `nama_ukuran_f9`, `alias_ukuran_f9`, `ukuran_f9`, `nama_ukuran_f10`, `alias_ukuran_f10`, `ukuran_f10`, `idStyle`, `artikel`, `idPo`, `idDept`, `status`, `tgl_proses`, `userId`, `isActive`, `isMateriallist`) VALUES
(1, 8, 2, 'Dra. WAHYU WIDAYATI', '20000290', 'BLAZER NAVY', 'BANDA ACEH', 'PEJABAT WANITA JILBAB', 'BISEP 18,5,PINGGANG ROK 79,PINGGUL 110,BOTTOM 70', 'EDP', 'SP/16', 'pj.baju', 'Panjang Baju', 66, 'pj.tgn', 'Panjang Tangan', 50, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(2, 8, 3, 'Dra. WAHYU WIDAYATI', '20000290', 'BLAZER NAVY', 'BANDA ACEH', 'PEJABAT WANITA JILBAB', 'BISEP 18,5,PINGGANG ROK 79,PINGGUL 110,BOTTOM 70', 'EDP', 'SP/16', 'pj.baju', 'Panjang Baju', 66, 'pj.tgn', 'Panjang Tangan', 50, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(3, 8, 2, 'ELIZA MAHDALENA', '20001538', 'BLAZER NAVY', 'PADANG', 'BACK OFFICE WANITA JILBAB', 'UK LM', 'EDP', 'SP', 'pj.baju', 'Panjang Baju', 0, 'pj.tgn', 'Panjang Tangan', 0, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(4, 8, 3, 'ELIZA MAHDALENA', '20001538', 'BLAZER NAVY', 'PADANG', 'BACK OFFICE WANITA JILBAB', 'UK LM', 'EDP', 'SP', 'pj.baju', 'Panjang Baju', 0, 'pj.tgn', 'Panjang Tangan', 0, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(5, 8, 2, 'ERLINDA WISMAI', '20001546', 'BLAZER NAVY', 'PADANG', 'PEJABAT WANITA JILBAB', 'UK LM', 'EDP', 'SP', 'pj.baju', 'Panjang Baju', 0, 'pj.tgn', 'Panjang Tangan', 0, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(6, 8, 3, 'ERLINDA WISMAI', '20001546', 'BLAZER NAVY', 'PADANG', 'PEJABAT WANITA JILBAB', 'UK LM', 'EDP', 'SP', 'pj.baju', 'Panjang Baju', 0, 'pj.tgn', 'Panjang Tangan', 0, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(7, 8, 2, 'LUSITA TITIANDRI', '20001620', 'BLAZER NAVY', 'PADANG', 'BACK OFFICE WANITA JILBAB', '', 'EDP', 'SP/12', 'pj.baju', 'Panjang Baju', 62, 'pj.tgn', 'Panjang Tangan', 50, 'lg.dada', 'Panjang Dada', 46, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(8, 8, 3, 'LUSITA TITIANDRI', '20001620', 'BLAZER NAVY', 'PADANG', 'BACK OFFICE WANITA JILBAB', '', 'EDP', 'SP/12', 'pj.baju', 'Panjang Baju', 62, 'pj.tgn', 'Panjang Tangan', 50, 'lg.dada', 'Panjang Dada', 46, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(9, 8, 2, 'YUSTIANA', '20001757', 'BLAZER NAVY', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'UK BAJU JADI', 'EDP', 'SP', 'pj.baju', 'Panjang Baju', 74, 'pj.tgn', 'Panjang Tangan', 54, 'lg.dada', 'Panjang Dada', 61, 'lg.pgg', 'Lingkar Pinggang', 56, 'lg.pggl', 'Lingkar pinggul', 73, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(10, 8, 3, 'YUSTIANA', '20001757', 'BLAZER NAVY', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'UK BAJU JADI', 'EDP', 'SP', 'pj.baju', 'Panjang Baju', 74, 'pj.tgn', 'Panjang Tangan', 54, 'lg.dada', 'Panjang Dada', 61, 'lg.pgg', 'Lingkar Pinggang', 56, 'lg.pggl', 'Lingkar pinggul', 73, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(11, 8, 2, 'FITRIA ZAUFA LAILI', '20002871', 'BLAZER NAVY', 'BANDA ACEH', 'PEJABAT WANITA JILBAB', 'PINGGUL BLAZER 57', 'EDP', 'SP/12', 'pj.baju', 'Panjang Baju', 64, 'pj.tgn', 'Panjang Tangan', 54, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(12, 8, 3, 'FITRIA ZAUFA LAILI', '20002871', 'BLAZER NAVY', 'BANDA ACEH', 'PEJABAT WANITA JILBAB', 'PINGGUL BLAZER 57', 'EDP', 'SP/12', 'pj.baju', 'Panjang Baju', 64, 'pj.tgn', 'Panjang Tangan', 54, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(13, 8, 2, 'FARIANTI LADIA', '20003121', 'BLAZER NAVY', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'PUNDAK BLZ 40,BOTTOM 53', 'EDP', 'SP/14', 'pj.baju', 'Panjang Baju', 66, 'pj.tgn', 'Panjang Tangan', 52, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(14, 8, 3, 'FARIANTI LADIA', '20003121', 'BLAZER NAVY', 'BANDA ACEH', 'BACK OFFICE WANITA JILBAB', 'PUNDAK BLZ 40,BOTTOM 53', 'EDP', 'SP/14', 'pj.baju', 'Panjang Baju', 66, 'pj.tgn', 'Panjang Tangan', 52, 'lg.dada', 'Panjang Dada', 0, 'lg.pgg', 'Lingkar Pinggang', 0, 'lg.pggl', 'Lingkar pinggul', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(15, 8, 2, 'LISDA YUNITA SIMATUPANG', '20003748', 'BLAZER NAVY', 'SILANGIT', 'PEJABAT WANITA NON JILBAB', 'JD', 'EDP', 'SP', 'pj.baju', 'Panjang Baju', 60, 'pj.tgn', 'Panjang Tangan', 55, 'lg.dada', 'Panjang Dada', 53, 'lg.pgg', 'Lingkar Pinggang', 43, 'lg.pggl', 'Lingkar pinggul', 54, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(16, 8, 3, 'LISDA YUNITA SIMATUPANG', '20003748', 'BLAZER NAVY', 'SILANGIT', 'PEJABAT WANITA NON JILBAB', 'JD', 'EDP', 'SP', 'pj.baju', 'Panjang Baju', 60, 'pj.tgn', 'Panjang Tangan', 55, 'lg.dada', 'Panjang Dada', 53, 'lg.pgg', 'Lingkar Pinggang', 43, 'lg.pggl', 'Lingkar pinggul', 54, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(17, 8, 2, 'MELARINI PAKPAHAN', '20005164', 'BLAZER NAVY', 'SILANGIT', 'BACK OFFICE WANITA NON JILBAB', 'JD', 'EDP', 'SP', 'pj.baju', 'Panjang Baju', 62, 'pj.tgn', 'Panjang Tangan', 54, 'lg.dada', 'Panjang Dada', 92, 'lg.pgg', 'Lingkar Pinggang', 78, 'lg.pggl', 'Lingkar pinggul', 102, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0),
(18, 8, 3, 'MELARINI PAKPAHAN', '20005164', 'BLAZER NAVY', 'SILANGIT', 'BACK OFFICE WANITA NON JILBAB', 'JD', 'EDP', 'SP', 'pj.baju', 'Panjang Baju', 62, 'pj.tgn', 'Panjang Tangan', 54, 'lg.dada', 'Panjang Dada', 92, 'lg.pgg', 'Lingkar Pinggang', 78, 'lg.pggl', 'Lingkar pinggul', 102, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 7, '', 1, 5, 'OPEN', '2019-02-09 07:22:11', 'Guest', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `log_cust_design`
--

CREATE TABLE `log_cust_design` (
  `idProspecting` int(11) NOT NULL,
  `keterangan` varchar(45) NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_param_bahan`
--

CREATE TABLE `log_param_bahan` (
  `idBahan` int(11) NOT NULL,
  `namaBahan` varchar(45) NOT NULL,
  `idSatuan` int(11) NOT NULL,
  `idKategori` int(11) NOT NULL,
  `keterangan` varchar(80) NOT NULL,
  `pict` varchar(100) NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_param_bahan`
--

INSERT INTO `log_param_bahan` (`idBahan`, `namaBahan`, `idSatuan`, `idKategori`, `keterangan`, `pict`, `tgl_proses`, `userId`) VALUES
(1, 'DOBBY COL', 1, 1, 'Insert Bahan', 'dominared.jpg', '2019-01-27 06:42:42', 'System'),
(1, 'DOBBY COL123', 1, 1, 'Update Bahan', 'dominared.jpg', '2019-01-27 06:51:32', 'System'),
(2, 'Taipan', 1, 1, 'Insert Bahan', 'Taipan2.jpg', '2019-01-27 06:52:04', 'System');

-- --------------------------------------------------------

--
-- Table structure for table `log_param_style`
--

CREATE TABLE `log_param_style` (
  `idStyle` int(11) NOT NULL,
  `namaStyle` varchar(35) NOT NULL,
  `status` varchar(15) NOT NULL,
  `keterangan` text NOT NULL,
  `pict` varchar(100) NOT NULL,
  `isActive` int(11) NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_param_style`
--

INSERT INTO `log_param_style` (`idStyle`, `namaStyle`, `status`, `keterangan`, `pict`, `isActive`, `tgl_proses`, `userId`) VALUES
(3, 'TEST STYLE 1', 'design', 'Insert Style', 'emas-12.jpg', 0, '2019-01-27 07:38:45', 'System'),
(3, 'TEST STYLE 1', 'design', 'update Style', 'emas-12.jpg', 0, '2019-01-27 09:50:13', 'System'),
(3, 'TEST STYLE 11', 'design', 'update Style', 'emas-12.jpg', 0, '2019-01-27 09:51:51', 'System'),
(3, 'TEST STYLE 1', 'design', 'update Style', 'emas-12.jpg', 1, '2019-01-27 09:51:57', 'System'),
(4, 'SMP-01', 'sample', 'Insert Style', 'user.png', 1, '2019-01-27 09:57:15', 'System'),
(5, 'STY01', 'style', 'Insert Style', 'plush.png', 1, '2019-01-27 09:59:18', 'System'),
(6, '8', 'style', 'Insert Style', 'foto.jpg', 1, '2019-02-09 05:44:31', 'Guest'),
(7, 'BLAZER NAVY', 'style', 'Insert Style', 'foto.jpg', 1, '2019-02-09 05:51:11', 'Guest'),
(8, 'BLOUSE KRAH BIRU', 'style', 'Insert Style', 'foto.jpg', 1, '2019-02-09 06:20:46', 'Guest');

-- --------------------------------------------------------

--
-- Table structure for table `mkt_po_apv`
--

CREATE TABLE `mkt_po_apv` (
  `idPo` int(11) NOT NULL,
  `keterangan` varchar(45) NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_po_apv`
--

INSERT INTO `mkt_po_apv` (`idPo`, `keterangan`, `status`, `tgl_proses`, `userId`) VALUES
(1, 'ok', 1, '2019-02-08 20:09:33', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `mkt_po_sm`
--

CREATE TABLE `mkt_po_sm` (
  `idPo` int(11) NOT NULL,
  `idProspecting` int(11) NOT NULL,
  `refNo` varchar(20) NOT NULL,
  `noPo` varchar(20) NOT NULL,
  `noOr` varchar(20) NOT NULL,
  `jenis` varchar(15) NOT NULL,
  `idClient` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `tglKirim` date NOT NULL,
  `qty` int(11) NOT NULL,
  `amount` double NOT NULL,
  `satuan` varchar(3) NOT NULL,
  `pcs` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status` varchar(25) NOT NULL,
  `idDept` int(11) NOT NULL,
  `tgl_proses` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_po_sm`
--

INSERT INTO `mkt_po_sm` (`idPo`, `idProspecting`, `refNo`, `noPo`, `noOr`, `jenis`, `idClient`, `tanggal`, `tglKirim`, `qty`, `amount`, `satuan`, `pcs`, `keterangan`, `status`, `idDept`, `tgl_proses`, `userId`, `isActive`) VALUES
(1, 1, '0001/MKT-PO/AP2/02/2', 'PO-01', 'OR-01', '', 1, '2019-02-09', '2019-02-28', 20, 10000000, 'PCS', 20, '', 'MKT-PO', 1, '2019-02-09 01:58:44', 'admin', 1),
(2, 0, '0002/MKT-PO/AP2/02/2', '1PO-011111', '1OR-011111', 'Uniform', 1, '2019-02-09', '2019-02-28', 201, 10000000, 'PCS', 201, 'cscscscsc', 'MKT-PO', 1, '2019-02-09 02:29:52', 'admin', 0),
(3, 0, '0003/MKT-PO/AP2/02/2', '1PO-01', '1OR-01', 'Uniform', 1, '2019-02-09', '2019-02-28', 11, 10000000, 'PCS', 20111, '', 'MKT-PO', 1, '2019-02-09 02:30:05', 'admin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mkt_po_sty`
--

CREATE TABLE `mkt_po_sty` (
  `idPo` int(11) NOT NULL,
  `idStyle` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `size` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_po_sty`
--

INSERT INTO `mkt_po_sty` (`idPo`, `idStyle`, `qty`, `size`) VALUES
(1, 3, 11, 'M'),
(1, 3, 10, 'S');

-- --------------------------------------------------------

--
-- Table structure for table `mkt_pros_cost`
--

CREATE TABLE `mkt_pros_cost` (
  `idProspecting` int(11) NOT NULL,
  `nourut` int(11) NOT NULL,
  `pict` varchar(100) NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL,
  `isActive` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_pros_cost`
--

INSERT INTO `mkt_pros_cost` (`idProspecting`, `nourut`, `pict`, `keterangan`, `tgl_proses`, `userId`, `isActive`) VALUES
(1, 1, 'Dika16_walet.png', 'aaa', '2019-02-02 07:15:40', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mkt_pros_cost_apv`
--

CREATE TABLE `mkt_pros_cost_apv` (
  `idProspecting` int(11) NOT NULL,
  `keterangan` varchar(45) NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_pros_cost_apv`
--

INSERT INTO `mkt_pros_cost_apv` (`idProspecting`, `keterangan`, `status`, `tgl_proses`, `userId`) VALUES
(1, 'Ok Cost', 1, '2019-02-02 07:44:40', 'admin'),
(1, 'Bandung', 1, '2019-02-02 07:45:36', 'admin'),
(1, 'Bandung', 1, '2019-02-02 07:46:07', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `mkt_pros_desgn`
--

CREATE TABLE `mkt_pros_desgn` (
  `idProspecting` int(11) NOT NULL,
  `nourut` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `idStyle` int(11) NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL,
  `isActive` int(11) NOT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_pros_desgn`
--

INSERT INTO `mkt_pros_desgn` (`idProspecting`, `nourut`, `tanggal`, `idStyle`, `keterangan`, `tgl_proses`, `userId`, `isActive`, `status`) VALUES
(1, 1, '2019-02-01', 3, 'aaaa', '2019-02-01 13:38:58', 'admin', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mkt_pros_desgn_apv`
--

CREATE TABLE `mkt_pros_desgn_apv` (
  `idProspecting` int(11) NOT NULL,
  `status` varchar(25) NOT NULL,
  `keterangan` text NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_pros_desgn_apv`
--

INSERT INTO `mkt_pros_desgn_apv` (`idProspecting`, `status`, `keterangan`, `tgl_proses`, `userId`) VALUES
(1, '1', 'ok', '2019-02-02 03:23:12', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `mkt_pros_desgn_bhn`
--

CREATE TABLE `mkt_pros_desgn_bhn` (
  `idProspecting` int(11) NOT NULL,
  `idStyle` int(11) NOT NULL,
  `idBahan` int(11) NOT NULL,
  `keterangan` varchar(45) NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_pros_desgn_bhn`
--

INSERT INTO `mkt_pros_desgn_bhn` (`idProspecting`, `idStyle`, `idBahan`, `keterangan`, `tgl_proses`) VALUES
(1, 3, 1, 'aa', '2019-02-01 13:51:32'),
(1, 3, 2, 'bb', '2019-02-01 13:51:37');

-- --------------------------------------------------------

--
-- Table structure for table `mkt_pros_dt`
--

CREATE TABLE `mkt_pros_dt` (
  `idProspecting` int(11) NOT NULL,
  `nourut` int(11) NOT NULL,
  `status` varchar(25) NOT NULL,
  `tgl_input` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_target` date NOT NULL,
  `keterangan` text NOT NULL,
  `tgl_proses` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_pros_dt`
--

INSERT INTO `mkt_pros_dt` (`idProspecting`, `nourut`, `status`, `tgl_input`, `tgl_target`, `keterangan`, `tgl_proses`, `userId`, `isActive`) VALUES
(1, 2, 'Admin Activity', '2019-02-01 17:30:04', '2019-02-01', 'Input Data Admin Activity', '2019-02-01 17:30:04', 'Guest', 1),
(1, 3, 'Design', '2019-02-01 17:39:30', '2019-02-01', 'Upload Design', '2019-02-01 17:39:30', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mkt_pros_sample`
--

CREATE TABLE `mkt_pros_sample` (
  `idSample` int(11) NOT NULL,
  `idProspecting` int(11) NOT NULL,
  `tgl_target` date NOT NULL,
  `refNo` varchar(20) NOT NULL,
  `item` text NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL,
  `isActive` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_pros_sample`
--

INSERT INTO `mkt_pros_sample` (`idSample`, `idProspecting`, `tgl_target`, `refNo`, `item`, `keterangan`, `tgl_proses`, `userId`, `isActive`, `status`) VALUES
(3, 1, '2019-02-02', '0003/SMP/02/2019', 'Baju', 'Cek Upload Sample', '2019-02-02 09:42:39', 'admin', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mkt_pros_sample_apv`
--

CREATE TABLE `mkt_pros_sample_apv` (
  `idSample` int(11) NOT NULL,
  `status` varchar(25) NOT NULL,
  `keterangan` text NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_pros_sample_apv`
--

INSERT INTO `mkt_pros_sample_apv` (`idSample`, `status`, `keterangan`, `tgl_proses`, `userId`) VALUES
(3, '1', 'kgj', '2019-02-02 11:17:38', 'admin'),
(3, '1', 'ok bro', '2019-02-08 17:38:59', 'Guest'),
(3, '1', 'axxxx', '2019-02-08 17:43:07', 'Guest'),
(3, '1', 'sss', '2019-02-08 17:43:58', 'Guest');

-- --------------------------------------------------------

--
-- Table structure for table `mkt_pros_sample_bhn`
--

CREATE TABLE `mkt_pros_sample_bhn` (
  `idSample` int(11) NOT NULL,
  `idBahan` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `tgl_proses` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_pros_sample_bhn`
--

INSERT INTO `mkt_pros_sample_bhn` (`idSample`, `idBahan`, `keterangan`, `tgl_proses`, `userId`) VALUES
(3, 1, 'data', '2019-02-02 17:05:09', 'admin'),
(3, 2, 'bbcc', '2019-02-02 17:05:15', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `mkt_pros_sample_sty`
--

CREATE TABLE `mkt_pros_sample_sty` (
  `idSample` int(11) NOT NULL,
  `idStyle` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `tgl_proses` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_pros_sample_sty`
--

INSERT INTO `mkt_pros_sample_sty` (`idSample`, `idStyle`, `keterangan`, `tgl_proses`, `userId`) VALUES
(3, 4, 'jjjj', '2019-02-02 17:47:16', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `mkt_pros_sm`
--

CREATE TABLE `mkt_pros_sm` (
  `idProspecting` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `refNo` varchar(20) NOT NULL,
  `idClient` int(11) NOT NULL,
  `idInformasi` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status` varchar(25) NOT NULL,
  `tgl_proses` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL,
  `isActive` int(11) NOT NULL,
  `isDesign` int(11) NOT NULL DEFAULT '0',
  `isApvCust` int(11) NOT NULL DEFAULT '0',
  `isCostCalc` int(11) NOT NULL DEFAULT '0',
  `isSample` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mkt_pros_sm`
--

INSERT INTO `mkt_pros_sm` (`idProspecting`, `tanggal`, `refNo`, `idClient`, `idInformasi`, `keterangan`, `status`, `tgl_proses`, `userId`, `isActive`, `isDesign`, `isApvCust`, `isCostCalc`, `isSample`) VALUES
(1, '2019-01-31', '0001/MKT-PROS/AP2/01', 1, 2, 'Coba prospecting 1', 'MKT_ADM', '2019-01-31 15:25:25', 'admin', 0, 1, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblbahan`
--

CREATE TABLE `tblbahan` (
  `idBahan` int(11) NOT NULL,
  `namaBahan` varchar(100) NOT NULL,
  `pict` varchar(100) NOT NULL,
  `idSatuan` int(11) NOT NULL,
  `idKategori` int(11) NOT NULL,
  `isActive` int(11) NOT NULL,
  `tgl_proses` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbahan`
--

INSERT INTO `tblbahan` (`idBahan`, `namaBahan`, `pict`, `idSatuan`, `idKategori`, `isActive`, `tgl_proses`, `userId`) VALUES
(1, 'DOBBY COL123', 'dominared.jpg', 1, 1, 1, '2019-01-27 13:42:42', 'System'),
(2, 'Taipan', 'Taipan2.jpg', 1, 1, 1, '2019-01-27 13:52:04', 'System');

--
-- Triggers `tblbahan`
--
DELIMITER $$
CREATE TRIGGER `tambah_log_bahan` AFTER INSERT ON `tblbahan` FOR EACH ROW BEGIN
    INSERT INTO log_param_bahan
    set idBahan = new.idBahan,
    namaBahan=new.namaBahan,
    pict=new.pict,
    idSatuan = new.idSatuan,
    idKategori = new.idKategori,
    keterangan='Insert Bahan',
    userId=new.userId;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_log_bahan` AFTER UPDATE ON `tblbahan` FOR EACH ROW BEGIN
    INSERT INTO log_param_bahan
    set idBahan = new.idBahan,
    namaBahan=new.namaBahan,
    pict=new.pict,
    idSatuan = new.idSatuan,
    idKategori = new.idKategori,
    keterangan='Update Bahan',
    userId=new.userId;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tblclient`
--

CREATE TABLE `tblclient` (
  `idClient` int(11) NOT NULL,
  `stClient` varchar(10) NOT NULL,
  `initClient` varchar(5) NOT NULL,
  `namaClient` varchar(80) NOT NULL,
  `alamat` text NOT NULL,
  `kodepos` varchar(5) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `mobilePhone` varchar(12) NOT NULL,
  `pic` varchar(45) NOT NULL,
  `jabatan` varchar(45) NOT NULL,
  `email` varchar(60) NOT NULL,
  `isActive` int(11) NOT NULL,
  `tgl_proses` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblclient`
--

INSERT INTO `tblclient` (`idClient`, `stClient`, `initClient`, `namaClient`, `alamat`, `kodepos`, `phone`, `mobilePhone`, `pic`, `jabatan`, `email`, `isActive`, `tgl_proses`, `userId`) VALUES
(1, 'Company', 'AP2', 'PT. ANGKASA PURA 2', 'BANDARA SUKARNO HATTA', '11313', '02131313131', '085719322919', 'Pranajaya', 'Manager', 'pranajaya161@gmail.com', 1, '2019-01-27 19:36:33', 'System');

-- --------------------------------------------------------

--
-- Table structure for table `tbldept`
--

CREATE TABLE `tbldept` (
  `idDept` int(11) NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbldept`
--

INSERT INTO `tbldept` (`idDept`, `keterangan`, `isActive`) VALUES
(1, 'Marketing', 1),
(2, 'Design', 1),
(3, 'Sample', 1),
(4, 'MD', 1),
(5, 'EDP', 1),
(6, 'PPIC', 1),
(7, 'Cutting', 1),
(8, 'Sewing', 1),
(9, 'Triming', 1),
(10, 'QC Produksi', 1),
(11, 'Setting', 1),
(12, 'Packing', 1),
(13, 'Purchashing', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblinformasi`
--

CREATE TABLE `tblinformasi` (
  `idInformasi` int(11) NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblinformasi`
--

INSERT INTO `tblinformasi` (`idInformasi`, `keterangan`, `isActive`) VALUES
(1, 'Canvasing - Mkt', 1),
(2, 'Agent', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblkategori`
--

CREATE TABLE `tblkategori` (
  `idKategori` int(11) NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblkategori`
--

INSERT INTO `tblkategori` (`idKategori`, `keterangan`, `isActive`) VALUES
(1, 'Bahan Baku', 1),
(2, 'Assesoris', 1),
(3, 'Lainnya', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblmenu`
--

CREATE TABLE `tblmenu` (
  `idMenu` int(11) NOT NULL,
  `headMenu` int(11) NOT NULL,
  `subMenu` int(11) NOT NULL,
  `childMenu` int(11) NOT NULL,
  `namaMenu` varchar(45) NOT NULL,
  `link` varchar(35) NOT NULL,
  `nourut` int(11) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblmenu`
--

INSERT INTO `tblmenu` (`idMenu`, `headMenu`, `subMenu`, `childMenu`, `namaMenu`, `link`, `nourut`, `isActive`) VALUES
(1, 1, 0, 0, 'Setting', '#', 0, 1),
(2, 1, 1, 0, 'User', 'sett/user', 1, 1),
(3, 1, 2, 0, 'Master Menu', 'sett/tmenu', 2, 0),
(4, 1, 3, 0, 'Master Modul', 'sett/tmodul', 3, 1),
(5, 5, 0, 0, 'Parameter', '#', 0, 1),
(6, 5, 1, 0, 'Style Bahan', 'param/MStyle1', 1, 1),
(7, 5, 2, 0, 'Style Produk', 'param/MStyle2', 2, 0),
(8, 5, 3, 0, 'Style Design', 'param/MStyle3', 2, 1),
(9, 5, 4, 0, 'Style Sample', 'param/MStyle4', 4, 1),
(10, 5, 5, 0, 'Satuan', 'param/Msatuan', 5, 1),
(11, 5, 6, 0, 'Data Customer (Buyer)', 'param/Mclient', 6, 1),
(12, 12, 0, 0, 'Marketing', '#', 0, 1),
(13, 12, 1, 0, 'Prospecting', '#', 1, 1),
(14, 12, 1, 1, 'Input&nbsp;Admin&nbsp;Activity', 'mkt/prospecting', 1, 1),
(15, 12, 1, 1, 'Appv&nbsp;Cust&nbsp;Design', 'mkt/apvmktDesgn', 2, 1),
(16, 12, 1, 1, 'Upload&nbsp;Cost&nbsp;Calculation', 'mkt/costCalculate', 3, 1),
(17, 12, 1, 1, 'Appv&nbsp;Cost&nbsp;Calculation', 'mkt/apvmktCost', 4, 1),
(18, 12, 2, 0, 'Input&nbsp;Purchase&nbsp;Order', 'mkt/purchaseOrder', 2, 1),
(19, 12, 3, 0, 'Input&nbsp;Purchase&nbsp;Order&nbsp;[Manual] ', 'mkt/purchaseOrder_1', 3, 1),
(20, 12, 4, 0, 'Appv&nbsp;Purchase&nbsp;Order', 'mkt/ApvpurchaseOrder', 4, 1),
(21, 21, 0, 0, 'Design', '#', 0, 1),
(22, 21, 1, 0, 'Design Request', 'desg/prosDesignReq', 1, 1),
(23, 21, 6, 0, 'Material Request ', 'desg/designReqPurc', 3, 1),
(24, 21, 5, 0, 'Approve&nbsp;Material', 'desg/ApvdesignReqPurc', 4, 1),
(25, 21, 7, 0, 'Laporan Design', '#', 4, 1),
(26, 21, 7, 1, 'Laporan&nbsp;Design', 'desg/QueryDesignReq', 1, 1),
(27, 21, 7, 1, 'Laporan&nbsp;Material', 'desg/QueryDesignReq1', 2, 1),
(28, 28, 0, 0, 'Sample', '#', 0, 1),
(29, 28, 1, 0, 'Sample Request', 'samp/prosSampleReq', 1, 1),
(30, 28, 2, 0, 'Material Request ', 'samp/sampReqPurc', 2, 0),
(31, 28, 3, 0, 'Approve Material Request', 'samp/ApvSampReqPurc', 3, 0),
(32, 28, 4, 0, 'Laporan Sample', '#', 4, 1),
(33, 28, 4, 1, 'Lap&nbsp;Sample&nbsp;Request', 'samp/QuerySampleReq', 1, 1),
(34, 12, 1, 1, 'Appv&nbsp;Customer&nbsp;Sample', 'mkt/apvmktSample', 5, 1),
(35, 28, 4, 1, 'Laporan Design Log', 'samp/QuerySampleReq1', 2, 0),
(36, 12, 5, 0, 'Upload Data Karyawan', 'upl/dockaryawan', 5, 1),
(37, 12, 6, 0, 'Referensi & Jaminan Penawaran', 'mkt/refJaminan', 6, 1),
(38, 12, 7, 0, 'Laporan', '#', 8, 1),
(39, 12, 7, 1, 'Lap Admin Activity', 'mkt/rptActadm', 1, 1),
(40, 12, 7, 1, 'Lap Purchase Order', 'mkt/quepurchaseOrder', 2, 1),
(41, 12, 7, 1, 'Lap Cost Calculation', 'mkt/qrymktCost', 3, 1),
(42, 12, 7, 1, 'Tracking PO', 'mkt/qrytrackpo', 4, 1),
(43, 41, 0, 0, 'EDP', '#', 0, 1),
(44, 43, 1, 0, 'Assortment', '#', 1, 1),
(45, 43, 1, 1, 'Assortment Edp (1)', 'edp/uploadAssortMent1', 1, 1),
(46, 43, 1, 1, 'Assortment Edp (2)', 'edp/uploadAssortMent2', 2, 1),
(47, 43, 2, 0, 'Transfer Data Assortment', 'edp/edpCutting', 2, 1),
(48, 43, 3, 0, 'Approval Assortment', 'edp/edpApvCutting', 3, 1),
(49, 43, 4, 0, 'Laporan', '#', 4, 1),
(50, 43, 4, 1, 'Query Assortment Upload', 'edp/queUplAssortment', 1, 1),
(51, 43, 4, 1, 'Generate Barcode', 'mds/genBarcode', 2, 1),
(52, 52, 0, 0, 'MD', '#', 0, 1),
(53, 52, 1, 0, 'Task Assortment', 'mds/taskAssort', 1, 0),
(54, 52, 2, 0, 'WorkSheet', '#', 2, 1),
(55, 52, 2, 1, 'Input Worksheet', 'mds/inputWorksheet', 1, 1),
(56, 52, 2, 1, 'Approval WorkSheet', 'mds/apvWorksheet', 2, 1),
(57, 5, 7, 0, 'Kategori Bahan', 'param/MStyle5', 7, 1),
(58, 21, 2, 0, 'Approve Design', 'desg/ApvprosDesignReq', 2, 1),
(59, 21, 4, 0, 'Approve Sample', 'desg/ApvprosSampleReq', 4, 1),
(60, 12, 8, 0, 'Additional Proses', '#', 10, 1),
(61, 12, 8, 1, 'Input&nbsp;Perubahan&nbsp;PO', 'adt/inputpo', 1, 1),
(62, 12, 8, 2, 'Approval&nbsp;Perubahan&nbsp;PO', 'adt/Apvinputpo', 1, 1),
(63, 12, 7, 5, 'Lap&nbsp;Perubahan&nbsp;PO', 'mkt/qryadditional', 5, 1),
(64, 52, 3, 0, 'Material List', '#', 0, 1),
(65, 52, 3, 1, 'Input&nbsp;MaterialList', 'mds/inputMaterial', 1, 1),
(66, 52, 3, 2, 'Apv MaterialList', 'mds/ApvMaterial', 2, 1),
(67, 67, 0, 0, 'PPIC', '#', 0, 1),
(68, 67, 1, 0, 'Schedule Cutting', 'ppic/cschcutting', 1, 1),
(69, 67, 1, 0, 'Schedule Sewing', 'ppic/cschsewing', 2, 1),
(70, 70, 0, 0, 'Cutting', '#', 0, 1),
(71, 70, 1, 0, 'Input Cutting', 'cut/inCutting', 1, 1),
(72, 70, 1, 0, 'Approval Cutting', 'cut/ApvinCutting', 2, 1),
(73, 21, 3, 0, 'Sample Request', 'desg/reqSample', 3, 1),
(74, 70, 2, 0, 'Keluar&nbsp;Cutting', 'cut/outCutting', 1, 1),
(75, 70, 2, 0, 'Approve&nbsp;Keluar&nbsp;Cutting', 'cut/ApvoutCutting', 2, 1),
(76, 76, 0, 0, 'Sewing', '#', 0, 1),
(77, 76, 1, 0, 'Penerimaan&nbsp;Cutting', 'sewing/terimaCutting', 1, 1),
(78, 76, 1, 0, 'Input Sewing', 'sewing/inputSewing', 2, 1),
(79, 76, 1, 0, 'QC Sewing', 'sewing/inputQcSewing', 3, 1),
(80, 76, 1, 0, 'Keluar Sewing', 'sewing/outSewing', 4, 1),
(81, 76, 1, 0, 'Apv&nbsp;Keluar&nbsp;Sewing', 'sewing/ApvoutSewing', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblmodul`
--

CREATE TABLE `tblmodul` (
  `idModul` int(11) NOT NULL,
  `namaModul` varchar(25) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblmodul`
--

INSERT INTO `tblmodul` (`idModul`, `namaModul`, `isActive`) VALUES
(1, 'Admin', 1),
(2, 'Marketing', 1),
(3, 'Design', 1),
(4, 'Sewing', 1),
(5, 'khusus', 1),
(6, 'sample', 1),
(7, 'edp', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblmodul_list`
--

CREATE TABLE `tblmodul_list` (
  `idModul` int(11) NOT NULL,
  `idMenu` int(11) NOT NULL,
  `headMenu` int(11) NOT NULL,
  `subMenu` int(11) NOT NULL,
  `childMenu` int(11) NOT NULL,
  `nourut` int(11) NOT NULL,
  `tgl_proses` date NOT NULL,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblmodul_list`
--

INSERT INTO `tblmodul_list` (`idModul`, `idMenu`, `headMenu`, `subMenu`, `childMenu`, `nourut`, `tgl_proses`, `userId`) VALUES
(4, 83, 83, 0, 0, 0, '2018-04-28', '0'),
(4, 84, 83, 1, 0, 1, '2018-04-28', '0'),
(4, 85, 83, 2, 0, 2, '2018-04-28', '0'),
(4, 86, 83, 3, 0, 3, '2018-04-28', '0'),
(4, 87, 83, 4, 0, 4, '2018-04-28', '0'),
(4, 88, 83, 4, 1, 1, '2018-04-28', '0'),
(4, 89, 83, 4, 1, 2, '2018-04-28', '0'),
(4, 90, 83, 4, 1, 3, '2018-04-28', '0'),
(4, 91, 83, 5, 0, 5, '2018-04-28', '0'),
(4, 92, 92, 0, 0, 0, '2018-04-28', '0'),
(4, 93, 92, 1, 0, 1, '2018-04-28', '0'),
(4, 94, 92, 2, 0, 2, '2018-04-28', '0'),
(4, 95, 92, 3, 0, 3, '2018-04-28', '0'),
(4, 112, 92, 4, 0, 5, '2018-04-28', '0'),
(4, 96, 96, 0, 0, 0, '2018-04-28', '0'),
(4, 97, 96, 1, 0, 1, '2018-04-28', '0'),
(4, 98, 96, 2, 0, 2, '2018-04-28', '0'),
(4, 99, 96, 3, 0, 3, '2018-04-28', '0'),
(4, 100, 96, 3, 1, 1, '2018-04-28', '0'),
(4, 101, 96, 3, 1, 2, '2018-04-28', '0'),
(4, 113, 96, 4, 0, 4, '2018-04-28', '0'),
(4, 102, 102, 0, 0, 0, '2018-04-28', '0'),
(4, 104, 102, 1, 0, 1, '2018-04-28', '0'),
(4, 105, 102, 2, 0, 2, '2018-04-28', '0'),
(4, 106, 102, 3, 0, 3, '2018-04-28', '0'),
(4, 107, 102, 4, 0, 4, '2018-04-28', '0'),
(4, 103, 103, 0, 0, 0, '2018-04-28', '0'),
(4, 108, 103, 1, 0, 1, '2018-04-28', '0'),
(4, 109, 103, 2, 0, 2, '2018-04-28', '0'),
(4, 110, 103, 3, 0, 3, '2018-04-28', '0'),
(4, 111, 103, 4, 0, 4, '2018-04-28', '0'),
(0, 1, 1, 0, 0, 0, '2018-06-03', '0'),
(0, 2, 1, 1, 0, 1, '2018-06-03', '0'),
(5, 1, 1, 0, 0, 0, '2018-06-03', '0'),
(5, 2, 1, 1, 0, 1, '2018-06-03', '0'),
(6, 5, 5, 0, 0, 0, '2018-06-23', '0'),
(6, 9, 5, 4, 0, 4, '2018-06-23', '0'),
(6, 28, 28, 0, 0, 0, '2018-06-23', '0'),
(6, 29, 28, 1, 0, 1, '2018-06-23', '0'),
(6, 32, 28, 4, 0, 4, '2018-06-23', '0'),
(6, 33, 28, 4, 1, 1, '2018-06-23', '0'),
(2, 5, 5, 0, 0, 0, '2018-07-09', '0'),
(2, 11, 5, 6, 0, 6, '2018-07-09', '0'),
(2, 12, 12, 0, 0, 0, '2018-07-09', '0'),
(2, 13, 12, 1, 0, 1, '2018-07-09', '0'),
(2, 14, 12, 1, 1, 1, '2018-07-09', '0'),
(2, 15, 12, 1, 1, 2, '2018-07-09', '0'),
(2, 16, 12, 1, 1, 3, '2018-07-09', '0'),
(2, 17, 12, 1, 1, 4, '2018-07-09', '0'),
(2, 34, 12, 1, 1, 5, '2018-07-09', '0'),
(2, 18, 12, 2, 0, 2, '2018-07-09', '0'),
(2, 19, 12, 3, 0, 3, '2018-07-09', '0'),
(2, 20, 12, 4, 0, 4, '2018-07-09', '0'),
(2, 36, 12, 5, 0, 5, '2018-07-09', '0'),
(2, 37, 12, 6, 0, 6, '2018-07-09', '0'),
(2, 38, 12, 7, 0, 8, '2018-07-09', '0'),
(2, 39, 12, 7, 1, 1, '2018-07-09', '0'),
(2, 40, 12, 7, 1, 2, '2018-07-09', '0'),
(2, 41, 12, 7, 1, 3, '2018-07-09', '0'),
(2, 42, 12, 7, 1, 4, '2018-07-09', '0'),
(3, 5, 5, 0, 0, 0, '2018-07-09', '0'),
(3, 6, 5, 1, 0, 1, '2018-07-09', '0'),
(3, 8, 5, 3, 0, 2, '2018-07-09', '0'),
(3, 57, 5, 7, 0, 7, '2018-07-09', '0'),
(3, 21, 21, 0, 0, 0, '2018-07-09', '0'),
(3, 22, 21, 1, 0, 1, '2018-07-09', '0'),
(3, 58, 21, 2, 0, 2, '2018-07-09', '0'),
(3, 73, 21, 3, 0, 3, '2018-07-09', '0'),
(3, 59, 21, 4, 0, 4, '2018-07-09', '0'),
(3, 24, 21, 5, 0, 4, '2018-07-09', '0'),
(3, 23, 21, 6, 0, 3, '2018-07-09', '0'),
(3, 25, 21, 7, 0, 4, '2018-07-09', '0'),
(3, 26, 21, 7, 1, 1, '2018-07-09', '0'),
(3, 27, 21, 7, 1, 2, '2018-07-09', '0'),
(1, 1, 1, 0, 0, 0, '2018-07-10', '0'),
(1, 2, 1, 1, 0, 1, '2018-07-10', '0'),
(1, 4, 1, 3, 0, 3, '2018-07-10', '0'),
(1, 5, 5, 0, 0, 0, '2018-07-10', '0'),
(1, 6, 5, 1, 0, 1, '2018-07-10', '0'),
(1, 8, 5, 3, 0, 2, '2018-07-10', '0'),
(1, 9, 5, 4, 0, 4, '2018-07-10', '0'),
(1, 10, 5, 5, 0, 5, '2018-07-10', '0'),
(1, 11, 5, 6, 0, 6, '2018-07-10', '0'),
(1, 57, 5, 7, 0, 7, '2018-07-10', '0'),
(1, 12, 12, 0, 0, 0, '2018-07-10', '0'),
(1, 13, 12, 1, 0, 1, '2018-07-10', '0'),
(1, 14, 12, 1, 1, 1, '2018-07-10', '0'),
(1, 15, 12, 1, 1, 2, '2018-07-10', '0'),
(1, 16, 12, 1, 1, 3, '2018-07-10', '0'),
(1, 17, 12, 1, 1, 4, '2018-07-10', '0'),
(1, 34, 12, 1, 1, 5, '2018-07-10', '0'),
(1, 18, 12, 2, 0, 2, '2018-07-10', '0'),
(1, 19, 12, 3, 0, 3, '2018-07-10', '0'),
(1, 20, 12, 4, 0, 4, '2018-07-10', '0'),
(1, 36, 12, 5, 0, 5, '2018-07-10', '0'),
(1, 37, 12, 6, 0, 6, '2018-07-10', '0'),
(1, 38, 12, 7, 0, 8, '2018-07-10', '0'),
(1, 39, 12, 7, 1, 1, '2018-07-10', '0'),
(1, 40, 12, 7, 1, 2, '2018-07-10', '0'),
(1, 41, 12, 7, 1, 3, '2018-07-10', '0'),
(1, 42, 12, 7, 1, 4, '2018-07-10', '0'),
(1, 63, 12, 7, 5, 5, '2018-07-10', '0'),
(1, 60, 12, 8, 0, 10, '2018-07-10', '0'),
(1, 61, 12, 8, 1, 1, '2018-07-10', '0'),
(1, 62, 12, 8, 2, 1, '2018-07-10', '0'),
(1, 21, 21, 0, 0, 0, '2018-07-10', '0'),
(1, 22, 21, 1, 0, 1, '2018-07-10', '0'),
(1, 58, 21, 2, 0, 2, '2018-07-10', '0'),
(1, 73, 21, 3, 0, 3, '2018-07-10', '0'),
(1, 59, 21, 4, 0, 4, '2018-07-10', '0'),
(1, 24, 21, 5, 0, 4, '2018-07-10', '0'),
(1, 23, 21, 6, 0, 3, '2018-07-10', '0'),
(1, 25, 21, 7, 0, 4, '2018-07-10', '0'),
(1, 26, 21, 7, 1, 1, '2018-07-10', '0'),
(1, 27, 21, 7, 1, 2, '2018-07-10', '0'),
(1, 28, 28, 0, 0, 0, '2018-07-10', '0'),
(1, 29, 28, 1, 0, 1, '2018-07-10', '0'),
(1, 32, 28, 4, 0, 4, '2018-07-10', '0'),
(1, 33, 28, 4, 1, 1, '2018-07-10', '0'),
(1, 43, 41, 0, 0, 0, '2018-07-10', '0'),
(1, 44, 43, 1, 0, 1, '2018-07-10', '0'),
(1, 45, 43, 1, 1, 1, '2018-07-10', '0'),
(1, 46, 43, 1, 1, 2, '2018-07-10', '0'),
(1, 47, 43, 2, 0, 2, '2018-07-10', '0'),
(1, 48, 43, 3, 0, 3, '2018-07-10', '0'),
(1, 49, 43, 4, 0, 4, '2018-07-10', '0'),
(1, 50, 43, 4, 1, 1, '2018-07-10', '0'),
(1, 51, 43, 4, 1, 2, '2018-07-10', '0'),
(1, 52, 52, 0, 0, 0, '2018-07-10', '0'),
(1, 54, 52, 2, 0, 2, '2018-07-10', '0'),
(1, 55, 52, 2, 1, 1, '2018-07-10', '0'),
(1, 56, 52, 2, 1, 2, '2018-07-10', '0'),
(1, 64, 52, 3, 0, 0, '2018-07-10', '0'),
(1, 65, 52, 3, 1, 1, '2018-07-10', '0'),
(1, 66, 52, 3, 2, 2, '2018-07-10', '0'),
(1, 67, 67, 0, 0, 0, '2018-07-10', '0'),
(1, 68, 67, 1, 0, 1, '2018-07-10', '0'),
(1, 69, 67, 1, 0, 2, '2018-07-10', '0'),
(1, 70, 70, 0, 0, 0, '2018-07-10', '0'),
(1, 71, 70, 1, 0, 1, '2018-07-10', '0'),
(1, 72, 70, 1, 0, 2, '2018-07-10', '0'),
(1, 74, 70, 2, 0, 1, '2018-07-10', '0'),
(1, 75, 70, 2, 0, 2, '2018-07-10', '0'),
(1, 76, 76, 0, 0, 0, '2018-07-10', '0'),
(1, 77, 76, 1, 0, 1, '2018-07-10', '0'),
(1, 78, 76, 1, 0, 2, '2018-07-10', '0'),
(1, 79, 76, 1, 0, 3, '2018-07-10', '0'),
(1, 80, 76, 1, 0, 4, '2018-07-10', '0'),
(1, 81, 76, 1, 0, 5, '2018-07-10', '0'),
(7, 43, 41, 0, 0, 0, '2018-09-17', '0'),
(7, 44, 43, 1, 0, 1, '2018-09-17', '0'),
(7, 45, 43, 1, 1, 1, '2018-09-17', '0'),
(7, 46, 43, 1, 1, 2, '2018-09-17', '0'),
(7, 47, 43, 2, 0, 2, '2018-09-17', '0'),
(7, 48, 43, 3, 0, 3, '2018-09-17', '0'),
(7, 49, 43, 4, 0, 4, '2018-09-17', '0'),
(7, 50, 43, 4, 1, 1, '2018-09-17', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tblsatuan`
--

CREATE TABLE `tblsatuan` (
  `idSatuan` int(11) NOT NULL,
  `initSatuan` varchar(5) NOT NULL,
  `namaSatuan` varchar(15) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblsatuan`
--

INSERT INTO `tblsatuan` (`idSatuan`, `initSatuan`, `namaSatuan`, `isActive`) VALUES
(1, 'YRD', 'Yard', 1),
(2, 'MTR', 'Meter', 1),
(3, 'PCS', 'Pcs', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblstyle`
--

CREATE TABLE `tblstyle` (
  `idStyle` int(11) NOT NULL,
  `namaStyle` varchar(35) NOT NULL,
  `isActive` int(11) NOT NULL,
  `pict` varchar(100) NOT NULL,
  `status` varchar(15) NOT NULL,
  `tgl_proses` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstyle`
--

INSERT INTO `tblstyle` (`idStyle`, `namaStyle`, `isActive`, `pict`, `status`, `tgl_proses`, `userId`) VALUES
(3, 'TEST STYLE 1', 1, 'emas-12.jpg', 'design', '2019-01-27 14:38:45', 'System'),
(4, 'SMP-01', 1, 'user.png', 'sample', '2019-01-27 16:57:15', 'System'),
(5, 'STY01', 1, 'plush.png', 'style', '2019-01-27 16:59:18', 'System'),
(6, '8', 1, 'foto.jpg', 'style', '2019-02-09 12:44:31', 'Guest'),
(7, 'BLAZER NAVY', 1, 'foto.jpg', 'style', '2019-02-09 12:51:11', 'Guest'),
(8, 'BLOUSE KRAH BIRU', 1, 'foto.jpg', 'style', '2019-02-09 13:20:46', 'Guest');

--
-- Triggers `tblstyle`
--
DELIMITER $$
CREATE TRIGGER `tambah_log_style` AFTER INSERT ON `tblstyle` FOR EACH ROW BEGIN
    INSERT INTO log_param_style
    set idStyle = new.idStyle,
    namaStyle=new.namaStyle,
    pict=new.pict,
    status = new.status,
    keterangan='Insert Style',
    isActive = new.isActive,
    userId=new.userId;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_log_style` AFTER UPDATE ON `tblstyle` FOR EACH ROW BEGIN
    INSERT INTO log_param_style
    set idStyle = new.idStyle,
    namaStyle=new.namaStyle,
    pict=new.pict,
    status = new.status,
    keterangan='update Style',
    isActive = new.isActive,
    userId=new.userId;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idUser` int(11) NOT NULL,
  `userId` varchar(25) NOT NULL,
  `nama` text NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `idModul` int(11) NOT NULL,
  `isActive` int(11) NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idUser`, `userId`, `nama`, `password`, `email`, `idModul`, `isActive`, `tgl_proses`) VALUES
(1, 'prana', 'Pranajaya', '$2y$10$yhPP49wiRZ3HkQ1vFDprLOpp8QgpmPvPgHCXN/Br9Irkxz3Pgfebu', 'pranajaya161@gmail.com', 1, 0, '2019-01-27 04:48:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `edp_assortment`
--
ALTER TABLE `edp_assortment`
  ADD PRIMARY KEY (`import_id`,`project_no`,`pc`),
  ADD KEY `import_id` (`import_id`,`pc`);

--
-- Indexes for table `edp_assortment_dt`
--
ALTER TABLE `edp_assortment_dt`
  ADD PRIMARY KEY (`import_id`,`nama`,`nik`,`nama_pakaian`),
  ADD KEY `nama_pakaian_lokasi` (`nama_pakaian`,`lokasi`,`import_id`),
  ADD KEY `datetime_uploaded` (`datetime_uploaded`),
  ADD KEY `import_id` (`import_id`),
  ADD KEY `karton_id` (`karton_id`);

--
-- Indexes for table `edp_assortment_pros`
--
ALTER TABLE `edp_assortment_pros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mkt_po_sm`
--
ALTER TABLE `mkt_po_sm`
  ADD PRIMARY KEY (`idPo`);

--
-- Indexes for table `mkt_po_sty`
--
ALTER TABLE `mkt_po_sty`
  ADD PRIMARY KEY (`idPo`,`idStyle`,`size`);

--
-- Indexes for table `mkt_pros_cost`
--
ALTER TABLE `mkt_pros_cost`
  ADD PRIMARY KEY (`idProspecting`,`nourut`);

--
-- Indexes for table `mkt_pros_desgn`
--
ALTER TABLE `mkt_pros_desgn`
  ADD UNIQUE KEY `idProspecting_2` (`idProspecting`,`nourut`),
  ADD KEY `idProspecting` (`idProspecting`);

--
-- Indexes for table `mkt_pros_desgn_bhn`
--
ALTER TABLE `mkt_pros_desgn_bhn`
  ADD KEY `idProspecting` (`idProspecting`);

--
-- Indexes for table `mkt_pros_sample`
--
ALTER TABLE `mkt_pros_sample`
  ADD UNIQUE KEY `idProspecting_2` (`idSample`),
  ADD KEY `idProspecting` (`idSample`);

--
-- Indexes for table `mkt_pros_sm`
--
ALTER TABLE `mkt_pros_sm`
  ADD PRIMARY KEY (`idProspecting`);

--
-- Indexes for table `tblbahan`
--
ALTER TABLE `tblbahan`
  ADD PRIMARY KEY (`idBahan`);

--
-- Indexes for table `tblclient`
--
ALTER TABLE `tblclient`
  ADD PRIMARY KEY (`idClient`);

--
-- Indexes for table `tbldept`
--
ALTER TABLE `tbldept`
  ADD PRIMARY KEY (`idDept`);

--
-- Indexes for table `tblinformasi`
--
ALTER TABLE `tblinformasi`
  ADD PRIMARY KEY (`idInformasi`);

--
-- Indexes for table `tblkategori`
--
ALTER TABLE `tblkategori`
  ADD PRIMARY KEY (`idKategori`);

--
-- Indexes for table `tblmenu`
--
ALTER TABLE `tblmenu`
  ADD PRIMARY KEY (`idMenu`);

--
-- Indexes for table `tblmodul`
--
ALTER TABLE `tblmodul`
  ADD PRIMARY KEY (`idModul`);

--
-- Indexes for table `tblmodul_list`
--
ALTER TABLE `tblmodul_list`
  ADD KEY `idModul` (`idModul`,`headMenu`),
  ADD KEY `idMenu` (`idMenu`);

--
-- Indexes for table `tblsatuan`
--
ALTER TABLE `tblsatuan`
  ADD PRIMARY KEY (`idSatuan`);

--
-- Indexes for table `tblstyle`
--
ALTER TABLE `tblstyle`
  ADD PRIMARY KEY (`idStyle`),
  ADD KEY `idStyle` (`idStyle`),
  ADD KEY `namaStyle` (`namaStyle`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `edp_assortment_pros`
--
ALTER TABLE `edp_assortment_pros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `mkt_po_sm`
--
ALTER TABLE `mkt_po_sm`
  MODIFY `idPo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mkt_pros_sample`
--
ALTER TABLE `mkt_pros_sample`
  MODIFY `idSample` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mkt_pros_sm`
--
ALTER TABLE `mkt_pros_sm`
  MODIFY `idProspecting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tblbahan`
--
ALTER TABLE `tblbahan`
  MODIFY `idBahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tblclient`
--
ALTER TABLE `tblclient`
  MODIFY `idClient` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbldept`
--
ALTER TABLE `tbldept`
  MODIFY `idDept` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tblinformasi`
--
ALTER TABLE `tblinformasi`
  MODIFY `idInformasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tblkategori`
--
ALTER TABLE `tblkategori`
  MODIFY `idKategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblmenu`
--
ALTER TABLE `tblmenu`
  MODIFY `idMenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `tblmodul`
--
ALTER TABLE `tblmodul`
  MODIFY `idModul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tblsatuan`
--
ALTER TABLE `tblsatuan`
  MODIFY `idSatuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblstyle`
--
ALTER TABLE `tblstyle`
  MODIFY `idStyle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
