<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_parameter extends CI_Model {
	//==== Bahan
	public function show()
	{
		$this->db->order_by('idBahan','ASC');
		$data = $this->db->get("tblbahan");
		return $data->result();
	}
	public function log_bahan($id)
	{
		$this->db->where('idBahan', $idBahan);
		$data = $this->db->get("log_param_bahan");
		return $data->result();
	}
	public function add_bahan($data)
	{
		$tambah=$this->db->insert('tblbahan',$data);
		return $tambah;
	}
	public function edit_bahan($id, $data)
	{
		$this->db->where('idBahan',$id);
		$update=$this->db->update('tblbahan',$data);
		return $update;
	}
	//=== Style Design
	public function show_style($id)
	{
		$this->db->where('status', $id);
		$data = $this->db->get("tblstyle");
		return $data->result();
	}
	public function add_style($data)
	{
		$tambah=$this->db->insert('tblstyle',$data);
		return $tambah;
	}
	public function edit_style($id, $data)
	{
		$this->db->where('idStyle',$id);
		$update=$this->db->update('tblstyle',$data);
		return $update;
	}
	//=== Buyer
	public function show_buyer()
	{
		$this->db->order_by('idClient','ASC');
		$data = $this->db->get("tblclient");
		return $data->result();
	}
	public function add_buyer($data)
	{
		$tambah=$this->db->insert('tblclient',$data);
		return $tambah;
	}
	public function edit_buyer($id, $data)
	{
		$this->db->where('idClient',$id);
		$update=$this->db->update('tblclient',$data);
		return $update;
	}
}