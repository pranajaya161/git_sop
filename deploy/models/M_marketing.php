<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_marketing extends CI_Model {
	public function show_purchaseOrder()
	{
		$this->db->order_by('idPo','ASC');
		$data = $this->db->get("mkt_po_sm");
		return $data->result();
	}
	public function show_purchaseOrder_apv()
	{
		$this->db->where('isActive', 0);
		$data = $this->db->get("mkt_po_sm");
		return $data->result();
	}
	public function show_purchaseOrder_id($id)
	{
		$this->db->where('idPo', $id);
		$data = $this->db->get("mkt_po_sm");
		return $data->result();
	}
	public function show_prospecting()
	{
		$this->db->where('isActive', 0);
		$data = $this->db->get("mkt_pros_sm");
		return $data->result();
	}
	public function tambah_purchaseorder($data)
	{
		$tambah=$this->db->insert('mkt_po_sm',$data);
		return $tambah;
	}
	public function update_purchaseorder($id, $data)
	{
		$this->db->where('idPo',$id);
		$update=$this->db->update('mkt_po_sm',$data);
		return $update;
	}
	public function tambah_purchaseorder_style($data)
	{
		$tambah=$this->db->insert('mkt_po_sty',$data);
		return $tambah;
	}
	public function tambah_po_apv($data)
	{
		$tambah=$this->db->insert('mkt_po_apv',$data);
		return $tambah;
	}
}
