<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_edp extends CI_Model {
	private $db2;
	public function __construct()
	 {
	  parent::__construct();
			 $this->db2 = $this->load->database('mega', TRUE);
	 }
	public function show_edp_assort()
	{
		$this->db2->where('right(project_no,1)<>','X');
		$this->db2->where('is_data_completed', 'Y');
		$data = $this->db2->get("t_import_pengukuran_list");
		return $data->result();
	}
	public function show_edp_assort_id($id)
	{
		$this->db2->where('import_id', $id);
		$data = $this->db2->get("t_import_pengukuran_list");
		return $data->result();
	}
	
	public function tambah_assortDetail($data)
	{
		$tambah=$this->db->insert('edp_assortment_dt',$data);
		return $tambah;
	}
	public function tambah_assortDetailHead($data)
	{
		$tambah=$this->db->insert('edp_assortment',$data);
		return $tambah;
	}
	public function show_edp_assort_trans()
	{
		$this->db->where('isActive', 0);
		$data = $this->db->get("edp_assortment");
		return $data->result();
	}
	public function show_edp_assort_trans_id($id)
	{
		$this->db->where('isActive', 0);
		$this->db->where('import_id', $id);
		$data = $this->db->get("edp_assortment");
		return $data->result();
	}
	public function show_edp_assort_apv()
	{
		$this->db->select('idPo, import_id, idStyle, lokasi, jabatan, count(*) as nom, tgl_proses, userId, info_tambahan');
		$this->db->where('isActive', 0);
		$this->db->group_by('idPo', 'ASC');
		$this->db->group_by('import_id', 'ASC');
		$this->db->group_by('idStyle', 'ASC');
		$data = $this->db->get("edp_assortment_pros");
		return $data->result();
	}

	
}
