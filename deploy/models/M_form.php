<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_form extends CI_Model {
	private $db2;
	public function __construct()
	 {
	  parent::__construct();
			 $this->db2 = $this->load->database('danain', TRUE);
	 }
	public function show_aktif()
	{
		$this->db2->order_by('isActive','ASC');
		$data = $this->db2->get("form_user");
		return $data->result();
	}
	/*
	function record_count()
	{
		$this->db->select('*'); 
		$this->db->from('tblbank');
		$query = $this->db->get();
		return $query->num_rows();
	}
	function show(){
		$this->db->select('*'); 
		$this->db->from('tblbank');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result();
		} else {
			return array();
		}
    }  
	function add_bank($data)
	{
		$tambah=$this->db->insert('tblbank',$data);
		return $tambah;
	}
	function update($data, $id)
	{
		$this->db->where('idBank',$id);
		$update=$this->db->update('tblbank',$data);
		return $update;
	}
	*/
}