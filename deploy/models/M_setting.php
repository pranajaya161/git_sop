<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_setting extends CI_Model {
	public function show()
	{
		$this->db->order_by('isActive','ASC');
		$data = $this->db->get("users");
		return $data->result();
	}
	public function add_user($data)
	{
		$tambah=$this->db->insert('users',$data);
		return $tambah;
	}
	function edit_user($id, $data)
	{
		$this->db->where('idUser',$id);
		$update=$this->db->update('users',$data);
		return $update;
	}
	
}