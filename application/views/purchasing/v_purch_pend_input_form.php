<form name="cabang" action="<?php echo base_URL()?>purc_beli_request/tambah_det" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
	<input type="hidden" name="idPurcPembelian" value="<?php echo $idPurcPembelian?>">
	<div class="col-lg-4">
		<div class="form-group">
			<label for="exampleInputEmail1">Nama Bahan</label>
			<select name="idBahan" class="form-control">
				<option>-- Pilih Bahan --</option>
				<?php
				$lihat=$this->db->query("select * from tblbahan where isActive = 1")->result();
				foreach($lihat as $a):?>
					<option value="<?php echo $a->idBahan?>"><?php echo getBahan($a->idBahan)?> [ <?php echo getSatuan($a->idSatuan)?> ]</option>
				<?php endforeach;?>
			</select>
		</div>
	</div>	
	<div class="col-lg-2">
		<div class="form-group">
			<label for="exampleInputEmail1">PCS</label>
			<input type="text" class="form-control" name="qty" required="">
		</div>
	</div>	
	<div class="col-lg-4">
		<div class="form-group">
			<br>
			<button type="submit" class="btn btn-primary">Tambah</button>
			<a href="<?php echo base_URL()?>purc_beli_request" class="btn btn-warning">Kembali</a>
		</div>
	</div>	
</form>
