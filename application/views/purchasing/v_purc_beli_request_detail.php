<head>
    meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript">
    $(document).ready(function()
	{
	    $('td input[type="checkbox"]').click(function () {
	        $(this).closest('tr').find('input[type="text"]').prop('disabled', !this.checked);
	        $(this).closest('tr').find('input[type="hidden"]').prop('disabled', !this.checked);
	    }).change();
	});
	</script>
</head>
<?php
error_reporting(0);
$mode		= $this->uri->segment(4);

?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b><?php echo $title?> | RefNo. : <?php echo getRefNoPurcPembelian($idPurcPembelian)?></b>
                </div>
                <div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
					<?php if(getRequestPembelian($idPurcPembelian)=='Manual') { 
						$this->load->view('purchasing/v_purch_pend_input_form');
                    } else { 

                    ?>
                    	<div class="row mt">
				        	<div class="col-lg-12">
								<div class="form-panel">
									<form action="<?php echo base_URL()?>purc_beli_request/add_detail" method="post" class="form-horizontal" accept-charset="utf-8" enctype="multipart/form-data">
										<table class="table table-bordered table-striped table-condensed">
											<input type="hidden" name="idPurcPembelian" value="<?php echo $idPurcPembelian?>" />
											<input type="hidden" name="idPurcPermintaan" value="<?php echo $idPurcPermintaan?>" />
											<tr>
												<th style="text-align:left">Worksheet</th>
												<th style="text-align:left">No.PR</th>
												<th style="text-align:left">Bahan</th>
												<th style="text-align:left">Qty</th>
												<th style="text-align:left">Satuan</th>
												<th style="text-align:left">Buy</th>
												<th style="text-align:left">Sisa</th>
												<th style="text-align:left">Check</th>
												<th style="text-align:left">Qty Input</th>
											</tr>
											<?php
											$gene = $this->db->query("select a.idMaterialist, a.idPurcPermintaan, a.idBahan, sum(a.total) as ttl, a.satuan from purc_permintaan_dt a inner join (select idPurcPermintaan from purc_pembelian_sm where isActive = 0 group by idPurcPermintaan) b on b.idPurcPermintaan = a.idPurcPermintaan where a.isActive=0  group by a.idMaterialist, a.idPurcPermintaan, a.idBahan")->result();
											foreach($gene as $x):
												$idMaterialist = $x->idMaterialist;
												$idBahan = $x->idBahan;
												$cekBuy = $this->db->query("select sum(qty) as tot from purc_pembelian_dt a where idMaterialist ='$idMaterialist' and idBahan = '$idBahan'")->row();
												$totQty = $cekBuy->tot;
												$sisa = $x->ttl - $totQty;
												if($sisa > 0) {
												?>		
														<input type="hidden" name="idMaterialist[]" value="<?php echo $x->idMaterialist?>" />
														<input type="hidden" name="idPurcPermintaan[]" value="<?php echo $x->idPurcPermintaan?>" />
													<tr>
														<td><?php echo getrefNoMaterialist($x->idMaterialist)?></td>
														<td><?php echo getRefnoPurcPermintaan($x->idPurcPermintaan);?></td>
														<td><?php echo getBahan($x->idBahan)?></td>
														<td><?php echo number_format($x->ttl,2)?></td>
														<td><?php echo getSatuan($x->satuan)?></td>
														<td><?php echo number_format($totQty,2)?></td>
														<td><?php echo number_format($x->ttl - $totQty,2)?></td>
														<td><input type="checkbox" class="others1"></td>
														<td><input type="text" name="qty[]" style="width: 100%;" disabled="" value="0"></td>
														<td><input type="hidden" name="idBahan[]" value="<?php echo $x->idBahan?>" disabled=""/></td>
													</tr>
												<?php } endforeach;?>
										</table>	
										<div class="col-lg-4">
											<div class="form-group">
												<br>
												<input name="submit" type="submit" class="btn btn-success" value="Proses" onClick="return confirm('Data akan di Proses...?');">
												<a href="<?php echo base_URL()?>purc_beli_request" class="btn btn-warning">Kembali</a>
											</div>
										</div>	

								</form>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>	
						<div class="row">
						    <div class="col-lg-12">
								<div class="form-panel">
								<p>List</p>
									<table class="table table-bordered table-striped table-condensed">
										<tr>
											<th style="text-align:left">Bahan</th>	
											<th style="text-align:left">Qty</th>	
											<th style="text-align:left">Satuan</th>
											<th style="text-align:center">Hapus</th>
										</tr>
										<?php
										$detail = $this->db->query("select * from purc_pembelian_dt where idPurcPembelian='$idPurcPembelian'")->result();
										foreach($detail as $xx):?>
										<tr>
											<td><?php echo getBahan($xx->idBahan)?></td>
											<td><?php echo number_format($xx->qty,2)?></td>
											<td><?php echo $xx->satuan?></td>
											<td style="text-align: center">
												<a href="<?php echo base_URL()?>purc_beli_request/hapus/<?php echo $xx->id?>/<?php echo $xx->idPurcPembelian?>" class="btn btn-xs btn-danger" onClick="return confirm('Data akan di hapus...?');"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
										<?php endforeach;?>
									</table>	
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>	
	</div>
</div>

<!--- End View -->	
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchStyle(){
	openWindow('<?php echo base_URL()?>desg_request/cari','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedsStyle(idStyle, namaStyle, pict){   
    document.getElementById('idStyle').value = idStyle;
    document.getElementById('namaStyle').value = namaStyle;
	document.getElementById('pict').value = pict;
}
</script>