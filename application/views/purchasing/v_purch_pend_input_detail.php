<head>
    meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
$mode		= $this->uri->segment(4);
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b>Input Materialist Detail | RefNo. : <?php echo getRefnoPurcPermintaan($idPurcPermintaan)?> | Keterangan : <?php echo getKetPurcPermintaan($idPurcPermintaan)?> </b>
                </div>
                <div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
						<form name="cabang" action="<?php echo base_URL()?>purc_pend_input/detail/<?php echo $idPurcPermintaan?>/generate" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
							<input type="hidden" name="idPurcPermintaan" value="<?php echo $idPurcPermintaan?>">
							<div class="col-lg-4">
								<div class="form-group">
									<label for="exampleInputEmail1">Nama Materlialist</label>
									<select name="idMaterialist" class="form-control">
										<option>-- Pilih Materlialist --</option>
									<?php
									$lihat=$this->db->query("select * from md_materialist where isActive = 1")->result();
									foreach($lihat as $a):
									?>
									<option value="<?php echo $a->idMaterialist?>"><?php echo getrefNoMaterialist($a->idMaterialist)?></option>
								<?php endforeach;?>
									</select>
								</div>
							</div>	
							<div class="col-lg-4">
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-primary">Tambah</button>
									<a href="<?php echo base_URL()?>purc_pend_input" class="btn btn-warning">Kembali</a>
								</div>
							</div>	
                       </form>
					</div>
					<?php if(!empty($mode)){
						$idMaterialist = addslashes($this->input->post('idMaterialist'));
						?>
						<div class="row mt">
				        	<div class="col-lg-12">
								<div class="form-panel">
									<p><?php echo getrefNoMaterialist($idMaterialist)?> [<?php echo $idPurcPermintaan?>]</p>
									<form action="<?php echo base_URL()?>purc_pend_input/add_detail" method="post" class="form-horizontal" accept-charset="utf-8" enctype="multipart/form-data">
										<table class="table table-bordered table-striped table-condensed">
											<input type="hidden" name="idPurcPermintaan" value="<?php echo $idPurcPermintaan?>" />
											<input type="hidden" name="idMaterialist" value="<?php echo $idMaterialist?>" />
											<tr>
												<th style="text-align:left">Bahan</th>	
												<th style="text-align:left">Total Cons</th>
												<th style="text-align:left">Input Pengajuan</th>
												<th style="text-align:left">Satuan</th>
											</tr>
											<?php
											$gene = $this->db->query("select idBahan, sum(pcs) as qty, sum(pcs * cons) as tot, satuan  from md_materialist_dt where idMaterialist='$idMaterialist' group by idBahan")->result();
											foreach($gene as $x):?>
												<input type="hidden" name="idBahan[]" value="<?php echo $x->idBahan?>" readonly />
												<input type="hidden" name="satuan[]" value="<?php echo $x->satuan?>" readonly />
												<input type="hidden" name="qty[]" value="<?php echo $x->qty?>" readonly />
												<input type="hidden" name="tot[]" value="<?php echo $x->tot?>" readonly />
											<tr>
												<td><?php echo getBahan($x->idBahan)?></td>
												<td><?php echo number_format($x->tot,2)?></td>
												<td><input type="text" class="form-control" name="pcs[]" value="<?php echo $x->tot?> "></td>
												<td><?php echo getSatuan($x->satuan)?></td>
											</tr>
										<?php endforeach;?>
										</table>	
									<p><input name="submit" type="submit" class="btn btn-success" value="Proses" onClick="return confirm('Data akan di Proses...?');">&nbsp;&nbsp;</p>
								</form>
								</div>
							</div>
						</div>
					<?php } ?>	
					<div class="row">
				        <div class="col-lg-12">
							<div class="form-panel">
							<p>List</p>
								<table class="table table-bordered table-striped table-condensed">
									<tr>
										<th style="text-align:left">Materialist</th>	
										<th style="text-align:left">Bahan</th>	
										<th style="text-align:left">Total</th>
										<th style="text-align:left">Satuan</th>
										<th style="text-align:center">Hapus</th>
									</tr>
									<?php
									$detail = $this->db->query("select * from purc_permintaan_dt where idMaterialist='$idMaterialist'")->result();
									foreach($detail as $xx):?>
									<tr>
										<td><?php echo getrefNoMaterialist($xx->idMaterialist)?></td>
										<td><?php echo getBahan($xx->idBahan)?></td>
										<td><?php echo number_format($xx->total,2)?></td>
										<td><?php echo getSatuan($xx->satuan)?></td>
										<td style="text-align: center">
											<a href="<?php echo base_URL()?>purc_pend_input/hapus/<?php echo $xx->id?>/<?php echo $xx->idMaterialist?>" class="btn btn-xs btn-danger" onClick="return confirm('Data akan di hapus...?');"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<?php endforeach;?>
								</table>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchStyle(){
	openWindow('<?php echo base_URL()?>desg_request/cari','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedsStyle(idStyle, namaStyle, pict){   
    document.getElementById('idStyle').value = idStyle;
    document.getElementById('namaStyle').value = namaStyle;
	document.getElementById('pict').value = pict;
}
</script>