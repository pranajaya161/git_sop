<head>
    meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
foreach ($data as $a):
  $idProspecting	= $a->idProspecting;
  $tanggal 			= $a->tanggal;
  $refNo			= $a->refNo;
  $keterangan 		= $a->keterangan;
  $idClient 		= $a->idClient;
endforeach;
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b>Tanggal : <?php echo $tanggal?> | Ref.No : <?php echo $refNo?> | Buyer : <?php echo getClient($idClient)?> [ <?php echo getStyle($this->uri->segment(4))?> -  Bahan ]</b>
                </div>
                <div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
					<form name="cabang" action="<?php echo base_URL()?>desg_request/bahan_design" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
							<div class="col-lg-4">
								<input type="hidden" name="idProspecting" value="<?php echo $idProspecting?>" />
								<input type="hidden" name="idStyle" value="<?php echo $this->uri->segment(4)?>" />
								<div class="form-group">
									<label class="col-sm-12 control-label">Bahan&nbsp;&nbsp;&nbsp;<a href="#" onclick="searchBahan()"><span><i class="fa fa-search"></i></span></a></label>
									<input type="hidden" name="idBahan" id="idBahan" class="tfield" readonly />
									<input type="hidden" name="pict" id="pict" class="tfield" readonly />
									<input type="text" class="form-control" name="namaBahan" id="namaBahan" readonly>
								</div>	
						    </div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="exampleInputEmail1">Keterangan</label>
									<input type="text" class="form-control" name="keterangan" required>
								</div>
							</div>	
							<div class="col-lg-4">
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-primary">Simpan</button>
									<a href="<?php echo base_URL()?>desg_request/upload/<?php echo $idProspecting?>" class="btn btn-warning">Kembali</a>
								</div>
							</div>	
                       </form>
					</div>
					<div class="row">
						<div class="panel-body">
							<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>No</th>
										<th>Tanggal</th>
										<th>Bahan</th>
										<th>Keterangan</th>
										<th style="text-align: center">Hapus</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($detail as $b): ?>
									<tr>
							  			<td><?php echo $i++;?></td>
							  			<td><?php echo $b->tgl_proses;?></td>
							  			<td><?php echo getBahan($b->idBahan);?></td>
							  			<td><?php echo $b->keterangan;?></td>
							  			<td style="text-align: center">
										<?php if($b->isActive ==0){ ?>
											<form name="cabang" action="<?php echo base_URL()?>desg_request/hapus_bahan" method="post" accept-charset="utf-8" enctype="multipart/form-data">
												<input type="hidden" name="idProspecting" value="<?php echo $b->idProspecting?>" />
												<input type="hidden" name= "idStyle" value="<?php echo $b->idStyle?>" />
												<input type="hidden" name= "idBahan" value="<?php echo $b->idBahan?>" />
												<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
											</form>	
										<?php } ?>	
										</td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchBahan(){
	openWindow('<?php echo base_URL()?>desg_request/cari_bahan','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedBahan(idBahan, namaBahan, pict){   
    document.getElementById('idBahan').value = idBahan;
    document.getElementById('namaBahan').value = namaBahan;
	document.getElementById('pict').value = pict;
}
</script>