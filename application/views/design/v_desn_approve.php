<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><?php echo $title;?></p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th>No</th>
								<th>Tanggal</th>
								<th>Ref.No</th>
								<th>Buyer</th>
								<th>Keterangan</th>
								<th>From</th>
								<th style="text-align: center">Approve</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; $idProspecting=0; foreach ($data as $a): $idProspecting = $a->idProspecting;?>
							<tr class="odd gradeX">
								<td><?php echo $i++;?></td>
								<td><?php echo $a->tanggal;?></td>
								<td><a href="#" data-toggle="modal" data-target="#modal_view<?php echo $a->idProspecting?>"><?php echo $a->refNo;?></a></td>
								<td><?php echo getClient($a->idClient);?></td>
								<td><?php echo $a->keterangan;?></td>
								<td><?php echo $a->userId;?></td>
								<td style="text-align: center"><?php echo $a->idActive;?><i>
									<?php if($a->idDesign ==1 and $a->isActive==1){ echo 'Approve'; } else if($a->isActive==0) { echo 'Proses';} else { ?>
									<a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_approve<?php echo $a->idProspecting?>"><i class="fa fa-check"></i></a>
								<?php } ?>
								</i></td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<!--- End View -->
<?php foreach($data as $b): $idProspecting=$b->idProspecting; $idStyle = $b->idStyle;?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_view<?php echo $b->idProspecting?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Tanggal</th>
									<th>Buyer</th>
									<th>Keterangan</th>
									<th>User</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$lihat  = $this->db->query("select * from mkt_pros_sm where idProspecting='$idProspecting'")->result();
							foreach($lihat as $x):
							?>	
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo $x->tanggal?></td>
									<td><?php echo getClient($x->idClient)?></td>
									<td><?php echo $x->keterangan?></td>
									<td><?php echo $x->userId?></td>
							<?php endforeach;?>
							</tbody>
						</table>
						<hr>
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Style</th>
									<th>Image</th>
									<th>Bahan</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$image = $this->db->query("select * from mkt_pros_desgn where idProspecting='$idProspecting'")->result();
							foreach($image as $img):?>
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo getStyle($img->idStyle)?></td>
									<td><img src="<?php echo base_URL()?>uploads/parameter/style/<?php echo getStylePict($img->idStyle)?>" style="max-width: 100px"></td>
									<td>
							  		<?php
							  			$idStyle = $img->idStyle;
										$xx=$this->db->query("select a.idProspecting, a.idBahan, b.keterangan from mkt_pros_desgn_bhn a inner join mkt_pros_desgn b on b.idProspecting = a.idProspecting and b.idStyle = a.idStyle and a.idProspecting='$idProspecting' and a.idStyle='$idStyle'")->result();
										foreach($xx as $z):
												echo getBahan($z->idBahan); ?><br>
											<?php endforeach; ?>	
							  			</td>
								</tr>
							<?php
							endforeach;
							?>
							</tbody>
						</table>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End View -->
<?php foreach($data as $b): $idProspecting=$b->idProspecting; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_approve<?php echo $b->idProspecting?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<form name="cabang" action="<?php echo base_URL()?>desg_approve/approve" method="post" accept-charset="utf-8" enctype="multipart/form-data">
							<input type="hidden" name="idProspecting" value="<?php echo $idProspecting?>">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<label for="exampleInputEmail1">Tanggal</label>
										<input type="text" name="tanggal" value="<?php echo date('Y-m-d')?>" class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
									</div>	
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label for="exampleInputEmail1">Status</label>
										<select name="status" class="form-control">
											<option value="Approve">Approve</option>
											<option value="Reject">Reject</option>
										</select>
									</div>	
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label for="exampleInputEmail1">Keterangan</label>
										<textarea class="form-control" name="keterangan" rows="3"></textarea>
									</div>	
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<button type="submit" class="btn btn-success">Simpan</button>
										<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
									</div>	
								</div>	
							</div>
						</form>					
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
