<head>
    meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
$mode		= $this->uri->segment(4);
foreach($data as $a):
	$idWorksheet=$a->idWorksheet; 	
endforeach;

?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b><?php echo $title?> | RefNo. : <?php echo getRefNoWorksheet($idWorksheet)?></b>
                </div>
                <div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
					<form name="cabang" action="<?php echo base_URL()?>cutting_input/tambah_det" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
						<input type="hidden" name="idWorksheet" value="<?php echo $idWorksheet?>">
						<div class="col-lg-4">
							<div class="form-group">
								<label for="exampleInputEmail1">Numbering Dari</label>
								<input type="text" class="form-control" name="dari" required="">
							</div>
						</div>	
						<div class="col-lg-2">
							<div class="form-group">
								<label for="exampleInputEmail1">Numbering Sampai</label>
								<input type="text" class="form-control" name="sampai" required="">
							</div>
						</div>	
						<div class="col-lg-4">
							<div class="form-group">
								<br>
								<button type="submit" class="btn btn-primary">Tambah</button>
								<a href="<?php echo base_URL()?>cutting_input" class="btn btn-warning">Kembali</a>
							</div>
						</div>	
					</form>
					</div>
					<div class="row">
					    <div class="col-lg-12">
							<div class="form-panel">
							<p>List</p>
								<table class="table table-bordered table-striped table-condensed" style="font-size: 12px">
										<tr>
											<th style="text-align:left">Numbering</th>	
											<th style="text-align:left">Style</th>
											<th style="text-align:left">Size</th>								
											<th style="text-align:right"><?php echo $head->nama_ukuran_f1?></th>			
											<th style="text-align:right"><?php echo $head->nama_ukuran_f2?></th>
											<th style="text-align:right"><?php echo $head->nama_ukuran_f3?></th>				
											<th style="text-align:right"><?php echo $head->nama_ukuran_f4?></th>
											<th style="text-align:right"><?php echo $head->nama_ukuran_f5?></th>			
											<th style="text-align:right"><?php echo $head->nama_ukuran_f6?></th>
											<th style="text-align:right"><?php echo $head->nama_ukuran_f7?></th>			
											<th style="text-align:right"><?php echo $head->nama_ukuran_f8?></th>
											<th style="text-align:right"><?php echo $head->nama_ukuran_f9?></th>			
											<th style="text-align:right"><?php echo $head->nama_ukuran_f10?></th>
											<th style="text-align:left">Hapus</th>		
										</tr>
									<?php
									//$det=$this->db->query("select a.* from edp_assortment_pros a inner join cutting_in b on b.id = a.id  where b.idWorksheet='$idWorksheet' and b.isActive = 0")->result();
									foreach ($detail as $b) { $i=1;?>
										<tr>
											<td><?php echo $b->nourut;?></td>
											<td><?php echo getStyle($b->idStyle);?></td>
											<td><?php echo $b->size;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f1;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f2;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f3;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f4;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f5;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f6;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f7;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f8;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f9;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f10;?></td>
											<td style="text-align: center">
												<a href="<?php echo base_URL()?>cutting_input/hapus/<?php echo $idWorksheet?>/<?php echo $b->id?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
											</td>
										</tr>	
									<?php } ?>
									</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchStyle(){
	openWindow('<?php echo base_URL()?>desg_request/cari','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedsStyle(idStyle, namaStyle, pict){   
    document.getElementById('idStyle').value = idStyle;
    document.getElementById('namaStyle').value = namaStyle;
	document.getElementById('pict').value = pict;
}
</script>