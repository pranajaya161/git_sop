<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
	
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<?php
error_reporting(0);
foreach ($data as $a):
  $nik 				= $a->nik;
  $userName			= $a->userName;
  $ktp 				= $a->ktp;
  $npwp 			= $a->npwp;
  $tempatLahir 		= $a->tempatLahir;
  $tglLahir 		= $a->tglLahir;
  $jenKel 			= $a->jenKel;
  $alamat 			= $a->alamat;
  $hp 				= $a->hp;
  $idJabatan 		= $a->idJabatan;
  $idDivisi 		= $a->idDivisi;
  $idPendidikan 	= $a->idPendidikan;
  $idKompetensi 	= $a->idKompetensi;
  $StKaryawan 		= $a->StKaryawan;
  $tgl_join 		= $a->tgl_join;
  $idEmployee_head 	= $a->idEmployee_head;
endforeach;
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $title?>
                </div>
                <div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
					<form name="cabang" action="<?php echo base_URL()?>employee/simpan" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
						<div class="col-lg-6">
							<input type="hidden" name="idEmployee" value="0" />
								<div class="form-group">
									<label>NIK</label>
									<input class="form-control" name="nik" placeholder="NIK / NIP">
                                </div>
                                <div class="form-group">
									<label>User Name</label>
									<input class="form-control" name="userName" value="<?php echo $userName?>" placeholder="User Name">
                                </div>
                                <div class="form-group">
									<label>KTP</label>
									<input class="form-control" name="ktp" value="<?php echo $ktp?>" placeholder="KTP">
                                </div>  
								<div class="form-group">
									<label>Tempat Lahir</label>
									<input class="form-control" name="tempatLahir" value="<?php echo $tempatLahir?>" placeholder="Tempat Lahir">
                                </div>
								<div class="form-group">
									<label>Tgl Lahir</label>
									<input class="form-control" name="tglLahir" value="<?php echo $tglLahir?>" id="input1" placeholder="Tgl Lahir">
                                </div>
								<div class="form-group">
									<label>Jenis Kelamin</label>
									<select name="jenKel" class="form-control">
									<?php if(!empty($jenKel)){?>
										<option value="<?php echo $jenKel?>"><?php if($jenKel=='L'){ echo 'Laki-Laki';} else { 'Perempuan'; }?></option>
									<?php } ?>	
										<option value="L">Laki-Laki</option>
										<option value="P">Perempuan</option>
									</select>
                                </div>
								<div class="form-group">
									<label>Alamat</label>
									<textarea class="form-control" name="alamat" rows="3"><?php echo $alamat?></textarea>
                                </div>
								<div class="form-group">
									<label>File input</label>
									<input type="file" name="pict" >
                                </div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Atasan</label>
									<select name="idEmployee_head" class="form-control">
									<?php if(!empty($jenKel)){?>
										<option value="<?php echo $idEmployee_head?>"><?php echo getEmployee($idEmployee_head)?></option>
									<?php } ?>
									<?php
									$lihat = $this->db->query("select * from employee where isActive = 1")->result();
									foreach($lihat as $a){?>
										<option value="<?php echo $a->idEmployee?>"><?php echo $a->userName?></option>
									<?php } ?>
									</select>
                                </div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>HP</label>
									<input class="form-control" name="hp" value="<?php echo $hp?>" placeholder="HP">
                                </div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Jabatan</label>
									<select name="idJabatan" class="form-control">
									<?php if(!empty($idJabatan)){?>
										<option value="<?php echo $idJabatan?>"><?php echo getJabatan($idJabatan)?></option>
									<?php } ?>
									<?php
									$lihat = $this->db->query("select * from tbljabatan where isActive = 1 and idJabatan<>'$idJabatan'")->result();
									foreach($lihat as $a){?>
										<option value="<?php echo $a->idJabatan?>"><?php echo $a->keterangan?></option>
									<?php } ?>
									</select>
                                </div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Divisi</label>
									<select name="idDivisi" class="form-control">
									<?php if(!empty($idJabatan)){?>
										<option value="<?php echo $idDivisi?>"><?php echo getDivisi($idDivisi)?></option>
									<?php } ?>
									<?php
									$lihat = $this->db->query("select * from tbldivisi where isActive = 1 and idDivisi<>'$idDivisi'")->result();
									foreach($lihat as $a){?>
										<option value="<?php echo $a->idDivisi?>"><?php echo $a->keterangan?></option>
									<?php } ?>
									</select>
                                </div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Pendidikan</label>
									<select name="idJabatan" class="form-control">
									<?php
									$lihat = $this->db->query("select * from tblpendidikan where isActive = 1")->result();
									foreach($lihat as $a){?>
										<option value="<?php echo $a->idPendidikan?>"><?php echo $a->keterangan?></option>
									<?php } ?>
									</select>
                                </div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Kompetensi</label>
									<select name="idJabatan" class="form-control">
									<?php
									$lihat = $this->db->query("select * from tblkompetensi where isActive = 1")->result();
									foreach($lihat as $a){?>
										<option value="<?php echo $a->idKompetensi?>"><?php echo $a->keterangan?></option>
									<?php } ?>
									</select>
                                </div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Status Karyawan</label>
									<select name="StKaryawan" class="form-control">
										<option value="Karyawan">Karyawan</option>
										<option value="Kontrak">Kontrak</option>
										<option value="Magang">Magang</option>
									</select>
                                </div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Tgl Lahir</label>
									<input class="form-control" name="tgl_join" id="input2" placeholder="Tgl Join">
                                </div>
							</div>
							<button type="submit" class="btn btn-default">Submit Button</button>
                       </form>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>