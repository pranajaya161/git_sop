<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
$mode	= $this->uri->segment(2);
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b><?php echo $title;?> <?php echo $mode?></b>
                </div>
                <div class="panel-body">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
					<div class="row">
						<div class="col-lg-12">
							<?php echo form_open($aplikasi.'/proses');?>
								<div class="form-horizontal style-form">
									<div class="form-group">
										<div class="col-sm-3">
											<label>Ref.No</label>
											<select class="form-control" name="idPo">
											<?php
											foreach($data as $a):
											?>
												<option value="<?php echo $a->idPo?>"><?php echo $a->refNo?></option>
										<?php endforeach;?>
											</select>
										</div>
										<div class="col-sm-2">
										  <br>
										  <input class="btn btn-primary" name="submit" type="submit" class="button" value="Proses">
										</div>
									</div>	
								</div>
							</form>
							<?php if($mode === 'proses')
							{ ?>
								<p>List Detail <?php echo getPoRefno($head->idPo)?></p>
								<p>&nbsp;</p>
								<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>Style</th>
											<th>Numbering</th>
											<th>Kode</th>
											<th>Departement</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach($detail as $b): ?>
										<tr>
											<td><?php echo getStyle($b->idStyle)?></td>
											<td><?php echo $b->nourut?></td>
											<td><a href="#" data-toggle="modal" data-target="#modal_view<?php echo $b->id?>"><?php echo $b->kode;?></a></td>
											<td><?php echo getDept($b->idDept)?></td>
										</tr>
									<?php endforeach; ?>
									</tbody>
								</table>

							<?php } ?>	
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<!--- End View -->
<?php foreach($detail as $b): $id=$b->id; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_view<?php echo $b->id?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Tgl.Proses</th>
									<th>Init</th>
									<th>Keterangan</th>
									<th>Depart</th>
									<th>User</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$lihat  = $this->db->query("select * from barcode_hist where id='$id' order by tgl_proses")->result();
							foreach($lihat as $x):
							?>	
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo $x->tgl_proses?></td>
									<td><?php echo $x->kdTrans?></td>
									<td><?php echo $x->keterangan?></td>
									<td><?php echo getDept($x->idDept)?></td>
									<td><?php echo $x->userId?></td>
							<?php endforeach;?>
							</tbody>
						</table>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End View -->
<!-- END Modal Edit -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script type="text/javascript">
		$('#btnsubmit').on('click',function()
	    {
		   $(this).val('Please wait ...')
		      .attr('disabled','disabled');
		    $('#cabang').submit();
		});
	</script> 

