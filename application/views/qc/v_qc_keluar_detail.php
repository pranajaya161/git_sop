<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
foreach($data as $aa):
	$idQc = $aa->idQc;
	$idWorksheet = $aa->idWorksheet;
endforeach;
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
	<?php
		if($this->session->flashdata('pesan')==TRUE):
		echo'<div class="alert alert-warning" role="alert">';
		echo $this->session->flashdata('pesan');
		echo "</div>";
	endif;
	?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><?php echo $title?> | refNo : <?php echo getRefNoSewing($idQc)?>   <a href="<?php echo base_URL()?>qc_keluar_input" class="btn btn-info" style="float: right;">Kembali</a></p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
                	<form name="cekbox" action="<?php echo base_URL()?>qc_keluar_input/tambah_detail" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
                		<input type="hidden" name="idQc" value="<?php echo $idQc?>">
                		<?php
						$cekHead = $this->db->query("select nama_ukuran_f1, nama_ukuran_f2, nama_ukuran_f3, nama_ukuran_f4, nama_ukuran_f5, nama_ukuran_f6, nama_ukuran_f7, nama_ukuran_f8,nama_ukuran_f9, nama_ukuran_f10 from  edp_assortment_pros a inner join md_worksheet_dt b on b.id = a.id  where b.idWorksheet ='$idWorksheet'")->row();
						?>
						<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th style="text-align:left">Check</th>	
									<th style="text-align:left">Numbering</th>	
									<th style="text-align:left">Style</th>
									<th style="text-align:left">Size</th>								
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f1?></th>			
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f2?></th>
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f3?></th>				
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f4?></th>
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f5?></th>			
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f6?></th>
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f7?></th>			
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f8?></th>
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f9?></th>			
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f10?></th>
								</tr>
							</thead>
							<tbody>
							<?php
							$det=$this->db->query("select a.*, b.nourut, b.id from edp_assortment_pros a inner join qc_stock02 b on b.id = a.id  where b.idWorksheet='$idWorksheet' and b.isActive =0")->result();
							foreach ($det as $b): $i=1; 
								$id = $b->id;
								?>
								<tr class="odd gradeX">
									<td><input type="checkbox" id="cekbox" name="cekbox[]" value="<?php echo $b->id; ?>"/></td>
									<td><?php echo $b->nourut;?></td>
									<td><?php echo getStyle($b->idStyle);?></td>
									<td><?php echo $b->size;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f1;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f2;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f3;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f4;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f5;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f6;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f7;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f8;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f9;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f10;?></td>
								</tr>
							<?php  endforeach;?>	
							</tbody>
						 </table>
							<input type="button" onclick="cek(this.form.cekbox)" value="Select All" />
						    <input type="button" onclick="uncek(this.form.cekbox)" value="Clear All" />
						    <input type="submit" value="Proses" name="submit" /><br />
						    <script>
								function cek(cekbox){
								    for(i=0; i < cekbox.length; i++){
								        cekbox[i].checked = true;
								    }
								}
								function uncek(cekbox){
								    for(i=0; i < cekbox.length; i++){
								        cekbox[i].checked = false;
								    }
								}
								</script>
					</form>
                  <!-- /.table-responsive -->
                  <hr>
                  <p>List</p>
                  		<table width="100%" class="table table-striped table-bordered table-hover"  style="font-size: 12px">
							<thead>
								<tr>
									<th style="text-align:left">Numbering</th>	
									<th style="text-align:left">Style</th>
									<th style="text-align:left">Size</th>								
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f1?></th>			
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f2?></th>
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f3?></th>				
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f4?></th>
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f5?></th>			
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f6?></th>
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f7?></th>			
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f8?></th>
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f9?></th>			
									<th style="text-align:right"><?php echo $cekHead->nama_ukuran_f10?></th>
								</tr>
							</thead>
							<tbody>
							<?php
							$det=$this->db->query("select a.*, b.nourut, b.id from edp_assortment_pros a inner join barcode b on b.id = a.id inner join qc_out_sm c on c.idWorksheet = b.idWorksheet  inner join qc_out d on d.id = b.id and d.idQc = c.idQc  where b.idWorksheet='$idWorksheet' and c.idQc = '$idQc' and b.idDept=10 ")->result();
							foreach ($det as $b): $i=1; 
								$id = $b->id;
								?>
								<tr class="odd gradeX">
									<td><?php echo $b->nourut;?></td>
									<td><?php echo getStyle($b->idStyle);?></td>
									<td><?php echo $b->size;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f1;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f2;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f3;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f4;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f5;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f6;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f7;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f8;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f9;?></td>
									<td style="text-align:right"><?php echo $b->ukuran_f10;?></td>
								</tr>
							<?php  endforeach;?>	
							</tbody>
						 </table>
                 </div>
			</div>
		</div>
	</div>
</div>


<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<!--
<script language="JavaScript" type="text/JavaScript">
 
 function ShowData()
 {
	 <?php
	 $query = $this->db->query("select idPo from edp_assortment_pros where isActive=1 and idMaterialist =0 group by idPo")->result();
	 foreach($query as $data){
	   $idPo = $data->idPo;
	   // membuat IF untuk masing-masing Group
	   echo "if (document.cabang.idPo.value == \"".$idPo."\")";
	   echo "{";

	   // membuat option kabupaten untuk masing-masing propinsi
	   $query2 = $this->db->query("select a.idStyle, b.namaStyle from edp_assortment_pros a inner join tblstyle b on b.idStyle = a.idStyle where a.idPo='$idPo' group by a.idStyle")->result();


	   $content = "document.getElementById('idStyle').innerHTML = \"";
	   foreach($query2 as $data2){
	       $content .= "<option value='".$data2->idStyle."'>".$data2->namaStyle."</option>";   
	   }
	   $content .= "\"";
	   echo $content;
	   echo "}\n";   
	 }
	?> 

 }
</script>  
-->