<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
$mode		= $this->uri->segment(4);
foreach($data as $a):
	$idWorksheet=$a->idWorksheet; 	
	$idPpicQc=$a->idPpicQc;
endforeach;

?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b><?php echo $title?> | RefNo. : <?php echo getRefNoWorksheet($idWorksheet)?></b>
                </div>
                <div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
					</div>
					<div class="row">
					    <div class="col-lg-12">
							<div class="form-panel">
							<p>List <?php echo $idPpicQc;?></p>
								<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
									<?php
									$cek = $this->db->query("select nama_ukuran_f1, nama_ukuran_f2, nama_ukuran_f3, nama_ukuran_f4, nama_ukuran_f5, nama_ukuran_f6, nama_ukuran_f7, nama_ukuran_f8,nama_ukuran_f9, nama_ukuran_f10 from  edp_assortment_pros a where a.idWorksheet='$idWorksheet' limit 1")->row();
									?>
									<thead>
										<tr>
											<th style="text-align:left">Numbering</th>	
											<th style="text-align:left">Style</th>
											<th style="text-align:left">Size</th>								
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f1?></th>			
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f2?></th>
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f3?></th>				
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f4?></th>
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f5?></th>			
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f6?></th>
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f7?></th>			
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f8?></th>
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f9?></th>			
											<th style="text-align:right"><?php echo $cek->nama_ukuran_f10?></th>
											<th style="text-align:center">Approve</th>
										</tr>
									</thead>
									<?php
									$det=$this->db->query("select a.* from edp_assortment_pros a inner join barcode b on b.id = a.id inner join qc_stock01 c on c.id = a.id where c.isActive=0 ")->result();
									foreach ($det as $a) { $i=1; ?>
										<tr>
											<td><?php echo $a->nourut;?></td>
											<td><?php echo getStyle($a->idStyle);?></td>
											<td><?php echo $a->size;?></td>
											<td style="text-align:right"><?php echo $a->ukuran_f1;?></td>
											<td style="text-align:right"><?php echo $a->ukuran_f2;?></td>
											<td style="text-align:right"><?php echo $a->ukuran_f3;?></td>
											<td style="text-align:right"><?php echo $a->ukuran_f4;?></td>
											<td style="text-align:right"><?php echo $a->ukuran_f5;?></td>
											<td style="text-align:right"><?php echo $a->ukuran_f6;?></td>
											<td style="text-align:right"><?php echo $a->ukuran_f7;?></td>
											<td style="text-align:right"><?php echo $a->ukuran_f8;?></td>
											<td style="text-align:right"><?php echo $a->ukuran_f9;?></td>
											<td style="text-align:right"><?php echo $a->ukuran_f10;?></td>
											<td style="text-align: center">
												<a class="btn btn-xs btn-success" data-toggle="modal" data-target="#modal_apv<?php echo $a->id?>"><i class="fa fa-check"></i></a>
											</td>
										</tr>
										</tr>	
									<?php } ?>
									</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<?php
$data=$this->db->query("select a.* from edp_assortment_pros a inner join qc_stock01 b on b.id = a.id and b.nourut = a.nourut where b.isActive=0")->result();
foreach ($data as $b) :  ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_apv<?php echo $b->id?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> Approval <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
					<?php echo form_open_multipart('qc_input/tambah_det');?>
					<input type="hidden" name="nourut" value="<?php echo $b->nourut?>"/>
					<input type="hidden" name="id" value="<?php echo $b->id?>"/>
					<input type="hidden" name="idWorksheet" value="<?php echo $idWorksheet?>"/>
					<input type="hidden" name="idPpicQc" value="<?php echo $idPpicQc?>"/>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Tanggal</label>
									<input type="text" name="tgl" class="form-control upper" value="<?php echo date('Y-m-d')?>" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Status</label>
									<select name="status" class="form-control">
										<option value="Approve">Approve</option>
										<option value="Reture">Retur</option>
									</select>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Keterangan</label>
									<textarea class="form-control" name="keterangan" rows="3"></textarea>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<button type="submit" class="btn btn-success">Simpan</button>
									<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
								</div>	
							</div>	
						</div>	
					</form>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>