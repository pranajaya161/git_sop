<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
	<?php
		if($this->session->flashdata('pesan')==TRUE):
		echo'<div class="alert alert-warning" role="alert">';
		echo $this->session->flashdata('pesan');
		echo "</div>";
	endif;
	?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><?php echo $title?></p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th style="text-align:right">No.</th>
								<th style="text-align:left">Ref.No</th>
								<th style="text-align:left">No.PO</th>
								<th style="text-align:left">Style</th>
								<th style="text-align:right">Record</th>
								<th style="text-align:left">PIC</th>
								<th style="text-align:left">Tgl.Proses</th>
								<th style="text-align:center">Status</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach ($data as $a): ?>
							<tr class="odd gradeX">
								<td style="text-align:right"><?php echo $i++?></td>
								<td><?php echo getPoRefno($a->idPo);?></td>
								<td><?php echo getPoNoPo($a->idPo);?></td>
								<td><?php echo getStyle($a->idStyle);?></td>
								<td style="text-align:right"><a href="JavaScript:openWindow('<?php echo base_URL()?><?php echo $aplikasi?>/caritransassortment/<?php echo $a->idPo?>/<?php echo $a->idStyle?>');" ><?php echo $a->nom;?></a></td>
								<td><?php echo $a->info_tambahan;?></td>
								<td><?php echo $a->tgl_proses;?></td>
								<td style="text-align: center"><?php if($a->isActive==1){ echo 'Approve';} else { echo 'Open'; }?></td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<!--- End Hapus -->
<?php foreach($data as $b): $idPo = $b->idPo; $idStyle = $b->idStyle; $import_id = $b->import_id; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_apv<?php echo $idPo?><?php echo $idStyle?><?php echo $import_id?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> Approval <?php echo getPoNoPo($idPo)?></h3>
					</div>
					<div class="panel-body">
					<?php echo form_open_multipart($aplikasi.'/approve');?>
					<input type="hidden" name="idPo" value="<?php echo $b->idPo?>"/>
					<input type="hidden" name="idStyle" value="<?php echo $b->idStyle?>"/>
					<input type="hidden" name="import_id" value="<?php echo $b->import_id?>"/>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">No.Po</label>
									<input type="text" name="namaBahan" class="form-control upper" value="<?php echo getPoNoPo($b->idPo)?>" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Style</label>
									<input type="text" name="namaBahan" class="form-control upper" value="<?php echo getStyle($b->idStyle)?>" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Status</label>
									<select name="status" class="form-control">
										<option value="1">Approve</option>
										<option value="0">Reject</option>
									</select>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Keterangan</label>
									<input type="text" name="keterangan"  class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" requied>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<button type="submit" class="btn btn-success">Simpan</button>
									<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
								</div>	
							</div>	
						</div>	
					</form>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	

<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
</script>
