<head>
    meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
foreach ($data as $a):
  	$import_id			= $a->import_id;
  	$project_no 		= $a->project_no;
  	$lot_number			= $a->lot_number;
  	$filename 			= $a->filename;
  	$nama_perusahaan 	= $a->nama_perusahaan;
   	$total_records 		= $a->total_records;
    $import_datetime 	= $a->import_datetime;
endforeach;
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b>Project.No : <?php echo $project_no?> | Lot.No : <?php echo $lot_number?> | Buyer : <?php echo $nama_perusahaan?></b>
                </div>
                <div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
						<form name="cabang" action="<?php echo base_URL()?><?php echo $aplikasi?>/upload" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
							<input type="hidden" name="import_id" value="<?php echo $import_id?>">
							<div class="col-lg-4">
								<div class="form-group">
									<label for="exampleInputEmail1">Purchase Order</label>
									<select name="idPo" class="form-control">
									<?php
									$lihat  = $this->db->query("select * from mkt_po_sm where isActive=1")->result();
									foreach($lihat as $a):
									?>
									<option value="<?php echo $a->idPo?>"><?php echo $a->refNo?></option>
								<?php endforeach;?>
									</select>
								</div>
							</div>	
							<div class="col-lg-4">
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-primary">Simpan</button>
									<a href="<?php echo base_URL()?>edp_assort" class="btn btn-warning">Kembali</a>
								</div>
							</div>	
                       </form>
					</div>
					<div class="row">
						<div class="panel-body">
							<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th style="text-align:right">Import Id</th>
										<th style="text-align:left">Project No</th>
										<th style="text-align:left">Lot No</th>
										<th style="text-align:left">File Name</th>
										<th style="text-align:left">Nama Perusahaan</th>
										<th style="text-align:right">Total Record</th>
										<th style="text-align:left">Imp Date</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($data as $b): ?>
							  		<tr class="tabcont">
										<td style="text-align:right"><?php echo $b->import_id;?></td>
										<td><?php echo $b->project_no;?></td>
										<td><?php echo $b->lot_number;?></td>
										<td><?php echo $b->filename;?></td>
										<td><?php echo $b->nama_perusahaan;?></td>
										<td style="text-align:right"><a href="JavaScript:openWindow('<?php echo base_URL()?><?php echo $aplikasi?>/cariassorsment1/<?php echo $b->import_id?>');" ><?php echo number_format($b->total_records);?></a></td>
										<td><?php echo $b->import_datetime;?></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchStyle(){
	openWindow('<?php echo base_URL()?>desg_request/cari','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedsStyle(idStyle, namaStyle, pict){   
    document.getElementById('idStyle').value = idStyle;
    document.getElementById('namaStyle').value = namaStyle;
	document.getElementById('pict').value = pict;
}
</script>