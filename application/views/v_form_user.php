<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SB Admin 2 - Bootstrap Admin Theme</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $title?></h1>
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_tambah">Tambah</a>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th>UserId</th>
								<th>User Name</th>
								<th>Email</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($data as $a): ?>
							<tr class="odd gradeX">
								<td><?php echo $a->userId?></td>
								<td><?php echo $a->userId?></td>
								<td><?php echo $a->userId?></td>
								<td><?php echo $a->userId?></td>
								<td><?php echo $a->userId?></td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_tambah" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-skin">
				<div class="panel-heading">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h3 class="panel-title"><span class="fa fa-user"></span> Add <?php echo $title?></h3>
				</div>
				<div class="panel-body">
				<?php echo form_open($aplikasi.'/tambah');?>
				<input type="hidden" name="idFormUser" value="0" />
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">User Id</label>
							<input type="text" name="userId" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">User Name</label>
							<input type="text" name="userName" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">NIK/NIP</label>
							<input type="text" name="nik" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">idStKaryawan</label>
							<select name="isActive" class="form-control">
							<?php
							$this->db2 = $this->load->database('danain', TRUE);
							$lihat = $this->db2->query("select * from  tblunitkerja where isActive=1")->result();
							foreach ($lihat as $a): ?>
								<option value="<?php echo $a->idUnitKerja?>"><?php echo $a->keterangan?></option>
							<?php endforeach;?>		
							</select>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Telp</label>
							<input type="text" name="noTlp" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">HP</label>
							<input type="text" name="hp" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Status Kepegawaian</label>
							<select name="isActive" class="form-control">
								<option value="Karyawan">Karyawan</option>
								<option value="Kontrak">Kontrak</option>
								<option value="Lainnya">Lainnya</option>
							</select>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Modul</label>
							<select name="isActive" class="form-control">
							<?php
							$lihat = $this->db->query("select * from  tblmodul_adm where isActive=1")->result();
							foreach ($lihat as $a): ?>
								<option value="<?php echo $a->idModul?>"><?php echo $a->namaModul?></option>
							<?php endforeach;?>		
							</select>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success">Simpan</button>
							<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
						</div>	
					</div>	
				</div>	
				</form>
				</div>	
			</div>
	    </div>
	 </div>
</div>
    <!-- DataTables JavaScript -->
	<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
	<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
	 var $j = jQuery.noConflict();
	   $j(function() {
		 $j( "#input1" ).datepicker({
		 dateFormat : "yy-mm-dd",
		 changeMonth: true,
		 changeYear: true
		 });
		 $j( "#input2" ).datepicker({
		 dateFormat : "yy-mm-dd",
		 changeMonth: true,
		 changeYear: true
		 });
		});
	 </script>