<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
	<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
	 var $j = jQuery.noConflict();
	   $j(function() {
		 $j( "#input1" ).datepicker({
		 dateFormat : "yy-mm-dd",
		 changeMonth: true,
		 changeYear: true
		 });
		 $j( "#input2" ).datepicker({
		 dateFormat : "yy-mm-dd",
		 changeMonth: true,
		 changeYear: true
		 });
		});
	 </script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<legend class="page-header"><?php echo $title?></legend>
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_tambah">+ Tambah</a></p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th>No</th>
								<th>UserId</th>
								<th>Nama</th>
								<th>Email</th>
								<th>Modul</th>
								<th>Departement</th>
								<th>Line</th>
								<th>Status</th>
								<th style="text-align: center">Edit</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach ($data as $a): ?>
							<tr class="odd gradeX">
								<td><?php echo $i++;?></td>
								<td><?php echo $a->userId;?></td>
								<td><?php echo $a->userName;?></td>
								<td><?php echo $a->email;?></td>
								<td><?php echo getModul($a->idModul);?></td>
								<td><?php echo getDept($a->idDept);?></td>
								<td><?php echo getLine($a->idLine);?></td>
								<td><?php if($a->isActive==1){ echo 'Aktif'; } else { echo 'Non.Aktif';};?></td>
								<td style="text-align: center">
									<a class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal_edit<?php echo $a->idUser?>"><i class="fa fa-edit"></i></a>
								</td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<!--- Tambah -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_tambah" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-skin">
				<div class="panel-heading">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h3 class="panel-title"><span class="fa fa-user"></span> Tambah <?php echo $title?></h3>
				</div>
				<div class="panel-body">
				<?php echo form_open_multipart($aplikasi.'/tambah');?>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">User Id</label>
							<input type="text" name="userId" class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" required>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama</label>
							<input type="text" name="nama" class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" required>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Password</label>
							<input type="password" name="password" class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" required>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Email</label>
							<input type="email" name="email" class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" required>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Modul</label>
							<select name="idModul" class="form-control">
							<?php $modul = $this->db->query("select * from tblmodul_adm where isActive=1")->result(); ?>	
							<?php foreach($modul as $a): ?>
								<option value="<?php echo $a->idModul?>"><?php echo getModul($a->idModul)?></option>
							<?php endforeach; ?>
							</select>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Departement</label>
							<select name="idDept" class="form-control">
							<?php $modul = $this->db->query("select * from tbldept where isActive=1")->result(); ?>	
							<?php foreach($modul as $a): ?>
								<option value="<?php echo $a->idDept?>"><?php echo getDept($a->idDept)?></option>
							<?php endforeach; ?>
							</select>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Line</label>
							<select class="form-control" name="idLine">
								<option value=0>No.Line</option>
								<?php
								$line = $this->db->query('select * from tblline')->result();
								foreach($line as $ln):?>
									<option value="<?php echo $ln->idLine?>"><?php echo $ln->keterangan?></option>
								<?php endforeach; ?>
							</select>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success">Simpan</button>
							<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
						</div>	
					</div>	
				</div>	
				</form>
				</div>	
			</div>
	    </div>
	 </div>
</div>
<!--- End Tambah -->
<!--- End Edit -->
<?php foreach($data as $b): $idModul = $b->idModul; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_edit<?php echo $b->idUser?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> Edit <?php echo $title?></h3>
					</div>
					<div class="panel-body">
					<?php echo form_open_multipart($aplikasi.'/edit');?>
					<input type="hidden" name="idUser" value="<?php echo $b->idUser?>"/>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="exampleInputEmail1">User Id</label>
								<input type="text" name="userId" class="form-control upper" value="<?php echo $b->userId?>" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
							</div>	
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label for="exampleInputEmail1">Nama</label>
								<input type="text" name="nama" class="form-control upper" value="<?php echo $b->userName?>" id="exampleInputEmail1" aria-describedby="emailHelp" required>
							</div>	
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label for="exampleInputEmail1">Password</label>
								<input type="password" name="password" class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" required>
							</div>	
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label for="exampleInputEmail1">Email</label>
								<input type="email" name="email" class="form-control upper" value="<?php echo $b->email?>" id="exampleInputEmail1"  aria-describedby="emailHelp" required>
							</div>	
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label for="exampleInputEmail1">Modul</label>
								<select name="idModul" class="form-control">
									<option value="<?php echo $b->idModul?>"><?php echo getModul($b->idModul)?></option>
								<?php $modul = $this->db->query("select * from tblmodul_adm where isActive=1 and idModul<>'$idModul'")->result(); ?>	
								<?php foreach($modul as $a): ?>
									<option value="<?php echo $a->idModul?>"><?php echo getModul($a->idModul)?></option>
								<?php endforeach; ?>
								</select>
							</div>	
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label for="exampleInputEmail1">Departement</label>
								<select name="idDept" class="form-control">
								<?php $modul = $this->db->query("select * from tbldept where isActive=1")->result(); ?>	
								<?php foreach($modul as $a): ?>
									<option value="<?php echo $a->idDept?>"><?php echo getDept($a->idDept)?></option>
								<?php endforeach; ?>
								</select>
							</div>	
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<button type="submit" class="btn btn-success">Simpan</button>
								<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
							</div>	
							<p style="color: red">Setiap Perubahan user akan menyebabkan Password ke Reset</p>
						</div>	

					</div>	
					</form>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End Edit -->
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
	