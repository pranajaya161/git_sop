<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
foreach ($data as $a):
  	$tanggal 	= $a->tanggal;
	$idPo 		= $a->idPo;
	$refNo		= $a->refNo;
	$keterangan = $a->keterangan;
	$lot_number	= $a->lot_number;
	$idSubCount	= $a->idSubCount;
	$idWorksheet= $a->idWorksheet;
 endforeach;
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b>Input Materialist Detail | RefNo. : <?php echo $refNo?> | No.Po : <?php echo getPoNoPo($idPo)?> | Lot : <?php echo $lot_number?> </b>
                </div>
                <div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
						<?php echo form_open_multipart('md_inputworksheet/add_detail');?>
						<input type="hidden" name="idWorksheet" value="<?php echo $idWorksheet?>" />
						<input type="hidden" name="idPo" value="<?php echo $idPo?>" />
							<input type="hidden" name="idMaterialist" value="<?php echo $idMaterialist?>">
							<div class="col-lg-4">
								<div class="form-group">
									<label class="col-sm-12 control-label">Style&nbsp;&nbsp;&nbsp;<a href="#" onclick="searchStyleMd()"><span><i class="fa fa-search"></i></span></a></label>
									<input type="hidden" name="idStyle" id="idStyle" class="tfield" readonly />
									<input type="hidden" name="import_id" id="import_id" class="tfield" readonly />
									<input type="text" name="namaStyle" id="namaStyle" class="form-control"  readonly />
								</div>
							</div>	
							<div class="col-lg-1">
								<div class="form-group">
									<label for="exampleInputEmail1">Size</label>
									<input type="text" name="size" id="size" class="form-control"  readonly />
								</div>	
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label for="exampleInputEmail1">Qty</label>
									<input type="text" name="qty" id="qty"  class="form-control" readonly />
								</div>	
							</div>
							<div class="col-lg-1">
								<div class="form-group">
									<label for="exampleInputEmail1">T.Qty</label>
									<input type="text" name="tqty" id="tqty"  class="form-control" required />
								</div>	
							</div>	
							<div class="col-lg-4">
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-primary">Simpan</button>
									<a href="<?php echo base_URL()?>md_inputworksheet" class="btn btn-warning">Kembali</a>
								</div>
							</div>	
                       </form>
					</div>
					<div class="row">
				        <div class="col-lg-12">
							<div class="form-panel">
							<p>List</p>
							<?php
							$cek = $this->db->query("select nama_ukuran_f1, nama_ukuran_f2, nama_ukuran_f3, nama_ukuran_f4, nama_ukuran_f5, nama_ukuran_f6, nama_ukuran_f7, nama_ukuran_f8,nama_ukuran_f9, nama_ukuran_f10 from  edp_assortment_pros a inner join md_worksheet_dt b on b.id = a.id  where b.idWorksheet ='$idWorksheet'")->row();
							?>
								<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
									<tr>
										<th style="text-align:left">Numbering</th>	
										<th style="text-align:left">Style</th>
										<th style="text-align:left">Size</th>								
										<th style="text-align:right"><?php echo $cek->nama_ukuran_f1?></th>			
										<th style="text-align:right"><?php echo $cek->nama_ukuran_f2?></th>
										<th style="text-align:right"><?php echo $cek->nama_ukuran_f3?></th>				
										<th style="text-align:right"><?php echo $cek->nama_ukuran_f4?></th>
										<th style="text-align:right"><?php echo $cek->nama_ukuran_f5?></th>			
										<th style="text-align:right"><?php echo $cek->nama_ukuran_f6?></th>
										<th style="text-align:right"><?php echo $cek->nama_ukuran_f7?></th>			
										<th style="text-align:right"><?php echo $cek->nama_ukuran_f8?></th>
										<th style="text-align:right"><?php echo $cek->nama_ukuran_f9?></th>			
										<th style="text-align:right"><?php echo $cek->nama_ukuran_f10?></th>
										<th style="text-align:center">Hapus</th>
									</tr>
									<?php
									$det=$this->db->query("select a.*, b.nourut as nom from edp_assortment_pros a inner join md_worksheet_dt b on b.id = a.id  where b.idWorksheet='$idWorksheet'")->result();
									foreach ($det as $b) {
										$i=1;
									?>
									
										<tr>
											<td><?php echo $b->nourut;?></td>
											<td><?php echo getStyle($b->idStyle);?></td>
											<td><?php echo $b->size;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f1;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f2;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f3;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f4;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f5;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f6;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f7;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f8;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f9;?></td>
											<td style="text-align:right"><?php echo $b->ukuran_f10;?></td>
											<td class="tabtxt" style="text-align: center">
												<a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal_hapus<?php echo $b->id?>"><i class="fa fa-trash-o"></i></a>
											</td>
										</tr>
									<?php } ?>
								</table>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<?php $detail=$this->db->query("select a.*, b.nourut as nom, b.idWorksheet from edp_assortment_pros a inner join md_worksheet_dt b on b.id = a.id  where b.idWorksheet='$idWorksheet'")->result();?>
<?php foreach($detail as $c): ?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_hapus<?php echo $c->id?>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-skin">
				<div class="panel-heading">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h3 class="panel-title"><span class="fa fa-user"></span> Hapus <?php echo $title?>&nbsp; <?php echo $c->id?>&nbsp; <?php echo $c->idWorksheet?></h3>
				</div>
				<div class="panel-body">
				<?php echo form_open_multipart('md_inputworksheet/hapus');?>
				<input type="hidden" name="id" value="<?php echo $c->id?>" />
				<input type="hidden" name="idWorksheet" value="<?php echo $c->idWorksheet?>" />
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="exampleInputEmail1">Data Akan di hapus</label>
							</div>	
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<button type="submit" class="btn btn-danger">Hapus</button>
								<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
							</div>	
						</div>	
					</div>	
				</form>
				</div>	
			</div>
	    </div>
	 </div>
</div>
<?php endforeach; ?>	
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>

<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>	
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchStyleMd(){
	openWindow('<?php echo base_URL()?>md_inputworksheet/cari/<?php echo $idPo?>/<?php echo $lot_number?>','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedStyleMd(idStyle, namaStyle, size, qty){   
    document.getElementById('idStyle').value = idStyle;
    document.getElementById('namaStyle').value = namaStyle;
	document.getElementById('size').value = size;
	document.getElementById('qty').value = qty;
}
</script>
