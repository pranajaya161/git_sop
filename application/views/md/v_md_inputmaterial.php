<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_URL()?>css/select2-master/dist/css/select2.min.css"/>
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
	<?php
		if($this->session->flashdata('pesan')==TRUE):
		echo'<div class="alert alert-warning" role="alert">';
		echo $this->session->flashdata('pesan');
		echo "</div>";
	endif;
	?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><?php echo $title?> </p>
                </div>
                <div class="panel-body">
                	<form action="<?php echo base_URL()?><?php echo $aplikasi?>/tambah" name="cabang" method="post"  accept-charset="utf-8" enctype="multipart/form-data">
	                	<div class="col-lg-3">
							<div class="form-group">
								<label for="exampleInputEmail1">No. OR</label>
								<select name="idPo" id="idPo" class="form-control">
									<option>--Pilih OR--</option>
								<?php
								$lihat = $this->db->query("select idPo, noOr from mkt_po_sm where isActive=1 and isActive =1 group by idPo")->result();
								foreach($lihat as $a):
								?>
									<option value="<?php echo $a->idPo?>"><?php echo $a->noOr?></option>
							<?php endforeach;?>
								</select>
							</div>	
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label for="exampleInputEmail1">Keterangan</label>
								<input type="text" name="keterangan" class="form-control" />
							</div>	
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<br>
								<button type="submit" class="btn btn-success">Simpan</button>
							</div>	
						</div>
					</form>
				</div>	
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th style="text-align:right">No.</th>
								<th style="text-align:left">Tanggal</th>
								<th style="text-align:left">Ref.No</th>
								<th style="text-align:left">Keterangan</th>
								<th style="text-align:center">Status</th>
								<th style="text-align:center">Edit</th>
								<th style="text-align:center">Detail</th>
								<th style="text-align:center">Send</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach ($data as $a): ?>
							<tr class="odd gradeX">
								<td style="text-align:right"><?php echo $i++;?></td>
								<td><?php echo $a->tanggal;?></td>
								<td><?php echo $a->refNo;?></td>
								<td><?php echo $a->keterangan;?></td>
								<td style="text-align: center"><?php echo $a->status;?></td>
								<td style="text-align: center">
									<a class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal_edit<?php echo $a->idMaterialist?>"><i class="fa fa-edit"></i></a>
								</td>
								<td style="text-align: center">
									<a href="<?php echo base_URL()?><?php echo $aplikasi?>/detail/<?php echo $a->idMaterialist?>" class="btn btn-xs btn-info"><i class="fa fa-list"></i></a>
								</td>
								<td style="text-align: center">
									<a class="btn btn-xs btn-success" data-toggle="modal" data-target="#modal_apv<?php echo $a->idMaterialist?>"><i class="fa fa-check"></i></a>
								</td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<!--- Tambah -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_tambah" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-skin">
				<div class="panel-heading">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h3 class="panel-title"><span class="fa fa-user"></span> Tambah <?php echo $title?></h3>
				</div>
				<div class="panel-body">
				<!--<?php echo form_open_multipart($aplikasi.'/tambah');?>-->
				<form action="<?php echo base_URL()?><?php echo $aplikasi?>/tambah" name="cabang" method="post"  accept-charset="utf-8" enctype="multipart/form-data">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Tanggal</label>
							<input type="text" name="tanggal" value="<?php echo date('Y-m-d')?>" class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
						</div>	
					</div>
					
					
					
				</div>	
				</form>
				</div>	
			</div>
	    </div>
	 </div>
</div>
<!--- End Tambah -->
<!--- End Edit -->
<?php foreach($data as $b): $idMaterialist = $b->idMaterialist; $idPo = $b->idPo;?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_edit<?php echo $b->idMaterialist?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> Edit <?php echo $title?></h3>
					</div>
					<div class="panel-body">
					<?php echo form_open_multipart($aplikasi.'/edit');?>
					<input type="hidden" name="idMaterialist" value="<?php echo $b->idMaterialist?>"/>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Tanggal</label>
									<input type="text" name="tanggal" value="<?php echo $b->tanggal?>" class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">No.Po</label>
									<select name="idPo" class="form-control">
										<option value="<?php echo $b->idPo?>"><?php echo getPoNoPo($b->idPo)?></option>
										<?php
										$lihat = $this->db->query("select * from barcode where isActive=1 and idWorksheet=0 and idPo <> '$idPo' group by idPo")->result();
										foreach($lihat as $a):
										?>
											<option value="<?php echo $a->idPo?>"><?php echo getPoNoPo($a->idPo)?></option>
									<?php endforeach;?>
										</select>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Keterangan</label>
									<textarea class="form-control" name="keterangan" rows="3"><?php echo $b->keterangan?></textarea>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Userd</label>
									<input type="text" name="userId" value="<?php echo $b->userId?>" class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<button type="submit" class="btn btn-success">Simpan</button>
									<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
								</div>	
							</div>	
						</div>	
					</form>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End Edit -->
<!--- End Approve -->
<?php foreach($data as $b): $idMaterialist = $b->idMaterialist; $idPo = $b->idPo;?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_apv<?php echo $b->idMaterialist?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> Send <?php echo $title?></h3>
					</div>
					<div class="panel-body">
					<?php echo form_open_multipart($aplikasi.'/send');?>
					<input type="hidden" name="idMaterialist" value="<?php echo $b->idMaterialist?>"/>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Data akan di kirim untuk di Approve</label>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<button type="submit" class="btn btn-success">Simpan</button>
									<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
								</div>	
							</div>	
						</div>	
					</form>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End Approve -->
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script src="<?php echo base_URL()?>css/select2-master/dist/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>

	 <script>
            $(document).ready(function () {
                $("#idPo").select2({
                    placeholder: "Please Select"
                });
				
            });
        </script>		  