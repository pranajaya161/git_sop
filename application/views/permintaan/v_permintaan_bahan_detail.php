<head>
    meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
$mode		= $this->uri->segment(4);
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b>Input Detail Permintaan Bahan</b>
                </div>
                <div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
						<form name="cabang" action="<?php echo base_URL()?>permintaan_bahan/tambah_det/<?php echo $idPermintaanBahan?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
							<input type="hidden" name="idPermintaanBahan" value="<?php echo $idPermintaanBahan?>">
							<div class="col-lg-4">
								<div class="form-group">
									<label for="exampleInputEmail1">Nama Bahan</label>
									<select name="idBahan" class="form-control">
										<option>-- Pilih Bahan --</option>
									<?php
									$lihat=$this->db->query("select * from tblbahan where isActive = 1")->result();
									foreach($lihat as $a):
									?>
									<option value="<?php echo $a->idBahan?>"><?php echo getBahan($a->idBahan)?> [<?php echo getSatuan(getBahanSatuan($a->idBahan))?>]</option>
								<?php endforeach;?>
									</select>
								</div>
							</div>	
							<div class="col-lg-4">
								<div class="form-group">
									<label for="exampleInputEmail1">Qty</label>
									<input type="text" name="qty" class="form-control">
								</div>
							</div>	
							<div class="col-lg-4">
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-primary">Tambah</button>
									<a href="<?php echo base_URL()?>permintaan_bahan" class="btn btn-warning">Kembali</a>
								</div>
							</div>	
                       </form>
					</div>
					
					<div class="row">
				        <div class="col-lg-12">
							<div class="form-panel">
							<p>List</p>
								<table class="table table-bordered table-striped table-condensed">
									<tr>
										<th style="text-align:left">No.</th>	
										<th style="text-align:left">Bahan</th>	
										<th style="text-align:left">Total</th>
										<th style="text-align:left">Satuan</th>
										<th style="text-align:center">Hapus</th>
									</tr>
									<?php
									$i=1;
									$detail = $this->db->query("select * from permintaan_bahan_dt where idPermintaanBahan='$idPermintaanBahan'")->result();
									foreach($detail as $xx):?>
									<tr>
										<td><?php echo $i++;?></td>
										<td><?php echo getBahan($xx->idBahan)?></td>
										<td><?php echo number_format($xx->qty,2)?></td>
										<td><?php echo getSatuan(getBahanSatuan($xx->idBahan))?></td>
										<td style="text-align: center">
											<a href="<?php echo base_URL()?>permintaan_bahan/hapus/<?php echo $xx->idPermintaanBahan?>/<?php echo $xx->idBahan?>" class="btn btn-xs btn-danger" onClick="return confirm('Data akan di hapus...?');"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<?php endforeach;?>
								</table>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchStyle(){
	openWindow('<?php echo base_URL()?>desg_request/cari','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedsStyle(idStyle, namaStyle, pict){   
    document.getElementById('idStyle').value = idStyle;
    document.getElementById('namaStyle').value = namaStyle;
	document.getElementById('pict').value = pict;
}
</script>