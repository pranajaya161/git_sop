<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <?php
                                $prospecting = $this->db->query("select count(*) as unit from mkt_pros_sm")->row();
                                ?>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo number_format($prospecting->unit)?></div>
                                    <div>Prospecting</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <?php
                                $po = $this->db->query("select count(*) as unit from mkt_po_sm")->row();
                                ?>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo number_format($po->unit)?></div>
                                    <div>Purchase Order</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">0</div>
                                    <div>Worksheet</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">0</div>
                                    <div>Pengiriman</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Top New 10 Prospecting
                         </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div>
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <th>No.</th>
                                        <th>Tanggal</th>
                                        <th>Ref.Po</th>
                                        <th>Buyer</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        $detail1 = $this->db->query("select * from mkt_pros_sm order by Tanggal desc limit 10")->result();
                                        foreach($detail1 as $det1):?>
                                            <tr>
                                                <td><?php echo $no++;?></td>
                                                <td><?php echo $det1->tanggal?></td>
                                                <td><?php echo $det1->refNo?></td>
                                                <td><?php echo getClient($det1->idClient)?></td>
                                            </tr>
                                        <?php
                                        endforeach;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Top New 10 Prospecting
                         </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div>
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <th>No.</th>
                                        <th>Tanggal</th>
                                        <th>Ref.Po</th>
                                        <th>Buyer</th>
                                        <th>Qty</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        $detail1 = $this->db->query("select * from mkt_po_sm order by Tanggal desc limit 10")->result();
                                        foreach($detail1 as $det1):?>
                                            <tr>
                                                <td><?php echo $no++;?></td>
                                                <td><?php echo $det1->tanggal?></td>
                                                <td><?php echo $det1->refNo?></td>
                                                <td><?php echo getClient($det1->idClient)?></td>
                                                <td><?php echo number_format($det1->qty)?></td>
                                            </tr>
                                        <?php
                                        endforeach;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Top New 10 Tracking Prospecting
                         </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div>
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <th>Tanggal</th>
                                        <th>Ref.No</th>
                                        <th style="text-align: center">Design</th>
                                        <th style="text-align: center">Sample</th>
                                        <th style="text-align: center">Customer Approve</th>
                                        <th style="text-align: center">Cost Calculation</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        $detail1 = $this->db->query("select * from mkt_pros_sm order by idProspecting")->result();
                                        foreach($detail1 as $det1): $idProspecting = $det1->idProspecting;?>
                                                <tr>
                                                    <td><?php echo $det1->tanggal?></td>
                                                    <td><?php echo $det1->refNo?></td>
                                                    <td style="text-align: center"><?php if($det1->isDesign==1){ ?> <i class="fa fa-check"></i> <?php }?></td>
                                                    <td style="text-align: center"><?php if($det1->isSample==1){ ?> <i class="fa fa-check"></i> <?php }?></td>
                                                    <td style="text-align: center"><?php if($det1->isApvCust==1){ ?> <i class="fa fa-check"></i> <?php }?></td>
                                                    <td style="text-align: center"><?php if($det1->isCostCalc==1){ ?> <i class="fa fa-check"></i> <?php }?></td>
                                                </tr>    

                                        <?php 
                                        endforeach;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Top New 10 Tracking
                         </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div>
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <th>Tanggal</th>
                                        <th>Purchase Order</th>
                                        <th>WorkSheet</th>
                                        <th>MD</th>
                                        <th>Cut-IN</th>
                                        <th>Cut-Out</th>
                                        <th>Sewing-IN</th>
                                        <th>Sewing-Out</th>
                                        <th>Triming-IN</th>
                                        <th>Triming-Out</th>
                                        <th>QC-IN</th>
                                        <th>Qc-Out</th>
                                        <th>Setting-IN</th>
                                        <th>Packing-Out</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        $detail1 = $this->db->query("select * from mkt_po_sm where isActive = 1")->result();
                                        foreach($detail1 as $det1): $idPo = $det1->idPo;
                                                $Worksheet = $this->db->query("select * from md_worksheet where idPo='$idPo'")->result();
                                                foreach($Worksheet as $work){
                                                    $idWorksheet = $work->idWorksheet;?>
                                                <tr>
                                                    <td><?php echo $work->tanggal?></td>
                                                    <td><?php echo getPoRefno($work->idPo)?></td>
                                                    <td><?php echo getRefNoWorksheet($work->idWorksheet)?></td>
                                                    <?php
                                                    $mdin = $this->db->query("select count(*) as nom from md_worksheet_dt where idWorksheet='$idWorksheet'")->row();
                                                    ?>
                                                    <td><?php echo number_format($mdin->nom)?></td>
                                                    <?php
                                                    $cutin = $this->db->query("select count(*) as nom from cutting_in where idWorksheet='$idWorksheet'")->row();
                                                    ?>
                                                    <td><?php echo number_format($cutin->nom)?></td>
                                                    <?php
                                                    $cutout = $this->db->query("select count(*) as nom from cutting_out a inner join cutting_out_sm b on b.idCutting  = a.idCutting where b.idWorksheet='$idWorksheet'")->row();
                                                    ?>
                                                    <td><?php echo number_format($cutout->nom)?></td>
                                                    <?php
                                                    $sewin = $this->db->query("select count(*) as nom from sewing_in where idWorksheet='$idWorksheet'")->row();
                                                    ?>
                                                    <td><?php echo number_format($sewin->nom)?></td>
                                                    <?php
                                                    $sewout = $this->db->query("select count(*) as nom from sewing_out a inner join sewing_out_sm b on b.idSewing  = a.idSewing where b.idWorksheet='$idWorksheet'")->row();
                                                    ?>
                                                    <td><?php echo number_format($sewout->nom)?></td>
                                                    <?php
                                                    $trimin = $this->db->query("select count(*) as nom from triming_in where idWorksheet='$idWorksheet'")->row();
                                                    ?>
                                                    <td><?php echo number_format($trimin->nom)?></td>
                                                    <?php
                                                    $trimout = $this->db->query("select count(*) as nom from triming_out a inner join triming_out_sm b on b.idTriming  = a.idTriming where b.idWorksheet='$idWorksheet'")->row();
                                                    ?>
                                                    <td><?php echo number_format($trimout->nom)?></td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>        
                                                </tr>    

                                        <?php }
                                        endforeach;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- jQuery -->
    <script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_URL()?>vendor/raphael/raphael.min.js"></script>
    <script src="<?php echo base_URL()?>vendor/morrisjs/morris.min.js"></script>
    <script src="<?php echo base_URL()?>data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script> 
    