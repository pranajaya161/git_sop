<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cutting_stock extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_cutting'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'cutting_stock';
		$data['title']		= 'Stock Input Cutting';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_cutting->show_cutting_input_stock();
		$this->template->load('role','isi','cutting/v_cutting_stock',$data);
	}
		
}