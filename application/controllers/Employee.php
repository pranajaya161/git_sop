<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_employee');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'employee';
		$data['title']		= 'Personal Employee';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_employee->show();
		$this->template->load('role','isi','v_employee',$data);
	}
	public function add()
	{
		$aplikasi 			= 'employee';
		$data['title']		= 'Add Personal Employee';
		$data['aplikasi'] 	= $aplikasi;
		$this->template->load('role','isi','v_employee_add',$data);
	}
	public function simpan()
	{
		$aplikasi 			= 'employee';
		$data['title']		= 'Personal Employee';
		$data['aplikasi'] 	= $aplikasi;
		$this->form_validation->set_rules('ktp','ktp','trim|required|is_unique[employee.ktp]|xss_clean|max_length[128]');
		if($this->form_validation->run() == FALSE)
		{
		  $this->index();
		} 
		else 
		{
			$this->db->trans_begin();
			$fileName 	= $_FILES['pict']['name'];
			$pisahName	= substr(strrchr($fileName, '.'), 1);
			$imageName  = addslashes($this->input->post('ktp')).'.'.$pisahName;
			$config['upload_path']      = 'uploads/ktp/';
			$config['allowed_types'] 	= 'pdf|jpg|png|jpeg';
			$config['max_size']			= '5000000';
			$config['max_width']  		= '5000000';
			$config['max_height']  		= '5000000';
			$config['file_name'] 		= $imageName;  
			$this->load->library('upload', $config);
			$this->upload->initialize($config);	
			if($this->upload->do_upload('pict'))
			{
				$up_data	= $this->upload->data();
				$pict		= $up_data['file_name'];
				$data = array(
					'idEmployee'=>0,
					'nik'=>addslashes($this->input->post('nik')),
					'userName'=>addslashes($this->input->post('userName')),
					'ktp'=>addslashes($this->input->post('ktp')),
					'npwp'=>addslashes($this->input->post('npwp')),
					'tempatLahir'=>addslashes($this->input->post('tempatLahir')),
					'tglLahir'=>addslashes($this->input->post('tglLahir')),
					'jenKel'=>addslashes($this->input->post('jenKel')),
					'alamat'=>addslashes($this->input->post('alamat')),
					'hp'=>addslashes($this->input->post('hp')),
					'idJabatan'=>addslashes($this->input->post('idJabatan')),
					'idDivisi'=>addslashes($this->input->post('idDivisi')),
					'idPendidikan'=>addslashes($this->input->post('idPendidikan')),
					'idKompetensi'=>addslashes($this->input->post('idKompetensi')),
					'StKaryawan'=>addslashes($this->input->post('StKaryawan')),
					'tgl_join'=>addslashes($this->input->post('tgl_join')),
					'idEmployee_head'=>addslashes($this->input->post('idEmployee_head')),
					'pict'=>$pict,
					'isActive'=>0,
					'created_usr'=> $this->session->userdata('userId')
				);
				$this->m_employee->tambah($data);
				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
					redirect($aplikasi.'/add','refresh');
				} else {
					$this->db->trans_commit();
					$this->session->set_flashdata('pesan','Data berhasil di tambah');
					redirect($aplikasi,'refresh');
				}
			} // Upload KTP
			else 
			{
				$this->db->trans_complete();
				$this->db->trans_rollback();
				$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
				redirect($aplikasi.'/add','refresh');
			}
			
		}
	}
	public function edit()
	{
		$aplikasi 			= 'employee';
		$data['title']		= 'Add Personal Employee';
		$data['aplikasi'] 	= $aplikasi;
		$idEmployee			= $this->uri->segment(3);
		$data['data']		= $this->m_employee->show_edit($idEmployee);
		$this->template->load('role','isi','v_employee_add',$data);
	}
}