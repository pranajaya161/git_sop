<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gudang_penerimaan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_gudang'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'gudang_penerimaan';
		$data['title']		= 'Gudang Penerimaan Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_gudang->show_penerimaan_summ();
		$this->template->load('role','isi','gudang/v_gudang_penerimaan_sum',$data);
	}
	public function terima()
	{
		$aplikasi 			= 'gudang_penerimaan';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPurcPembelian	= addslashes($this->input->post('idPurcPembelian'));
		$idSupplier			= $this->input->post('idSupplier');
		$refNo	 			= addslashes($this->input->post('refNo'));
		$idBahan	 		= $this->input->post('idBahan');
		$terima 			= $this->input->post('terima');
		$satuan		 		= $this->input->post('satuan');
		$this->db->trans_begin();
			$jumlah=count($idSupplier);
			for($i=0;$i<$jumlah;$i++)
			{
				$data = array(
					'idPurcPembelian'=>0,
					'refNo'=>$refNo,
					'idClient'=>0,
					'idClient'=>0,
					'idSupplier'=>$idSupplier[$i],
					'idBahan'=>$idBahan,
					'satuan'=>getBahanSatuan($idBahan),
					'qty'=>$terima[$i],
					'status'=>'Open',
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_gudang->tambah_penerimaan($data);
				$lihat = $this->db->query("select * from purc_pembelian_supp where idBahan ='$idBahan' and idSupplier ='$idSupplier[$i]'")->row();
				if($lihat)
				{
					$this->db->query("update purc_pembelian_supp set qty_input = qty_input + '$terima[$i]' where idBahan ='$idBahan' and idSupplier ='$idSupplier[$i]'");
				}


				
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di ');
			redirect($aplikasi,'refresh');
		}
	}
	
}