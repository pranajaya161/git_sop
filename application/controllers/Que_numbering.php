<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Que_numbering extends CI_Controller {
	private $db2;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_query');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'que_numbering';
		$data['title']		= 'Query : Numbering';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_query->show_po();
		$this->template->load('role','isi','query/v_numbering', $data);
	}
	public function proses()
	{
		$aplikasi 			= 'que_numbering';
		$data['title']		= 'Query : Numbering';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_query->show_po();
		$data['head']		= $this->m_query->data_po($this->input->post('idPo'))->row();
		$data['detail']		= $this->m_query->data_po($this->input->post('idPo'))->result(); 
		$this->template->load('role','isi','query/v_numbering', $data);
	}
	

}