<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Samp_approve_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_prospecting');
		$this->load->model('m_parameter');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'samp_approve_input';
		$data['title']		= 'Sample : Approve Input Sample ';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_prospecting->show_samp_input_apv();
		$this->template->load('role','isi','sample/v_samp_input_apv',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'samp_approve_input';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idSample			= addslashes($this->input->post('idSample'));
		$status 			= addslashes($this->input->post('status'));
		$idProspecting 		= addslashes($this->input->post('idProspecting'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			if($status=='Approve')
			{
				$xStatus = 1;
			} else 
			{
				$xStatus = 0;
			}
			$this->db->query("update mkt_pros_sample set status = '$xStatus', isActive='$xStatus' where idSample='$idSample'");
			$data = array(
				'idSample'=>$idSample,
				'status'=>$xStatus,
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'userId'=>$userId,
			);
			$this->m_prospecting->tambah_sample_approve($data);
			$this->db->query("update mkt_pros_sm set isSample = '$xStatus' where idProspecting ='$idProspecting'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Tambah');
			redirect($aplikasi,'refresh');
		}
	}
	
}