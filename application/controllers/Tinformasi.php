<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tinformasi extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_parameter');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'tinformasi';
		$data['title']		= 'Parameter : Informasi';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_parameter->show_informasi();
		$this->template->load('role','isi','parameter/v_informasi',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'tinformasi';
		$data['aplikasi'] 	= $aplikasi;
		$this->db->trans_begin();
			$data = array(
				'idInformasi'=>0,
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'isActive'=>1
			);
			$this->m_parameter->add_informasi($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$aplikasi 			= 'tinformasi';
		$data['aplikasi'] 	= $aplikasi;
		$this->db->trans_begin();
			$data = array(
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'isActive'=>addslashes($this->input->post('isActive'))
			);
			$this->m_parameter->edit_informasi(addslashes($this->input->post('idInformasi')), $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	
}