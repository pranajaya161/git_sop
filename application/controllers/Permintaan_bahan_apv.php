<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permintaan_bahan_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_permintaan');
	}
	public function index()
	{
		$aplikasi 			= 'permintaan_bahan_apv';
		$idDept				= $this->session->userdata('idDept');
		$data['title']		= getDept($idDept).' Approve Permintaan Bahan';
		$data['aplikasi'] 	= $aplikasi;
		$data['idDept']		= $idDept;
		$data['data']		= $this->m_permintaan->show_permintaan_input_id_apv($this->session->userdata('idDept'));
		$this->template->load('role','isi','permintaan/v_permintaan_bahan_input_apv',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'permintaan_bahan_apv';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPermintaanBahan	= addslashes($this->input->post('idPermintaanBahan'));
		$status 			= addslashes($this->input->post('status'));
		$keterangan 		= addslashes($this->input->post('keterangan'));
		$this->db->trans_begin();
			if($status ==1)
			{
				$this->db->query("update permintaan_bahan set isActive = 1 where idPermintaanBahan='$idPermintaanBahan'");
				$xStatus = 'Tambah';	
			} else {
				$this->db->query("update permintaan_bahan set isActive = 0 where idPermintaanBahan='$idPermintaanBahan'");
				$xStatus = 'Hapus'; 
			}
			$data = array(
				'idPermintaanBahan'=>$idPermintaanBahan,
				'isStatus'=>$status,
				'keterangan'=>$keterangan,
				'userId'=>$userId,
			);
			$this->m_permintaan->tambah_approve($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di '.$xStatus);
			redirect($aplikasi,'refresh');
		}
	}

}