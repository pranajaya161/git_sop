<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sewing_input_penerimaan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_sewing'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'sewing_input_stock';
		$data['title']		= 'Stock Penerimaan';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_sewing->show_sewing_stock_penerimaan();
		$this->template->load('role','isi','sewing/v_sewing_stock_penerimaan',$data);
	}
		
}