<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Smp_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_prospecting');
		$this->load->model('m_parameter');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'smp_input';
		$data['title']		= 'Sample : Input Sample ';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_prospecting->show_samp_input();
		$this->template->load('role','isi','sample/v_samp_input',$data);
	}
	public function kirim()
	{
		$aplikasi 			= 'smp_input';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idSample			= addslashes($this->input->post('idSample'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$this->db->query("update mkt_pros_sample set status=99 where idSample='$idSample'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Simpan');
			redirect($aplikasi,'refresh');
		}
	}
	public function sample_style()
	{
		$aplikasi 			= 'smp_input/sample_style/'.$this->uri->segment(3);
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idSample			= addslashes($this->uri->segment(3));
		$data['data']		= $this->m_prospecting->show_sample_show($idSample);
		$data['detail']		= $this->m_prospecting->show_sample_style($idSample);
		$this->template->load('role','isi','sample/v_samp_input_style',$data);
	}
	public function upload_style()
	{
		$aplikasi 			= 'smp_input/sample_style/'.addslashes($this->input->post('idSample'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'idSample'=>addslashes($this->input->post('idSample')),
				'idStyle'=>addslashes($this->input->post('idStyle')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'userId'=>$userId
			);
			$this->m_prospecting->tambah_sample_style($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Simpan');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus_upload()
	{
		$aplikasi 			= 'smp_input/sample_style/'.addslashes($this->input->post('idSample'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idSample			= addslashes($this->input->post('idSample'));
		$idStyle			= addslashes($this->input->post('idStyle'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$this->db->query("delete from mkt_pros_sample_sty where idSample='$idSample' and idStyle='$idStyle'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Simpan');
			redirect($aplikasi,'refresh');
		}
	}
	
}