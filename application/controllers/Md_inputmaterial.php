<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Md_inputmaterial extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_md');
	}
	public function index()
	{
		$aplikasi 			= 'md_inputmaterial';
		$data['title']		= 'MD Input Materialist';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_md->show_materialist();
		$this->template->load('role','isi','md/v_md_inputmaterial',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'md_inputmaterial';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$kode				= 'MD-MT';
		$bulan 				= date('m');
		$tahun 				= date('Y');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'idMaterialist'=>0,
				'refNo'=>getRefMaterial($kode, $bulan, $tahun),
				'idPo'=>addslashes($this->input->post('idPo')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>'Open',
				'userId'=>$userId,
				'isActive'=>0
			);
			$this->m_md->tambah($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$aplikasi 			= 'md_inputmaterial';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'idPo'=>addslashes($this->input->post('idPo')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'userId'=>$userId,
			);
			$this->m_md->edit($this->input->post('idMaterialist'), $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Ubah');
			redirect($aplikasi,'refresh');
		}
	}
	public function send()
	{
		$aplikasi 			= 'md_inputmaterial';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idMaterialist		= addslashes($this->input->post('idMaterialist'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$cek = $this->db->query("select count(*) as nom from md_materialist_dt where idMaterialist='$idMaterialist'")->row();
		if($cek->nom > 0)
		{	
			$this->db->trans_begin();
				$this->db->query("update md_materialist set status ='Send' where status in ('Open','Reject') and idMaterialist='$idMaterialist'");
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
				redirect($aplikasi,'refresh');
			} else {
				$this->db->trans_commit();
				$this->session->set_flashdata('pesan','Data berhasil di Kirim');
				redirect($aplikasi,'refresh');
			}
		} else {
			$this->session->set_flashdata('Error','Data detail belum terisi');
				redirect($aplikasi,'refresh');
		}
		
	}
	//===== Detail
	public function detail()
	{
		$aplikasi 			= 'md_inputmaterial/detail/'.$this->uri->segment(3);
		$data['title']		= 'MD Input Materialist Detail';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_md->show_materialist_id($this->uri->segment(3));
		$this->template->load('role','isi','md/v_md_inputmaterial_detail',$data);
	}
	public function add_detail()
	{
		$aplikasi 			= 'md_inputmaterial/detail/'.$this->input->post('idMaterialist');
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idStyle			= $this->input->post('idStyle');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$cek = $this->db->query("select * from md_maaping where idStyle='$idStyle'")->result();
			foreach($cek as $a)
			{
				$data = array(
					'idMaterialist'=>addslashes($this->input->post('idMaterialist')),
					'idStyle'=>addslashes($this->input->post('idStyle')),
					'idBahan'=>$a->idBahan,
					'pcs'=>addslashes($this->input->post('pcs')),
					'cons'=>$a->cons,
					'total'=>($a->cons + $this->input->post('idBahan')), 
					'satuan'=>$a->satuan,
					'userId'=>$userId
				);
				$this->m_md->tambah_detail($data);
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
}