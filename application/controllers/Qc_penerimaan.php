<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qc_penerimaan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_qc'); 
		$this->load->model('m_log'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'Qc_penerimaan';
		$data['title']		= 'Penerimaan QC';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_qc->show_qc_penerimaan();
		$this->template->load('role','isi','qc/v_qc_penerimaan',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'Qc_penerimaan';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$id 				= $this->input->post('id');
		$nourut				= $this->input->post('nourut');
		$idPpicQc			= $this->input->post('idPpicQc');
		$idWorksheet		= $this->input->post('idWorksheet');
		$refNo				= $this->input->post('refNo');
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$jumlah = count($id);
			for($i=0;$i<$jumlah;$i++)
			{
				$data = array(
					'idPpicQc'=>$idPpicQc,
					'idWorksheet'=>$idWorksheet,
					'id'=>$id[$i],
					'nourut'=>$nourut[$i],
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_qc->tambah_penerimaan($data);

				$hist = [
					'id'=>$id[$i],
					'nourut'=>$nourut[$i],
					'tanggal'=>date('Y-m-d'),
					'idDept'=>10,
					'kdTrans'=>'IN',
					'keterangan'=>'Penerimaan Triming',
					'userId'=>$userId
				];
				$this->m_barcode->tambah_hist($hist);
				$this->m_log->tambah($id[$i], $nourut[$i], 'QC Penerimaan', 'Penerimaan QC', $userId);
				
			}
			$this->db->query("update ppic_qc set isActive=1 where idPpicQc='$idPpicQc' and isActive=0");
			
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di ');
			redirect($aplikasi,'refresh');
		}
	}
}