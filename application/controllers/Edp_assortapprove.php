<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edp_assortapprove extends CI_Controller {
	private $db2;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_edp');
		$this->load->model('m_log');
		$this->db2 = $this->load->database('mega', TRUE);
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'edp_assortapprove';
		$data['title']		= 'Edp : Assortment Transfer Approve';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_edp->show_edp_assort_apv();
		$this->template->load('role','isi','edp/v_edp_assort_apv', $data);
	}
	public function caritransassortment()
	{
		$aplikasi 			= 'edp_assort';
		$data['title']		= 'Edp : Assortment';
		$data['aplikasi'] 	= $aplikasi;
		$idPo 				= addslashes($this->uri->segment(3));
		$idStyle 			= addslashes($this->uri->segment(4));
		$data['data']		= $this->m_edp->edp_assort_pros_cari($idPo, $idStyle);
		$this->load->view('edp/v_edp_assort_pros_cari',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'edp_assortapprove';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPo				= addslashes($this->input->post('idPo'));
		$idStyle			= addslashes($this->input->post('idStyle'));
		$import_id			= addslashes($this->input->post('import_id'));
		$status 			= addslashes($this->input->post('status'));
		$keterangan 		= addslashes($this->input->post('keterangan'));
		$nama_pakaian       = getStyle($idStyle);
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			if($status ==1)
			{
				//$cek$data = $this->db->query(''); 
				//die;
				$this->m_edp->barcode_hist($idPo, $idStyle, $import_id);
				$this->db->query("update edp_assortment_pros set isActive = 1, idDept = 4 where idStyle='$idStyle' and idPo='$idPo' and import_id='$import_id'");
				$xStatus = 'Tambah';	
			} else {
				$this->db->query("delete from edp_assortment_pros where idStyle='$idStyle' and idPo='$idPo' and import_id='$import_id'");
				$this->db->query("update edp_assortment_dt set artikel = '' where import_id='$import_id' and nama_pakaian='$nama_pakaian'");
				$xStatus = 'Hapus'; 
			}
			$data = array(
				'idAssortmentApv'=>0,
				'idPo'=>$idPo,
				'idStyle'=>$idStyle,
				'import_id'=>$import_id,
				'isStatus'=>$status,
				'keterangan'=>$keterangan,
				'userId'=>$userId,
			);
			$this->m_edp->tambah_approve($data);
			$hist = $this->db->query("select * from edp_assortment_pros where idStyle='$idStyle' and idPo='$idPo' and import_id='$import_id'")->result();
			foreach($hist as $h):
				$this->m_log->tambah($h->id, $h->nourut, 'Proses IT Assortment', $keterangan, $userId);
			endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di '.$xStatus);
			redirect($aplikasi,'refresh');
		}
	}

}