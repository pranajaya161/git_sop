
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pros_apv_cust extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_prospecting');
		$this->load->model('m_parameter');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'pros_apv_cust';
		$data['title']		= 'Marketing : Approve Customer Design';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_prospecting->show_apv_cust();
		$this->template->load('role','isi','prospecting/v_apv_cust',$data);
	}
	public function approve()
	{
		$aplikasi 		  = 'pros_apv_cust';
		$data['title']	  = 'Marketing : Approve Customer Design';
		$data['aplikasi'] = $aplikasi;
		$tgl_proses 	  = date('Y-m-d H:i:s');
		$userId			  = $this->session->userdata('userId');
		$cek			  = $this->input->post('idStyle'); 
		$idProspecting	  = $this->input->post('idProspecting'); 
		$keterangan		  = $this->input->post('keterangan'); 
		$status			  = $this->input->post('status'); 
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$jumlah=count($cek);
		$this->db->trans_begin();
			for($i=0;$i<$jumlah;$i++)
			{
				$data = array(
					'status'=>$status[$i]
				);
				$this->m_prospecting->updateStatusSty($this->input->post('idProspecting'), $data);
				$log = array(
				'idProspecting'=>$idProspecting,
				'keterangan'=>$keterangan[$i],
				'userId'=>$userId
				);
			}
			$this->m_prospecting->tambah_cust_apv($log);
			$this->db->query("update mkt_pros_sm set isApvCust = '1' where idProspecting ='$idProspecting'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data Berhasil di Simpan');
			redirect($aplikasi,'refresh');
		}
	}
	
}