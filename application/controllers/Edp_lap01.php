<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edp_lap01 extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_edp');
		$this->load->model('m_log');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'edp_assortapprove';
		$data['title']		= 'Edp : Assortment Transfer Approve';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_edp->show_edp_lap01();
		$this->template->load('role','isi','edp/v_edp_lap01', $data);
	}
	public function caritransassortment()
	{
		$aplikasi 			= 'edp_assort';
		$data['title']		= 'Edp : Assortment';
		$data['aplikasi'] 	= $aplikasi;
		$idPo 				= addslashes($this->uri->segment(3));
		$idStyle 			= addslashes($this->uri->segment(4));
		$data['data']		= $this->m_edp->edp_assort_pros_cari($idPo, $idStyle);
		$this->load->view('edp/v_edp_assort_pros_cari',$data);
	}
	

}