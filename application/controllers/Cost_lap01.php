<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cost_lap01 extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_prospecting');
		$this->load->model('m_parameter');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'pros_apv_cost';
		$data['title']		= 'Marketing : Approval Cost Calculation';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_prospecting->show_upl_cost_lap();
		$this->template->load('role','isi','prospecting/v_upl_cost_lap',$data);
	}
	
	
}