<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tkategori extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_parameter');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'Tkategori';
		$data['title']		= 'Parameter : Kategori Bahan';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_parameter->show_kategori();
		$this->template->load('role','isi','parameter/v_kategori_bahan',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'Tkategori';
		$data['aplikasi'] 	= $aplikasi;
		$this->db->trans_begin();
			$data = array(
				'idKategori'=>0,
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'isActive'=>1
			);
			$this->m_parameter->add_kategori_bahan($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$aplikasi 			= 'Tkategori';
		$data['aplikasi'] 	= $aplikasi;
		$this->db->trans_begin();
			$data = array(
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'isActive'=>addslashes($this->input->post('isActive'))
			);
			$this->m_parameter->edit_kategori_bahan(addslashes($this->input->post('idKategori')), $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	
}