<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cutting_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_cutting'); 
		$this->load->model('m_log'); 
		$this->load->model('m_barcode'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'cutting_apv';
		$data['title']		= 'Approve Input Cutting';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_cutting->show_cutting_input_apv();
		$this->template->load('role','isi','cutting/v_cutting_apv',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'cutting_apv';
		$data['title']		= 'Approve Input Cutting';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$cekbox				= $this->input->post('cekbox');

		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$jumlah = count($cekbox);
			for($i=0;$i<$jumlah;$i++)
			{
				$this->db->query("update cutting_in set isActive =1 where id='$cekbox[$i]'");
				$h = $this->db->query("select * from cutting_in where id='$cekbox[$i]'")->row();
				$barcode = $this->m_barcode->show_id($cekbox[$i]);
				$this->m_log->tambah($barcode->id, $barcode->nourut, 'Cutting In Apv', 'Approve Input Cutting', $userId);
				$hist = [
					'id'=>$barcode->id,
					'nourut'=>$barcode->nourut,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>7,
					'kdTrans'=>'APV',
					'keterangan'=>'APV - Input Cutting',
					'userId'=>$userId
				];
				$this->m_barcode->tambah_hist($hist);
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
		
}