<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_user extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_form');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'form_user';
		$data['title']		= 'Form User';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_form->show_aktif();
		$this->template->load('role','isi','v_form_user',$data);
	}
}