<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packing_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_packing'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'packing_input';
		$data['title']		= 'Input Packing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_packing->show_packing_input();
		$this->template->load('role','isi','packing/v_packing_input',$data);
	}
	public function hapus()
	{
		$aplikasi 			= 'packing_input/detail/'.$this->uri->segment(5);
		$data['title']		= 'Input Packing';
		$data['aplikasi'] 	= $aplikasi;
		$idWorksheet		= addslashes($this->uri->segment(3));
		$id  				= addslashes($this->uri->segment(4));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$cek = $this->m_barcode->show_id($id);
			$hist = [
				'id'=>$cek->id,
				'nourut'=>$cek->nourut,
				'tanggal'=>date('Y-m-d'),
				'idDept'=>11,
				'kdTrans'=>'DEL',
				'keterangan'=>'DEL - Input Packing',
				'userId'=>$userId
			];
			$this->m_barcode->tambah_hist($hist);
			$this->db->query("delete from packing_in where id='$id'");
			$this->db->query("update packing_stock01 set isActive=0 where id='$id'");

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Hapus');
			redirect($aplikasi,'refresh');
		}
	}
	public function kirim()
	{
		$aplikasi 			= 'packing_input';
		$data['title']		= 'Input Packing';
		$data['aplikasi'] 	= $aplikasi;
		$idPpicPacking	    = addslashes($this->input->post('idPpicPacking'));
		$this->db->trans_begin();
			$cek  = $this->m_packing->cek_input_packing_kirim($idPpicPacking)->result();
			foreach($cek as $a):
				$id = $a->id;
				$this->db->query("update packing_stock01 set isActive=1 where isActive=99 and id='$id'");
				$hist = [
					'id'=>$a->id,
					'nourut'=>$a->nourut,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>11,
					'kdTrans'=>'SEND',
					'keterangan'=>'SEND - Input packing',
					'userId'=>addslashes($this->session->userdata('userId'))
				];
				$this->m_barcode->tambah_hist($hist);
				$this->db->query("update packing_in set isActive = 99 where id='$id'");
			endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function detail()
	{
		$aplikasi 			= 'packing_input/detail/'.$this->uri->segment(3);
		$data['title']		= 'Input Packing Detail';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_packing->show_packing_input_id($this->uri->segment(3));
		foreach($data['data'] as $a):
			$idWorksheet = $a->idWorksheet;
			$idPpicPacking = $a->idPpicPacking;
	   endforeach;
	   $data['detail']		= $this->m_packing->show_packing_input_detail($idPpicPacking, $idWorksheet);
	   $this->template->load('role','isi','packing/v_packing_input_detail',$data);
	}
	public function tambah_det()
	{
		$aplikasi 			= 'packing_input/detail/'.addslashes($this->input->post('idPpicPacking'));
		$data['title']		= 'Input QC Detail';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$numbering			= addslashes($this->input->post('numbering'));
		$idPpicPacking		= addslashes($this->input->post('idPpicPacking'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$lihat = $this->m_packing->cek_input_packing_add($idPpicPacking, $idWorksheet, $numbering);
			if($lihat)
			{
				$id = $lihat->id;
				$data = array(
					'idPpicPacking'=>$idPpicPacking,
					'idWorksheet'=>$idWorksheet,
					'id'=>$lihat->id,
					'nourut'=>$numbering,
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_packing->tambah_packing_input($data);

				$hist = [
					'id'=>$lihat->id,
					'nourut'=>$numbering,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>11,
					'kdTrans'=>'IN',
					'keterangan'=>'Input Packing',
					'userId'=>$userId
				];
				$this->m_barcode->tambah_hist($hist);
				$this->db->query("update packing_stock01 set isActive=99 where isActive=0 and id='$id'");
				
			} else {
				$this->db->trans_complete();
				$this->db->trans_rollback();
				$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
				redirect($aplikasi,'refresh');
			}	
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali - 2');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
}