<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edp_assort extends CI_Controller {
	private $db2;
	private $db3;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_edp');
		$this->db2 = $this->load->database('mega', TRUE);
		$this->db3 = $this->load->database('assort', TRUE);
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'edp_assort';
		$data['title']		= 'Edp : Assortment';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_edp->show_edp_assort();
		$this->template->load('role','isi','edp/v_edp_assort', $data);
	}
	public function add()
	{
		$aplikasi 			= 'edp_assort';
		$data['title']		= 'Edp : Assortment';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_edp->show_edp_assort_id($this->uri->segment(3));
		$this->template->load('role','isi','edp/v_edp_assort_add', $data);
	}
	public function cariassorsment1()
	{
		$aplikasi 			= 'edp_assort';
		$data['title']		= 'Edp : Assortment';
		$data['aplikasi'] 	= $aplikasi;
		$import_id 			= addslashes($this->uri->segment(3));
		$data['data']		= $this->m_edp->assort_edp_1($import_id);
		$data['head']		= $this->m_edp->show_edp_assort_id($import_id);
		$this->load->view('edp/v_cariassortment',$data);
	}
	public function upload()
	{
		$aplikasi 			= 'edp_assort';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$import_id			= addslashes($this->input->post('import_id'));
		$idPo				= addslashes($this->input->post('idPo'));
		
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$query = $this->m_edp->assort_edp_1($import_id);
			foreach($query as $a):

				$data = array(
					'import_id'=>$a->import_id,
					'nama'=>$a->nama,
					'nik'=>$a->nik,
					'nama_pakaian'=>$a->nama_pakaian,
					'lokasi'=>$a->lokasi,
					'jabatan'=>$a->jabatan,
					'keterangan'=>$a->keterangan,
					'info_tambahan'=>'EDP01',
					'qty'=>$a->qty,
					'size'=>$a->size,
					'nama_ukuran_f1'=>$a->nama_ukuran_f1,
					'alias_ukuran_f1'=>$a->alias_ukuran_f1,
					'ukuran_f1'=>$a->ukuran_f1,
					'nama_ukuran_f2'=>$a->nama_ukuran_f2,
					'alias_ukuran_f2'=>$a->alias_ukuran_f2,
					'ukuran_f2'=>$a->ukuran_f2,
					'nama_ukuran_f3'=>$a->nama_ukuran_f3,
					'alias_ukuran_f3'=>$a->alias_ukuran_f3,
					'ukuran_f3'=>$a->ukuran_f3,
					'nama_ukuran_f4'=>$a->nama_ukuran_f4,
					'alias_ukuran_f4'=>$a->alias_ukuran_f4,
					'ukuran_f4'=>$a->ukuran_f4,
					'nama_ukuran_f5'=>$a->nama_ukuran_f5,
					'alias_ukuran_f5'=>$a->alias_ukuran_f5,
					'ukuran_f5'=>$a->ukuran_f5,
					'nama_ukuran_f6'=>$a->nama_ukuran_f6,
					'alias_ukuran_f6'=>$a->alias_ukuran_f6,
					'ukuran_f6'=>$a->ukuran_f6,
					'nama_ukuran_f7'=>$a->nama_ukuran_f7,
					'alias_ukuran_f7'=>$a->alias_ukuran_f7,
					'ukuran_f7'=>$a->ukuran_f7,
					'nama_ukuran_f8'=>$a->nama_ukuran_f8,
					'alias_ukuran_f8'=>$a->alias_ukuran_f8,
					'ukuran_f8'=>$a->ukuran_f8,
					'nama_ukuran_f9'=>$a->nama_ukuran_f9,
					'alias_ukuran_f9'=>$a->alias_ukuran_f9,
					'ukuran_f9'=>$a->ukuran_f9,
					'nama_ukuran_f10'=>$a->nama_ukuran_f10,
					'alias_ukuran_f10'=>$a->alias_ukuran_f10,
					'ukuran_f10'=>$a->ukuran_f10,
					'datetime_uploaded'=>$a->datetime_uploaded,
					'code_ukuran'=>$a->code_ukuran,
					'no_karton'=>$a->no_karton,
					'karton_id'=>$a->karton_id,
					'idPo'=>$idPo
				);
				$this->m_edp->tambah_assortDetail($data);
			endforeach;
			$detail = array(
				'import_id' =>$a->import_id,
				'project_no'=>$a->project_no,
				'lot_number'=>$a->lot_number,
				'filename'=>$a->filename,
				'nama_perusahaan'=>$a->nama_perusahaan,
				'is_data_completed'=>$a->is_data_completed,
				'total_records'=>$a->total_records,
				'import_datetime'=>$a->import_datetime,
				'pc'=>'EDP01',
				'userId'=>$userId,
				'isActive'=>0,
				'idPo'=>$idPo
			);
			$this->m_edp->tambah_assortDetailHead($detail);
			$this->db3->query("update t_import_pengukuran_list set project_no=concat(project_no,'- X') where import_id='$import_id'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
}