<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Triming_stock_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_triming'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'Triming_stock_input';
		$data['title']		= 'Laporan Input Cutting';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_triming->show_triming_stock_input();
		$this->template->load('role','isi','triming/v_triming_stock_penerimaan',$data);
	}
	
	
}