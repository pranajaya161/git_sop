<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Md_maaping_material extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper("url");
		$this->load->model('m_worksheet');
	}
    public function index()
	{
		$aplikasi 		  = 'Maaping_material';	
		$data['title']    = 'Maaping Style';
		$data['idDept']	  = 7;
		$data['aplikasi'] = $aplikasi;
		$data['data']	  = $this->m_worksheet->maaping();
		$this->template->load('role','isi','md/v_maaping_material', $data);
	}
	
}
