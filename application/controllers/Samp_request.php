<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Samp_request extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_prospecting');
		$this->load->model('m_parameter');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'samp_request';
		$data['title']		= 'Design : Sample Request';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_prospecting->show_samp_req();
		$this->template->load('role','isi','sample/v_samp_request',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'samp_request';
		$data['aplikasi'] 	= $aplikasi;
		$kode				= 'SMP';
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'idSample'=>0,
				'idProspecting'=>addslashes($this->input->post('idProspecting')),
				'tgl_target'=>addslashes($this->input->post('tgl_target')),
				'refNo'=>getRefnoSmp($kode, $bulan, $tahun),
				'item'=>addslashes($this->input->post('item')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'userId'=>$userId,
				'isActive'=>addslashes($this->input->post('isActive'))
			);
			$this->m_prospecting->tambah_sample($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Simpan');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus()
	{
		$aplikasi 			= 'samp_request';
		$data['aplikasi'] 	= $aplikasi;
		$kode				= 'SMP';
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$userId				= addslashes($this->session->userdata('userId'));
		$idSample			= addslashes($this->input->post('idSample'));
		$cek = $this->db->query("select count(*) as nom from mkt_pros_sample_sty where idSample='$idSample'")->row();
		if($cek->nom > 0)
		{
			$this->session->set_flashdata('pesan','Error, Data tidak bisa di Hapus');
			redirect($aplikasi,'refresh');
		}
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$this->db->query("delete from mkt_pros_sample where idSample='$idSample'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Hapus');
			redirect($aplikasi,'refresh');
		}
	}
	public function sample_bahan()
	{
		$aplikasi 			= 'samp_request/sample_bahan/'.$this->uri->segment(3);
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idSample			= addslashes($this->uri->segment(3));
		$data['data']		= $this->m_prospecting->show_sample_show($idSample);
		$data['detail']		= $this->m_prospecting->show_sample_bahan($idSample);
		$this->template->load('role','isi','sample/v_smp_request_bahan',$data);
	}
	public function tambah_sample_bahan()
	{
		$aplikasi 			= 'samp_request/sample_bahan/'.addslashes($this->input->post('idSample'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idSample			= addslashes($this->input->post('idSample'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'idSample'=>$idSample,
				'idBahan'=>addslashes($this->input->post('idBahan')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'userId'=>$userId
			);
			$this->m_prospecting->tambah_sample_bahan($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Hapus');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus_bahan()
	{
		$aplikasi 			= 'samp_request/sample_bahan/'.addslashes($this->input->post('idSample'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idSample			= addslashes($this->input->post('idSample'));
		$idBahan			= addslashes($this->input->post('idBahan'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$this->db->query("delete from mkt_pros_sample_bhn where idSample='$idSample' and idBahan='$idBahan'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Hapus');
			redirect($aplikasi,'refresh');
		}
	}
}