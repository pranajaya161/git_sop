<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sewing_keluar_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_sewing');
		$this->load->model('m_log'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'sewing_keluar_apv';
		$data['title']		= 'Approve Keluar Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_sewing->show_sewing_keluar_input_apv();
		$this->template->load('role','isi','sewing/v_sewing_keluar_apv',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'sewing_keluar_apv';
		$data['title']		= 'Approve Keluar Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$cekbox				= $this->input->post('cekbox');

		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$jumlah = count($cekbox);
			for($i=0;$i<$jumlah;$i++)
			{
				$this->db->query("update sewing_out set isActive =1 where id='$cekbox[$i]'");
				$cek = $this->m_barcode->show_agremeent($cekbox[$i]);
				$id = $cek->id;
				$idSewing = $cek->idSewing;
				$line = $this->m_sewing->show_id($cekbox[$i]);
				$this->m_log->tambah($cek->id, $cek->nourut, 'Sewing Out Approve', 'Approve Keluar Sewing', $userId);
				$data = array(
					'idWorksheet'=>$cek->idWorksheet,
					'id'=>$cek->id,
					'nourut'=>$cek->nourut,
					'idLine'=>$line->idLine,
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_sewing->tambah_stock03($data);
				
				$hist = [
					'id'=>$cek->id,
					'nourut'=>$cek->nourut,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>8,
					'kdTrans'=>'APV',
					'keterangan'=>'APV - Keluar Sewing - '.getLine($line->idLine),
					'userId'=>addslashes($this->session->userdata('userId'))
				];
				$this->m_barcode->tambah_hist($hist);
				$this->db->query("update barcode set idDept = 6 where id='$id'");
				//$this->db->query("update sewing_out_sm set isActive = 1 where idSewing ='$idSewing'");
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
		// $aplikasi 			= 'sewing_keluar_apv';
		// $data['aplikasi'] 	= $aplikasi;
		// $userId				= addslashes($this->session->userdata('userId'));
		// $idSewing			= addslashes($this->input->post('idSewing'));
		// $status 			= addslashes($this->input->post('status'));
		// $keterangan 		= addslashes($this->input->post('keterangan'));
		// if(empty($userId))
		// {
		// 	$userId	= 'Guest';
		// }
		// $this->db->trans_begin();
		// 	if($status ==1)
		// 	{
		// 		$this->db->query("update sewing_out_sm set status='Approve', isActive = 1 where idSewing='$idSewing' and isActive=99");
		// 		$xStatus = 'Tambah';	
		// 	} else {
		// 		$this->db->query("update sewing_out_sm set status='Reject', isActive = 0 where idSewing='$idSewing' and isActive=99");
		// 		$xStatus = 'Hapus'; 
		// 	}
		// 	$data = array(
		// 		'idSewing'=>$idSewing,
		// 		'isStatus'=>$status,
		// 		'keterangan'=>$keterangan,
		// 		'userId'=>$userId,
		// 	);
		// 	$this->m_sewing->tambah_sewing_keluar_apv($data);
		// 	$hist = $this->db->query("select * from sewing_out where idSewing='$idSewing'")->result();
		// 	foreach($hist as $h):
		// 		$id = $h->id;
		// 		$this->m_log->tambah($h->id, $h->nourut, 'Sewing Out Approve', $keterangan, $userId);
		// 		$this->db->query("update edp_assortment_pros set idDept = 6 where id='$id'");
		// 	endforeach;	
		// $this->db->trans_complete();
		// if ($this->db->trans_status() === FALSE) {
		// 	$this->db->trans_rollback();
		// 	$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
		// 	redirect($aplikasi,'refresh');
		// } else {
		// 	$this->db->trans_commit();
		// 	$this->session->set_flashdata('pesan','Data berhasil di '.$xStatus);
		// 	redirect($aplikasi,'refresh');
		// }
	}
}