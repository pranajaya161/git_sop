<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lap_mkt_purchaseorder extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_laporan');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'lap_mkt_purchaseorder';
		$data['title']		= 'Laporan : Purchase Order';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_laporan->show_purchaseorder();
		$this->template->load('role','isi','laporan/v_lap_mkt_purchaseorder',$data);
	}
	
}