<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purc_beli_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_purchashing'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'purc_beli_apv';
		$data['title']		= 'Approve Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_purchashing->show_purchasing_apv();
		$this->template->load('role','isi','purchasing/v_purc_beli_request_apv',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'purc_beli_apv';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPurcPembelian	= addslashes($this->input->post('idPurcPembelian'));
		$status 			= addslashes($this->input->post('status'));
		$keterangan 		= addslashes($this->input->post('keterangan'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			if($status ==1)
			{
				$this->db->query("update purc_pembelian_sm set status='Approve', isActive = 1 where idPurcPembelian='$idPurcPembelian'");
				$xStatus = 'Tambah';	
				$data = $this->db->query("select a.*, b.idSupplier from purc_pembelian_dt a inner join purc_pembelian_sm b on b.idPurcPembelian = a.idPurcPembelian  where a.idPurcPembelian='$idPurcPembelian'")->result();
				foreach($data as $a):
					$idSupplier = $a->idSupplier;
					$idBahan = $a->idBahan;
					$qty = $a->qty;
					$cek = $this->db->query("select * from purc_pembelian_supp where idSupplier ='$idSupplier' and idBahan ='$idBahan'")->row();
					if($cek)
					{
						$this->db->query("update purc_pembelian_supp set qty = (qty + $qty) where idSupplier ='$idSupplier' and idBahan ='$idBahan'");
					} else {
						$detail = array(
							'idSupplier'=>$a->idSupplier,
							'idBahan'=>$a->idBahan,
							'qty'=>$qty,
							'satuan'=>$a->satuan,
							'qty_input'=>0,
							'userId'=>$userId
						);
						$this->db->insert('purc_pembelian_supp', $detail);
					}
				endforeach;
			} else {
				$this->db->query("update purc_pembelian_sm set status='Reject', isActive = 0 where idPurcPembelian='$idPurcPembelian'");
				$xStatus = 'Hapus'; 
			}
			$data = array(
				'idPurcPembelian'=>$idPurcPembelian,
				'isStatus'=>$status,
				'keterangan'=>$keterangan,
				'userId'=>$userId,
			);
			$this->m_purchashing->tambah_approve_beli($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di '.$xStatus);
			redirect($aplikasi,'refresh');
		}
	}
}