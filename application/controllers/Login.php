<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url', 'form'));
        $this->load->library(array('form_validation', 'Recaptcha'));
		$this->load->model('m_login');
		if($this->session->userdata('isLog')==TRUE){
            redirect('home','refresh');
        }
		
    }
    public function index()
    {
      $this->load->view('v_login');
      
	}
	public function auth()
	{
		$this->form_validation->set_rules('userId','userId','trim|required|xss_clean|max_length[128]');
		$this->form_validation->set_rules('password','Password','required|xss_clean|min_length[3]');
			
		if($this->form_validation->run() == FALSE) 
		{
			$this->index();
		} 
		else
		{
		   if($this->m_login->cek()->num_rows()==1)
		   {
			 $secure=$this->m_login->cek()->row();
			 if(hash_verified($this->input->post('password'),$secure->password))
			 {
				$ip = $this->input->ip_address();
				$ipServer = '101.255.53.98';
				
					$sessionArray = array(                   
					  'isLog'=>TRUE,
					  'idUser' => $secure->idUser,
					  'userId' => $secure->userId,
					  'idModul'=> $secure->idModul,
					  'idDept'=> $secure->idDept,
					  'idLine'=>$secure->idLine

					  );
					
					$this->session->set_userdata($sessionArray);
					redirect('home','refresh');
				
			 } 
			 else 
			 {
				  $this->session->set_flashdata('pesan','Password invalid');
				  redirect('login','refresh');
		     }
		  
		  }
		  else
		  {
			   $this->session->set_flashdata('pesan','Email tidak terdaftar');
			   redirect('login','refresh');
		  }
		}
	}
	
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
