<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qc_input_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_qc'); 
		$this->load->model('m_log'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'qc_input_apv';
		$data['title']		= 'Approve Input QC';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_qc->show_qc_input_apv();
		$this->template->load('role','isi','qc/v_qc_apv',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'qc_input_apv';
		$data['title']		= 'Approve Input QC';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$cekbox				= $this->input->post('cekbox');

		if(empty($userId))
		{
			$userId = 'System';
		}
		// $this->db->trans_begin();
		// 	$jumlah = count($cekbox);
		// 	for($i=0;$i<$jumlah;$i++)
		// 	{
		// 		$this->db->query("update qc_in set isActive =1 where id='$cekbox[$i]'");
		// 		$h = $this->db->query("select * from qc_in where id='$cekbox[$i]'")->row();
		// 		$this->m_log->tambah($h->id, $h->nourut, 'Qc In Approve', 'Approve Input Qc', $userId);
		// 		$data = array(
		// 			'idWorksheet'=>$h->idWorksheet,
		// 			'id'=>$h->id,
		// 			'nourut'=>$h->nourut,
		// 			'userId'=>$userId,
		// 			'isActive'=>0
		// 		);
		// 		$this->m_qc->tambah_stock02($data);
		// 	}
		// $this->db->trans_complete();
		$this->db->trans_begin();
			$jumlah = count($cekbox);
			for($i=0;$i<$jumlah;$i++)
			{
				$this->db->query("update qc_in set isActive =1 where id='$cekbox[$i]'");
				$cek = $this->m_barcode->show_agremeent($cekbox[$i]);
				$line = $this->m_qc->show_id($cekbox[$i]);
				$this->m_log->tambah($cek->id, $cek->nourut, 'Qc In Approve', 'Qc Input Triming', $userId);
				$data = array(
					'idWorksheet'=>$cek->idWorksheet,
					'id'=>$cek->id,
					'nourut'=>$cek->nourut,
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_qc->tambah_stock02($data);
				$hist = [
					'id'=>$cek->id,
					'nourut'=>$cek->nourut,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>10,
					'kdTrans'=>'APV',
					'keterangan'=>'APV - Input Qc',
					'userId'=>addslashes($this->session->userdata('userId'))
				];
				$this->m_barcode->tambah_hist($hist);
				
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
		
}