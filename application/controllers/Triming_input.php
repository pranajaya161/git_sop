<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Triming_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_triming'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'triming_input';
		$data['title']		= 'Input Triming';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_triming->show_triming_input();
		$this->template->load('role','isi','triming/v_triming_input',$data);
	}
	public function hapus()
	{
		$aplikasi 			= 'triming_input/detail/'.$this->uri->segment(5);
		$data['title']		= 'Input Sewing Detail';
		$data['aplikasi'] 	= $aplikasi;
		$idWorksheet		= addslashes($this->uri->segment(3));
		$id  				= addslashes($this->uri->segment(4));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$cek = $this->m_barcode->show_id($id);
			$hist = [
				'id'=>$cek->id,
				'nourut'=>$cek->nourut,
				'tanggal'=>date('Y-m-d'),
				'idDept'=>8,
				'kdTrans'=>'DEL',
				'keterangan'=>'DEL - Input Triming',
				'userId'=>$userId
			];
			$this->m_barcode->tambah_hist($hist);
			$this->db->query("delete from triming_in where id='$id'");
			$this->db->query("update triming_stock01 set isActive=0 where id='$id'");

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Hapus');
			redirect($aplikasi,'refresh');
		}
	}
	public function detail()
	{
		$aplikasi 			= 'triming_input/detail/'.$this->uri->segment(3);
		$data['title']		= 'Input Triming Detail';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_triming->show_triming_input_id($this->uri->segment(3));
		foreach($data['data'] as $a):
		 	$idWorksheet = $a->idWorksheet;
		 	$idPpicTriming = $a->idPpicTriming;
		endforeach;
		$data['detail']		= $this->m_triming->show_triming_input_detail($idPpicTriming, $idWorksheet);
		$this->template->load('role','isi','triming/v_triming_input_detail',$data);
	}
	public function tambah_det()
	{
		$aplikasi 			= 'triming_input/detail/'.addslashes($this->input->post('idPpicTriming'));
		$data['title']		= 'Input Sewing Detail';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$numbering			= addslashes($this->input->post('numbering'));
		$idPpicTriming		= addslashes($this->input->post('idPpicTriming'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$lihat = $this->m_triming->cek_input_triming_add($idPpicTriming, $idWorksheet, $numbering);
			if($lihat)
			{
				$id = $lihat->id;
				$data = array(
					'idPpicTriming'=>$idPpicTriming,
					'idWorksheet'=>$idWorksheet,
					'id'=>$lihat->id,
					'nourut'=>$numbering,
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_triming->tambah_triming_input($data);

				$hist = [
					'id'=>$lihat->id,
					'nourut'=>$numbering,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>9,
					'kdTrans'=>'IN',
					'keterangan'=>'Input Triming',
					'userId'=>$userId
				];
				$this->m_barcode->tambah_hist($hist);
				$this->db->query("update triming_stock01 set isActive=99 where isActive=0 and id='$id'");
				
			} else {
				$this->db->trans_complete();
				$this->db->trans_rollback();
				$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
				redirect($aplikasi,'refresh');
			}	
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function kirim()
	{
		$aplikasi 			= 'triming_input';
		$data['title']		= 'Input Triming';
		$data['aplikasi'] 	= $aplikasi;
		$idPpicTriming	    = addslashes($this->input->post('idPpicTriming'));
		$this->db->trans_begin();
			$cek  = $this->m_triming->cek_input_triming_kirim($idPpicTriming)->result();
			foreach($cek as $a):
				$id = $a->id;
				$this->db->query("update triming_stock01 set isActive=1 where isActive=99 and id='$id'");
				$hist = [
					'id'=>$a->id,
					'nourut'=>$a->nourut,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>9,
					'kdTrans'=>'SEND',
					'keterangan'=>'SEND - Input Triming',
					'userId'=>addslashes($this->session->userdata('userId'))
				];
				$this->m_barcode->tambah_hist($hist);
				$this->db->query("update triming_in set isActive = 99 where id='$id'");
			endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
}