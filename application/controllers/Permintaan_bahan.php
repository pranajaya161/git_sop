<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permintaan_bahan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_permintaan'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$idDept 			= $this->session->userdata('idDept');
		$aplikasi 			= 'permintaan_bahan';
		$data['title']		= 'Permintaan Bahan Gudang ';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_permintaan->show_permintaan($idDept);
		$data['idDept']		= $idDept;
		$this->template->load('role','isi','permintaan/v_permintaan_bahan',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'permintaan_bahan';
		$data['title']		= 'Permintaan Bahan ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idDept 			= $this->session->userdata('idDept');
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$keterangan  		= addslashes($this->input->post('keterangan'));
		$nom 	= $this->db->query("select count(*) as nom from permintaan_bahan where year(tanggal) ='$tahun'")->row();
		$refNo 				= ($nom->nom + 1).'/REQ-GD/'.$bulan.'/'.$tahun;
		$this->db->trans_begin();
			$data = array(
				'idPermintaanBahan'=>0,
				'refNo'=>$refNo,
				'idDept'=>$idDept,
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'userId'=>$userId,
				'isActive'=>0,
				'tgl_terima'=>null,
				'userTerima'=>''
			);
			$this->m_permintaan->tambah($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function detail()
	{
		$aplikasi 			= 'permintaan_bahan/detail/'.$this->uri->segment(3);
		$idDept				= 4;
		$data['title']		= 'Detail Permintaan Bahan';
		$data['idPermintaanBahan'] = $this->uri->segment(3);
		$this->template->load('role','isi','permintaan/v_permintaan_bahan_detail',$data);
	}
	public function tambah_det()
	{
		$aplikasi 			= 'permintaan_bahan/detail/'.addslashes($this->input->post('idPermintaanBahan'));
		$data['title']		= 'Peermintaan Bahan ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$this->db->trans_begin();
			$data = array(
				'idPermintaanBahan'=>addslashes($this->input->post('idPermintaanBahan')),
				'idBahan'=>addslashes($this->input->post('idBahan')),
				'qty'=>addslashes($this->input->post('qty')),
				'isActive'=>0
			);
			$this->m_permintaan->tambah_det($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus()
	{
		$aplikasi 			= 'permintaan_bahan/detail/'.addslashes($this->uri->segment(3));
		$data['title']		= 'Permintaan Bahan Gudang';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPermintaanBahan 	= addslashes($this->uri->segment(3));
		$idBahan            = addslashes($this->uri->segment(4));
		$this->db->trans_begin();
			$this->db->query("delete from permintaan_bahan_dt where idBahan ='$idBahan' and idPermintaanBahan='$idPermintaanBahan'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function send()
	{
		$aplikasi 			= 'permintaan_bahan';
		$data['title']		= 'Permintaan Bahan ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPermintaanBahan	= addslashes($this->input->post('idPermintaanBahan'));
		$this->db->trans_begin();
			$this->db->query("update permintaan_bahan set isActive=99 where idPermintaanBahan='$idPermintaanBahan'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Kirim');
			redirect($aplikasi,'refresh');
		}
	}
	
}