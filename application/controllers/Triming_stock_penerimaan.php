<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Triming_stock_penerimaan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_triming'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'triming_stock_penerimaan';
		$data['title']		= 'Penerimaan Stock Cutting';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_triming->show_triming_stock_penerimaan();
		$this->template->load('role','isi','triming/v_triming_stock_penerimaan',$data);
	}
	
	
}