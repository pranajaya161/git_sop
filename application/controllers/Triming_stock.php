<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Triming_stock extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_triming'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'triming_stock';
		$data['title']		= 'Stock Input Triming';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_triming->show_triming_input_stock();
		$this->template->load('role','isi','triming/v_triming_stock',$data);
	}
		
}