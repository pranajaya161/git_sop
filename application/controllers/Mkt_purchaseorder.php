<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkt_purchaseorder extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_marketing');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'mkt_purchaseorder';
		$data['title']		= 'Marketing : Purchase Order';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_marketing->show_purchaseOrder();
		$this->template->load('role','isi','purchaseorder/v_purchaseorder',$data);
	}
	public function add()
	{
		$aplikasi 			= 'mkt_purchaseorder';
		$data['aplikasi']	= $aplikasi;
		$data['title']		= 'Marketing : Input Purchase Order';
		$data['aplikasi'] 	= $aplikasi;
		$this->template->load('role','isi','purchaseorder/v_purchaseorder_add', $data);
	}
	public function cari_prospecting()
	{
		$data['title']	= 'Data Prospecting';
		$data['data']	= $this->m_marketing->show_prospecting();
		$this->load->view('prospecting/v_prospecting_cari', $data);
	}
	public function tambah()
	{
		$aplikasi 			= 'mkt_purchaseorder';
		$data['aplikasi'] 	= $aplikasi;
		$kode				= 'MKT-PO';
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$status 			= 'MKT-PO';
		$clientInit			= getClient_init(addslashes($this->input->post('idClient')));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'idPo'=>addslashes($this->input->post('idPo')),
				'idProspecting'=>addslashes($this->input->post('idProspecting')),
				'refNo'=>getRefnoPo($kode, $clientInit, $bulan, $tahun),
				'noPo'=>addslashes($this->input->post('noPo')),
				'noOr'=>addslashes($this->input->post('noOr')),
				'jenis'=>addslashes($this->input->post('jenis')),
				'idClient'=>addslashes($this->input->post('idClient')),
				'tanggal'=>addslashes($this->input->post('tanggal')),
				'tglKirim'=>addslashes($this->input->post('tglKirim')),
				'qty'=>addslashes($this->input->post('qty')),
				'amount'=>addslashes($this->input->post('amount')),
				'satuan'=>addslashes($this->input->post('satuan')),
				'pcs'=>addslashes($this->input->post('pcs')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>$status,
				'idDept'=>1,
				'userId'=>$userId,
				'isActive'=>0
			);
			$this->m_marketing->tambah_purchaseorder($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$aplikasi 			= 'mkt_purchaseorder';
		$data['aplikasi']	= $aplikasi;
		$data['title']		= 'Marketing : Edit Purchase Order';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_marketing->show_purchaseOrder_id($this->uri->segment(3));
		$this->template->load('role','isi','purchaseorder/v_purchaseorder_add', $data);
	}
	public function update()
	{
		$aplikasi 			= 'mkt_purchaseorder';
		$data['aplikasi'] 	= $aplikasi;
		$kode				= 'MKT-PO';
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$status 			= 'MKT-PO';
		$clientInit			= getClient_init(addslashes($this->input->post('idClient')));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'noPo'=>addslashes($this->input->post('noPo')),
				'noOr'=>addslashes($this->input->post('noOr')),
				'jenis'=>addslashes($this->input->post('jenis')),
				'idClient'=>addslashes($this->input->post('idClient')),
				'tanggal'=>addslashes($this->input->post('tanggal')),
				'tglKirim'=>addslashes($this->input->post('tglKirim')),
				'qty'=>addslashes($this->input->post('qty')),
				'amount'=>addslashes($this->input->post('amount')),
				'satuan'=>addslashes($this->input->post('satuan')),
				'pcs'=>addslashes($this->input->post('pcs')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>$status,
				'userId'=>$userId,
			);
			$this->m_marketing->update_purchaseorder(addslashes($this->input->post('idPo')), $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function add_detail()
	{
		$aplikasi 			= 'mkt_purchaseorder';
		$data['aplikasi']	= $aplikasi;
		$data['title']		= 'Marketing : Detail Purchase Order';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_marketing->show_purchaseOrder_id($this->uri->segment(3));
		$this->template->load('role','isi','purchaseorder/v_purchaseorder_style', $data);
	}
	public function tambah_det()
	{
		$aplikasi 			= 'mkt_purchaseorder/add_detail/'.addslashes($this->input->post('idPo'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'idPo'=>addslashes($this->input->post('idPo')),
				'idStyle'=>addslashes($this->input->post('idStyle')),
				'qty'=>addslashes($this->input->post('qty')),
				'size'=>addslashes($this->input->post('size'))
			);
			$this->m_marketing->tambah_purchaseorder_style($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus_style()
	{
		$aplikasi 			= 'mkt_purchaseorder/add_detail/'.addslashes($this->input->post('idPo'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPo     			= addslashes($this->input->post('idPo'));
		$idStyle     		= addslashes($this->input->post('idStyle'));
		$size     			= addslashes($this->input->post('size'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$this->db->query("delete from mkt_po_sty where idPo ='$idPo' and idStyle='$idStyle' and size='$size'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Hapus');
			redirect($aplikasi,'refresh');
		}
	}
}