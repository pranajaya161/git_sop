<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desg_request extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_prospecting');
		$this->load->model('m_parameter');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'desg_request';
		$data['title']		= 'Design : Upload Design';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_prospecting->show_design_req();
		$this->template->load('role','isi','design/v_desn_request',$data);
	}
	public function kirim()
	{
		$aplikasi 			= 'desg_request';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idProspecting		= addslashes($this->input->post('idProspecting'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$this->db->query("update mkt_pros_desgn set isActive=99 where idProspecting='$idProspecting'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Kirim');
			redirect($aplikasi,'refresh');
		}
	}
	public function upload()
	{
		$aplikasi 			= 'desg_request/upload/'.$this->uri->segment(3);
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idProspecting		= addslashes($this->uri->segment(3));
		$data['data']		= $this->m_prospecting->show_admin_head($idProspecting);
		$data['detail']		= $this->m_prospecting->show_design_req_det($idProspecting);
		$this->template->load('role','isi','design/v_desn_request_add',$data);
	}
	public function cari()
	{
		$data['title']	= 'Data Style';
		$id				= 'Design';  
		$data['data']	= $this->m_parameter->show_style($id);
		$this->load->view('parameter/v_design_cari', $data);
	} 
	public function cari_sample()
	{
		$data['title']	= 'Data Style';
		$id				= 'Sample';  
		$data['data']	= $this->m_parameter->show_style($id);
		$this->load->view('parameter/v_design_cari', $data);
	}
	public function upload_det()
	{
		$aplikasi 			= 'desg_request/upload/'.addslashes($this->input->post('idProspecting'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$idProspecting		= addslashes($this->input->post('idProspecting'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$cekId 			= $this->m_prospecting->show_nourut_design($this->input->post('idProspecting'))->row();
			$nourut			= $cekId->nom + 1;
			$data	= array(
				'idProspecting'=>$this->input->post('idProspecting'),
				'nourut'=>$nourut,
				'tanggal'=>date('Y-m-d H:i:s'),
				'idStyle'=>$this->input->post('idStyle'),
				'keterangan'=>$this->input->post('keterangan'),
				'userId'=>$userId
			);
			$this->m_prospecting->tambah_upload_design($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus_upload()
	{
		$aplikasi 			= 'desg_request/upload/'.addslashes($this->input->post('idProspecting'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$idProspecting		= addslashes($this->input->post('idProspecting'));
		$idStyle		= addslashes($this->input->post('idStyle'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$this->db->query("delete from mkt_pros_desgn where idStyle='$idStyle' and idProspecting='$idProspecting'");
			$this->db->query("delete from mkt_pros_desgn_bhn where idStyle='$idStyle' and idProspecting='$idProspecting'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Data Error Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Hapus');
			redirect($aplikasi,'refresh');
		}
	}
	public function design_bahan()
	{
		$aplikasi 			= 'desg_request/bahan/'.$this->uri->segment(3);
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idProspecting		= addslashes($this->uri->segment(3));
		$idStyle			= addslashes($this->uri->segment(4));
		$data['data']		= $this->m_prospecting->show_admin_head($idProspecting);
		$data['detail']		= $this->m_prospecting->show_design_req_bahan($idProspecting, $idStyle);
		$this->template->load('role','isi','design/v_desn_request_bahan',$data);
	}
	public function cari_bahan()
	{
		$data['title']	= 'Data Style';
		$id				= 'Design';  
		$data['data']	= $this->m_parameter->show();
		$this->load->view('parameter/v_bahan_cari', $data);
	}
	public function bahan_design()
	{
		$aplikasi 			= 'desg_request/design_bahan/'.addslashes($this->input->post('idProspecting')).'/'.addslashes($this->input->post('idStyle'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idProspecting		= addslashes($this->input->post('idProspecting'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$data=array(
				'idProspecting'=>$this->input->post('idProspecting'),
				'idStyle'=>$this->input->post('idStyle'),
				'idBahan'=>$this->input->post('idBahan'),
				'keterangan'=>$this->input->post('keterangan')
			);
			$this->m_prospecting->tambah_design_bahan($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus_bahan()
	{
		$aplikasi 			= 'desg_request/design_bahan/'.addslashes($this->input->post('idProspecting')).'/'.addslashes($this->input->post('idStyle'));
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idProspecting		= addslashes($this->input->post('idProspecting'));
		$idBahan 			= addslashes($this->input->post('idBahan'));
		$idStyle 			= addslashes($this->input->post('idStyle'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$this->db->query("delete from mkt_pros_desgn_bhn where idProspecting='$idProspecting' and idBahan='$idBahan' and idStyle='$idStyle'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Tambah');
			redirect($aplikasi,'refresh');
		}
	}

}