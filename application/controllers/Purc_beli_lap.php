<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purc_beli_lap extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_purchashing');
	}
	public function index()
	{
		$aplikasi 			= 'Purc_beli_lap';
		$data['title']		= 'Laporan Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_purchashing->show_purchasing_lap();
		$this->template->load('role','isi','purchasing/v_purc_beli_request_lap',$data);
	}
	

}