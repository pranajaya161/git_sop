<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppic_sewing extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_ppic'); 
		$this->load->model('m_log'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'ppic_sewing';
		$data['title']		= 'PPIC Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_ppic->show_ppic_sewing();
		$this->template->load('role','isi','ppic/v_ppic_sewing',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'ppic_sewing';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$keterangan			= addslashes($this->input->post('keterangan'));
		$idLine				= addslashes($this->input->post('idLine'));
		$idCutting			= addslashes($this->input->post('idCutting'));
		$refNo				= addslashes($this->input->post('refNo'));
		$startDate			= addslashes($this->input->post('startDate'));
		$endDate			= addslashes($this->input->post('endDate'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'idPpicSewing'=>0,
				'idCutting'=>$idCutting,
				'idWorksheet'=>$idWorksheet,
				'startDate'=>$startDate,
				'endDate'=>$endDate,
				'keterangan'=>$keterangan,
				'idLine'=>$idLine,
				'refNo'=>$refNo,
				'userId'=>$userId
			);	
			$this->m_ppic->tambah_sewing($data);
			$cek = $this->m_ppic->show_cutting($idCutting);
			foreach($cek as $h):
				$hist = [
					'id'=>$h->id,
					'nourut'=>$h->nourut,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>6,
					'kdTrans'=>'APV',
					'keterangan'=>'APV - PPIC Sewing ['.$startDate.' - '.$endDate.'] - '.getLine($idLine),
					'userId'=>$userId
				];
				$this->m_barcode->tambah_hist($hist);
				$id = $h->id;
				$this->m_log->tambah($h->id, $h->nourut, 'PPIC Sewing Approve', $keterangan, $userId);
				$this->db->query("update edp_assortment_pros set idDept ='8' where id ='$id'");
				$this->db->query("update barcode set idDept = 8 where id ='$id'");
			endforeach;
			$this->db->query("update cutting_out_sm set isActive =99 where idCutting='$idCutting' and isActive=1");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di ');
			redirect($aplikasi,'refresh');
		}
	}
	
}