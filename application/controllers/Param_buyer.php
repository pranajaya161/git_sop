<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Param_buyer extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_parameter');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'param_buyer';
		$data['title']		= 'Parameter --> Buyer';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_parameter->show_buyer();
		$this->template->load('role','isi','parameter/v_param_buyer',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'param_buyer';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'idClient'=>0,
				'stClient'=>addslashes($this->input->post('stClient')),
				'initClient'=>addslashes($this->input->post('initClient')),
				'namaClient'=>addslashes($this->input->post('namaClient')),
				'alamat'=>addslashes($this->input->post('alamat')),
				'kodepos'=>addslashes($this->input->post('kodepos')),
				'phone'=>addslashes($this->input->post('phone')),
				'mobilePhone'=>addslashes($this->input->post('mobilePhone')),
				'pic'=>addslashes($this->input->post('pic')),
				'jabatan'=>addslashes($this->input->post('jabatan')),
				'email'=>addslashes($this->input->post('email')),
				'isActive'=>1,
				'userId'=>$userId
			);
			$this->m_parameter->add_buyer($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "Error, Silahkan di coba kembali");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$aplikasi 			= 'param_buyer';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idClient 			= addslashes($this->input->post('idClient'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'stClient'=>addslashes($this->input->post('stClient')),
				'initClient'=>addslashes($this->input->post('initClient')),
				'namaClient'=>addslashes($this->input->post('namaClient')),
				'alamat'=>addslashes($this->input->post('alamat')),
				'kodepos'=>addslashes($this->input->post('kodepos')),
				'phone'=>addslashes($this->input->post('phone')),
				'mobilePhone'=>addslashes($this->input->post('mobilePhone')),
				'pic'=>addslashes($this->input->post('pic')),
				'jabatan'=>addslashes($this->input->post('jabatan')),
				'email'=>addslashes($this->input->post('email')),
				'isActive'=>addslashes($this->input->post('isActive')),
				'userId'=>$userId
			);
			$this->m_parameter->edit_buyer($idClient, $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Ubah');
			redirect($aplikasi,'refresh');
		}	
	}
	
}