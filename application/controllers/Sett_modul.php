<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sett_modul extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper("url");
		$this->load->model('m_menu');
		$this->load->model('m_modul');
		$this->load->library("pagination");
		if($this->session->userdata('isLog')==FALSE){
         	   redirect('login','refresh');
       		 }
	}
    public function index()
	{
		$aplikasi 			= 'Sett_modul';
		$data['title']		= 'Setting : Modul';
		$data['aplikasi'] 	= $aplikasi;
		$data['data'] =  $this->m_modul->show();
		$this->template->load('role','isi','setting/v_modul',$data);
	}
	public function tambah()
	{
		$data['aplikasi']= 'Sett_modul';
		$aplikasi 	= 'Sett_modul';
		$userId		= $this->session->userdata('userId');
		$tgl_proses	= date('Y-m-d H:i:s');
		$this->db->trans_begin();
		$data=array(
			'idModul'=>$this->input->post('idModul'),
			'namaModul'=>$this->input->post('namaModul'),
			'isActive'=>$this->input->post('isActive')
		);
		$this->m_modul->add($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$data['aplikasi']= 'Sett_modul';
		$aplikasi 	= 'Sett_modul';
		$userId		= $this->session->userdata('userId');
		$tgl_proses	= date('Y-m-d H:i:s');
		$this->db->trans_begin();
		$data=array(
			'namaModul'=>$this->input->post('namaModul'),
			'isActive'=>$this->input->post('isActive')
		);
		$this->m_modul->update($data, $this->input->post('idModul'));
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function detail()
	{
		$data['aplikasi']= 'Sett_modul';
		$aplikasi 	= 'Sett_modul';
		$userId		= $this->session->userdata('userId');
		$tgl_proses	= date('Y-m-d H:i:s');
		$idModul	= $this->input->post('idModul');
		$cek		= $this->input->post('idMenu');
		$jumlah		= count($cek);
		$this->db->trans_begin();
		$this->m_modul->hapusList($this->input->post('idModul'));
		for($i=0;$i<$jumlah;$i++)
		{
			$lihat = $this->m_menu->showMenu($cek[$i])->row();
			$data=array(
			'idModul'=>$this->input->post('idModul'),
			'idMenu'=>$cek[$i],
			'headMenu'=>$lihat->headMenu,
			'subMenu'=>$lihat->subMenu,
			'childMenu'=>$lihat->childMenu,
			'nourut'=>$lihat->nourut,
			'tgl_proses'=>$tgl_proses,
			'userId'=>$userId
			);
			$this->m_modul->add_list($data);
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
}

