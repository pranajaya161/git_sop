<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sewing_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_sewing'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'sewing_input';
		$data['title']		= 'Input Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_sewing->show_sewing_input();
		$this->template->load('role','isi','sewing/v_sewing_input',$data);
	}
	public function hapus()
	{
		$aplikasi 			= 'sewing_input/detail/'.$this->uri->segment(5);
		$data['title']		= 'Input Sewing Detail';
		$data['aplikasi'] 	= $aplikasi;
		$idWorksheet		= addslashes($this->uri->segment(3));
		$id  				= addslashes($this->uri->segment(4));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$cek = $this->m_barcode->show_id($id);
			$hist = [
				'id'=>$cek->id,
				'nourut'=>$cek->nourut,
				'tanggal'=>date('Y-m-d'),
				'idDept'=>8,
				'kdTrans'=>'DEL',
				'keterangan'=>'DEL - Input Sewing',
				'userId'=>$userId
			];
			$this->m_barcode->tambah_hist($hist);
			$this->db->query("delete from sewing_in where id='$id'");
			$this->db->query("update sewing_stock01 set isActive=0 where id='$id'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Hapus');
			redirect($aplikasi,'refresh');
		}
	}
	public function detail()
	{
		$aplikasi 			= 'sewing_input/detail/'.$this->uri->segment(3);
		$data['title']		= 'Input Sewing Detail';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_sewing->show_sewing_input_id($this->uri->segment(3));
		foreach($data['data'] as $a):
			$idWorksheet = $a->idWorksheet;
			$idPpicSewing = $a->idPpicSewing;
		endforeach;
		$data['detail']		= $this->m_sewing->show_sewing_input_detail($idPpicSewing, $idWorksheet);
		$this->template->load('role','isi','sewing/v_sewing_input_detail',$data);
	}
	public function tambah_det()
	{
		$aplikasi 			= 'sewing_input/detail/'.addslashes($this->input->post('idPpicSewing'));
		$data['title']		= 'Input Sewing Detail';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$numbering			= addslashes($this->input->post('numbering'));
		$idPpicSewing	    = addslashes($this->input->post('idPpicSewing'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$lihat = $this->m_sewing->cek_input_sewing_add($idPpicSewing, $idWorksheet, $numbering);
			if($lihat)
			{
				$id = $lihat->id;
				$data = array(
					'idPpicSewing'=>$idPpicSewing,
					'idWorksheet'=>$idWorksheet,
					'id'=>$lihat->id,
					'nourut'=>$numbering,
					'idLine'=>$lihat->idLine,
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_sewing->tambah_sewing_input($data);

				$hist = [
					'id'=>$lihat->id,
					'nourut'=>$numbering,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>8,
					'kdTrans'=>'IN',
					'keterangan'=>'Input Sewing - '.getLine($lihat->idLine),
					'userId'=>$userId
				];
				$this->m_barcode->tambah_hist($hist);


				$this->db->query("update sewing_stock01 set isActive=99 where isActive=0 and id='$id'");
			} else {
				$this->db->trans_complete();
				$this->db->trans_rollback();
				$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
				redirect($aplikasi,'refresh');
			}	
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function kirim()
	{
		$aplikasi 			= 'sewing_input';
		$data['title']		= 'Input Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$idPpicSewing	    = addslashes($this->input->post('idPpicSewing'));
		$this->db->trans_begin();
			$cek  = $this->m_sewing->cek_input_sewing_kirim($idPpicSewing)->result();
			foreach($cek as $a):
				$id = $a->id;
				$this->db->query("update sewing_stock01 set isActive=1 where isActive=99 and id='$id'");
				$hist = [
					'id'=>$a->id,
					'nourut'=>$a->nourut,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>8,
					'kdTrans'=>'SEND',
					'keterangan'=>'SEND - Input Sewing - '.getLine($a->idLine),
					'userId'=>addslashes($this->session->userdata('userId'))
				];
				$this->m_barcode->tambah_hist($hist);
				$this->db->query("update sewing_in set isActive = 99 where id='$id'");
			endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
}