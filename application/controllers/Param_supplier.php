<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Param_supplier extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_parameter'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'param_supplier';
		$data['title']		= 'Parameter Supplier';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_parameter->show_supplier();
		$this->template->load('role','isi','parameter/v_param_supplier',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'param_supplier';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId = 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'idSupplier'=>0,
				'initSupplier'=>addslashes($this->input->post('initSupplier')),
				'namaSupplier'=>addslashes($this->input->post('namaSupplier')),
				'alamat'=>addslashes($this->input->post('alamat')),
				'telp'=>addslashes($this->input->post('telp')),
				'OD'=>addslashes($this->input->post('OD')),
				'isActive'=>addslashes($this->input->post('isActive')),
				'userId'=>$userId
			);
			$this->m_parameter->tambah_supplier($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$aplikasi 			= 'param_supplier';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId = 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'initSupplier'=>addslashes($this->input->post('initSupplier')),
				'namaSupplier'=>addslashes($this->input->post('namaSupplier')),
				'alamat'=>addslashes($this->input->post('alamat')),
				'telp'=>addslashes($this->input->post('telp')),
				'isActive'=>addslashes($this->input->post('isActive')),
				'OD'=>addslashes($this->input->post('OD')),
				'userId'=>$userId,
				'tgl_proses'=>date('Y-m-d H:i:s')
			);
			$this->m_parameter->edit_supplier(addslashes($this->input->post('idSupplier')), $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
}