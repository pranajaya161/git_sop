<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Md_apvmaterial extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_md');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'md_apvmaterial';
		$data['title']		= 'MD Approve Materialist';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_md->show_materialist_apv();
		$this->template->load('role','isi','md/v_md_inputmaterial_apv',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'md_apvmaterial';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idMaterialist		= addslashes($this->input->post('idMaterialist'));
		$status 			= addslashes($this->input->post('status'));
		$keterangan 		= addslashes($this->input->post('keterangan'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			if($status ==1)
			{
				$this->db->query("update md_materialist set status='Approve', isActive = 1 where idMaterialist='$idMaterialist'");
				$xStatus = 'Tambah';	
			} else {
				$this->db->query("update md_materialist set status='Reject', isActive = 0 where idMaterialist='$idMaterialist'");
				$xStatus = 'Hapus'; 
			}
			$data = array(
				'idMaterialist'=>$idMaterialist,
				'isStatus'=>$status,
				'keterangan'=>$keterangan,
				'userId'=>$userId,
			);
			$this->m_md->tambah_approve($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di '.$xStatus);
			redirect($aplikasi,'refresh');
		}
	}
}