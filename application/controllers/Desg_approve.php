<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desg_approve extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_prospecting');
		$this->load->model('m_parameter');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'desg_approve';
		$data['title']		= 'Design : Approve Design';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_prospecting->show_design_approve();
		$this->template->load('role','isi','design/v_desn_approve',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'desg_approve';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idProspecting		= addslashes($this->input->post('idProspecting'));
		$status 			= addslashes($this->input->post('status'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			if($status=='Approve')
			{
				$xStatus = 1;
			} else 
			{
				$xStatus = 0;
			}
			$this->db->query("update mkt_pros_desgn set isActive = '$xStatus' where idProspecting='$idProspecting'");
			$this->db->query("update mkt_pros_sm set isDesign = '$xStatus', keterangan='Reject' where idProspecting='$idProspecting'");
			$data = array(
				'idProspecting'=>$idProspecting,
				'status'=>$xStatus,
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'userId'=>$userId,
			);
			$this->m_prospecting->tambah_design_approve($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Tambah');
			redirect($aplikasi,'refresh');
		}
	}
}