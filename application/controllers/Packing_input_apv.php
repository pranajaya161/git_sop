<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packing_input_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_packing'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'packing_input_apv';
		$data['title']		= 'Approve Input Packing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_packing->show_packing_input_apv();
		$this->template->load('role','isi','packing/v_packing_apv',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'packing_input_apv';
		$data['title']		= 'Approve Input Packing';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$cekbox				= $this->input->post('cekbox');

		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$jumlah = count($cekbox);
			for($i=0;$i<$jumlah;$i++)
			{
				$this->db->query("update packing_in set isActive =1 where id='$cekbox[$i]'");
				$cek = $this->m_barcode->show_agremeent($cekbox[$i]);
				$line = $this->m_packing->show_id($cekbox[$i]);
				$this->m_log->tambah($cek->id, $cek->nourut, 'PAcking In Approve', 'Approve Input Packing', $userId);
				$data = array(
					'idWorksheet'=>$cek->idWorksheet,
					'id'=>$cek->id,
					'nourut'=>$cek->nourut,
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_packing->tambah_stock02($data);
				$hist = [
					'id'=>$cek->id,
					'nourut'=>$cek->nourut,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>11,
					'kdTrans'=>'APV',
					'keterangan'=>'APV - Input Packing',
					'userId'=>addslashes($this->session->userdata('userId'))
				];
				$this->m_barcode->tambah_hist($hist);
				
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
		
}