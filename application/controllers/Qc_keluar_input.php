<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qc_keluar_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_qc'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'qc_keluar_input';
		$data['title']		= 'Keluar Qc';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_qc->show_qc_keluar_input();
		$this->template->load('role','isi','qc/v_qc_keluar_input',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'qc_keluar_input';
		$data['title']		= 'Keluar Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$kode				= 'QC-OUT';
		$bulan 				= date('m');
		$tahun 				= date('Y');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'idQc'=>0,
				'idWorksheet'=>addslashes($this->input->post('idWorksheet')),
				'refNo'=>getRefSewing($kode, $bulan, $tahun),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>'Open',
				'userId'=>$userId,
				'isActive'=>0
			);
			$this->m_qc->tambah_keluar($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}	
	}
	public function send()
	{
		$aplikasi 			= 'qc_keluar_input';
		$data['title']		= 'Keluar Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idQc			= addslashes($this->input->post('idQc'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$this->db->query("update qc_out_sm set status='Send', isActive=99 where idQc='$idQc' and isActive=0");
			$cekData = $this->m_qc->cek_qc_keluar_idQc($idQc);
			foreach($cekData as $cek):
				$id = $cek->id;
				$hist = [
					'id'=>$cek->id,
					'nourut'=>$cek->nourut,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>8,
					'kdTrans'=>'SEND',
					'keterangan'=>'SEND - Keluar Qc',
					'userId'=>addslashes($this->session->userdata('userId'))
				];
				$this->m_barcode->tambah_hist($hist);
				$this->db->query("update qc_out set isActive = 99 where id='$id'");
			endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}	
	}
	public function detail()
	{
		$aplikasi 			= 'qc_keluar_input/detail/'.$this->uri->segment(3);
		$data['title']		= 'Keluar Triming';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_qc->show_qc_keluar_input_id($this->uri->segment(3));
		$this->template->load('role','isi','qc/v_qc_keluar_detail',$data);
	}
	public function tambah_detail()
	{
		$aplikasi 			= 'qc_keluar_input/detail/'.$this->input->post('idQc');
		$data['title']		= 'Keluar Triming';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$cekbox				= $this->input->post('cekbox');
		$nourut 			= $this->input->post('nourut');
		$idQc 				= $this->input->post('idQc');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$jumlah = count($cekbox);
			for($i=0;$i<$jumlah;$i++)
			{
				$cek = $this->m_qc->cek_qc_keluar_id($cekbox[$i]);
				$id = $cek->id;
				$data = array(
					'idQc'=>addslashes($this->input->post('idQc')),
					'id'=>$cekbox[$i],
					'nourut'=>$cek->nourut,
					'status'=>'Keluar',
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_qc->tambah_keluar_detail($data);

				$hist = [
					'id'=>$cek->id,
					'nourut'=>$cek->nourut,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>10,
					'kdTrans'=>'IN',
					'keterangan'=>'Keluar Qc',
					'userId'=>addslashes($this->session->userdata('userId'))
				];
				$this->m_barcode->tambah_hist($hist);
				$this->db->query("update barcode set idQc = '$idQc' where id='$id'");

				$this->db->query("update qc_stock02 set isActive=1 where id='$id'");
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	
}