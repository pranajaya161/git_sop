<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Triming_input_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_triming'); 
		$this->load->model('m_log'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'triming_input_apv';
		$data['title']		= 'Approve Input Triming';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_triming->show_triming_input_apv();
		$this->template->load('role','isi','triming/v_triming_apv',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'triming_input_apv';
		$data['title']		= 'Approve Input Triming';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$cekbox				= $this->input->post('cekbox');

		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$jumlah = count($cekbox);
			for($i=0;$i<$jumlah;$i++)
			{
				$this->db->query("update triming_in set isActive =1 where id='$cekbox[$i]'");
				$cek = $this->m_barcode->show_agremeent($cekbox[$i]);
				$line = $this->m_triming->show_id($cekbox[$i]);
				$this->m_log->tambah($cek->id, $cek->nourut, 'Triming In Approve', 'Approve Input Triming', $userId);
				$data = array(
					'idWorksheet'=>$cek->idWorksheet,
					'id'=>$cek->id,
					'nourut'=>$cek->nourut,
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_triming->tambah_stock02($data);
				$hist = [
					'id'=>$cek->id,
					'nourut'=>$cek->nourut,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>9,
					'kdTrans'=>'APV',
					'keterangan'=>'APV - Input Triming',
					'userId'=>addslashes($this->session->userdata('userId'))
				];
				$this->m_barcode->tambah_hist($hist);
				
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
		
}