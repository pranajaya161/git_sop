<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qc_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_qc'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'qc_input';
		$data['title']		= 'Input QC';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_qc->show_qc_input();
		$this->template->load('role','isi','qc/v_qc_input',$data);
	}
	public function detail()
	{
		$aplikasi 			= 'qc_input/detail/'.$this->uri->segment(3);
		$data['title']		= 'Input QC Detail';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_qc->show_qc_input_id($this->uri->segment(3));
		$this->template->load('role','isi','qc/v_qc_input_detail',$data);
	}
	public function tambah_det()
	{
		$aplikasi 			= 'qc_input/detail/'.addslashes($this->input->post('idPpicQc'));
		$data['title']		= 'Input QC Detail';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$nourut				= addslashes($this->input->post('nourut'));
		$id   				= addslashes($this->input->post('id'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$status		 		= addslashes($this->input->post('status'));
		$idPpicQc			= addslashes($this->input->post('idPpicQc'));
		$keterangan			= addslashes($this->input->post('keterangan'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$lihat=$this->db->query("select count(*) as nom, id, idWorksheet, nourut from qc_stock01 where isActive=0 and id='$id' and idWorksheet='$idWorksheet'")->row();
			if($lihat)
			{
				$id = $lihat->id;
				if($status === 'Approve')
				{
					
					$data = array(
						'idPpicQc'=>$idPpicQc,
						'idWorksheet'=>$lihat->idWorksheet,
						'id'=>$lihat->id,
						'nourut'=>$lihat->nourut,
						'status'=>$status,
						'userId'=>$userId,
						'isActive'=>0
					);
					$this->m_qc->tambah_qc_input($data);

					$hist = [
						'id'=>$lihat->id,
						'nourut'=>$lihat->nourut,
						'tanggal'=>date('Y-m-d'),
						'idDept'=>10,
						'kdTrans'=>'IN',
						'keterangan'=>'Input QC Ukur',
						'userId'=>$userId
					];
					$this->m_barcode->tambah_hist($hist);
					$this->db->query("update qc_stock01 set isActive=99 where isActive=0 and id='$id'");
				} else {
					$data = array(
						'idStockRetur'=>0,
						'idWorksheet'=>$lihat->idWorksheet,
						'id'=>$lihat->id,
						'nourut'=>$lihat->nourut,
						'status'=>$status,
						'keterangan'=>$keterangan,
						'userId'=>$userId,
						'isActive'=>0
					);
					$this->m_qc->tambah_qc_retur($data);
					$this->db->query("update qc_stock01 set isActive=88 where isActive=0 and id='$id' ");
					$hist = [
						'id'=>$lihat->id,
						'nourut'=>$lihat->nourut,
						'tanggal'=>date('Y-m-d'),
						'idDept'=>10,
						'kdTrans'=>'RET',
						'keterangan'=>'RET - Retur Input QC Ukur',
						'userId'=>$userId
					];
					$this->m_barcode->tambah_hist($hist);
				}
			} else {
				$this->db->trans_complete();
				$this->db->trans_rollback();
				$this->session->set_flashdata('pesan','Error , Silahkan coba kembali - 1');
				redirect($aplikasi,'refresh');
			}	
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali - 2');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
}