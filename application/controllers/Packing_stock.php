<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packing_stock extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_packing'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'Packing_stock';
		$data['title']		= 'Stock Input Packing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_packing->show_packing_input_stock();
		$this->template->load('role','isi','packing/v_packing_stock',$data);
	}
		
}