<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maaping_material_det extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper("url");
		$this->load->model('m_worksheet');
	}
    public function index()
	{
		$aplikasi 		  = 'maaping_material_det/index/'.$this->uri->segment(3);
		$data['title']    = 'Detail Style Worksheet';
		$data['aplikasi'] = $aplikasi;
		$data['data']	  = $this->m_worksheet->maaping_id($this->uri->segment(3));
		$data['detail']	  = $this->m_worksheet->maaping_detail_id($this->uri->segment(3));
		$data['style']	  = $this->uri->segment(3);
		$this->template->load('role','isi','md/v_maaping_material_det', $data);
	}
	public function tambah()
	{
		$aplikasi 		  	= 'maaping_material_det/index/'.$this->input->post('idStyle');
		$data['title']    	= 'Detail Style Worksheet';
		$data['aplikasi'] 	= $aplikasi;
		$tgl_proses		  	= date('Y-m-d H:i:s');
		$userId		      	= $this->session->userdata('userId');
		$idStyle 			= $this->input->post('idStyle');
		$idBahan	 		= $this->input->post('idBahan');
		$qtysat				= $this->input->post('qtysat');
		$satuan				= getBahanSatuan($idBahan);
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
		$cekData = $this->db->query("select count(*) as nom from md_maaping where idStyle='$idStyle' and idBahan='$idBahan'")->row();
		if($cekData->nom > 0)
		{
			$this->session->set_flashdata('pesan','Error, Data sudah ada');
			redirect($aplikasi,'refresh');
		} else {
			$data = array(
				'idStyle'=>$idStyle,
				'idBahan'=>$idBahan,
				'satuan'=>$satuan,
				'cons'=>$qtysat,
				'tgl_proses'=>$tgl_proses,
				'userId'=>$userId
			);
			$this->m_worksheet->tambah_maaping($data);
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data Berhasil di Simpan');
			redirect($aplikasi,'refresh');
		}
	}
	function hapus()
	{
		$aplikasi 		  	= 'maaping_material_det/index/'.$this->input->post('idStyle');
		$data['title']    	= 'Detail Style Worksheet';
		$data['aplikasi'] 	= $aplikasi;
		$tgl_proses		  	= date('Y-m-d H:i:s');
		$userId		      	= $this->session->userdata('userId');
		$idStyle 			= addslashes($this->input->post('idStyle'));
		$idBahan	 		= addslashes($this->input->post('idBahan'));
		$this->db->trans_begin();
			$this->db->query("delete from md_maaping where idStyle='$idStyle' and idBahan='$idBahan'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data Berhasil di Simpan');
			redirect($aplikasi,'refresh');
		}
	}
}
