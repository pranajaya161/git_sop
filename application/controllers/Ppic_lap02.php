<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppic_lap02 extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_ppic');
		$this->load->model('m_log'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'Ppic_lap02';
		$data['title']		= 'PPIC Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_ppic->show_ppic_lap02();
		$this->template->load('role','isi','ppic/v_ppic_lap02',$data);
	}
	
	
}