<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packing_keluar_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_packing'); 
		$this->load->model('m_log'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'Packing_keluar_apv';
		$data['title']		= 'Approve Keluar Packing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_packing->show_packing_keluar_input_apv();
		$this->template->load('role','isi','packing/v_packing_keluar_apv',$data);
    }
    public function approve()
	{
		$aplikasi 			= 'Packing_keluar_apv';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$cekbox				= $this->input->post('cekbox');

		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$jumlah = count($cekbox);
			for($i=0;$i<$jumlah;$i++)
			{
				$this->db->query("update packing_out set isActive =1 where id='$cekbox[$i]'");
				$cek = $this->m_barcode->show_agremeent($cekbox[$i]);
				$id = $cek->id;
				$idTriming = $cek->idTriming;
				$this->m_log->tambah($cek->id, $cek->nourut, 'Packing Out Approve', 'Approve Keluar Packing', $userId);
				$data = array(
					'idWorksheet'=>$cek->idWorksheet,
					'id'=>$cek->id,
					'nourut'=>$cek->nourut,
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_packing->tambah_stock03($data);
				
				$hist = [
					'id'=>$cek->id,
					'nourut'=>$cek->nourut,
					'tanggal'=>date('Y-m-d'),
					'idDept'=>11,
					'kdTrans'=>'APV',
					'keterangan'=>'APV - Keluar Packing',
					'userId'=>addslashes($this->session->userdata('userId'))
				];
				$this->m_barcode->tambah_hist($hist);
				$this->db->query("update barcode set idDept = 6 where id='$id'");
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
    }
}