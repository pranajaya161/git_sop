<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Param_design extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_parameter');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'param_design';
		$style 				= 'design';
		$data['title']		= 'Parameter : Design';
		$data['aplikasi'] 	= $aplikasi;
		$data['status']     = $style;
		$data['data']		= $this->m_parameter->show_style($style);
		$this->template->load('role','isi','parameter/v_param_style',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'param_design';
		$data['aplikasi'] 	= $aplikasi;
		$style 				= 'design';
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$fileName 		= $_FILES['pict']['name'];
			$config['upload_path']      = 'uploads/parameter/style/';
			$config['allowed_types'] 	= 'jpg|png|jpeg';
			$config['max_size']			= '5000000';
			$config['max_width']  		= '5000000';
			$config['max_height']  		= '5000000';
			$config['file_name'] 		= $fileName;  
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload('pict'))
			{
				$up_data	= $this->upload->data();
				$pict		= $up_data['file_name'];
				$data = array(
					'idStyle'=>0,
					'namaStyle'=>addslashes($this->input->post('namaStyle')),
					'pict'=>$pict,
					'status'=>addslashes($this->input->post('status')),
					'isActive'=>1,
					'userId'=>$userId
				);
				$this->m_parameter->add_style($data);
			}
			else 
			{
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
				redirect($aplikasi,'refresh');
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$aplikasi 			= 'param_design';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idStyle			= addslashes($this->input->post('idStyle'));
		$pict				= $_FILES['pict']['name'];
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			if(empty($pict))
			{
				$data = array(
					'namaStyle'=>addslashes($this->input->post('namaStyle')),
					'status'=>addslashes($this->input->post('status')),
					'isActive'=>addslashes($this->input->post('isActive')),
					'userId'=>$userId
				);
				$this->m_parameter->edit_style($idStyle, $data);
			}
			else
			{
				$fileName 		= $_FILES['pict']['name'];
				$config['upload_path']      = 'uploads/parameter/style/';
				$config['allowed_types'] 	= 'jpg|png|jpeg';
				$config['max_size']			= '5000000';
				$config['max_width']  		= '5000000';
				$config['max_height']  		= '5000000';
				$config['file_name'] 		= $fileName;  
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('pict'))
				{
					$up_data	= $this->upload->data();
					$pict		= $up_data['file_name'];
					$data = array(
						'namaStyle'=>addslashes($this->input->post('namaStyle')),
						'pict'=>$pict,
						'status'=>addslashes($this->input->post('status')),
						'isActive'=>addslashes($this->input->post('isActive')),
						'userId'=>$userId
					);
					$this->m_parameter->edit_style($idStyle, $data);
				}
			}	
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Ubah');
			redirect($aplikasi,'refresh');
		}	
	}
	
}