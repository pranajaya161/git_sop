<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lap_mkt_prospecting extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_laporan');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'lap_mkt_prospecting';
		$data['title']		= 'Laporan : Prospecting';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_laporan->show_prospecting();
		$this->template->load('role','isi','laporan/v_lap_mkt_prospecting',$data);
	}
	
}