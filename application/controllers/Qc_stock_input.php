<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qc_stock_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_qc'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'qc_stock_input';
		$data['title']		= 'Query Input';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_qc->show_qc_stock_input();
		$this->template->load('role','isi','qc/v_qc_stock_penerimaan',$data);
	}
	
	
}