<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sewing_penerimaan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_sewing'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'sewing_penerimaan';
		$data['title']		= 'Penerimaan Cutting';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_sewing->show_sewing_penerimaan();
		$this->template->load('role','isi','sewing/v_sewing_penerimaan',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'sewing_penerimaan';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$id 				= $this->input->post('id');
		$nourut				= $this->input->post('nourut');
		$idPpicSewing		= $this->input->post('idPpicSewing');
		$idWorksheet		= $this->input->post('idWorksheet');
		$idLine				= $this->input->post('idLine');
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$jumlah = count($id);
			for($i=0;$i<$jumlah;$i++)
			{
				$data = array(
					'idPpicSewing'=>$idPpicSewing,
					'idWorksheet'=>$idWorksheet,
					'id'=>$id[$i],
					'idLine'=>$idLine,
					'nourut'=>$nourut[$i],
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_sewing->tambah_penerimaan($data);
					$hist = [
						'id'=>$id[$i],
						'nourut'=>$nourut[$i],
						'tanggal'=>date('Y-m-d'),
						'idDept'=>8,
						'kdTrans'=>'IN',
						'keterangan'=>'Penerimaan Cutting - '.getLine($idLine),
						'userId'=>$userId
					];
					$this->m_barcode->tambah_hist($hist);
					$this->m_log->tambah($id[$i], $nourut[$i], 'Sewing Penerimaan', 'Penerimaan Sewing', $userId);
			}
			$this->db->query("update ppic_sewing set isActive=1 where idPpicSewing='$idPpicSewing' and idWorksheet='$idWorksheet'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di ');
			redirect($aplikasi,'refresh');
		}
	}
	
}