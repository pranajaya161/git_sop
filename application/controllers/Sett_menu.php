<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sett_menu extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper("url");
		$this->load->model('m_menu');
		$this->load->library("pagination");
		if($this->session->userdata('isLog')==FALSE){
         	   redirect('login','refresh');
       		 }
	}
    public function index()
	{
		$data['aplikasi']= 'Sett_menu';
		$aplikasi 	= 'Sett_menu';
		$data['data']  = $this->m_menu->fetch_data();
		$data['head']  = $this->m_menu->showHead();
		$this->template->load('role','isi','setting/v_menu',$data);
	}
	public function tambah()
	{
		$data['aplikasi']= 'Sett_menu';
		$aplikasi 	= 'Sett_menu';
		$userId		= $this->session->userdata('userId');
		$tgl_proses	= date('Y-m-d H:i:s');
		$this->db->trans_begin();
		$cekHead = $this->m_menu->cekNomHead()->row();
		$data=array(
			'idMenu'=>$cekHead->nom + 1,
			'headMenu'=>$cekHead->nom + 1,
			'subMenu'=>0,
			'childMenu'=>0,
			'namaMenu'=>$this->input->post('namaMenu'),
			'link'=>'#',
			'nourut'=>0,
			'isActive'=>$this->input->post('isActive'),
		);
		$this->m_menu->add($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	function tambah_sub()
	{
		$data['aplikasi']= 'Sett_menu';
		$aplikasi 	= 'Sett_menu';
		$userId		= $this->session->userdata('userId');
		$tgl_proses	= date('Y-m-d H:i:s');
		$this->db->trans_begin();
		$cekHead = $this->m_menu->cekNomHead($this->input->post('idMenu'))->row();
		$data=array(
			'idMenu'=>0,
			'headMenu'=>$this->input->post('idMenu'),
			'subMenu'=>$cekHead->nom + 1,
			'childMenu'=>0,
			'namaMenu'=>$this->input->post('namaMenu'),
			'link'=>$this->input->post('link'),
			'nourut'=>$this->input->post('nourut'),
			'isActive'=>$this->input->post('isActive')
		);
		$this->m_menu->add($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	function tambah_child()
	{
		$data['aplikasi']= 'Sett_menu';
		$aplikasi 	= 'Sett_menu';
		$userId		= $this->session->userdata('userId');
		$tgl_proses	= date('Y-m-d H:i:s');
		$this->db->trans_begin();
		$cekHead = $this->m_menu->cekNomChild($this->input->post('idMenu'), $this->input->post('subMenu'))->row();
		$data=array(
			'idMenu'=>0,
			'headMenu'=>$this->input->post('headMenu'),
			'subMenu'=>$this->input->post('subMenu'),
			'childMenu'=>$cekHead->nom + 1,
			'namaMenu'=>$this->input->post('namaMenu'),
			'link'=>$this->input->post('link'),
			'nourut'=>$this->input->post('nourut'),
			'isActive'=>$this->input->post('isActive')
		);
		$this->m_menu->add($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error, Silahkan mengulang kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
		
	}
}

