<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Md_inputworksheet extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_md');
	}
	public function index()
	{
		$aplikasi 			= 'md_inputworksheet';
		$data['title']		= 'MD Input Worksheet';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_md->show_worksheet();
		$this->template->load('role','isi','md/v_md_inputworksheet',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'md_inputworksheet';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$kode				= 'MD-WS';
		$bulan 				= date('m');
		$tahun 				= date('Y');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'idWorksheet'=>0,
				'idMaterialist'=>addslashes($this->input->post('idMaterialist')),
				'refNo'=>getRefWorksheet($kode, $bulan, $tahun),
				'idPo'=>getidPoMaterialist($this->input->post('idMaterialist')),
				'lot_number'=>addslashes($this->input->post('lot_number')),
				'idSubCount'=>addslashes($this->input->post('idSubCount')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>'Open',
				'userId'=>$userId,
				'isActive'=>0
			);

			$this->m_md->tambah_worksheet($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$aplikasi 			= 'md_inputworksheet';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'lot_number'=>addslashes($this->input->post('lot_number')),
				'idSubCount'=>addslashes($this->input->post('idSubCount')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'userId'=>$userId,
			);
			$this->m_md->edit_worksheet($this->input->post('idWorksheet'), $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Ubah');
			redirect($aplikasi,'refresh');
		}
	}
	public function cari()
	{
		$data['data']	= $this->m_md->cariStyle($this->uri->segment(3),$this->uri->segment(4));
		$this->load->view('view/v_worksheet_style_cari', $data);
	}
	public function send()
	{
		$aplikasi 			= 'md_inputworksheet';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$cek = $this->db->query("select count(*) as nom from md_worksheet_dt where idWorksheet='$idWorksheet'")->row();
		if($cek->nom > 0)
		{	
			$this->db->trans_begin();
				$this->db->query("update md_worksheet set status ='Send' where status in ('Open','Reject') and idWorksheet='$idWorksheet'");
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
				redirect($aplikasi,'refresh');
			} else {
				$this->db->trans_commit();
				$this->session->set_flashdata('pesan','Data berhasil di Kirim');
				redirect($aplikasi,'refresh');
			}
		} else {
			$this->session->set_flashdata('Error','Data detail belum terisi');
				redirect($aplikasi,'refresh');
		}
		
	}
	//===== Detail
	public function detail()
	{
		$aplikasi 			= 'md_inputworksheet/detail/'.$this->uri->segment(3);
		$data['title']		= 'MD Input WorkSheet Detail';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_md->show_worksheet_id($this->uri->segment(3));
		$this->template->load('role','isi','md/v_md_inputworksheet_detail',$data);
	}
	public function add_detail()
	{
		$aplikasi 			= 'md_inputworksheet/detail/'.$this->input->post('idWorksheet');
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$idStyle			= addslashes($this->input->post('idStyle'));
		$size				= addslashes($this->input->post('size'));
		$qty				= addslashes($this->input->post('tqty'));
		$idPo				= addslashes($this->input->post('idPo'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$lihat=$this->db->query("select * from edp_assortment_pros where idPo='$idPo' and idStyle='$idStyle' and size='$size' and isActive=1 and idWorksheet=0 limit $qty")->result();
			foreach($lihat as $a){
				$id=$a->id;
				$nourut = $a->nourut;
				$data=array(
					'idWorksheet'=>$idWorksheet,
					'idStyle'=>$idStyle,
					'size'=>$size,
					'warna'=>'',
					'qty'=>$qty,
					'nourut'=>$nourut,
					'id'=>$id,
				);
				$this->m_md->tambah_worksheet_det($data);
			}
			$this->db->query("update edp_assortment_pros inner join  md_worksheet_dt b on b.id = edp_assortment_pros.id set edp_assortment_pros.isActive=99 where edp_assortment_pros.isActive = 1");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus()
	{
		$aplikasi 			= 'md_inputworksheet/detail/'.$this->input->post('idWorksheet');
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$id    				= addslashes($this->input->post('id'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$this->db->query("delete from md_worksheet_dt where id='$id' and idWorksheet='$idWorksheet'");
			$this->db->query("update edp_assortment_pros set edp_assortment_pros.isActive=1 where edp_assortment_pros.isActive = 99 and id ='$id'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Hapus');
			redirect($aplikasi,'refresh');
		}
	}
}