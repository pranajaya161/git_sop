<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// barcode
function getBarcodeStyle($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT idStyle FROM barcode WHERE id = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->idStyle;
	return $nama;
}
function getLine($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT keterangan FROM tblline WHERE idLine = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->keterangan;
	return $nama;
}
function getRefNoTriming($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT refNo FROM triming_out_sm WHERE idTriming = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->refNo;
	return $nama;
}
function getRefNoSewing($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT refNo FROM sewing_out_sm WHERE idSewing = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->refNo;
	return $nama;
}
//=== Cutting
function getRefNoCutting($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT refNo FROM cutting_out_sm WHERE idCutting = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->refNo;
	return $nama;
}
function getRefNoWorksheet($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT refNo FROM md_worksheet WHERE idWorksheet = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->refNo;
	return $nama;
}
function getRefNoPurcPembelian($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT refNo FROM purc_pembelian_sm WHERE idPurcPembelian = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->refNo;
	return $nama;
}

function getRequestPembelian($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT request FROM purc_pembelian_sm WHERE idPurcPembelian = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->request;
	return $nama;
}
function getSupplier($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT namaSupplier FROM tblsupplier WHERE idSupplier = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->namaSupplier;
	return $nama;
}
function getRefnoPurcPermintaan($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT refNo FROM purc_permintaan_sm WHERE idPurcPermintaan = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->refNo;
	return $nama;
}
function getKetPurcPermintaan($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT keterangan FROM purc_permintaan_sm WHERE idPurcPermintaan = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->keterangan;
	return $nama;
}
function getDept($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT keterangan FROM tbldept WHERE idDept = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->keterangan;
	return $nama;
}
function getSubCount($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT keterangan FROM tblsubcount WHERE idSubCount = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->keterangan;
	return $nama;
}
function getrefNoMaterialist($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT refNo FROM md_materialist WHERE idMaterialist = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->refNo;
	return $nama;
}
function getidPoMaterialist($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT idPo FROM md_materialist WHERE idMaterialist = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->idPo;
	return $nama;
}

function getMenu($id) {
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT namaMenu FROM tblmenu_adm WHERE idMenu = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->namaMenu;
	return $nama;
}
function getMenuLink($id) {
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT link FROM tblmenu_adm WHERE idMenu = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->link;
	return $nama;
}
function getModul($id) {
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT namaModul FROM tblmodul_adm WHERE idModul = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->namaModul;
	return $nama;
}

function getSatuan($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT namaSatuan FROM tblsatuan WHERE idSatuan = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->namaSatuan;
	return $nama;
}
function getKategori($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT keterangan FROM tblkategori WHERE idKategori = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->keterangan;
	return $nama;
}
function getStyle($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT namaStyle FROM tblstyle WHERE idStyle = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->namaStyle;
	return $nama;
}
function getStylePict($id)
{
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT pict FROM tblstyle WHERE idStyle = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->pict;
	return $nama;
}
function getBahan($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT namaBahan FROM tblbahan WHERE idBahan = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->namaBahan;
	return $nama;
}
function getBahanSatuan($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT idSatuan FROM tblbahan WHERE idBahan = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->idSatuan;
	return $nama;
}
//=== Table Client
function getClient($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT namaClient FROM tblclient WHERE idClient = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->namaClient;
	return $nama;
}
function getClient_init($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT initClient FROM tblclient WHERE idClient = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->initClient;
	return $nama;
}
//=== End Table Client
function getInformasi($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT keterangan FROM tblinformasi WHERE idInformasi = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->keterangan;
	return $nama;
}
function getPoRefno($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT refNo FROM mkt_po_sm WHERE idPo = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->refNo;
	return $nama;
}  
function getPoNoPo($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT noPo FROM mkt_po_sm WHERE idPo = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->noPo;
	return $nama;
}
//=== RefNo
function getRefGudangKeluar($kode, $bulan, $tahun)
{
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select max(idGudangKeluar) as id from gudang_keluar where month(tgl_proses) ='$bulan' and year(tgl_proses) = '$tahun'")->row();
	$MaksID 	= $noUrut->id + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
}
function getRefTriming($kode, $bulan, $tahun)
{
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select max(idTriming) as id from triming_out_sm where month(tanggal) ='$bulan' and year(tanggal) = '$tahun'")->row();
	$MaksID 	= $noUrut->id + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
}
function getRefSewing($kode, $bulan, $tahun)
{
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select max(idSewing) as id from sewing_out_sm where month(tanggal) ='$bulan' and year(tanggal) = '$tahun'")->row();
	$MaksID 	= $noUrut->id + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
}
function getRefCutting($kode, $bulan, $tahun)
{
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select max(idCutting) as id from cutting_out_sm where month(tanggal) ='$bulan' and year(tanggal) = '$tahun'")->row();
	$MaksID 	= $noUrut->id + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
}
function getRefPembelian($kode, $bulan, $tahun)
{
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select max(idPurcPembelian) as id from purc_pembelian_sm where month(tanggal) ='$bulan' and year(tanggal) = '$tahun'")->row();
	$MaksID 	= $noUrut->id + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
}
function getRefNoProspecting($id){
	$CI =& get_instance();
	$q_ambil	= $CI->db->query("SELECT refNo FROM mkt_pros_sm WHERE idProspecting = '$id'")->row();
	$nama		= empty($q_ambil) ? "-" : $q_ambil->refNo;
	return $nama;
}
function getRefPermintaanDept($kode, $bulan, $tahun)
{
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select max(idPurcPermintaan) as id from purc_permintaan_sm where month(tanggal) ='$bulan' and year(tanggal) = '$tahun'")->row();
	$MaksID 	= $noUrut->id + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
} 
function getRefMaterial($kode, $bulan, $tahun)
{
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select max(idMaterialist) as id from md_materialist where month(tanggal) ='$bulan' and year(tanggal) = '$tahun'")->row();
	$MaksID 	= $noUrut->id + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
} 
function getRefWorksheet($kode, $bulan, $tahun)
{
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select max(idWorksheet) as id from md_worksheet where month(tanggal) ='$bulan' and year(tanggal) = '$tahun'")->row();
	$MaksID 	= $noUrut->id + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
}
function getRefNo($kode, $init, $bulan, $tahun){
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select max(idProspecting) as idProspecting from mkt_pros_sm where month(tgl_proses) ='$bulan' and year(tgl_proses) ='$tahun'")->row();
	$MaksID 	= $noUrut->idProspecting + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$init.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
}
function getRefNoPo($kode, $init, $bulan, $tahun){
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select count(*) as nom from mkt_po_sm where month(tanggal) = '$bulan' and year(tanggal) ='$tahun'")->row();
	$MaksID 	= $noUrut->nom + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$init.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
}
function getRefnoSmp($kode, $bulan, $tahun){
	$CI =& get_instance();
	$noUrut 	= $CI->db->query("select max(idSample) as nom from mkt_pros_sample where month(tgl_proses) ='$bulan' and year(tgl_proses) ='$tahun'")->row();
	$MaksID 	= $noUrut->nom + 1;
		if($MaksID < 10){ $ID ="000".$MaksID; } // nilai kurang dari 1
            else if($MaksID < 100) { $ID = "00".$MaksID; } // nilai kurang dari 10
            else if($MaksID < 1000) { $ID ="0".$MaksID; } // nilai kurang dari 100
            else {$ID = $MaksID;} 
	$q_ambil		= $ID.'/'.$kode.'/'.$bulan.'/'.$tahun;
	$nama		= empty($q_ambil) ? "-" : $q_ambil;
	return $nama;
}
