<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gudang_penerimaan_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_gudang'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'gudang_penerimaan_apv';
		$data['title']		= 'Approve Gudang Penerimaan Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_gudang->show_penerimaan_supplier_apv();
		$this->template->load('role','isi','gudang/v_gudang_penerimaan_apv',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'gudang_penerimaan_apv';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPurcPembelian	= addslashes($this->input->post('idPurcPembelian'));
		$idSupplier			= addslashes($this->input->post('idSupplier'));
		$idBahan	 		= addslashes($this->input->post('idBahan'));
		$qty		 		= addslashes($this->input->post('qty'));
		$refNo	 			= addslashes($this->input->post('refNo'));
		$tgl_proses			= date('Y-m-d H:i:s');
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$dataHist = array(
				'idBahan'=>$idBahan,
				'idClient'=>0,
				'idSupplier'=>$idSupplier,
				'kdTrans'=>'PSG',
				'refNo'=>$refNo,
				'qty'=>$qty,
				'keterangan'=>'Penerimaan Supplier',
				'userId'=>$userId
			);
			$this->m_gudang->tambah_hist($dataHist);
			$cek = $this->db->query("select count(*) as nom from stock_01 where idBahan ='$idBahan'")->row();
			if($cek->nom > 0)
			{
				$this->db->query("update stock_01 set qty = (qty + $qty), userId='$userId', tgl_proses='$tgl_proses' where idBahan='$idBahan'");
			} else {
				$data = array(
					'idBahan'=>$idBahan,
					'idClient'=>0,
					'qty'=>$qty,
					'userId'=>$userId
				);
				$this->m_gudang->tambah_stock($data);
			}
			$this->db->query("update gudang_penerimaan_supplier set isActive = 1 where idPurcPembelian='$idPurcPembelian' and idBahan='$idBahan' and refNo='$refNo'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di ');
			redirect($aplikasi,'refresh');
		}
	}
	
}