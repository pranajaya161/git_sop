<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Md_lap_peng_purc extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_purchashing');
	}
	public function index()
	{
		$aplikasi 			= 'purc_pend_apv';
		$idDept				= 4;
		$data['title']		= getDept($idDept).' Approve Permintaan Purchasing';
		$data['aplikasi'] 	= $aplikasi;
		$data['idDept']		= $idDept;
		$data['data']		= $this->m_purchashing->show_pend_input_id_lap($idDept);
		$this->template->load('role','isi','purchasing/v_purch_pend_input_lap',$data);
	}
	

}