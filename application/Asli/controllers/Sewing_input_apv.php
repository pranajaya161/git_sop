<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sewing_input_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_sewing'); 
		$this->load->model('m_log'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'sewing_input_apv';
		$data['title']		= 'Approve Input Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_sewing->show_sewing_input_apv();
		$this->template->load('role','isi','sewing/v_sewing_apv',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'sewing_input_apv';
		$data['title']		= 'Approve Input Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$cekbox				= $this->input->post('cekbox');

		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$jumlah = count($cekbox);
			for($i=0;$i<$jumlah;$i++)
			{
				$this->db->query("update sewing_in set isActive =1 where id='$cekbox[$i]'");
				$h = $this->db->query("select * from sewing_in where id='$cekbox[$i]'")->row();
				$this->m_log->tambah($h->id, $h->nourut, 'Sewing In Approve', 'Approve Input Sewing', $userId);
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
		
}