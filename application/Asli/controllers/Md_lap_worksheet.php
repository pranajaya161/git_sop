<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Md_lap_worksheet extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_md');
	}
	public function index()
	{
		$aplikasi 			= 'md_apvworksheet';
		$data['title']		= 'MD Approve WorkSheet';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_md->show_worksheet_lap();
		$this->template->load('role','isi','md/v_md_inputworksheet_lap',$data);
	}
	
}