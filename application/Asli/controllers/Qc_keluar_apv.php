<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qc_keluar_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_qc'); 
		$this->load->model('m_log'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'qc_keluar_apv';
		$data['title']		= 'Approve Keluar QC';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_qc->show_qc_keluar_input_apv();
		$this->template->load('role','isi','qc/v_qc_keluar_apv',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'qc_keluar_apv';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idQc				= addslashes($this->input->post('idQc'));
		$status 			= addslashes($this->input->post('status'));
		$keterangan 		= addslashes($this->input->post('keterangan'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			if($status ==1)
			{
				$this->db->query("update qc_out_sm set status='Approve', isActive = 1 where idQc='$idQc'");
				$xStatus = 'Tambah';	
			} else {
				$this->db->query("update qc_out_sm set status='Reject', isActive = 0 where idQc='$idQc'");
				$xStatus = 'Hapus'; 
			}
			$data = array(
				'idQc'=>$idQc,
				'isStatus'=>$status,
				'keterangan'=>$keterangan,
				'userId'=>$userId,
			);
			$this->m_qc->tambah_qc_keluar_apv($data);
			$hist = $this->db->query("select * from qc_out where idQc='$idQc'")->result();
			foreach($hist as $h):
				$this->m_log->tambah($h->id, $h->nourut, 'Qc Out Approve', $keterangan, $userId);
			endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di '.$xStatus);
			redirect($aplikasi,'refresh');
		}
	}
}