<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Triming_stock_keluar extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_triming'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'triming_stock_keluar';
		$data['title']		= 'Stock Keluar Triming';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_triming->show_triming_keluar_stock();
		$this->template->load('role','isi','triming/v_triming_keluar_stock',$data);
	}
		
}