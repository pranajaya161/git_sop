<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desg_lap01 extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_prospecting');
		$this->load->model('m_parameter');
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'desg_lap01';
		$data['title']		= 'Laporan : Design ';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_prospecting->show_desg_lap01();
		$this->template->load('role','isi','design/v_desg_lap01',$data);
	}
}