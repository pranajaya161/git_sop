<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packing_input_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_packing'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'packing_input_apv';
		$data['title']		= 'Approve Input Packing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_packing->show_packing_input_apv();
		$this->template->load('role','isi','packing/v_packing_apv',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'packing_input_apv';
		$data['title']		= 'Approve Input Packing';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$cekbox				= $this->input->post('cekbox');

		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$jumlah = count($cekbox);
			for($i=0;$i<$jumlah;$i++)
			{
				$this->db->query("update packing_in set isActive =1 where id='$cekbox[$i]'");
				
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
		
}