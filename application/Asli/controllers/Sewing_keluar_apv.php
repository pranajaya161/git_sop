<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sewing_keluar_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_sewing');
		$this->load->model('m_log'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'sewing_keluar_apv';
		$data['title']		= 'Approve Keluar Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_sewing->show_sewing_keluar_input_apv();
		$this->template->load('role','isi','sewing/v_sewing_keluar_apv',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'sewing_keluar_apv';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idSewing			= addslashes($this->input->post('idSewing'));
		$status 			= addslashes($this->input->post('status'));
		$keterangan 		= addslashes($this->input->post('keterangan'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			if($status ==1)
			{
				$this->db->query("update sewing_out_sm set status='Approve', isActive = 1 where idSewing='$idSewing'");
				$xStatus = 'Tambah';	
			} else {
				$this->db->query("update sewing_out_sm set status='Reject', isActive = 0 where idSewing='$idSewing'");
				$xStatus = 'Hapus'; 
			}
			$data = array(
				'idSewing'=>$idSewing,
				'isStatus'=>$status,
				'keterangan'=>$keterangan,
				'userId'=>$userId,
			);
			$this->m_sewing->tambah_sewing_keluar_apv($data);
			$hist = $this->db->query("select * from sewing_out where idSewing='$idSewing'")->result();
			foreach($hist as $h):
				$this->m_log->tambah($h->id, $h->nourut, 'Sewing Out Approve', $keterangan, $userId);
			endforeach;	
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di '.$xStatus);
			redirect($aplikasi,'refresh');
		}
	}
}