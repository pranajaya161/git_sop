<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkt_purchaseorder_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_marketing');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'mkt_purchaseorder_apv';
		$data['title']		= 'Marketing --> Approval Purchase Order';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_marketing->show_purchaseOrder_apv();
		$this->template->load('role','isi','purchaseorder/v_purchaseorder_apv',$data);
	}
	public function Approval()
	{
		$aplikasi 			= 'mkt_purchaseorder_apv';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPo   			= addslashes($this->input->post('idPo'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$status = addslashes($this->input->post('status'));
		$this->db->trans_begin();
			$data = array(
				'idPo'=>$idPo,
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>addslashes($this->input->post('status')),
				'userId'=>$userId
			);
			$this->m_marketing->tambah_po_apv($data);
			$this->db->query("update mkt_po_sm set isActive = '$status' where idPo='$idPo'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Kirim');
			redirect($aplikasi,'refresh');
		}
	}
	
}