<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppic_packing extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_ppic'); 
		$this->load->model('m_log'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'Ppic_packing';
		$data['title']		= 'PPIC Packing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_ppic->show_ppic_packing();
		$this->template->load('role','isi','ppic/v_ppic_packing',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'Ppic_packing';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$keterangan			= addslashes($this->input->post('keterangan'));
		$line				= addslashes($this->input->post('line'));
		$idQc				= addslashes($this->input->post('idQc'));
		$refNo				= addslashes($this->input->post('refNo'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'idPpicPacking'=>0,
				'idWorksheet'=>$idWorksheet,
				'keterangan'=>$keterangan,
				'refNo'=>$refNo,
				'userId'=>$userId
			);	
			$this->m_ppic->tambah_packing($data);
			$this->db->query("update qc_out_sm set isActive =9 where idQc='$idQc' and isActive=1");
			$hist = $this->db->query("select * from qc_out where idQc='$idQc'")->result();
			foreach($hist as $h):
				$this->m_log->tambah($h->id, $h->nourut, 'PPIC Qc Approve', $keterangan, $userId);
			endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di ');
			redirect($aplikasi,'refresh');
		}
	}
	
}