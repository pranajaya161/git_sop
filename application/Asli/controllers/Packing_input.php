<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packing_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_packing'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'packing_input';
		$data['title']		= 'Input Packing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_packing->show_packing_input();
		$this->template->load('role','isi','packing/v_packing_input',$data);
	}
	public function detail()
	{
		$aplikasi 			= 'packing_input/detail/'.$this->uri->segment(3);
		$data['title']		= 'Input Packing Detail';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_packing->show_packing_input_id($this->uri->segment(3));
		$this->template->load('role','isi','packing/v_packing_input_detail',$data);
	}
	public function tambah_det()
	{
		$aplikasi 			= 'packing_input/detail/'.addslashes($this->input->post('idWorksheet'));
		$data['title']		= 'Input QC Detail';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$nourut				= addslashes($this->input->post('nourut'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$lihat=$this->db->query("select count(*) as nom, id, idWorksheet, nourut from packing_stock01 where isActive=0 and nourut='$nourut' and idWorksheet='$idWorksheet'")->row();
			if($lihat->nom > 0)
			{
				$data = array(
					'idWorksheet'=>$lihat->idWorksheet,
					'id'=>$lihat->id,
					'nourut'=>$lihat->nourut,
					'status'=>'Open',
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_packing->tambah_packing_input($data);
				$this->db->query("update packing_stock01 set isActive=1 where isActive=0 and nourut='$nourut' ");
			} else {
				$this->db->trans_complete();
				$this->db->trans_rollback();
				$this->session->set_flashdata('pesan','Error , Silahkan coba kembali - 1');
				redirect($aplikasi,'refresh');
			}	
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali - 2');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
}