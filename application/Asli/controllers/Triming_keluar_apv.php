<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Triming_keluar_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_triming'); 
		$this->load->model('m_log'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'Triming_keluar_apv';
		$data['title']		= 'Approve Keluar Triming';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_triming->show_triming_keluar_input_apv();
		$this->template->load('role','isi','triming/v_triming_keluar_apv',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'Triming_keluar_apv';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idTriming			= addslashes($this->input->post('idTriming'));
		$status 			= addslashes($this->input->post('status'));
		$keterangan 		= addslashes($this->input->post('keterangan'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			if($status ==1)
			{
				$this->db->query("update triming_out_sm set status='Approve', isActive = 1 where idTriming='$idTriming'");
				$xStatus = 'Tambah';	
			} else {
				$this->db->query("update triming_out_sm set status='Reject', isActive = 0 where idTriming='$idTriming'");
				$xStatus = 'Hapus'; 
			}
			$data = array(
				'idTriming'=>$idTriming,
				'isStatus'=>$status,
				'keterangan'=>$keterangan,
				'userId'=>$userId,
			);
			$this->m_triming->tambah_triming_keluar_apv($data);
			$hist = $this->db->query("select * from triming_out where idTriming='$idTriming'")->result();
			foreach($hist as $h):
				$this->m_log->tambah($h->id, $h->nourut, 'Triming Out Approve', $keterangan, $userId);
			endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di '.$xStatus);
			redirect($aplikasi,'refresh');
		}
	}
}