<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pros_upl_cost extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_prospecting');
		$this->load->model('m_parameter');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'pros_upl_cost';
		$data['title']		= 'Marketing : Uplodad Cost Calculation';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_prospecting->show_upl_cost();
		$this->template->load('role','isi','prospecting/v_upl_cost',$data);
	}
	public function upload()	
	{
		$aplikasi 			= 'pros_upl_cost';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$fileName 		= $_FILES['pict']['name'];
			$config['upload_path']      = 'uploads/costcalculation/';
			$config['allowed_types'] 	= 'xls|xlsx|pdf|jpg|png|jpeg';
			$config['max_size']			= '5000000';
			$config['max_width']  		= '5000000';
			$config['max_height']  		= '5000000';
			$config['file_name'] 		= $fileName;  
			$this->load->library('upload', $config);
			$this->upload->initialize($config); 
			if($this->upload->do_upload('pict'))
			{
				$idProspecting 	= addslashes($this->input->post('idProspecting'));
				$nom = $this->db->query("select max(nourut) as nom from mkt_pros_cost where idProspecting='$idProspecting'")->row();
				$up_data	= $this->upload->data();
				$pict		= $up_data['file_name'];
				$data = array(
					'idProspecting'=>addslashes($this->input->post('idProspecting')),
					'nourut'=>$nom->nom + 1,
					'keterangan'=>addslashes($this->input->post('keterangan')),
					'pict'=>$pict,
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_prospecting->tambah_upl_cost($data);
			}
			else 
			{
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
				redirect($aplikasi,'refresh');
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Upload');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus()
	{
		$aplikasi 			= 'pros_upl_cost';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$idProspecting		= $this->uri->segment(3);
		$nourut				= $this->uri->segment(4);
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$this->db->query("delete from mkt_pros_cost where idProspecting='$idProspecting' and nourut='$nourut'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Data Error Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Hapus');
			redirect($aplikasi,'refresh');
		}
	}
	public function kirim()
	{
		$aplikasi 			= 'pros_upl_cost';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$idProspecting		= addslashes($this->input->post('idProspecting'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$this->db->query("update mkt_pros_cost set isActive=99 where idProspecting='$idProspecting'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Data Error Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Hapus');
			redirect($aplikasi,'refresh');
		}
	}
}