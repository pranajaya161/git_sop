<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packing_keluar_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_packing'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'packing_keluar_input';
		$data['title']		= 'Keluar Packing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_packing->show_packing_keluar_input();
		$this->template->load('role','isi','packing/v_packing_keluar_input',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'packing_keluar_input';
		$data['title']		= 'Keluar Packing';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$kode				= 'TRM-OUT';
		$bulan 				= date('m');
		$tahun 				= date('Y');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'idPacking'=>0,
				'idWorksheet'=>addslashes($this->input->post('idWorksheet')),
				'refNo'=>getRefSewing($kode, $bulan, $tahun),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>'Open',
				'userId'=>$userId,
				'isActive'=>0
			);
			$this->m_packing->tambah_keluar($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}	
	}
	public function send()
	{
		$aplikasi 			= 'packing_keluar_input';
		$data['title']		= 'Keluar Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPacking			= addslashes($this->input->post('idPacking'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$this->db->query("update packing_out_sm set status='Send' where idPacking='$idPacking' and isActive=0 and status='Open'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}	
	}
	public function detail()
	{
		$aplikasi 			= 'packing_keluar_input/detail/'.$this->uri->segment(3);
		$data['title']		= 'Keluar Triming';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_packing->show_packing_keluar_input_id($this->uri->segment(3));
		$this->template->load('role','isi','packing/v_packing_keluar_detail',$data);
	}
	public function tambah_detail()
	{
		$aplikasi 			= 'packing_keluar_input/detail/'.$this->input->post('idPacking');
		$data['title']		= 'Keluar Triming';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$cekbox				= $this->input->post('cekbox');
		$nourut 			= $this->input->post('nourut');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$jumlah = count($cekbox);
			for($i=0;$i<$jumlah;$i++)
			{
				$cek = $this->db->query("select nourut from packing_in where id = '$cekbox[$i]'")->row();
				$data = array(
					'idPacking'=>addslashes($this->input->post('idPacking')),
					'id'=>$cekbox[$i],
					'nourut'=>$cek->nourut,
					'status'=>'Keluar',
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_qc->tambah_keluar_detail($data);
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	
}