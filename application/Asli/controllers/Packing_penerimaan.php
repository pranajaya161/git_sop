<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packing_penerimaan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_packing'); 
		$this->load->model('m_log'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'packing_penerimaan';
		$data['title']		= 'Penerimaan Packing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_packing->show_packing_penerimaan();
		$this->template->load('role','isi','packing/v_packing_penerimaan',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'packing_penerimaan';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$id 				= $this->input->post('id');
		$nourut				= $this->input->post('nourut');
		$idPpicPacking		= $this->input->post('idPpicPacking');
		$idWorksheet		= $this->input->post('idWorksheet');
		$refNo				= $this->input->post('refNo');
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$dt = $this->db->query("select * from qc_out_sm a inner join qc_out b on b.idQc = a.idQc where a.idWorksheet='$idWorksheet' and a.refNo ='$refNo'")->result();
			foreach($dt as $i)
			{
				$idQc = $i->idQc;
				$nourut = $i->nourut;
				$data = array(
					'idPpicPacking'=>$idPpicPacking,
					'idWorksheet'=>$idWorksheet,
					'id'=>$i->id,
					'nourut'=>$i->nourut,
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_packing->tambah_penerimaan($data);
				$this->m_log->tambah($i->id, $i->nourut, 'Packing Penerimaan', 'Penerimaan Packing', $userId);
			}
			$this->db->query("update ppic_packing set isActive=1 where idPpicPacking='$idPpicPacking' and idWorksheet='$idWorksheet'");
			
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di ');
			redirect($aplikasi,'refresh');
		}
	}
}