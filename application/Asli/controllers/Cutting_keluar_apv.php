<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cutting_keluar_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_cutting'); 
		$this->load->model('m_log'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'cutting_keluar_apv';
		$data['title']		= 'Approve Keluar Cutting';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_cutting->show_cutting_keluar_input_apv();
		$this->template->load('role','isi','cutting/v_cutting_keluar_apv',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'cutting_keluar_apv';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idCutting	= addslashes($this->input->post('idCutting'));
		$status 			= addslashes($this->input->post('status'));
		$keterangan 		= addslashes($this->input->post('keterangan'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			if($status ==1)
			{
				$this->db->query("update cutting_out_sm set status='Approve', isActive = 1 where idCutting='$idCutting'");
				$xStatus = 'Tambah';	
			} else {
				$this->db->query("update cutting_out_sm set status='Reject', isActive = 0 where idCutting='$idCutting'");
				$xStatus = 'Hapus'; 
			}
			$data = array(
				'idCutting'=>$idCutting,
				'isStatus'=>$status,
				'keterangan'=>$keterangan,
				'userId'=>$userId,
			);
			$this->m_cutting->tambah_cutting_keluar_apv($data);

			$hist = $this->db->query("select * from cutting_out where idCutting='$idCutting'")->result();
			foreach($hist as $h):
			$this->m_log->tambah($h->id, $h->nourut, 'Cutting Out Apv', 'Approve Outs Cutting', $userId);
		endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di '.$xStatus);
			redirect($aplikasi,'refresh');
		}
	}
}