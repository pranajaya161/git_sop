<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppic_triming extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_ppic'); 
		$this->load->model('m_log'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'ppic_triming';
		$data['title']		= 'PPIC Triming';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_ppic->show_ppic_triming();
		$this->template->load('role','isi','ppic/v_ppic_triming',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'ppic_triming';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$keterangan			= addslashes($this->input->post('keterangan'));
		$line				= addslashes($this->input->post('line'));
		$idSewing			= addslashes($this->input->post('idSewing'));
		$refNo				= addslashes($this->input->post('refNo'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'idPpicTriming'=>0,
				'idWorksheet'=>$idWorksheet,
				'keterangan'=>$keterangan,
				'refNo'=>$refNo,
				'userId'=>$userId
			);	
			$this->m_ppic->tambah_triming($data);
			$this->db->query("update sewing_out_sm set isActive =9 where idSewing='$idSewing' and isActive=1");
			$hist = $this->db->query("select * from sewing_out where idSewing='$idSewing'")->result();
			foreach($hist as $h):
				$this->m_log->tambah($h->id, $h->nourut, 'PPIC Triming Approve', $keterangan, $userId);
			endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di ');
			redirect($aplikasi,'refresh');
		}
	}
	
}