<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cutting_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_cutting'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'cutting_input';
		$data['title']		= 'Input Cutting';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_cutting->show_cutting_input();
		$this->template->load('role','isi','cutting/v_cutting_input',$data);
	}
	public function detail()
	{
		$aplikasi 			= 'cutting_input/detail/'.$this->uri->segment(3);
		$data['title']		= 'Input Cutting Detail';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_cutting->show_cutting_input_id($this->uri->segment(3));
		$this->template->load('role','isi','cutting/v_cutting_input_detail',$data);
	}
	public function tambah_det()
	{
		$aplikasi 			= 'cutting_input/detail/'.addslashes($this->input->post('idWorksheet'));
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$dari				= addslashes($this->input->post('dari'));
		$sampai				= addslashes($this->input->post('sampai'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$lihat=$this->db->query("select a.* from edp_assortment_pros a inner join md_worksheet_dt b on b.id = a.id left join cutting_in c on c.id = a.id where b.idWorksheet='$idWorksheet' and a.nourut between '$dari' and '$sampai' and c.id is null")->result();
			foreach($lihat as $a){
				$id 	= $a->id;
				$nourut = $a->nourut;
				$cek	=	$this->db->query("select count(*) as nom from cutting_in where cutting_in.id = '$id'")->row();
				if($cek->nom==0)
				{
					$data=array(
						'idWorksheet'=>$idWorksheet,
						'id'=>$id,
						'nourut'=>$nourut,
						'status'=>'Input',
						'userId'=>$userId,
						'isActive'=>0
					);
					$this->db->insert('cutting_in',$data);
				}
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus()
	{
		$aplikasi 			= 'cutting_input/detail/'.addslashes($this->uri->segment(3));
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->uri->segment(3));
		$id 				= $this->uri->segment(4);
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$this->db->query("delete from cutting_in where idWorksheet='$idWorksheet' and id='$id'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
		
}