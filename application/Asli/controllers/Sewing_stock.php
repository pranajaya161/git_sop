<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sewing_stock extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_sewing'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'sewing_stock';
		$data['title']		= 'Stock Input Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_sewing->show_sewing_input_stock();
		$this->template->load('role','isi','sewing/v_sewing_stock',$data);
	}
		
}