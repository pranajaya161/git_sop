<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purc_pend_apv extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_purchashing');
	}
	public function index()
	{
		$aplikasi 			= 'purc_pend_apv';
		$idDept				= 4;
		$data['title']		= getDept($idDept).' Approve Permintaan Purchasing';
		$data['aplikasi'] 	= $aplikasi;
		$data['idDept']		= $idDept;
		$data['data']		= $this->m_purchashing->show_pend_input_id_apv($idDept);
		$this->template->load('role','isi','purchasing/v_purch_pend_input_apv',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'purc_pend_apv';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPurcPermintaan	= addslashes($this->input->post('idPurcPermintaan'));
		$status 			= addslashes($this->input->post('status'));
		$keterangan 		= addslashes($this->input->post('keterangan'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			if($status ==1)
			{
				$this->db->query("update purc_permintaan_sm set status='Approve', isActive = 1 where idPurcPermintaan='$idPurcPermintaan'");
				$xStatus = 'Tambah';	
			} else {
				$this->db->query("update purc_permintaan_sm set status='Reject', isActive = 0 where idPurcPermintaan='$idPurcPermintaan'");
				$xStatus = 'Hapus'; 
			}
			$data = array(
				'idPurcPermintaan'=>$idPurcPermintaan,
				'isStatus'=>$status,
				'keterangan'=>$keterangan,
				'userId'=>$userId,
			);
			$this->m_purchashing->tambah_approve($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di '.$xStatus);
			redirect($aplikasi,'refresh');
		}
	}

}