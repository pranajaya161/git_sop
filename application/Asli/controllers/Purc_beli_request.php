<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purc_beli_request extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_purchashing'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'purc_beli_request';
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_purchashing->show_purchasing();
		$this->template->load('role','isi','purchasing/v_purc_beli_request',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'purc_beli_request';
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$request  			= addslashes($this->input->post('request'));
		if($request=='Manual')
		{
			$kode 	= 'PO-MNL';
		} else {
			$kode 	= 'PO-REQ';
		}
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'idPurcPembelian'=>0,
				'refNo'=>getRefPembelian($kode, $bulan, $tahun),
				'request'=>addslashes($this->input->post('request')),
				'idSupplier'=>addslashes($this->input->post('idSupplier')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>'Open',
				'userId'=>$userId,
				'isActive'=>0
			);
			$this->m_purchashing->tambah_pembelian($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$aplikasi 			= 'purc_beli_request';
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'idSupplier'=>addslashes($this->input->post('idSupplier')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>'Open',
				'userId'=>$userId,
			);
			$this->m_purchashing->edit_pembelian(addslashes($this->input->post('idPurcPembelian')), $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function send()
	{
		$aplikasi 			= 'purc_beli_request';
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPurcPembelian	= addslashes($this->input->post('idPurcPembelian'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$this->db->query("update purc_pembelian_sm set status='Send' where idPurcPembelian='$idPurcPembelian'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Kirim');
			redirect($aplikasi,'refresh');
		}
	}
	//===== Detail
	public function detail()
	{
		$aplikasi 			= 'purc_beli_request/detail/'.$this->uri->segment(3);
		$idDept				= 4;
		$data['title']		= 'Detail Permintaan Purchasing';
		$data['idPurcPembelian'] = $this->uri->segment(3);
		$this->template->load('role','isi','purchasing/v_purc_beli_request_detail',$data);
	}
	public function tambah_det()
	{
		$aplikasi 			= 'purc_beli_request/detail/'.addslashes($this->input->post('idPurcPembelian'));
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'idPurcPembelian'=>addslashes($this->input->post('idPurcPembelian')),
				'idMaterialist'=>0,
				'idStyle'=>0,
				'idBahan'=>addslashes($this->input->post('idBahan')),
				'qty'=>addslashes($this->input->post('qty')),
				'satuan'=>addslashes(getSatuan(getBahanSatuan($this->input->post('idBahan')))),
				'userId'=>$userId
			);
			$this->m_purchashing->tambah_pembelian_detail_manual($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus()
	{
		$aplikasi 			= 'purc_beli_request/detail/'.$this->uri->segment(4);
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$id   				= $this->uri->segment(3);
		$idPurcPembelian	= $this->uri->segment(4);
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$this->db->query("delete from purc_pembelian_dt where idPurcPembelian ='$idPurcPembelian' and id='$id'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	// detail materialist
	public function add_detail()
	{
		$aplikasi 			= 'purc_beli_request/detail/'.addslashes($this->input->post('idPurcPembelian'));
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPurcPembelian	= addslashes($this->input->post('idPurcPembelian'));
		$idMaterialist		= addslashes($this->input->post('idMaterialist'));
		$idStyle			= $this->input->post('idStyle');
		$idBahan			= $this->input->post('idBahan');
		$qty				= $this->input->post('qty');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$jumlah=count($idStyle);
			for($i=0;$i<$jumlah;$i++)
			{
				$data = array(
					'idMaterialist'=>$idMaterialist,
					'idPurcPembelian'=>$idPurcPembelian,
					'idStyle'=>$idStyle[$i],
					'idBahan'=>$idBahan[$i],
					'qty'=>$qty[$i],
					'satuan'=>addslashes(getSatuan(getBahanSatuan($idBahan[$i]))),
					'userId'=>$userId
				);
				$this->m_purchashing->tambah_det_work($data);
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
}