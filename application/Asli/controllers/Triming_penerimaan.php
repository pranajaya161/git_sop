<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Triming_penerimaan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_triming'); 
		$this->load->model('m_log'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'triming_penerimaan';
		$data['title']		= 'Penerimaan Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_triming->show_triming_penerimaan();
		$this->template->load('role','isi','triming/v_triming_penerimaan',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'triming_penerimaan';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$id 				= $this->input->post('id');
		$nourut				= $this->input->post('nourut');
		$idPpicTriming		= $this->input->post('idPpicTriming');
		$idWorksheet		= $this->input->post('idWorksheet');
		$line				= $this->input->post('line');
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$jumlah = count($id);
			for($i=0;$i<$jumlah;$i++)
			{
				$data = array(
					'idPpicTriming'=>$idPpicTriming,
					'idWorksheet'=>$idWorksheet,
					'id'=>$id[$i],
					'nourut'=>$nourut[$i],
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_triming->tambah_penerimaan($data);
				$h = $this->db->query("select * from sewing_out where id='$id[$i]'")->row();
				$this->m_log->tambah($h->id, $h->nourut, 'Triming Penerimaan', 'Penerimaan Triming', $userId);
			}
			$this->db->query("update ppic_triming set isActive=1 where idPpicTriming='$idPpicTriming' and idWorksheet='$idWorksheet'");
			
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di ');
			redirect($aplikasi,'refresh');
		}
	}
	
}