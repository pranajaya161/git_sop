<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Md_apvworksheet extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_md');
		$this->load->model('m_log');
	}
	public function index()
	{
		$aplikasi 			= 'md_apvworksheet';
		$data['title']		= 'MD Approve WorkSheet';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_md->show_worksheet_apv();
		$this->template->load('role','isi','md/v_md_inputworksheet_apv',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'md_apvworksheet';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$status 			= addslashes($this->input->post('status'));
		$keterangan 		= addslashes($this->input->post('keterangan'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			if($status ==1)
			{
				$this->db->query("update md_worksheet set status='Approve', isActive = 1 where idWorksheet='$idWorksheet'");
				$cek = $this->db->query("select id from md_worksheet_dt where idWorksheet='$idWorksheet'")->result();
				foreach($cek as $a){
					$idNum = $a->id;
					$this->db->query("update barcode set idWorksheet='$idWorksheet', idMaterialist=1, idDept =4, isActive=1 where id ='$idNum'");
				}
				$xStatus = 'Tambah';	
			} else {
				$this->db->query("update md_worksheet set status='Reject', isActive = 0 where idWorksheet='$idWorksheet'");
				$xStatus = 'Hapus'; 
			}
			$data = array(
				'idWorksheet'=>$idWorksheet,
				'isStatus'=>$status,
				'keterangan'=>$keterangan,
				'userId'=>$userId,
			);
			$this->m_md->tambah_apv_worksheet($data);
			$hist = $this->db->query("select * from md_worksheet_dt where idWorksheet='$idWorksheet'")->result();
			foreach($hist as $h):
				$this->m_log->tambah($h->id, $h->nourut, 'WorkSheet Approve', $keterangan, $userId);
			endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di '.$xStatus);
			redirect($aplikasi,'refresh');
		}
	}
}