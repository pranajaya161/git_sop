<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Md_lap_material extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_md');
	}
	public function index()
	{
		$aplikasi 			= 'md_inputmaterial';
		$data['title']		= 'MD Input Materialist';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_md->show_materialist_lap();
		$this->template->load('role','isi','md/v_md_inputmaterial_lap',$data);
	}
	
}