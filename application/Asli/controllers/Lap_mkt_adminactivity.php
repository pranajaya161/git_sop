<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lap_mkt_adminactivity extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_prospecting');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'Lap_mkt_adminactivity';
		$data['title']		= 'Laporan : Admin Activity';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_prospecting->show_admin_act();
		$this->template->load('role','isi','prospecting/v_pros_admin',$data);
	}
	
}