<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppic_sewing extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_ppic'); 
		$this->load->model('m_log'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'ppic_sewing';
		$data['title']		= 'PPIC Sewing';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_ppic->show_ppic_sewing();
		$this->template->load('role','isi','ppic/v_ppic_sewing',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'ppic_sewing';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$keterangan			= addslashes($this->input->post('keterangan'));
		$line				= addslashes($this->input->post('line'));
		$idCutting			= addslashes($this->input->post('idCutting'));
		$refNo				= addslashes($this->input->post('refNo'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'idPpicSewing'=>0,
				'idWorksheet'=>$idWorksheet,
				'keterangan'=>$keterangan,
				'line'=>$line,
				'refNo'=>$refNo,
				'userId'=>$userId
			);	
			$this->m_ppic->tambah_sewing($data);
			$this->db->query("update cutting_out_sm set isActive =9 where idCutting='$idCutting' and isActive=1");
			$hist = $this->db->query("select * from cutting_out where idCutting='$idCutting'")->result();
			foreach($hist as $h):
				$this->m_log->tambah($h->id, $h->nourut, 'PPIC Sewing Approve', $keterangan, $userId);
			endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di ');
			redirect($aplikasi,'refresh');
		}
	}
	
}