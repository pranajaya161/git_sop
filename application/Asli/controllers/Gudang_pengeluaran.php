<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gudang_pengeluaran extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_gudang'); 
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
	}
	public function index()
	{
		$aplikasi 			= 'gudang_pengeluaran';
		$data['title']		= 'Gudang Keluar ';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_gudang->show_gudang_keluar();
		$this->template->load('role','isi','gudang/v_gudang_keluar',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'gudang_pengeluaran';
		$data['title']		= 'Gudang Keluar ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		$request  			= addslashes($this->input->post('request'));
		$kode 				= 'GKL';
		$this->db->trans_begin();
			$data = array(
				'idGudangKeluar'=>0,
				'refNo'=>getRefGudangKeluar($kode, $bulan, $tahun),
				'idPo'=>addslashes($this->input->post('idPo')),
				'idDept'=>addslashes($this->input->post('idDept')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>'Open',
				'userId'=>$userId,
				'isActive'=>0
			);
			$this->m_gudang->tambah_keluar($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$aplikasi 			= 'gudang_pengeluaran';
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'idSupplier'=>addslashes($this->input->post('idSupplier')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>'Open',
				'userId'=>$userId,
			);
			$this->m_purchashing->edit_pembelian(addslashes($this->input->post('idPurcPembelian')), $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function send()
	{
		$aplikasi 			= 'gudang_pengeluaran';
		$data['title']		= 'Keluar Stock ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idGudangKeluar		= addslashes($this->input->post('idGudangKeluar'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$this->db->query("update gudang_keluar set status='Send' where idGudangKeluar='$idGudangKeluar'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Kirim');
			redirect($aplikasi,'refresh');
		}
	}
	//===== Detail
	public function detail()
	{
		$aplikasi 			= 'gudang_pengeluaran/detail/'.$this->uri->segment(3);
		$idDept				= 4;
		$data['title']		= 'Detail Permintaan Gudang Keluar';
		$data['idGudangKeluar'] = $this->uri->segment(3);
		$this->template->load('role','isi','gudang/v_gudang_keluar_detail',$data);
	}
	public function tambah_det()
	{
		$aplikasi 			= 'gudang_pengeluaran/detail/'.addslashes($this->input->post('idGudangKeluar'));
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$this->db->trans_begin();
			$data = array(
				'idGudangKeluar'=>addslashes($this->input->post('idGudangKeluar')),
				'idBahan'=>addslashes($this->input->post('idBahan')),
				'qty'=>addslashes($this->input->post('qty')),
				'isActive'=>1
			);
			$this->m_gudang->tambah_gudang_keluar($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus()
	{
		$aplikasi 			= 'gudang_pengeluaran/detail/'.$this->uri->segment(4);
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$id   				= $this->uri->segment(3);
		$idPurcPembelian	= $this->uri->segment(4);
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$this->db->query("delete from purc_pembelian_dt where idPurcPembelian ='$idPurcPembelian' and id='$id'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	// detail materialist
	public function add_detail()
	{
		$aplikasi 			= 'gudang_pengeluaran/detail/'.addslashes($this->input->post('idPurcPembelian'));
		$data['title']		= 'Pembelian Supplier ';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPurcPembelian	= addslashes($this->input->post('idPurcPembelian'));
		$idMaterialist		= addslashes($this->input->post('idMaterialist'));
		$idStyle			= $this->input->post('idStyle');
		$idBahan			= $this->input->post('idBahan');
		$qty				= $this->input->post('qty');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$jumlah=count($idStyle);
			for($i=0;$i<$jumlah;$i++)
			{
				$data = array(
					'idMaterialist'=>$idMaterialist,
					'idPurcPembelian'=>$idPurcPembelian,
					'idStyle'=>$idStyle[$i],
					'idBahan'=>$idBahan[$i],
					'qty'=>$qty[$i],
					'satuan'=>addslashes(getSatuan(getBahanSatuan($idBahan[$i]))),
					'userId'=>$userId
				);
				$this->m_purchashing->tambah_det_work($data);
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error , Silahkan coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
}