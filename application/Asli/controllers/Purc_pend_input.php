<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purc_pend_input extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_purchashing');
	}
	public function index()
	{
		$aplikasi 			= 'purc_pend_input';
		$idDept				= 4;
		$data['title']		= getDept($idDept).' Permintaan Purchasing';
		$data['aplikasi'] 	= $aplikasi;
		$data['idDept']		= $idDept;
		$data['data']		= $this->m_purchashing->show_pend_input_id($idDept);
		$this->template->load('role','isi','purchasing/v_purch_pend_input',$data);
	}
	public function tambah()
	{
		$aplikasi 			= 'purc_pend_input';
		$idDept				= 4;
		$data['title']		= getDept($idDept).' Permintaan Purchasing';
		$data['aplikasi'] 	= $aplikasi;
		$data['idDept']		= $idDept;
		$userId				= addslashes($this->session->userdata('userId'));
		$kode				= 'PO-'.addslashes(getDept($this->input->post('idDept')));
		$bulan 				= date('m');
		$tahun 				= date('Y');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'idPurcPermintaan'=>0,
				'refNo'=>getRefPermintaanDept($kode, $bulan, $tahun),
				'idDept'=>addslashes($this->input->post('idDept')),
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>'Open',
				'userId'=>$userId,
				'isActive'=>0
			);
			$this->m_purchashing->tambah_dept($data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function edit()
	{
		$aplikasi 			= 'purc_pend_input';
		$idDept				= 4;
		$data['title']		= getDept($idDept).' Permintaan Purchasing';
		$data['aplikasi'] 	= $aplikasi;
		$data['idDept']		= $idDept;
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$data = array(
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'userId'=>$userId,
				'tgl_proses'=>date('Y-m-d H:i:s')
			);
			$this->m_purchashing->edit_dept($this->input->post('idPurcPermintaan'), $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Ubah');
			redirect($aplikasi,'refresh');
		}
	}
	public function send()
	{
		$aplikasi 			= 'purc_pend_input';
		$idDept				= 4;
		$data['title']		= getDept($idDept).' Permintaan Purchasing';
		$data['aplikasi'] 	= $aplikasi;
		$data['idDept']		= $idDept;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPurcPermintaan   = addslashes($this->input->post('idPurcPermintaan'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$this->db->query("update purc_permintaan_sm set status='Send' where idPurcPermintaan='$idPurcPermintaan'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Ubah');
			redirect($aplikasi,'refresh');
		}
		
	}
	//===== Detail
	public function detail()
	{
		$aplikasi 			= 'purc_pend_input/detail/'.$this->uri->segment(3);
		$idDept				= 4;
		$data['title']		= getDept($idDept).' Permintaan Purchasing';
		$data['idDept']		= $idDept;
		$data['idPurcPermintaan'] = $this->uri->segment(3);
		$this->template->load('role','isi','purchasing/v_purch_pend_input_detail',$data);
	}
	public function add_detail()
	{
		$aplikasi 			= 'purc_pend_input/detail/'.$this->input->post('idPurcPermintaan');
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idPurcPermintaan	= addslashes($this->input->post('idPurcPermintaan'));
		$idMaterialist		= addslashes($this->input->post('idMaterialist'));
		$idStyle			= $this->input->post('idStyle');
		$idBahan			= $this->input->post('idBahan');
		$pcs		 		= $this->input->post('pcs');
		$satuan				= $this->input->post('satuan');
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$jumlah=count($idBahan);
			for($i=0;$i<$jumlah;$i++)
			{
				$data = array(
					'idPurcPermintaan'=>$idPurcPermintaan,
					'idMaterialist'=>$idMaterialist,
					'idStyle'=>$idStyle[$i],
					'idBahan'=>$idBahan[$i],
					'total'=>$pcs[$i],
					'satuan'=>$satuan[$i],
					'userId'=>$userId,
					'isActive'=>0
				);
				$this->m_purchashing->tambah_detail_purc($data);
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan Coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
	public function hapus()
	{
		$aplikasi 			= 'purc_pend_input/detail/'.$this->uri->segment(3);
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		if(empty($userId))
		{
			$userId = 'System';
		}
		$this->db->trans_begin();
			$id = $this->uri->segment(3);
			$this->db->query("delete from purc_permintaan_dt where id='$id'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan Coba kembali');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di tambah');
			redirect($aplikasi,'refresh');
		}
	}
}