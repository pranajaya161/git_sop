<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppic_cutting extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_ppic');
		$this->load->model('m_log'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'ppic_cutting';
		$data['title']		= 'PPIC Cutting';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_ppic->show_ppic_cutting();
		$this->template->load('role','isi','ppic/v_ppic_cutting',$data);
	}
	public function approve()
	{
		$aplikasi 			= 'ppic_cutting';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idWorksheet		= addslashes($this->input->post('idWorksheet'));
		$keterangan			= addslashes($this->input->post('keterangan'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$this->db->trans_begin();
			$data = array(
				'idPpicCutting'=>0,
				'idWorksheet'=>$idWorksheet,
				'keterangan'=>$keterangan,
				'userId'=>$userId
			);	
			$this->m_ppic->tambah($data);
			$this->db->query("update md_worksheet set status='Cutting' , isActive =9 where idWorksheet='$idWorksheet' and isActive=1");
			$hist = $this->db->query("select * from md_worksheet_dt where idWorksheet='$idWorksheet'")->result();
			foreach($hist as $h):
				$this->m_log->tambah($h->id, $h->nourut, 'PPIC Cutting Approve', $keterangan, $userId);
			endforeach;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('pesan','Error Silahkan coba kembali ');
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di ');
			redirect($aplikasi,'refresh');
		}
	}
	
}