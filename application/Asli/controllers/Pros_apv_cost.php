<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pros_apv_cost extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_prospecting');
		$this->load->model('m_parameter');
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'pros_apv_cost';
		$data['title']		= 'Marketing : Approval Cost Calculation';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_prospecting->show_upl_cost_apv();
		$this->template->load('role','isi','prospecting/v_upl_cost_apv',$data);
	}
	public function Approval()
	{
		$aplikasi 			= 'pros_apv_cost';
		$data['aplikasi'] 	= $aplikasi;
		$userId				= addslashes($this->session->userdata('userId'));
		$idProspecting		= addslashes($this->input->post('idProspecting'));
		if(empty($userId))
		{
			$userId	= 'Guest';
		}
		$status = addslashes($this->input->post('status'));
		$this->db->trans_begin();
			$data = array(
				'idProspecting'=>$idProspecting,
				'keterangan'=>addslashes($this->input->post('keterangan')),
				'status'=>addslashes($this->input->post('status')),
				'userId'=>$userId
			);
			$this->m_prospecting->tambah_cost_apv($data);
			$this->db->query("update mkt_pros_cost set isActive = '$status' where idProspecting='$idProspecting'");
			$this->db->query("update mkt_pros_sm set isCostCalc = '$status' where idProspecting='$idProspecting'");
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-error\">".$this->upload->display_errors()."</div>");
			redirect($aplikasi,'refresh');
		} else {
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan','Data berhasil di Kirim');
			redirect($aplikasi,'refresh');
		}
	}
	
}