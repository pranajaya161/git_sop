<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_bahan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_gudang'); 
		/*
		if($this->session->userdata('isLog')==FALSE){
		  redirect('login','refresh');
		 }
		 */
	}
	public function index()
	{
		$aplikasi 			= 'stock_bahan';
		$data['title']		= 'Stock Bahan ';
		$data['aplikasi'] 	= $aplikasi;
		$data['data']		= $this->m_gudang->show_stock_bahan();
		$this->template->load('role','isi','gudang/v_stock_bahan',$data);
	}
	
	
}