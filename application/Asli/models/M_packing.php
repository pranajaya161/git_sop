<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_packing extends CI_Model {
	//=== Penerimaan
	public function show_packing_penerimaan()
	{
		$this->db->where('isActive', 0);
		$data = $this->db->get("ppic_packing");
		return $data->result();
	}
	public function tambah_penerimaan($data)
	{
		$tambah=$this->db->insert('packing_stock01',$data);
		return $tambah;
	}
	//==== Stock
	public function show_packing_stock_penerimaan()
	{
		$this->db->select('a.*, b.nik, b.nama, b.jabatan');
		$this->db->where('a.isActive', 0);
		$this->db->join('edp_assortment_pros b','b.id = a.id','inner');
		$data = $this->db->get("packing_stock01 a");
		return $data->result();
	}
	//=== Input 
	public function show_packing_input()
	{
		$this->db->select('idPpicPacking, idWorksheet, max(tgl_proses) as tgl');
		$this->db->where('isActive', 0);
		$this->db->group_by('idPpicPacking');
		$data = $this->db->get("packing_stock01");
		return $data->result();
	}
	public function show_packing_input_id($id)
	{
		$this->db->select('idPpicPacking, idWorksheet,  max(tgl_proses) as tgl');
		$this->db->where('isActive', 0);
		$this->db->where('idPpicPacking', $id);
		$this->db->group_by('idPpicPacking');
		$data = $this->db->get("packing_stock01");
		return $data->result();
	}
	public function tambah_packing_input($data)
	{
		$tambah=$this->db->insert('packing_in',$data);
		return $tambah;
	}
	//== Approval
	public function show_packing_input_apv()
	{
		$this->db->where('a.isActive', 0);
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt c', 'c.idWorksheet = a.idWorksheet and c.nourut = a.nourut and a.id = c.id', 'inner');
		$data = $this->db->get('packing_in a');
		return $data->result();
	}
	//==== Stock
	public function show_packing_input_stock()
	{
		$this->db->select('*, a.id, x.nik, x.nama, x.jabatan');
		$this->db->where('a.isActive', 1);
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('edp_assortment_pros x', 'x.id = a.id', 'inner');
		$this->db->join('md_worksheet_dt c', 'c.idWorksheet = a.idWorksheet and c.nourut = a.nourut and a.id = c.id', 'inner');
		$data = $this->db->get('packing_in a');
		return $data->result();
	}
	//=== Keluar
	public function show_packing_keluar_input()
	{
		$status = array('Open','Reject');
		$this->db->where_in('status', $status);
		$data = $this->db->get('packing_out_sm a');
		return $data->result();
	}
	public function tambah_keluar($data)
	{
		$tambah=$this->db->insert('packing_out_sm',$data);
		return $tambah;
	}
	public function show_packing_keluar_input_id($id)
	{
		$this->db->where('idPacking', $id);
		$data = $this->db->get('packing_out_sm a');
		return $data->result();
	}
}
