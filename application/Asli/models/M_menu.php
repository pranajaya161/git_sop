<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_menu extends CI_Model {
	public function fetch_data()
	{
		$this->db->order_by('headMenu','ASC');
		$this->db->order_by('subMenu','ASC'); 
		$this->db->order_by('childMenu','ASC'); 
		$this->db->order_by('nourut','ASC'); 
		$data = $this->db->get("tblmenu_adm");
		return $data->result();
 
	}
	function record_count()
	{
		$this->db->select('*'); 
		$this->db->from('tblmenu_adm');
		$query = $this->db->get();
		return $query->num_rows();
	}
	function showMenu($id)
	{
		$this->db->select('*'); 
		$this->db->where('idMenu', $id);
		$this->db->from('tblmenu_adm');
		return $this->db->get();
	}
	function showHead()
	{
		$this->db->order_by('idMenu','ASC');
		$this->db->where('headMenu >', 0);
		$data = $this->db->get("tblmenu_adm");
		return $data->result();
	}
	function cekNomHead()
	{
		$this->db->select('max(idMenu) as nom'); 
		$this->db->from('tblmenu_adm');
		return $this->db->get();
	}
	function cekNomChild($idMenu, $idSubMenu)
	{
		$this->db->select('max(childMenu) as nom'); 
		$this->db->where('idMenu', $idMenu);
		$this->db->where('subMenu', $idSubMenu);
		$this->db->from('tblmenu_adm');
		return $this->db->get();
	}
	function add($data)
	{
		$tambah=$this->db->insert('tblmenu_adm',$data);
		return $tambah;
	}
	function update($data, $id)
	{
		$this->db->where('idMenu',$id);
		$update=$this->db->update('tblmenu_adm',$data);
		return $update;
	}
}
