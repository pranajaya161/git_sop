<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_sewing extends CI_Model {
	//=== Cutting
	public function show_sewing_penerimaan()
	{
		$this->db->where('isActive', 0);
		$data = $this->db->get("ppic_sewing");
		return $data->result();
	}
	public function tambah_penerimaan($data)
	{
		$tambah=$this->db->insert('sewing_stock01',$data);
		return $tambah;
	}
	//== Stock Sewing Penerimaan
	public function show_sewing_stock_penerimaan()
	{
		$this->db->where('isActive', 0);
		$data = $this->db->get("sewing_stock01");
		return $data->result();
	}
	//=== Input Sewing
	public function show_sewing_input()
	{
		$this->db->select('idPpicSewing, idWorksheet, line, max(tgl_proses) as tgl');
		$this->db->where('isActive', 0);
		$this->db->group_by('idPpicSewing');
		$data = $this->db->get("sewing_stock01");
		return $data->result();
	}
	public function show_sewing_input_id($id)
	{
		$this->db->select('idPpicSewing, idWorksheet, line, max(tgl_proses) as tgl');
		$this->db->where('isActive', 0);
		$this->db->where('idPpicSewing', $id);
		$this->db->group_by('idPpicSewing');
		$data = $this->db->get("sewing_stock01");
		return $data->result();
	}
	public function tambah_sewing_input($data)
	{
		$tambah=$this->db->insert('sewing_in',$data);
		return $tambah;
	}
	//== Approval
	public function show_sewing_input_apv()
	{
		$this->db->where('a.isActive', 0);
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt c', 'c.idWorksheet = a.idWorksheet and c.nourut = a.nourut and a.id = c.id', 'inner');
		$data = $this->db->get('sewing_in a');
		return $data->result();
	}
	//==== Stock
	public function show_sewing_input_stock()
	{
		$this->db->select('*, a.id');
		$this->db->where('a.isActive', 1);
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt c', 'c.idWorksheet = a.idWorksheet and c.nourut = a.nourut and a.id = c.id', 'inner');
		$data = $this->db->get('sewing_in a');
		return $data->result();
	}
	//=== Sewing Keluar
	public function show_sewing_keluar_input()
	{
		$status = array('Open','Reject');
		$this->db->where_in('status', $status);
		$data = $this->db->get('sewing_out_sm a');
		return $data->result();
	}
	public function tambah_keluar($data)
	{
		$tambah=$this->db->insert('sewing_out_sm',$data);
		return $tambah;
	}
	public function show_sewing_keluar_input_id($id)
	{
		$this->db->where('idSewing', $id);
		$data = $this->db->get('sewing_out_sm a');
		return $data->result();
	}
	public function tambah_keluar_detail($data)
	{
		$tambah=$this->db->insert('sewing_out',$data);
		return $tambah;
	}
	// === Approve
	public function show_sewing_keluar_input_apv()
	{
		$status = array('Send');
		$this->db->where_in('status', $status);
		$data = $this->db->get('sewing_out_sm a');
		return $data->result();
	}
	public function tambah_sewing_keluar_apv($data)
	{
		$tambah=$this->db->insert('sewing_out_apv',$data);
		return $tambah;
	}
	//== Stock Keluar Sewing
	public function show_sewing_keluar_stock()
	{
		//$this->db->where('a.isActive', 1);
		$this->db->join('cutting_in b', 'b.id = a.id and b.nourut = a.nourut', 'inner');
		$this->db->join('md_worksheet c', 'c.idWorksheet = b.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt d', 'd.idWorksheet = b.idWorksheet and d.nourut = b.nourut and d.id = b.id', 'inner');
		$data = $this->db->get('sewing_out a');
		return $data->result();
	}
}
