<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_purchashing extends CI_Model {
	
	public function show_pend_input_id($id)
	{
		$status = array('Open','Reject');
		$this->db->where_in('status', $status);
		$this->db->where('idDept', $id);
		$data = $this->db->get("purc_permintaan_sm");
		return $data->result();
	}
	public function tambah_dept($data)
	{
		$tambah=$this->db->insert('purc_permintaan_sm',$data);
		return $tambah;
	}
	public function edit_dept($id, $data)
	{
		$this->db->where('idPurcPermintaan',$id);
		$update=$this->db->update('purc_permintaan_sm',$data);
		return $update;
	}
	public function tambah_detail_purc($data)
	{
		$tambah=$this->db->insert('purc_permintaan_dt',$data);
		return $tambah;
	}
	//=== Approve
	public function show_pend_input_id_apv($id)
	{
		$this->db->where('status', 'Send');
		$this->db->where('idDept', $id);
		$data = $this->db->get("purc_permintaan_sm");
		return $data->result();
	}
	public function show_pend_input_id_lap($id)
	{
		$this->db->where('idDept', $id);
		$data = $this->db->get("purc_permintaan_sm");
		return $data->result();
	}
	public function tambah_approve($data)
	{
		$tambah=$this->db->insert('purc_permintaan_apv',$data);
		return $tambah;
	}
	//=== pembelian Purchasing
	public function show_purchasing()
	{
		$status = array('Open','Reject');
		$this->db->where_in('status', $status);
		$data = $this->db->get("purc_pembelian_sm");
		return $data->result();
	}
	public function tambah_pembelian($data)
	{
		$tambah=$this->db->insert('purc_pembelian_sm',$data);
		return $tambah;
	}
	public function edit_pembelian($id, $data)
	{
		$this->db->where('idPurcPembelian',$id);
		$update=$this->db->update('purc_pembelian_sm',$data);
		return $update;
	}
	public function tambah_pembelian_detail_manual($data)
	{
		$tambah=$this->db->insert('purc_pembelian_dt',$data);
		return $tambah;
	}
	public function tambah_det_work($data)
	{
		$tambah=$this->db->insert('purc_pembelian_dt',$data);
		return $tambah;
	}
	//== Approval 
	public function show_purchasing_apv()
	{
		return $this->db->query("select a.*, qry.idMaterialist, c.refNo as ref from purc_pembelian_sm a inner join (select idPurcPembelian, max(idMaterialist) as idMaterialist from purc_pembelian_dt GROUP by idMaterialist) qry on qry.idPurcPembelian = a.idPurcPembelian inner join md_materialist c on c.idMaterialist = qry.idMaterialist")->result();
		/*
		$this->db->where_in('status', 'Send');
		$this->db->join('pur')
		$data = $this->db->get("purc_pembelian_sm a");
		return $data->result();
		*/
	}
	public function tambah_approve_beli($data)
	{
		$tambah=$this->db->insert('purc_pembelian_apv',$data);
		return $tambah;
	}
	public function show_purchasing_lap()
	{
		$data = $this->db->get("purc_pembelian_sm");
		return $data->result();
	}
}
