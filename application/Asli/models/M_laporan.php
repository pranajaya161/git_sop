<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_laporan extends CI_Model {
	public function show_prospecting()
	{
		$this->db->order_by('idProspecting','ASC');
		$data = $this->db->get("mkt_pros_sm");
		return $data->result();
	}
	public function show_purchaseorder()
	{
		$this->db->order_by('idPo','ASC');
		$data = $this->db->get("mkt_po_sm");
		return $data->result();
	}
	
}
