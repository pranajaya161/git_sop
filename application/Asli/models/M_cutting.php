<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_cutting extends CI_Model {
	//=== Cutting
	public function show_cutting_input()
	{
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$data = $this->db->get('ppic_cutting a');
		return $data->result();
	}
	public function show_cutting_input_id($id)
	{
		$this->db->where('a.idWorksheet', $id);
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$data = $this->db->get('ppic_cutting a');
		return $data->result();
	}
	public function tambah_detail($data)
	{
		$tambah=$this->db->insert('cutting_in',$data);
		return $tambah;
	}
	//== Apv Cutting
	public function show_cutting_input_apv()
	{
		$this->db->where('a.isActive', 0);
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt c', 'c.idWorksheet = a.idWorksheet and c.nourut = a.nourut and a.id = c.id', 'inner');
		$data = $this->db->get('cutting_in a');
		return $data->result();
	}
	// Stock Cutting
	public function show_cutting_input_stock()
	{
		$this->db->select('*, a.id');
		$this->db->where('a.isActive', 1);
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt c', 'c.idWorksheet = a.idWorksheet and c.nourut = a.nourut and a.id = c.id', 'inner');
		$data = $this->db->get('cutting_in a');
		return $data->result();
	}
	public function show_cutting_keluar_stock()
	{
		//$this->db->where('a.isActive', 1);
		$this->db->join('cutting_in b', 'b.id = a.id and b.nourut = a.nourut', 'inner');
		$this->db->join('md_worksheet c', 'c.idWorksheet = b.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt d', 'd.idWorksheet = b.idWorksheet and d.nourut = b.nourut and d.id = b.id', 'inner');
		$data = $this->db->get('cutting_out a');
		return $data->result();
	}
	//=== Cutting Keluar
	public function show_cutting_keluar_input()
	{
		$status = array('Open','Reject');
		$this->db->where_in('status', $status);
		$data = $this->db->get('cutting_out_sm a');
		return $data->result();
	}
	public function tambah_keluar($data)
	{
		$tambah=$this->db->insert('cutting_out_sm',$data);
		return $tambah;
	}
	public function show_cutting_keluar_input_id($id)
	{
		$this->db->where('idCutting', $id);
		$data = $this->db->get('cutting_out_sm a');
		return $data->result();
	}
	public function tambah_keluar_detail($data)
	{
		$tambah=$this->db->insert('cutting_out',$data);
		return $tambah;
	}
	//== Approve Cutting
	public function show_cutting_keluar_input_apv()
	{
		$status = array('Send');
		$this->db->where_in('status', $status);
		$data = $this->db->get('cutting_out_sm a');
		return $data->result();
	}
	public function tambah_cutting_keluar_apv($data)
	{
		$tambah=$this->db->insert('cutting_out_apv',$data);
		return $tambah;
	}
}
