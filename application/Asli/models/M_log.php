<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_log extends CI_Model {

	public function log(){
		$data = array(
			'account_id' => $this->session->userdata('idUser'),
			'project_id' => 'ads.danain.co.id',
			'user_id' => $this->session->userdata('userId'),
			'section' => $this->router->class,
			'action'=>$this->router->method,
			'when'=>time(),
			'uri'=> uri_string(),
			'ip'=>$this->input->ip_address()
		);
       	$this->db->insert('statistics', $data);
	}
	public function tambah($id, $nourut, $ket1, $ket2, $userId)
	{
		$data = array(
			'id'=>$id,
			'nourut'=>$nourut,
			'subject'=>$ket1,
			'keterangan'=>$ket2,
			'userId'=>$userId
		);
		$this->db->insert('hist_numbering', $data);
	}
   
}

