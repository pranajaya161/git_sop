<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_ppic extends CI_Model {
	//=== Cutting
	public function show_ppic_cutting()
	{
		$this->db->where('isActive', 1);
		$data = $this->db->get("md_worksheet");
		return $data->result();
	}
	public function tambah($data)
	{
		$tambah=$this->db->insert('ppic_cutting',$data);
		return $tambah;
	}
	//=== Sewing
	public function show_ppic_sewing()
	{
		$this->db->where('isActive', 1);
		$data = $this->db->get("cutting_out_sm");
		return $data->result();
	}
	public function tambah_sewing($data)
	{
		$tambah=$this->db->insert('ppic_sewing',$data);
		return $tambah;
	}
	//== triming
	public function show_ppic_triming()
	{
		$this->db->where('isActive', 1);
		$data = $this->db->get("sewing_out_sm");
		return $data->result();
	}
	public function tambah_triming($data)
	{
		$tambah=$this->db->insert('ppic_triming',$data);
		return $tambah;
	}
	//=== QC
	public function show_ppic_qc()
	{
		$this->db->where('isActive', 1);
		$data = $this->db->get("triming_out_sm");
		return $data->result();
	}
	public function tambah_qc($data)
	{
		$tambah=$this->db->insert('ppic_qc',$data);
		return $tambah;
	}
	//=== Packing
	public function show_ppic_packing()
	{
		$this->db->where('isActive', 1);
		$data = $this->db->get("qc_out_sm");
		return $data->result();
	}
	public function tambah_packing($data)
	{
		$tambah=$this->db->insert('ppic_packing',$data);
		return $tambah;
	}
}
