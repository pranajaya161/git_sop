<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_worksheet extends CI_Model {
	public function maaping()
	{
		$this->db->where('status', 'style');
		$data = $this->db->get("tblstyle");
		return $data->result();	
	}
	public function maaping_id($id)
	{
		$this->db->where('idStyle', $id);
		$data = $this->db->get("tblstyle");
		return $data->result();
	}
	public function maaping_detail_id($id)
	{
		$this->db->where('idStyle', $id);
		$data = $this->db->get("md_maaping");
		return $data->result();
	}
	function tambah_maaping($data)
	{
		$tambah=$this->db->insert('md_maaping',$data);
		return $tambah;
	}
}
