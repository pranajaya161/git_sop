<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_gudang extends CI_Model {
	
	public function show_penerimaan_supplier()
	{
		$this->db->where_in('status', 'Approve');
		$data = $this->db->get("purc_pembelian_sm");
		return $data->result();
	}
	public function tambah_penerimaan($data)
	{
		$tambah=$this->db->insert('gudang_penerimaan_supplier',$data);
		return $tambah;
	}
	//=== Approve
	public function show_penerimaan_supplier_apv()
	{
		$this->db->where('isActive', 0);
		$data = $this->db->get("gudang_penerimaan_supplier");
		return $data->result();
	}
	public function tambah_hist($data)
	{
		$tambah=$this->db->insert('stock_01_hist',$data);
		return $tambah;
	}
	public function tambah_stock($data)
	{
		$tambah=$this->db->insert('stock_01',$data);
		return $tambah;
	}
	//== Stock
	public function show_stock_bahan()
	{
		$data = $this->db->get("stock_01");
		return $data->result();
	}
	//=== Pengeluaran
	public function show_gudang_keluar()
	{
		$this->db->where('isActive', 0);
		$data = $this->db->get("gudang_keluar");
		return $data->result();
	}
	public function tambah_keluar($data)
	{
		$tambah=$this->db->insert('gudang_keluar',$data);
		return $tambah;
	}
	public function tambah_gudang_keluar($data)
	{
		$tambah=$this->db->insert('gudang_keluar_dt',$data);
		return $tambah;
	}
}
