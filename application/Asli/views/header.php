<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>SOP-Megacorp</title>
		<link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
		<link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
		<link href="<?php echo base_URL()?>vendor/morrisjs/morris.css" rel="stylesheet">
		<link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	</head>
	<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_URL()?>home">IT-danaIN</a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_URL()?>logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <?php $idModul = $this->session->userdata('idModul'); $idUser = $this->session->userdata('idUser'); ?>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="<?php echo base_URL()?>home"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <?php
                          $lihat=$this->db->query("select * from tblmodul_list_adm where subMenu=0 and idModul='$idModul'")->result();
                          foreach($lihat as $a){
                            $idMenu=$a->idMenu;
                          ?>
                        <li class="sub-menu"><a href="#"><i class="fa fa-check fa-fw"></i><?php echo getMenu($a->idMenu)?></span></a>
                            <ul class="nav nav-second-level">
                            <?php
                            $lihat2=$this->db->query("select * from tblmodul_list_adm where headMenu='$idMenu' and idModul='$idModul' and subMenu > 0 and childMenu=0 ")->result();
                            foreach($lihat2 as $b){
                                $headMenu=$b->headMenu;
                                $subMenu=$b->subMenu;
                                $cekLink = getMenuLink($b->idMenu);
                                if($cekLink=='#'){ ?>
                                    <li><a href="#"><?php echo getMenu($b->idMenu)?>&nbsp;<i class="fa arrow"></i></a>
                                        <ul class="nav nav-second-level">
                                            
                                            <?php
                                                $lihat3=$this->db->query("select * from tblmodul_list_adm a inner join tblmenu_adm b on b.idMenu = a.idMenu where a.headMenu='$headMenu' and a.subMenu = '$subMenu' and a.childMenu>0 and a.idModul='$idModul' and b.isActive = 1")->result();
                                                foreach($lihat3 as $c){ ?>
                                                    <li><a href="<?php echo base_URL()?><?php echo getMenuLink($c->idMenu)?>">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo getMenu($c->idMenu)?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <?php } else { ?>
                                    <li><a href="<?php echo base_URL()?><?php echo $cekLink?>"><?php echo getMenu($b->idMenu)?></a></li>
                                    <?php } ?>
                            <?php } ?>
                              </ul>
                    </li>
                 <?php } ?>
                        <?php if($idUser==1){ ?>
						<li>
                            <a href="#"><i class="fa fa-cogs fa-fw"></i> Setting<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_URL()?>sett_user">User Aplikasi</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_URL()?>sett_menu">Menu</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_URL()?>sett_modul">Modul</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cog fa-fw"></i> Parameter<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_URL()?>param_bahan">Params Bahan</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_URL()?>param_design">Params Design</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_URL()?>param_sample">Params Sample</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_URL()?>param_style">Params Style</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_URL()?>param_buyer">Params Buyer</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Marketing<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Prospecting<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>pros_admin">&nbsp;&nbsp;Admin Activity</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li><a href="<?php echo base_URL()?>pros_apv_cust">Approve Cust Design</a>
                                </li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Cost Calculation<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>pros_upl_cost">&nbsp;&nbsp;Upload Cost Calculation</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>pros_apv_cost">&nbsp;&nbsp;Approve Cost Calculation</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Purchase Order<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>mkt_purchaseorder">&nbsp;&nbsp;Input Purchase Order</a>
                                        </li>
                                   </ul>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>mkt_purchaseorder_apv">&nbsp;&nbsp;Approve Purchase Order</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Laporan<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>lap_mkt_prospecting">&nbsp;&nbsp;Laporan Prospecting</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>lap_mkt_purchaseorder">&nbsp;&nbsp;Laporan Purchase Order</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Design<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Design Request<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>desg_request">&nbsp;&nbsp;Upload Design </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>desg_approve">&nbsp;&nbsp;Approve Design </a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Sample Request<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>samp_request">&nbsp;&nbsp;Request </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>samp_approve_input">&nbsp;&nbsp;Approve Sample </a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Sample<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Sample Request<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>smp_input">&nbsp;&nbsp;Upload Sample </a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Edp<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Assortment<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>edp_assort">&nbsp;&nbsp;EDP 1 </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>edp_assort2">&nbsp;&nbsp;EDP 2 </a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="<?php echo base_URL()?>edp_transassort">Transfer Data Assortment</a></li>
                                <li><a href="<?php echo base_URL()?>edp_assortapprove">Approve Assortment</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> MD<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Material List<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>md_maaping_material">&nbsp;&nbsp;Maaping Material</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>md_inputmaterial">&nbsp;&nbsp;Input Material</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>md_apvmaterial">&nbsp;&nbsp;Approve Material</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li><a href="#">WorkSheet<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>md_inputworksheet">&nbsp;&nbsp;Input Worksheet</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>md_apvworksheet">&nbsp;&nbsp;Approve Worksheet</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Pengajuan Purchasing<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>purc_pend_input">&nbsp;&nbsp;Input Pengajuan</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>purc_pend_apv">&nbsp;&nbsp;Approve Pengajuan</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Purchasing<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="<?php echo base_URL()?>param_supplier">Supplier</a></li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Pembelian<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>purc_beli_request">&nbsp;&nbsp;Input Request Pembelian</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>purc_beli_apv">&nbsp;&nbsp;Approve Pembelian</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Gudang<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Penerimaan Supplier<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>gudang_penerimaan">&nbsp;&nbsp;Input Penerimaan</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>gudang_penerimaan_apv">&nbsp;&nbsp;Approve Penerimaan</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Pengeluaran<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>gudang_pengeluaran">&nbsp;&nbsp;Request Pengeluaran</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Stock Gudang<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>stock_bahan">&nbsp;&nbsp;Stock Bahan</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> PPIC<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="<?php echo base_URL()?>ppic_cutting">Cutting</a></li>
                                <li><a href="<?php echo base_URL()?>ppic_sewing">Sewing</a></li>
                                <li><a href="<?php echo base_URL()?>ppic_triming">Triming</a></li>
                                <li><a href="<?php echo base_URL()?>ppic_qc">QC</a></li>
                                <li><a href="<?php echo base_URL()?>ppic_packing">Packing</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Cutting<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Input Cutting<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>cutting_input">&nbsp;&nbsp;Input Cutting</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>cutting_apv">&nbsp;&nbsp;Approve Cutting</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Pengeluaran<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>cutting_keluar_input">&nbsp;&nbsp;Input Pengeluaran</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>cutting_keluar_apv">&nbsp;&nbsp;Approve Pengeluaran</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Stock Cutting<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>cutting_stock">&nbsp;&nbsp;Stock Cutting</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>cutting_keluar_stock">&nbsp;&nbsp;Stock Keluar</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Sewing<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="<?php echo base_URL()?>sewing_penerimaan">Penerimaan Sewing</a></li>
                                <li><a href="#">Input Sewing<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>sewing_input">&nbsp;&nbsp;Input Sewing</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>sewing_input_apv">&nbsp;&nbsp;Approve Sewing</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Pengeluaran<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>sewinig_keluar_input">&nbsp;&nbsp;Input Pengeluaran</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>sewing_keluar_apv">&nbsp;&nbsp;Approve Pengeluaran</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Stock Sewing<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>sewing_stock_penerimaan">&nbsp;&nbsp;Stock Penerimaan</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>sewing_stock">&nbsp;&nbsp;Stock Sewing</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>sewing_stock_keluar">&nbsp;&nbsp;Stock Keluar</a>
                                        </li>
                                   </ul>
                                </li>
                               
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Triming<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="<?php echo base_URL()?>triming_penerimaan">Penerimaan Triming</a></li>
                                <li><a href="#">Input Triming<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>triming_input">&nbsp;&nbsp;Input Triming</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>triming_input_apv">&nbsp;&nbsp;Approve Triming</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Pengeluaran<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>triming_keluar_input">&nbsp;&nbsp;Input Pengeluaran</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>triming_keluar_apv">&nbsp;&nbsp;Approve Pengeluaran</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Stock Triming<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>triming_stock_penerimaan">&nbsp;&nbsp;Stock Penerimaan</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>triming_stock">&nbsp;&nbsp;Stock Triming</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>triming_stock_keluar">&nbsp;&nbsp;Stock Keluar</a>
                                        </li>
                                   </ul>
                                </li>
                               
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> QC <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="<?php echo base_URL()?>qc_penerimaan">Penerimaan QC</a></li>
                                <li><a href="#">Input QC<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>qc_input">&nbsp;&nbsp;Input QC</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>qc_input_apv">&nbsp;&nbsp;Approve QC</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Pengeluaran<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>qc_keluar_input">&nbsp;&nbsp;Input Pengeluaran</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>qc_keluar_apv">&nbsp;&nbsp;Approve Pengeluaran</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Stock QC<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>qc_stock_penerimaan">&nbsp;&nbsp;Stock Penerimaan</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>qc_stock">&nbsp;&nbsp;Stock QC</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>qc_stock_keluar">&nbsp;&nbsp;Stock Keluar</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Packing<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li><a href="<?php echo base_URL()?>packing_penerimaan">Penerimaan Packing</a></li>
                                <li><a href="#">Input Packing<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>packing_input">&nbsp;&nbsp;Input Packing</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>packing_input_apv">&nbsp;&nbsp;Approve Packing</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Pengiriman<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>packing_keluar_input">&nbsp;&nbsp;Input Pengiriman</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>packing_keluar_apv">&nbsp;&nbsp;Approve Pengiriman</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Stock Packing<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>packing_stock_penerimaan">&nbsp;&nbsp;Stock Penerimaan</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>packing_stock">&nbsp;&nbsp;Stock Packing</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>packing_stock_keluar">&nbsp;&nbsp;Stock Keluar</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check fa-fw"></i> Setting<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="<?php echo base_URL()?>setting_penerimaan">Penerimaan Setting</a></li>
                                <li><a href="#">Input Setting<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>setting_input">&nbsp;&nbsp;Input Setting</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>setting_input_apv">&nbsp;&nbsp;Approve Setting</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Pengeluaran<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>setting_keluar_input">&nbsp;&nbsp;Input Pengeluaran</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>setting_keluar_apv">&nbsp;&nbsp;Approve Pengeluaran</a>
                                        </li>
                                   </ul>
                                </li>
                                <li><a href="#">Stock Setting<span class="fa arrow"></span></a>
                                   <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_URL()?>setting_stock_penerimaan">&nbsp;&nbsp;Stock Penerimaan</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>setting_stock">&nbsp;&nbsp;Stock Setting</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_URL()?>setting_stock_keluar">&nbsp;&nbsp;Stock Keluar</a>
                                        </li>
                                   </ul>
                                </li>
                            </ul>
                        </li>
                        
                   <?php }?>
                    </ul>
                </div>
            </div>
        </nav>