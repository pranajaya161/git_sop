<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
	<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
	 var $j = jQuery.noConflict();
	   $j(function() {
		 $j( "#input1" ).datepicker({
		 dateFormat : "yy-mm-dd",
		 changeMonth: true,
		 changeYear: true
		 });
		 $j( "#input2" ).datepicker({
		 dateFormat : "yy-mm-dd",
		 changeMonth: true,
		 changeYear: true
		 });
		});
	 </script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<legend class="page-header">Modul</legend>
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_tambah">+ Tambah</a></p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
								<tr>
									<th>No</th>
									<th style="text-align:left">Modul</th>
									<th style="text-align:center">Status</th>
									<th style="text-align:center">Edit</th>
									<th style="text-align:center">Detail</th>
								</tr>
							</thead>
						<tbody>
						<?php $i=1; foreach ($data as $a): ?>
							<tr class="odd gradeX">
								<td><?php echo $i++;?></td>
									<td><?php echo $a->namaModul?></td>
									<td style="text-align:center"><?php if($a->isActive==1){ echo 'Active';} else { echo 'Disabled';}?></td>
									<td class="tabtxt" style="text-align: center">
										<a class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal_edit<?php echo $a->idModul?>"><i class="fa fa-edit"></i></a>
									</td>
									<td class="tabtxt" style="text-align: center">
										<a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_detail<?php echo $a->idModul?>"><i class="fa fa-list"></i></a>
									</td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>
					 
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<!-- Modal Tambah -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_tambah" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-skin">
				<div class="panel-heading">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h3 class="panel-title"><span class="fa fa-user"></span> Tambah</h3>
				</div>
				<div class="panel-body">
				<?php echo form_open($aplikasi.'/tambah');?>
				<input type="hidden" name="idModul" value="0" />
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Modul</label>
							<input type="text" name="namaModul" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Status</label>
							<select name="isActive" class="form-control">
								<option value="1">Active</option>
								<option value="0">Disabled</option>
							</select>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success">Simpan</button>
							<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
						</div>	
					</div>	
				</div>	
				</form>
				</div>	
			</div>
	    </div>
	 </div>
</div>
<!-- END Modal Tambah -->
<!-- Modal Edit -->
<?php foreach ($data as $b):?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_edit<?php echo $b->idModul?>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-skin">
				<div class="panel-heading">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h3 class="panel-title"><span class="fa fa-user"></span> Edit Data </h3>
				</div>
				<div class="panel-body">
				<?php echo form_open($aplikasi.'/edit');?>
				<input type="hidden" name="idModul" value="<?php echo $b->idModul?>" />
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Modul</label>
							<input type="text" name="namaModul" value="<?php echo $b->namaModul?>" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Status</label>
							<select name="isActive" class="form-control">
								<option value="1">Active</option>
								<option value="0">Disabled</option>
							</select>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success">Update</button>
							<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
						</div>	
					</div>
				</form>
				</div>	
			</div>
	    </div>
	 </div>
</div>
<?php endforeach;?>
<!-- END Modal Edit -->
<!-- Modal detail -->
<?php foreach ($data as $c): $idModul = $c->idModul?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_detail<?php echo $c->idModul?>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-skin">
				<div class="panel-heading">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h3 class="panel-title"><span class="fa fa-user"></span> Detail Data </h3>
				</div>
				<div class="panel-body">
				<?php echo form_open($aplikasi.'/detail');?>
				<input type="hidden" name="idModul" value="<?php echo $c->idModul?>" />
					<table width="100%"  border="0" cellspacing="0" cellpadding="3">
						<thead>
							<tr>
								<th width="2%" style="text-align:left">Check</th>
								<th width="*" style="text-align:left">Modul</th>
							</tr>
						</thead>	
						<tbody>
						<?php
							$data1=$this->db->query("select a.*, b.idModul from tblmenu_adm a left join tblmodul_list_adm b on b.idMenu =a.idMenu and b.idModul = '$idModul' where a.isActive = 1 order by a.headMenu, a.subMenu, a.childMenu, a.nourut")->result();
							foreach($data1 as $det){	
								if($det->subMenu > 0 and  $det->childMenu==0){
									$ket='&nbsp;&nbsp;&nbsp;&nbsp;';
								} else if($det->subMenu > 0 and $det->childMenu > 0){
									$ket='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
								} else {
									$ket='&nbsp;';
								}
								if(empty($det->idModul)){
									$x_mod = '';
								} else {
									$x_mod = 'checked';
								}
							?>
								<tr>
									<td><input type="checkbox" name="idMenu[]" value="<?php echo $det->idMenu?>"  <?php echo $x_mod?> /> </td>
									<td><?php echo $ket?><?php echo $det->namaMenu?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<div class="col-lg-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success">Update</button>
							<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
						</div>	
					</div>
				</form>
				</div>	
			</div>
	    </div>
	 </div>
</div>
<?php endforeach;?>
<!-- END Modal detail -->
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
	