<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
	<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
	 var $j = jQuery.noConflict();
	   $j(function() {
		 $j( "#input1" ).datepicker({
		 dateFormat : "yy-mm-dd",
		 changeMonth: true,
		 changeYear: true
		 });
		 $j( "#input2" ).datepicker({
		 dateFormat : "yy-mm-dd",
		 changeMonth: true,
		 changeYear: true
		 });
		});
	 </script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<legend class="page-header">Menu</legend>
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_tambah">+ Tambah</a></p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
								<tr>
									<th>No</th>
									<th style="text-align:left">Menu</th>
									<th style="text-align:left">Link</th>
									<th style="text-align:center">Status</th>
									<th style="text-align:center">Edit</th>
								</tr>
							</thead>
						<tbody>
						<?php  $no=1; foreach ($data as $a): ?>
							<?php if($a->subMenu==0){
								$ket='add_sub';
								$sp='';
							} 
							else if($a->subMenu>0 and $a->childMenu==0)
							{
								$ket='add_chi';
								$sp='&nbsp;&nbsp;&nbsp;';
							} 
							else
							{
								$sp='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							}?>
							<?php?>
							<tr class="odd gradeX">
								<td><?php echo $no++?></td>
									<td><a href="#" data-toggle="modal" data-target="#modal_det<?php echo $ket?><?php echo $a->idMenu?>"><?php echo $sp?><?php echo $a->namaMenu?></a>
									</td>
									<td><?php echo $a->link?></td>
									<td style="text-align:center"><?php if($a->isActive==1){ echo 'Active';} else { echo 'Disabled';}?></td>
									<td class="tabtxt" style="text-align: center">
										<a class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal_edit<?php echo $a->idMenu?>"><i class="fa fa-edit"></i></a>
									</td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>

                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<!-- Modal Tambah -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_tambah" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-skin">
				<div class="panel-heading">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h3 class="panel-title"><span class="fa fa-user"></span> Tambah Menu</h3>
				</div>
				<div class="panel-body">
				<?php echo form_open($aplikasi.'/tambah');?>
				<input type="hidden" name="idMenu" value="0" />
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Menu</label>
							<input type="text" name="namaMenu" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Status</label>
							<select name="isActive" class="form-control">
								<option value="1">Active</option>
								<option value="0">Disabled</option>
							</select>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success">Simpan</button>
							<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
						</div>	
					</div>	
				</div>	
				</form>
				</div>	
			</div>
	    </div>
	 </div>
</div>
<!-- END Modal Tambah -->
<!-- Modal Edit -->
<?php foreach ($data as $b):  if($b->subMenu==0){ $ket='add_sub'; $sp=''; } else if($b->subMenu>0 and $b->childMenu==0) { $ket ='add_chi'; } else { $ket=''; } ?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_det<?php echo $ket?><?php echo $b->idMenu?>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-skin">
				<div class="panel-heading">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h3 class="panel-title"><span class="fa fa-user"></span> Edit Data </h3>
				</div>
				<div class="panel-body">
				<?php if($ket=='add_sub'){ ?>
				<?php echo form_open($aplikasi.'/tambah_sub');?>
				<?php } else { echo form_open($aplikasi.'/tambah_child'); }?>
				<input type="hidden" name="idMenu" value="<?php echo $b->idMenu?>" />
				<input type="hidden" name="headMenu" value="<?php echo $b->headMenu?>" />
				<input type="hidden" name="subMenu" value="<?php echo $b->subMenu?>" />
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Menu</label>
							<input type="text" name="menu" value="<?php echo $b->namaMenu?>" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Menu</label>
							<input type="text" name="namaMenu" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Link</label>
							<input type="text" name="link" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">No.Urut</label>
							<input type="text" name="nourut" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Status</label>
							<select name="isActive" class="form-control">
								<option value="1">Active</option>
								<option value="0">Disabled</option>
							</select>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success">Simpan</button>
							<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
						</div>	
					</div>
				</form>
				</div>	
			</div>
	    </div>
	 </div>
</div>
<?php endforeach;?>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
	