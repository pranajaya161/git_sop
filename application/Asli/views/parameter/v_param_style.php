<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
	<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
	 var $j = jQuery.noConflict();
	   $j(function() {
		 $j( "#input1" ).datepicker({
		 dateFormat : "yy-mm-dd",
		 changeMonth: true,
		 changeYear: true
		 });
		 $j( "#input2" ).datepicker({
		 dateFormat : "yy-mm-dd",
		 changeMonth: true,
		 changeYear: true
		 });
		});
	 </script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<legend class="page-header"><?php echo $title?></legend>
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_tambah">+ Tambah</a></p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th>No</th>
								<th>Style</th>
								<th>Tgl.Proses</th>
								<th>User</th>
								<th>Status</th>
								<th style="text-align: center">Edit</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach ($data as $a): ?>
							<tr class="odd gradeX">
								<td><?php echo $i++;?></td>
								<td><a href="#" data-toggle="modal" data-target="#modal_view<?php echo $a->idStyle?>"><?php echo $a->namaStyle;?></a></td>
								<td><?php echo $a->tgl_proses;?></td>
								<td><?php echo $a->userId;?></td>
								<td><?php if($a->isActive==1){ echo 'Aktif'; } else { echo 'Non.Aktif';};?></td>
								<td style="text-align: center">
									<a class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal_edit<?php echo $a->idStyle?>"><i class="fa fa-edit"></i></a>
								</td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<!--- Tambah -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_tambah" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-skin">
				<div class="panel-heading">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h3 class="panel-title"><span class="fa fa-user"></span> Tambah  <?php echo $status?></h3>
				</div>
				<div class="panel-body">
				<?php echo form_open_multipart($aplikasi.'/tambah');?>
				<input type="hidden" name="status" value="<?php echo $status?>">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Style</label>
							<input type="text" name="namaStyle" class="form-control upper" id="exampleInputEmail1" aria-describedby="emailHelp" required>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Gambar</label>
							<input type="file" name="pict" >
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success">Simpan</button>
							<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
						</div>	
					</div>	
				</div>	
				</form>
				</div>	
			</div>
	    </div>
	 </div>
</div>
<!--- End Tambah -->
<!--- End Edit -->
<?php foreach($data as $b): $idStyle = $b->idStyle; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_edit<?php echo $b->idStyle?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> Edit <?php echo $status?></h3>
					</div>
					<div class="panel-body">
					<?php echo form_open_multipart($aplikasi.'/edit');?>
					<input type="hidden" name="idStyle" value="<?php echo $b->idStyle?>"/>
					<input type="hidden" name="status" value="<?php echo $status?>" />
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Nama Style</label>
									<input type="text" name="namaStyle" class="form-control upper" value="<?php echo $b->namaStyle?>" id="exampleInputEmail1" aria-describedby="emailHelp" required>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Gambar</label>
									<input type="file" name="pict" >
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Status</label>
									<select name="isActive" class="form-control">
										<option value="1">Aktif</option>
										<option value="0">Non.Aktif</option>
									</select>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<button type="submit" class="btn btn-success">Simpan</button>
									<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
								</div>	
							</div>	
						</div>	
					</form>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End Edit -->
<!--- End View  -->
<?php foreach($data as $b): $idStyle=$b->idStyle; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_view<?php echo $b->idStyle?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->namaStyle?></h3>
					</div>
					<div class="panel-body">
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Tgl.Proses</th>
									<th>Ket</th>
									<th>Style</th>
									<th>Status</th>
									<th>UserId</th>
									<th>Gambar</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$lihat  = $this->db->query("select * from log_param_style where idStyle='$idStyle' order by tgl_proses")->result();
							foreach($lihat as $x):
							?>	
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo $x->tgl_proses?></td>
									<td><?php echo $x->keterangan?></td>
									<td><?php echo $x->namaStyle?></td>
									<td><?php if($x->isActive==1) { echo 'Aktif'; } else { echo 'Non.Aktif';}?></td>
									<td><?php echo $x->userId?></td>
									<td><img src="<?php echo base_URL()?>uploads/parameter/style/<?php echo $x->pict?>" style="width:100px"></td>
								</tr>
							<?php endforeach;?>
							</tbody>
						</table>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	

<!--- End View -->
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
	