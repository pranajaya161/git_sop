<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><b><?php echo $title?></b></p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th>No</th>
								<th>Tanggal</th>
								<th>Ref.No</th>
								<th>Admin Activity</th>
								<th>Design</th>
								<th>Sample</th>
								<th>Approve Cust</th>
								<th>Cost Calculation</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach ($data as $a): ?>
							<tr class="odd gradeX">
								<td><?php echo $i++;?></td>
								<td><?php echo $a->tanggal;?></td>
								<td><a href="#" data-toggle="modal" data-target="#modal_refno<?php echo $a->idProspecting?>"><?php echo $a->refNo;?></a></td>
								<td style="text-align: center"><a href="#" data-toggle="modal" data-target="#modal_adm<?php echo $a->idProspecting?>"><i class="fa fa-search"></i></a></td>
								<td style="text-align: center"><a href="#" data-toggle="modal" data-target="#modal_design<?php echo $a->idProspecting?>"><i class="fa fa-search"></i></a></td>
								<td style="text-align: center"><a href="#" data-toggle="modal" data-target="#modal_sample<?php echo $a->idProspecting?>"><i class="fa fa-search"></i></a></td>
								<td style="text-align: center"><a href="#" data-toggle="modal" data-target="#modal_apvcust<?php echo $a->idProspecting?>"><i class="fa fa-search"></i></a></td>
								<td style="text-align: center"><a href="#" data-toggle="modal" data-target="#modal_cost<?php echo $a->idProspecting?>"><i class="fa fa-search"></i></a></td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<!--- RefNo -->
<?php foreach($data as $b): $idProspecting=$b->idProspecting; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_refno<?php echo $b->idProspecting?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<table width="100%" >
							<tbody>
							<?php
							$i=1;
							$lihat  = $this->db->query("select * from mkt_pros_sm where idProspecting='$idProspecting'")->result();
							foreach($lihat as $x):
							?>	
								<tr>
									<td>RefNo</td>
									<td>:</td>
									<td><?php echo $b->refNo?></td>
									<td>Buyer</td>
									<td>:</td>
									<td><?php echo getClient($b->idClient);?></td>
								</tr>
								<tr>
									<td>Informasi</td>
									<td>:</td>
									<td><?php echo getInformasi($b->idInformasi);?></td>
									<td>User ID</td>
									<td>:</td>
									<td><?php echo $b->userId;?></td>
								</tr>	
								<tr>
									<td>Tgl.Proses</td>
									<td>:</td>
									<td><?php echo $b->tgl_proses;?></td>
									<td>Keterangan</td>
									<td>:</td>
									<td><?php echo $b->keterangan;?></td>
								</tr>	
							<?php endforeach;?>
							</tbody>
						</table>
						<hr>
						Approve Design
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Tgl.Proses</th>
									<th>Keterangan</th>
									<th>UserID</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$log = $this->db->query("select * from mkt_pros_desgn_apv where idProspecting='$idProspecting'")->result();
							foreach($log as $lg):?>
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo $lg->tgl_proses?></td>
									<td><?php echo $lg->keterangan?></td>
									<td><?php echo $lg->userId?></td>
									<td><?php if($lg->status ==1) { echo 'Approve'; } else if($lg->userId==2){ echo 'Reject'; } else { echo 'Proses'; }?></td>
								</tr>
							<?php
							endforeach;
							?>
							</tbody>
						</table>
						<hr>
						Approve Sample
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Tgl.Proses</th>
									<th>Keterangan</th>
									<th>UserID</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$log = $this->db->query("select b.* from mkt_pros_sample a inner join mkt_pros_sample_apv b on b.idSample = a.idSample where a.idProspecting='$idProspecting'")->result();
							foreach($log as $lg):?>
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo $lg->tgl_proses?></td>
									<td><?php echo $lg->keterangan?></td>
									<td><?php echo $lg->userId?></td>
									<td><?php if($lg->status ==1) { echo 'Approve'; } else if($lg->userId==2){ echo 'Reject'; } else { echo 'Proses'; }?></td>
								</tr>
							<?php
							endforeach;
							?>
							</tbody>
						</table>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End RefNo -->
<!--- ADM -->
<?php foreach($data as $b): $idProspecting=$b->idProspecting; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_adm<?php echo $b->idProspecting?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Status</th>
									<th>Tgl.Input</th>
									<th>Tgl.Target</th>
									<th>Keterangan</th>
									<th>User</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$lihat  = $this->db->query("select * from mkt_pros_dt where idProspecting='$idProspecting' order by tgl_proses")->result();
							foreach($lihat as $x):
							?>	
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo $x->status?></td>
									<td><?php echo $x->tgl_input?></td>
									<td><?php echo $x->tgl_target?></td>
									<td><?php echo $x->keterangan?></td>
									<td><?php echo $x->userId?></td>
							<?php endforeach;?>
							</tbody>
						</table>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End Adm -->
<!--- Design -->
<?php foreach($data as $b): $idProspecting=$b->idProspecting; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_design<?php echo $b->idProspecting?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Status</th>
									<th>Tgl.Input</th>
									<th>Tgl.Target</th>
									<th>Keterangan</th>
									<th>User</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$lihat  = $this->db->query("select * from mkt_pros_dt where idProspecting='$idProspecting' order by tgl_proses")->result();
							foreach($lihat as $x):
							?>	
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo $x->status?></td>
									<td><?php echo $x->tgl_input?></td>
									<td><?php echo $x->tgl_target?></td>
									<td><?php echo $x->keterangan?></td>
									<td><?php echo $x->userId?></td>
							<?php endforeach;?>
							</tbody>
						</table>
						<hr>
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Style</th>
									<th>Image</th>
									<th>Bahan</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$image = $this->db->query("select * from mkt_pros_desgn where idProspecting='$idProspecting'")->result();
							foreach($image as $img):?>
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo getStyle($img->idStyle)?></td>
									<td><img src="<?php echo base_URL()?>uploads/parameter/style/<?php echo getStylePict($img->idStyle)?>" style="max-width: 100px"></td>
									<td>
							  		<?php
							  			$idStyle = $img->idStyle;
										$xx=$this->db->query("select a.idProspecting, a.idBahan, b.keterangan from mkt_pros_desgn_bhn a inner join mkt_pros_desgn b on b.idProspecting = a.idProspecting and b.idStyle = a.idStyle and a.idProspecting='$idProspecting' and a.idStyle='$idStyle'")->result();
										foreach($xx as $z):
												echo getBahan($z->idBahan); ?><br>
											<?php endforeach; ?>	
							  			</td>
								</tr>
							<?php
							endforeach;
							?>
							</tbody>
						</table>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End Design -->
<!--- Sample -->
<?php foreach($data as $b): $idProspecting=$b->idProspecting; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_sample<?php echo $b->idProspecting?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Bahan</th>
									<th>Keterangan</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$image = $this->db->query("select b.* from mkt_pros_sample a inner join mkt_pros_sample_bhn b on b.idSample = a.idSample where a.idProspecting='$idProspecting'")->result();
							foreach($image as $img):?>
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo getBahan($img->idBahan)?></td>
									<td><?php echo $img->keterangan?></td>
								</tr>
							<?php
							endforeach;
							?>
							</tbody>
						</table>
						<hr>
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Style</th>
									<th>Img</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$st = $this->db->query("select b.* from mkt_pros_sample a inner join mkt_pros_sample_sty b on b.idSample = a.idSample where a.idProspecting='$idProspecting'")->result();
							foreach($st as $img1):?>
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo getStyle($img1->idStyle)?></td>
									<td><img src="<?php echo base_URL()?>uploads/parameter/style/<?php echo getStylePict($img1->idStyle)?>" style="max-width: 100px"></td>
								</tr>
							<?php
							endforeach;
							?>
							</tbody>
						</table>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End Sample -->
<!---  Approve Customer -->
<?php foreach($data as $b): $idProspecting=$b->idProspecting; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_apvcust<?php echo $b->idProspecting?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Style</th>
									<th>Pict</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$image = $this->db->query("select * from mkt_pros_desgn where idProspecting='$idProspecting' and status=1")->result();
							foreach($image as $img):?>
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo getStyle($img->idStyle)?></td>
									<td><img src="<?php echo base_URL()?>uploads/parameter/style/<?php echo getStylePict($img->idStyle)?>" style="max-width: 100px"></td>
								</tr>
							<?php
							endforeach;
							?>
							</tbody>
						</table>
						
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End Approve Customer -->
<!---  Approve Customer -->
<?php foreach($data as $b): $idProspecting=$b->idProspecting; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_cost<?php echo $b->idProspecting?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Tgl</th>
									<th>Keterangan</th>
									<th style="text-align:center">Data</th>
								</tr>
							</thead>
							<tbody>
							<?php $no=1;
								$lihat = $this->db->query("select * from mkt_pros_cost where idProspecting='$idProspecting'")->result();
								foreach($lihat as $x):?>
									<tr>
										<td style="text-align:center"><?php echo $no++?></td>
										<td style="text-align:center"><?php echo $x->tgl_proses?></td>
										<td style="text-align:left"><?php echo $x->keterangan?></td>
										<td	style="text-align:center">
											<a href='<?php echo base_URL()?>uploads/costcalculation/<?php echo $x->pict?>' target='__blank'>Download</a>	
										</td>	
										
								<?php endforeach; ?>
							</tbody>
						</table>
						<hr>
						Approve	
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Tgl.Proses</th>
									<th>Keterangan</th>
									<th>User Id</th>
									<th style="text-align:center">Status</th>
								</tr>
							</thead>
							<tbody>
							<?php $no=1;
								$lihat = $this->db->query("select * from mkt_pros_cost_apv where idProspecting='$idProspecting'")->result();
								foreach($lihat as $x):?>
									<tr>
										<td style="text-align:center"><?php echo $no++?></td>
										<td style="text-align:center"><?php echo $x->tgl_proses?></td>
										<td style="text-align:left"><?php echo $x->keterangan?></td>
										<td style="text-align:left"><?php echo $x->userId?></td>
										<td	style="text-align:center">
											<?php if($x->status==1){ echo 'Approve'; } else if($x->status==2) { echo 'Reject'; } else { echo 'Proses';}?>
										</td>	
										
								<?php endforeach; ?>
							</tbody>
						</table>	
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End Approve Customer -->
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
