<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<legend class="page-header"><?php echo $title?></legend>
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th>No</th>
								<th>Tanggal</th>
								<th>Ref.No</th>
								<th>Buyer</th>
								<th>Informasi</th>
								<th>Keterangan</th>
								<th>Tgl.Update</th>
								<th>User</th>
								<th style="text-align: center">Approve</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach ($data as $a): ?>
							<tr class="odd gradeX">
								<td><?php echo $i++;?></td>
								<td><?php echo $a->tanggal;?></td>
								<td><a href="#" data-toggle="modal" data-target="#modal_view<?php echo $a->idProspecting?>"><?php echo $a->refNo;?></a></td>
								<td><?php echo getClient($a->idClient);?></td>
								<td><?php echo getInformasi($a->idInformasi);?></td>
								<td><?php echo $a->keterangan;?></td>
								<td><?php echo $a->tgl_proses;?></td>
								<td><?php echo $a->userId;?></td>
								<td style="text-align: center">
									<a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_approve<?php echo $a->idProspecting?>"><i class="fa fa-check"></i></a>
								</td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>

                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<!--- End Approve -->
<?php foreach($data as $b): $idProspecting = $b->idProspecting;?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_approve<?php echo $b->idProspecting?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> Approve <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<?php echo form_open_multipart($aplikasi.'/approve');?>
						<input type="hidden" name="idProspecting" value="<?php echo $b->idProspecting?>" />
							<table class="table table-bordered table-striped table-condensed">
								<thead>
									<tr>
										<th style="text-align:left">Style</th>
										<th style="text-align:left">UserId</th>
										<th style="text-align:center">Pict</th>
										<th style="text-align:center">Status</th>
										<th style="text-align:left">Keterangan</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$i=1;
								$cek = $this->db->query("select * from mkt_pros_desgn a  where a.idProspecting ='$idProspecting'")->result();
								foreach($cek as $xx): ?>	
									<input type="hidden" name="idStyle[]" value="<?php echo $xx->idStyle?>" readonly />
									<tr>
										<td style="text-align:left"><?php echo getStyle($xx->idStyle)?></td>
										<td class="tabtxt" style="text-align:left"><?php echo $xx->userId?></td>
										<td style="text-align:center"><img src="<?php echo base_URL()?>uploads/parameter/style/<?php echo getStylePict($xx->idStyle)?>" width="50px" /></td>
										<td class="tabtxt" style="text-align:left;">
											<select name="status[]" class="form-control">
												<option value="1">Approve</option>
												<option value="0">Reject</option>
											</select>
										</td>
										<td class="tabtxt" style="text-align:left"><input type="text" class="form-control" name="keterangan[]"></td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>		
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<button type="submit" class="btn btn-success">Simpan</button>
										<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
									</div>	
								</div>	
							</div>	
						</form>
						</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End Edit -->
<!--- End View -->
<?php foreach($data as $b): $idProspecting=$b->idProspecting; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_view<?php echo $b->idProspecting?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Tanggal</th>
									<th>Buyer</th>
									<th>Keterangan</th>
									<th>User</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$lihat  = $this->db->query("select * from mkt_pros_sm where idProspecting='$idProspecting'")->result();
							foreach($lihat as $x):
							?>	
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo $x->tanggal?></td>
									<td><?php echo getClient($x->idClient)?></td>
									<td><?php echo $x->keterangan?></td>
									<td><?php echo $x->userId?></td>
							<?php endforeach;?>
							</tbody>
						</table>
						<hr>
						<table width="100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Style</th>
									<th>Image</th>
									<th>Bahan</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$i=1;
							$image = $this->db->query("select * from mkt_pros_desgn where idProspecting='$idProspecting'")->result();
							foreach($image as $img):?>
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo getStyle($img->idStyle)?></td>
									<td><img src="<?php echo base_URL()?>uploads/parameter/style/<?php echo getStylePict($img->idStyle)?>" style="max-width: 100px"></td>
									<td>
							  		<?php
							  			$idStyle = $img->idStyle;
										$xx=$this->db->query("select a.idProspecting, a.idBahan, b.keterangan from mkt_pros_desgn_bhn a inner join mkt_pros_desgn b on b.idProspecting = a.idProspecting and b.idStyle = a.idStyle and a.idProspecting='$idProspecting' and a.idStyle='$idStyle'")->result();
										foreach($xx as $z):
												echo getBahan($z->idBahan); ?><br>
											<?php endforeach; ?>	
							  		</td>
								</tr>
							<?php
							endforeach;
							?>
							</tbody>
						</table>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End View -->
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
