<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>

<div class="form-panel">
	<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th style="text-align:right">No</th>	
				<th style="text-align:left">Style</th>	
				<th style="text-align:left">Size</th>	
				<th style="text-align:left">Lot</th>
				<th style="text-align:right">Qty</th>
			</tr>
		</thead>
		<tbody>
		<?php $i=1; foreach($data as $a): ?>
			<tr>
				<td  style="text-align:right;font-size:11px"><?php echo $i?></td>
				<td  style="text-align:left;font-size:11px">
					<a onclick="pilih('<?php echo $a->idStyle?>'
								,'<?php echo getStyle($a->idStyle);?>'
								,'<?php echo $a->size;?>'
								,'<?php echo $a->qty;?>'
								)" href="#">
							<?php echo getStyle($a->idStyle);?>
					</a>
				</td>
				<td  style="text-align:left;font-size:11px"><?php echo $a->size?></td>
				<td  style="text-align:left;font-size:11px"><?php echo $a->lot_number?></td>
				<td  style="text-align:right;font-size:11px"><?php echo number_format($a->qty)?></td>
			</tr>
			
		<?php endforeach; ?>	
		</tbody>
	</table>
</div>
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>

<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>		  
<script>

    function pilih(idStyle, namaStyle, size, qty){
	    parent.opener.fungsiKetikaPopupClosedStyleMd(idStyle, namaStyle, size, qty);
	    window.close();
    }

</script>


