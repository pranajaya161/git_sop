<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
	<?php
		if($this->session->flashdata('pesan')==TRUE):
		echo'<div class="alert alert-warning" role="alert">';
		echo $this->session->flashdata('pesan');
		echo "</div>";
	endif;
	?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><?php echo $title?> </p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th style="text-align:right">No.</th>
								<th style="text-align:left">Bahan</th>
								<th style="text-align:right">Qty</th>
								<th style="text-align:left">Last Update</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach ($data as $a): ?>
							<tr class="odd gradeX">
								<td style="text-align:right"><?php echo $i++;?></td>
								<td><a href="#" data-toggle="modal" data-target="#modal_view<?php echo $a->idBahan?>"><?php echo getBahan($a->idBahan);?></a></td>
								<td style="text-align:right"><?php echo number_format($a->qty,2);?></td>
								<td><?php echo $a->tgl_proses;?></td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>
<?php foreach($data as $b): $idBahan = $b->idBahan; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_view<?php echo $b->idBahan?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> <?php echo $title?> <?php echo getBahan($b->idBahan)?></h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<table class="table table-bordered table-striped table-condensed">
								<tr>
									<th style="text-align:left">Tanggal</th>
									<th style="text-align:left">Supplier</th>	
									<th style="text-align:left">Ref.No</th>	
									<th style="text-align:left">Keterangan</th>
									<th style="text-align:left">Qty</th>
								</tr>
								<?php
								$detail = $this->db->query("select * from stock_01_hist where idBahan='$idBahan'")->result();
								foreach($detail as $xx):?>
								<tr>
									<td><?php echo $xx->tanggal?></td>
									<td><?php echo getSupplier($xx->idSupplier)?></td>
									<td><?php echo $xx->refNo?></td>
									<td><?php echo $xx->keterangan?></td>
									<td><?php echo number_format($xx->qty,2)?></td>
								</tr>
								<?php endforeach;?>
							</table>
							<div class="col-lg-12">
								<div class="form-group">
									<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
								</div>	
							</div>	
						</div>	
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End Approve -->
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<!--
<script language="JavaScript" type="text/JavaScript">
 
 function ShowData()
 {
	 <?php
	 $query = $this->db->query("select idPo from edp_assortment_pros where isActive=1 and idMaterialist =0 group by idPo")->result();
	 foreach($query as $data){
	   $idPo = $data->idPo;
	   // membuat IF untuk masing-masing Group
	   echo "if (document.cabang.idPo.value == \"".$idPo."\")";
	   echo "{";

	   // membuat option kabupaten untuk masing-masing propinsi
	   $query2 = $this->db->query("select a.idStyle, b.namaStyle from edp_assortment_pros a inner join tblstyle b on b.idStyle = a.idStyle where a.idPo='$idPo' group by a.idStyle")->result();


	   $content = "document.getElementById('idStyle').innerHTML = \"";
	   foreach($query2 as $data2){
	       $content .= "<option value='".$data2->idStyle."'>".$data2->namaStyle."</option>";   
	   }
	   $content .= "\"";
	   echo $content;
	   echo "}\n";   
	 }
	?> 

 }
</script>  
-->