<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
	<?php
		if($this->session->flashdata('pesan')==TRUE):
		echo'<div class="alert alert-warning" role="alert">';
		echo $this->session->flashdata('pesan');
		echo "</div>";
	endif;
	?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><?php echo $title?> <a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_tambah" style="float:right">+ Tambah</a></p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th style="text-align:right">No.</th>
								<th style="text-align:left">Tanggal</th>
								<th style="text-align:left">RefNo</th>
								<th style="text-align:left">Supplier</th>
								<th style="text-align:left">Request</th>
								<th style="text-align:left">Keterangan</th>
								<th style="text-align:center">Status</th>
								<th style="text-align:center">Edit</th>
								<th style="text-align:center">Detail</th>
								<th style="text-align:center">Send</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach ($data as $a): ?>
							<tr class="odd gradeX">
								<td style="text-align:right"><?php echo $i++;?></td>
								<td><?php echo $a->tanggal;?></td>
								<td><?php echo $a->refNo;?></td>
								<td><?php echo getSupplier($a->idSupplier);?></td>
								<td><?php echo $a->request;?></td>
								<td><?php echo $a->keterangan;?></td>
								<td style="text-align: center"><?php echo $a->status;?></td>
								<td style="text-align: center">
									<a class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal_edit<?php echo $a->idPurcPembelian?>"><i class="fa fa-edit"></i></a>
								</td>
								<td style="text-align: center">
									<a href="<?php echo base_URL()?><?php echo $aplikasi?>/detail/<?php echo $a->idPurcPembelian?>" class="btn btn-xs btn-info"><i class="fa fa-list"></i></a>
								</td>
								<td style="text-align: center">
									<a class="btn btn-xs btn-success" data-toggle="modal" data-target="#modal_send<?php echo $a->idPurcPembelian?>"><i class="fa fa-send"></i></a>
								</td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>
<!-- Tambah -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_tambah" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-skin">
				<div class="panel-heading">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h3 class="panel-title"><span class="fa fa-user"></span> Tambah <?php echo $title?></h3>
				</div>
				<div class="panel-body">
				<?php echo form_open_multipart($aplikasi.'/tambah');?>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Tanggal</label>
							<input type="text" name="tanggal" class="form-control upper" value="<?php echo date('Y-m-d')?>" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Request</label>
							<select class="form-control" name="request">
								<option value="Request">Request</option>
								<option value="Manual">Manual</option>
							</select>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Supplier</label>
							<select class="form-control" name="idSupplier">
							<?php
							$lihat = $this->db->query('select * from tblsupplier where isActive = 1')->result();
							foreach($lihat as $a):?>
								<option value="<?php echo $a->idSupplier?>"><?php echo $a->namaSupplier?></option>
							<?php endforeach;?>
							</select>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="exampleInputEmail1">Keterangan</label>
							<textarea class="form-control" name="keterangan" rows="3"></textarea>
						</div>	
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success">Simpan</button>
							<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
						</div>	
					</div>	
				</div>	
				</form>
				</div>	
			</div>
	    </div>
	 </div>
</div>
<!-- End Tambah -->	
<!--- Edit -->
<?php foreach($data as $b): $idPurcPembelian = $b->idPurcPembelian; $idSupplier = $b->idSupplier;?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_edit<?php echo $b->idPurcPembelian?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> Send <?php echo $title?> <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
					<?php echo form_open_multipart($aplikasi.'/edit');?>
					<input type="hidden" name="idPurcPembelian" value="<?php echo $b->idPurcPembelian?>"/>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Tanggal</label>
									<input type="text" name="tanggal" class="form-control upper" value="<?php echo date('Y-m-d')?>" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Request</label>
									<input type="text" name="request" class="form-control upper" value="<?php echo $b->request?>" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Supplier</label>
									<select class="form-control" name="idSupplier">
										<option value="<?php echo $b->idSupplier?>"><?php echo getSupplier($b->idSupplier)?></option>
									<?php
									$lihat = $this->db->query("select * from tblsupplier where isActive = 1 and idSupplier <> '$idSupplier'")->result();
									foreach($lihat as $a):?>
										<option value="<?php echo $a->idSupplier?>"><?php echo $a->namaSupplier?></option>
									<?php endforeach;?>
									</select>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Keterangan</label>
									<textarea class="form-control" name="keterangan" rows="3"><?php echo $b->keterangan?></textarea>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<button type="submit" class="btn btn-success">Edit</button>
									<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
								</div>	
							</div>	
						</div>	
					</form>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- Send -->
<?php foreach($data as $b): $idPurcPembelian = $b->idPurcPembelian; $idSupplier = $b->idSupplier;?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_send<?php echo $b->idPurcPembelian?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> Send <?php echo $title?> <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
					<?php echo form_open_multipart($aplikasi.'/send');?>
					<input type="hidden" name="idPurcPembelian" value="<?php echo $b->idPurcPembelian?>"/>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Data Akan di Kirim untuk di Approve</label>
									<input type="text" name="Supplier" class="form-control upper" value="<?php echo getSupplier($idSupplier)?>" id="exampleInputEmail1" aria-describedby="emailHelp" readonly>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<button type="submit" class="btn btn-success">Send</button>
									<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
								</div>	
							</div>	
						</div>	
					</form>
						<div class="row">
						    <div class="col-lg-12">
								<div class="form-panel">
								<p>List</p>
									<table class="table table-bordered table-striped table-condensed">
										<tr>
											<th style="text-align:left">Bahan</th>	
											<th style="text-align:left">Qty</th>	
											<th style="text-align:left">Satuan</th>
										</tr>
										<?php
										$detail = $this->db->query("select * from purc_pembelian_dt where idPurcPembelian='$idPurcPembelian'")->result();
										foreach($detail as $xx):?>
										<tr>
											<td><?php echo getBahan($xx->idBahan)?></td>
											<td><?php echo number_format($xx->qty,2)?></td>
											<td><?php echo $xx->satuan?></td>
										</tr>
										<?php endforeach;?>
									</table>	
								</div>
							</div>
						</div>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!--- End Approve -->
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<!--
<script language="JavaScript" type="text/JavaScript">
 
 function ShowData()
 {
	 <?php
	 $query = $this->db->query("select idPo from edp_assortment_pros where isActive=1 and idMaterialist =0 group by idPo")->result();
	 foreach($query as $data){
	   $idPo = $data->idPo;
	   // membuat IF untuk masing-masing Group
	   echo "if (document.cabang.idPo.value == \"".$idPo."\")";
	   echo "{";

	   // membuat option kabupaten untuk masing-masing propinsi
	   $query2 = $this->db->query("select a.idStyle, b.namaStyle from edp_assortment_pros a inner join tblstyle b on b.idStyle = a.idStyle where a.idPo='$idPo' group by a.idStyle")->result();


	   $content = "document.getElementById('idStyle').innerHTML = \"";
	   foreach($query2 as $data2){
	       $content .= "<option value='".$data2->idStyle."'>".$data2->namaStyle."</option>";   
	   }
	   $content .= "\"";
	   echo $content;
	   echo "}\n";   
	 }
	?> 

 }
</script>  
-->