<form name="cabang" action="<?php echo base_URL()?>purc_beli_request/detail/<?php echo $idPurcPembelian?>/generate" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
	<input type="hidden" name="idPurcPembelian" value="<?php echo $idPurcPembelian?>">
	<div class="col-lg-4">
		<div class="form-group">
			<label for="exampleInputEmail1">Nama Bahan</label>
			<select name="idMaterialist" class="form-control">
				<option>-- Pilih MaterialList --</option>
				<?php
				$lihat=$this->db->query("select * from md_materialist where isActive = 1")->result();
				foreach($lihat as $a):?>
					<option value="<?php echo $a->idMaterialist?>"><?php echo getrefNoMaterialist($a->idMaterialist)?></option>
				<?php endforeach;?>
			</select>
		</div>
	</div>	
	<div class="col-lg-4">
		<div class="form-group">
			<br>
			<button type="submit" class="btn btn-primary">Generate</button>
			<a href="<?php echo base_URL()?>purc_beli_request" class="btn btn-warning">Kembali</a>
		</div>
	</div>	
</form>
