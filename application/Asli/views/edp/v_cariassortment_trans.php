<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<hr>
<legend>Project No: <?php echo $head->project_no?> Lot: <?php echo $head->lot_number?></legend>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example"  style="font-size: 10px">
	<thead>
		<tr>
			<th style="text-align:right">No.</th>
			<th style="text-align:left">NIK</th>
			<th style="text-align:left">Nama</th>
			<th style="text-align:left">Nama Pakaian</th>
			<th style="text-align:left">Lokasi</th>
			<th style="text-align:left">Pejabat</th>
			<th style="text-align:left">Keterangan</th>
			<th style="text-align:right">Qty</th>
			<th style="text-align:left">Size</th>
		</tr>
	</thead>
	<tbody>
	<?php $i=1; foreach ($data as $b): ?>
  		<tr class="tabcont">
			<td style="text-align:right"><?php echo $i++;?></td>
			<td><?php echo $b->nik;?></td>
			<td><?php echo $b->nama;?></td>
			<td><?php echo $b->nama_pakaian;?></td>
			<td><?php echo $b->lokasi;?></td>
			<td><?php echo $b->jabatan;?></td>
			<td><?php echo $b->keterangan;?></td>
			<td style="text-align: right"><?php echo number_format($b->qty);?></td>
			<td><?php echo $b->size;?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>