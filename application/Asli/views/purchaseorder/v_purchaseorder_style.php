<head>
    meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
foreach ($data as $a):
  $idPo    			= $a->idPo;
  $tanggal 			= $a->tanggal;
  $refNo			= $a->refNo;
  $keterangan 		= $a->keterangan;
  $idClient 		= $a->idClient;
endforeach;
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b>Tanggal : <?php echo $tanggal?> | Ref.No : <?php echo $refNo?> | Buyer : <?php echo getClient($idClient)?></b>
                </div>
                <div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
					<form name="cabang" action="<?php echo base_URL()?><?php echo $aplikasi?>/tambah_det" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
							<div class="col-lg-4">
								<input type="hidden" name="idPo" value="<?php echo $idPo?>" />
								<div class="form-group">
									<label class="col-sm-12 control-label">Style&nbsp;&nbsp;&nbsp;<a href="#" onclick="searchStyle()"><span><i class="fa fa-search"></i></span></a></label>
									<input type="hidden" name="idStyle" id="idStyle" class="tfield" readonly />
									<input type="hidden" name="pict" id="pict" class="tfield" readonly />
									<input type="text" class="form-control" name="namaStyle" id="namaStyle" readonly>
								</div>	
						    </div>
							<div class="col-lg-2">
								<div class="form-group">
									<label for="exampleInputEmail1">Qty</label>
									<input type="text" class="form-control" name="qty" required>
								</div>
							</div>	
							<div class="col-lg-2">
								<div class="form-group">
									<label for="exampleInputEmail1">Size</label>
									<select name="size" class="form-control">
										<option value="P">Pengukuran</option>
										<option value="S">S</option>
										<option value="M">M</option>
										<option value="L">L</option>
										<option value="XL">XL</option>
									</select>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-primary">Simpan</button>
									<a href="<?php echo base_URL()?>mkt_purchaseorder" class="btn btn-warning">Kembali</a>
								</div>
							</div>	
                       </form>
					</div>
					<div class="row">
						<div class="panel-body">
							<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>No</th>
										<th>Style</th>
										<th>Size</th>
										<th>Qty</th>
										<th style="text-align: center">Hapus</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1;
									$detail = $this->db->query("select * from mkt_po_sty where idPo ='$idPo'")->result();
									foreach ($detail as $b): ?>
							  		<tr>
							  			<td><?php echo $i++;?></td>
							  			<td><?php echo getStyle($b->idStyle);?></td>
							  			<td><?php echo $b->size;?></td>
							  			<td><?php echo $b->qty;?></td>
							  			<td style="text-align: center">
										<?php if($b->isActive ==0){ ?>
											<form name="cabang" action="<?php echo base_URL()?>mkt_purchaseorder/hapus_style" method="post" accept-charset="utf-8" enctype="multipart/form-data">
												<input type="hidden" name="idPo" value="<?php echo $b->idPo ?>" />
												<input type="hidden" name= "idStyle" value="<?php echo $b->idStyle?>" />
												<input type="hidden" name= "size" value="<?php echo $b->size?>" />
												<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
											</form>	
										<?php } ?>	
										</td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchStyle(){
	openWindow('<?php echo base_URL()?>desg_request/cari','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedsStyle(idStyle, namaStyle, pict){   
    document.getElementById('idStyle').value = idStyle;
    document.getElementById('namaStyle').value = namaStyle;
	document.getElementById('pict').value = pict;
}
</script>