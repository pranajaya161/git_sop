<head>
    meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<?php
error_reporting(0);
$mode = $this->uri->segment(2);
if($mode =='edit')
{
		$submit = 'update';
	foreach($data as $x):
		$idPo 		= $x->idPo;
		$tanggal 	= $x->tanggal;
		$tglKirim	= $x->tglKirim;
		$noPo 		= $x->noPo;
		$idClient 	= $x->idClient;
		$noOr		= $x->noOr;
		$refNo		= $x->refNo;
		$keterangan = $x->keterangan;
		$amount		= $x->amount;
		$satuan 	= $x->satuan;
		$pcs 		= $x->pcs;
		$qty 		= $x->qty;
	endforeach;
} else {
		$submit = 'tambah';
		$idPo 		= 0;
		$tanggal 	= date('Y-m-d');
		$tglKirim	= date('Y-m-d');
		$noPo 		= "";
		$idClient 	= "";
		$noOr		= "";
		$refNo		= "";
		$keterangan = "";
		$amount		= "";
		$satuan 	= "";
		$pcs 		= "";
		$qty 		= "";
}
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b><?php echo $title?></b>
                </div>
                <div class="panel-body">
					<div class="row">
					<?php echo validation_errors('<font color=red>', '</font>'); ?>
					<?php
					if($this->session->flashdata('pesan')==TRUE):
						echo'<div class="alert alert-warning" role="alert">';
						echo $this->session->flashdata('pesan');
						echo "</div>";
					endif;
					?>
					<form name="cabang" action="<?php echo base_URL()?><?php echo $aplikasi?>/<?php echo $submit?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validasi_input(this)">
							<div class="col-lg-5">
								<input type="hidden" name="idPo" value="<?php echo $idPo?>" />
								<div class="form-group">
									<label>Tgl Po</label>
									<input class="form-control" name="tanggal" value="<?php echo $tanggal?>" id="input1" placeholder="Tgl.Target">
	                            </div>
	                            <div class="form-group">
									<label>Tgl tglKirim</label>
									<input class="form-control" name="tglKirim" value="<?php echo $tglKirim?>" id="input2" placeholder="Tgl.Target">
	                            </div>
	                             <div class="form-group">
									<label>Buyer</label>
									<select name="idClient" class="form-control">
										<?php if($idClient>0){ ?> 
											<option value="<?php echo $idClient?>"><?php echo getClient($idClient)?></option>
										<?php } ?>
										<option value="0">--- Pilih Buyer ---</option>
									<?php
									$lihat = $this->db->query("select * from tblclient where isActive=1 and idClient<>'$idClient'")->result();
									foreach ($lihat as $a) : ?>
										<option value="<?php echo $a->idClient?>"><?php echo getClient($a->idClient)?></option>
									<?php endforeach;?>
									</select>
	                            </div>
	                            <div class="form-group">
									<label>No.Po</label>
									<input class="form-control" name="noPo" value="<?php echo $noPo?>" placeholder="NO.Po" required>
	                            </div>
	                            <div class="form-group">
									<label>No.OR</label>
									<input class="form-control" name="noOr" value="<?php echo $noOr?>" placeholder="NO.OR" required>
	                            </div>
	                            <div class="form-group">
									<label class="col-sm-12 control-label">Prospecting&nbsp;&nbsp;&nbsp;<a href="#" onclick="searchProspecting()"><span><i class="fa fa-search"></i></span></a></label>
									<input type="hidden" name="idProspecting" id="idProspecting" class="tfield" readonly />
									<input type="hidden" name="namaClient" id="namaClient" class="tfield" readonly />
									<input type="hidden" name="xidClient" id="idClient" class="tfield" readonly />
									<input type="text" class="form-control" name="refNo" id="refNo" value="<?php echo $refNo?>" readonly>
								</div>
	                            <div class="form-group">
									<label>Keterangan</label>
									<textarea class="form-control" name="keterangan" rows="3"><?php echo $keterangan?></textarea>
                                </div>
	                        </div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="exampleInputEmail1">Jenis</label>
									<select name="jenis" class="form-control">
										<option value="Uniform">Uniform</option>
										<option value="Retail">Retail</option>
									</select>
								</div>
								<div class="form-group">
									<label>Qty</label>
									<input class="form-control" name="qty" value="<?php echo $qty?>" placeholder="Qty" required>
	                            </div>
	                            <div class="form-group">
									<label>Nominal</label>
									<input class="form-control" name="amount" value="<?php echo $amount?>" placeholder="Nominal" required>
	                            </div>
	                            <div class="form-group">
									<label>Satuan</label>
									<select name="satuan" class="form-control">
										<option value="PCS">PCS</option>
										<option value="Set">Set</option>
									</select>
	                            </div>
	                            <div class="form-group">
									<label>Unit</label>
									<input class="form-control" name="pcs" value="<?php echo $pcs?>" placeholder="Nominal" required>
	                            </div>
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-primary">Simpan</button>
									<a href="<?php echo base_URL()?><?php echo $aplikasi?>" class="btn btn-warning">Kembali</a>
								</div>
							</div>	
                       </form>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_URL()?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        $("#input1").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});
		 $("#input2").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth: true,
			changeYear: true
			});	
        });
</script>
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchProspecting(){
	openWindow('<?php echo base_URL()?>mkt_purchaseorder/cari_prospecting','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedProspecting(idProspecting, refNo, idClient, namaClient){   
    document.getElementById('idProspecting').value = idProspecting;
    document.getElementById('refNo').value = refNo;
	document.getElementById('idClient').value = idClient;
	document.getElementById('namaClient').value = namaClient;
}
</script>