<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
	<?php
		if($this->session->flashdata('pesan')==TRUE):
		echo'<div class="alert alert-warning" role="alert">';
		echo $this->session->flashdata('pesan');
		echo "</div>";
	endif;
	?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><?php echo $title?></p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th style="text-align:right">No.</th>
								<th style="text-align:left">Tanggal</th>
								<th style="text-align:left">Ref.No</th>
								<th style="text-align:left">Keterangan</th>
								<th style="text-align:center">Status</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach ($data as $a): ?>
							<tr class="odd gradeX">
								<td style="text-align:right"><?php echo $i++;?></td>
								<td><?php echo $a->tanggal;?></td>
								<td><a href="#" data-toggle="modal" data-target="#modal_view<?php echo $a->idMaterialist?>"><?php echo $a->refNo;?></a></td>
								<td><?php echo $a->keterangan;?></td>
								<td style="text-align: center"><?php echo $a->status;?></td>
							</tr>
						<?php endforeach;?>	
						</tbody>
					 </table>
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<?php foreach($data as $b): $idMaterialist = $b->idMaterialist; ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_view<?php echo $b->idMaterialist?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> View <?php echo $b->refNo?></h3>
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-striped table-condensed">
							<tr>
								<th style="text-align:left">Style</th>
								<th style="text-align:left">Bahan</th>	
								<th style="text-align:left">Qty [PO]</th>	
								<th style="text-align:left">Qty [Cons]</th>
								<th style="text-align:left">Qty [MT]</th>
								<th style="text-align:left">Satuan</th>
							</tr>
							<?php
							$detail = $this->db->query("select * from md_materialist_dt where idMaterialist='$idMaterialist'")->result();
							foreach($detail as $xx):?>
							<tr>
								<td><?php echo getStyle($xx->idStyle)?></td>
								<td><?php echo getBahan($xx->idBahan)?></td>
								<td><?php echo $xx->pcs?></td>
								<td><?php echo $xx->cons?></td>
								<td><?php echo number_format(($xx->pcs + $xx->cons),2)?></td>
								<td><?php echo getSatuan($xx->satuan)?></td>
							</tr>
							<?php endforeach;?>
						</table>	
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>
<!--- End Approve -->
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<!--
<script language="JavaScript" type="text/JavaScript">
 
 function ShowData()
 {
	 <?php
	 $query = $this->db->query("select idPo from edp_assortment_pros where isActive=1 and idMaterialist =0 group by idPo")->result();
	 foreach($query as $data){
	   $idPo = $data->idPo;
	   // membuat IF untuk masing-masing Group
	   echo "if (document.cabang.idPo.value == \"".$idPo."\")";
	   echo "{";

	   // membuat option kabupaten untuk masing-masing propinsi
	   $query2 = $this->db->query("select a.idStyle, b.namaStyle from edp_assortment_pros a inner join tblstyle b on b.idStyle = a.idStyle where a.idPo='$idPo' group by a.idStyle")->result();


	   $content = "document.getElementById('idStyle').innerHTML = \"";
	   foreach($query2 as $data2){
	       $content .= "<option value='".$data2->idStyle."'>".$data2->namaStyle."</option>";   
	   }
	   $content .= "\"";
	   echo $content;
	   echo "}\n";   
	 }
	?> 

 }
</script>  
-->