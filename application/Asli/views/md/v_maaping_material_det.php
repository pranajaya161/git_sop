<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_URL()?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_URL()?>dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_URL()?>dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_URL()?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_URL()?>js/jquery-ui.js"></script>
</head>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			&nbsp;
        </div>
    <!-- /.col-lg-12 -->
    </div>
	<?php echo validation_errors('<font color=red>', '</font>'); ?>
	<?php
		if($this->session->flashdata('pesan')==TRUE):
		echo'<div class="alert alert-warning" role="alert">';
		echo $this->session->flashdata('pesan');
		echo "</div>";
	endif;
	?>
	<?php
	error_reporting(0);
	foreach($data as $a):
		$idStyle 	= $a->idStyle;
	endforeach;
	?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><?php echo $title?></p>
                </div>
                        <!-- /.panel-heading -->
                <div class="panel-body">
					<?php echo form_open_multipart('maaping_material_det/tambah');?>
						<input type="hidden" name="idStyle" value="<?php echo $idStyle?>" />
							<div class="col-lg-3">
								<div class="form-group">
									<label class="col-sm-12 control-label">Bahan&nbsp;&nbsp;&nbsp;<a href="#" onclick="searchBahan()"><span><i class="fa fa-search"></i></span></a></label>
									<input type="hidden" name="idBahan" id="idBahan" class="tfield" readonly />
									<input type="text" class="form-control" name="namaBahan" id="namaBahan" readonly>
									<input type="hidden" class="form-control" name="pict" id="pict" readonly>
								</div>	
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label for="exampleInputEmail1">Cons</label>
									<input type="text" name="qtysat"  class="form-control" required />
								</div>	
							</div>
							<div class="col-lg-3">
								<label for="exampleInputEmail1">&nbsp;</label>
								<div class="form-group">	
									<input class="btn btn-primary" name="submit" type="submit" class="button" value="Simpan">
									<a href="<?php echo base_URL()?>md_maaping_material"><input  class="btn btn-warning" type="button" class="button" value="Kembali" ></a>
								 </div>
							</div>	  
						</form>	
						<hr>
						<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
							<thead>
								<tr>
									<th style="text-align:left">No</th>
									<th style="text-align:left">Bahan</th>
									<th style="text-align:left">Cons</th>				
									<th style="text-align:left">Satuan</th>
									<th style="text-align:center">Hapus</th>
								</tr>
							</thead>
							<tbody>
							<?php $i=1 ; foreach($detail as $a):  ?>
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo getBahan($a->idBahan)?></td>
									<td><?php echo $a->cons?></td>
									<td><?php echo getSatuan($a->satuan)?></td>
									<td class="tabtxt" style="text-align: center">
										<a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal_hapus<?php echo $a->idStyle?><?php echo $a->idBahan?>"><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>
							<?php endforeach; ?>	
							</tbody>
						</table>
                  <!-- /.table-responsive -->
                 </div>
			</div>
		</div>
	</div>
</div>	
<?php foreach($detail as $b): ?>
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_hapus<?php echo $b->idStyle?><?php echo $b->idBahan?>" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="panel panel-skin">
					<div class="panel-heading">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h3 class="panel-title"><span class="fa fa-user"></span> Hapus <?php echo $title?></h3>
					</div>
					<div class="panel-body">
					<?php echo form_open_multipart('maaping_material_det/hapus');?>
					<input type="hidden" name="idStyle" value="<?php echo $b->idStyle?>"/>
					<input type="hidden" name="idBahan" value="<?php echo $b->idBahan?>"/>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Data akan di kirim untuk di Hapus</label>
								</div>	
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<button type="submit" class="btn btn-success">Simpan</button>
									<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
								</div>	
							</div>	
						</div>	
					</form>
					</div>	
				</div>
		    </div>
		 </div>
	</div>
<?php endforeach;?>	
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_URL()?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_URL()?>vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_URL()?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_URL()?>vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_URL()?>dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="javascript">
var win = null;
function openWindow(mypage,myname,w,h,features) {
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings).focus();
}
function searchBahan(){
	openWindow('<?php echo base_URL()?>desg_request/cari_bahan','title',900,300, 'scrollbars=yes');
}
function fungsiKetikaPopupClosedBahan(idBahan, namaBahan, pict){   
    document.getElementById('idBahan').value = idBahan;
    document.getElementById('namaBahan').value = namaBahan;
	document.getElementById('pict').value = pict;
}
</script>
