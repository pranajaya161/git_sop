<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_query extends CI_Model {
	public function show_po()
	{
		$this->db->where('isActive', 1);
		return $this->db->get('mkt_po_sm')->result();
	}
	public function data_po($id)
	{
		$this->db->select('a.*, b.idStyle');
		$this->db->where('a.idPo', $id);
		$this->db->order_by('a.idStyle', 'ASC');
		$this->db->join('edp_assortment_pros b', 'b.id = a.id', 'inner');
		return $this->db->get('barcode a');
	}
	
}
