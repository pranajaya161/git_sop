<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_edp2 extends CI_Model {
	private $db2;
	private $db3;
	private $db4;
	public function __construct()
	{
	  parent::__construct();
			$this->db2 = $this->load->database('mega', TRUE);
			//$this->db3 = $this->load->database('assort', TRUE);
			$this->db4 = $this->load->database('assort2', TRUE);
	}
	public function show_edp_assort()
	{
		$this->db3->where('right(project_no,1)<>','X');
		$this->db3->where('is_data_completed', 'Y');
		$data = $this->db3->get("t_import_pengukuran_list");
		return $data->result();
	}
	public function show_edp_assort2()
	{
		$this->db4->where('right(project_no,1)<>','X');
		$this->db4->where('is_data_completed', 'Y');
		$data = $this->db4->get("t_import_pengukuran_list");
		return $data->result();
	}
	public function show_edp_assort_id($id)
	{
		$this->db3->where('import_id', $id);
		$data = $this->db3->get("t_import_pengukuran_list");
		return $data->result();
	}
	public function show_edp_assort_id_2($id)
	{
		$this->db4->where('import_id', $id);
		$data = $this->db4->get("t_import_pengukuran_list");
		return $data->result();
	}
	
	public function tambah_assortDetail($data)
	{
		$tambah=$this->db->insert('edp_assortment_dt',$data);
		return $tambah;
	}
	public function tambah_assortDetailHead($data)
	{
		$tambah=$this->db->insert('edp_assortment',$data);
		return $tambah;
	}
	public function show_edp_assort_trans()
	{
		$this->db->where('isActive', 0);
		$data = $this->db->get("edp_assortment");
		return $data->result();
	}
	public function show_edp_assort_trans_id($id)
	{
		$this->db->where('isActive', 0);
		$this->db->where('import_id', $id);
		$data = $this->db->get("edp_assortment");
		return $data->result();
	}
	public function show_edp_assort_apv()
	{
		$this->db->select('idPo, import_id, idStyle, lokasi, jabatan, count(*) as nom, tgl_proses, userId, info_tambahan');
		$this->db->where('isActive', 0);
		$this->db->group_by('idPo', 'ASC');
		$this->db->group_by('import_id', 'ASC');
		$this->db->group_by('idStyle', 'ASC');
		$data = $this->db->get("edp_assortment_pros");
		return $data->result();
	}
	public function tambah_approve($data)
	{
		$tambah=$this->db->insert('edp_assortment_apv',$data);
		return $tambah;
	}
	public function assort_edp_1($data)
	{
		$this->db3->where('a.import_id', $data);
		$this->db3->join('t_import_pengukuran b', 'b.import_id = a.import_id', 'inner');
		$data = $this->db3->get("t_import_pengukuran_list a");	
		return $data->result();

	}
	public function assort_edp_2($data)
	{
		$this->db4->where('a.import_id', $data);
		$this->db4->join('t_import_pengukuran b', 'b.import_id = a.import_id', 'inner');
		$data = $this->db4->get("t_import_pengukuran_list a");	
		return $data->result();

	}
	//=== TRans 
	public function show_edp_assort_trans_cari($id, $pc)
	{
		$this->db->where('a.import_id', $id);
		$this->db->where('a.pc', $pc);
		$data = $this->db->get("edp_assortment a");	
		return $data;	
	}
	public function assort_edp_trans_cari($id, $pc)
	{
		$this->db->where('a.import_id', $id);
		$this->db->where('a.info_tambahan', $pc);
		$data = $this->db->get("edp_assortment_dt a");	
		return $data->result();	
	}
	public function tambah_edp_assort_pros($data)
	{
		$tambah=$this->db->insert('edp_assortment_pros',$data);
		return $tambah;
	}
}
