<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_cutting extends CI_Model {
	//=== Input Cutting
	public function show_cutting_input()
	{
		$idDept = 7;
		$date = date('Y-m-d');
		$this->db->select('a.idWorksheet, b.*, c.startDate, c.endDate');
		$this->db->where('a.idDept', $idDept);
		$this->db->group_by('a.idWorksheet');
		$this->db->order_by('a.idWorksheet', 'DESC');
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('ppic_cutting c', 'c.idWorksheet = a.idWorksheet', 'inner');
		return $this->db->get('barcode a')->result();
	}
	public function show_cutting_input_id($id)
	{
		$this->db->where('a.idWorksheet', $id);
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$data = $this->db->get('ppic_cutting a');
		return $data->result();
	}
	public function show_cutting_input_ok($idWorksheet)
	{
		$this->db->select('a.*');
		$this->db->where('b.idWorksheet', $idWorksheet);
		$this->db->where('b.isActive', 0);
		$this->db->join('cutting_in b', 'b.id = a.id', 'inner');
		$this->db->join('barcode c', 'c.id = a.id', 'inner');
		$data = $this->db->get('edp_assortment_pros a');
		return $data->result();
	}
	public function cek_cutting_data($idWorksheet, $dari, $sampai)
	{
		$idDept = 7;
		$this->db->select('a.id, a.nourut');
		$this->db->where('a.idDept', $idDept);
		$this->db->where('a.nourut >=', $dari);
		$this->db->where('a.nourut <=', $sampai);
		$this->db->where('b.idWorksheet', $idWorksheet);
		$this->db->join('md_worksheet_dt b', 'b.id = a.id', 'inner');
		return $this->db->get('barcode a')->result();
	}
	public function tambah_detail($data)
	{
		$tambah=$this->db->insert('cutting_in',$data);
		return $tambah;
	}
	//== Apv Cutting
	public function show_cutting_input_apv()
	{
		$this->db->where('a.isActive', 0);
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt c', 'c.idWorksheet = a.idWorksheet and c.nourut = a.nourut and a.id = c.id', 'inner');
		$data = $this->db->get('cutting_in a');
		return $data->result();
	}
	// Stock Cutting
	public function show_cutting_input_stock_lap()
	{
		$this->db->select('*, a.id, a.status');
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt c', 'c.idWorksheet = a.idWorksheet and c.nourut = a.nourut and a.id = c.id', 'inner');
		$data = $this->db->get('cutting_in a');
		return $data->result();
	}
	public function show_cutting_input_stock()
	{
		$this->db->select('*, a.id');
		$this->db->where('a.isActive', 1);
		$this->db->where('a.status', 'Input');
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt c', 'c.idWorksheet = a.idWorksheet and c.nourut = a.nourut and a.id = c.id', 'inner');
		$data = $this->db->get('cutting_in a');
		return $data->result();
	}
	public function show_cutting_keluar_stock()
	{
		//$this->db->where('a.isActive', 1);
		$this->db->join('cutting_in b', 'b.id = a.id and b.nourut = a.nourut', 'inner');
		$this->db->join('md_worksheet c', 'c.idWorksheet = b.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt d', 'd.idWorksheet = b.idWorksheet and d.nourut = b.nourut and d.id = b.id', 'inner');
		$this->db->join('cutting_out_sm e', 'e.idCutting = a.idCutting', 'inner');
		$data = $this->db->get('cutting_out a');
		return $data->result();
	}
	//=== Cutting Keluar
	public function show_cutting_keluar_send($idCutting)
	{
		return $this->db->get_where('cutting_out', ['idCutting'=>$idCutting])->result();
	}
	public function show_cutting_keluar_input()
	{
		$status = array('Open','Reject');
		$this->db->where_in('status', $status);
		$data = $this->db->get('cutting_out_sm a');
		return $data->result();
	}
	public function tambah_keluar($data)
	{
		$tambah=$this->db->insert('cutting_out_sm',$data);
		return $tambah;
	}
	public function show_cutting_keluar_input_id($id)
	{
		$this->db->where('idCutting', $id);
		$data = $this->db->get('cutting_out_sm a');
		return $data;
	}
	public function show_cutting_keluar_input_detail($idWorksheet)
	{
		$idDept = 7;
		$this->db->select('a.*, b.nourut, b.id, c.idDept');
		$this->db->where('b.isActive', 1);
		$this->db->where('a.idDept', $idDept);
		$this->db->where('c.idWorksheet', $idWorksheet);
		$this->db->join('cutting_in b', 'b.id = a.id', 'inner');
		$this->db->join('barcode c', 'c.id = a.id', 'inner');
		$query = $this->db->get('edp_assortment_pros a')->result();
		return $query;

	}
	public function tambah_keluar_detail($data)
	{
		$tambah=$this->db->insert('cutting_out',$data);
		return $tambah;
	}
	//== Approve Cutting
	public function show_cutting_keluar_input_apv()
	{
		$status = array('Send');
		$this->db->where_in('status', $status);
		$data = $this->db->get('cutting_out_sm a');
		return $data->result();
	}
	public function tambah_cutting_keluar_apv($data)
	{
		$tambah=$this->db->insert('cutting_out_apv',$data);
		return $tambah;
	}
}
