<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_permintaan extends CI_Model {
	
	public function show_permintaan($id)
	{
		$this->db->where('isActive', 0);
		$this->db->where('idDept', $id);
		$data = $this->db->get("permintaan_bahan");
		return $data->result();
	}
	public function tambah($data)
	{	
		return $this->db->insert('permintaan_bahan',$data);
	}
	public function tambah_det($data)
	{
		return $this->db->insert('permintaan_bahan_dt',$data);
	}
	public function show_permintaan_input_id_apv($id)
	{
		$this->db->where('isActive', 99);
		$this->db->where('idDept', $id);
		$data = $this->db->get("permintaan_bahan");
		return $data->result();
	}
	public function tambah_approve($data)
	{
		return $this->db->insert('permintaan_bahan_apv', $data);
	}
	//== GUdang
	public function show_permintaan_gudang()
	{
		$this->db->where('isActive', 1);
		$this->db->where('tgl_terima', null);
		$data = $this->db->get("permintaan_bahan");
		return $data->result();
	}
	/*
	public function tambah_dept($data)
	{
		$tambah=$this->db->insert('purc_permintaan_sm',$data);
		return $tambah;
	}
	public function edit_dept($id, $data)
	{
		$this->db->where('idPurcPermintaan',$id);
		$update=$this->db->update('purc_permintaan_sm',$data);
		return $update;
	}
	public function tambah_detail_purc($data)
	{
		$tambah=$this->db->insert('purc_permintaan_dt',$data);
		return $tambah;
	}
	//=== Approve
	public function show_pend_input_id_apv($id)
	{
		$this->db->where('status', 'Send');
		$this->db->where('idDept', $id);
		$data = $this->db->get("purc_permintaan_sm");
		return $data->result();
	}
	public function show_pend_input_id_lap($id)
	{
		$this->db->where('idDept', $id);
		$data = $this->db->get("purc_permintaan_sm");
		return $data->result();
	}
	public function tambah_approve($data)
	{
		$tambah=$this->db->insert('purc_permintaan_apv',$data);
		return $tambah;
	}
	//=== pembelian Purchasing
	public function show_purchasing()
	{
		$status = array('Open','Reject');
		$this->db->where_in('status', $status);
		$data = $this->db->get("purc_pembelian_sm");
		return $data->result();
	}
	public function tambah_pembelian($data)
	{
		$tambah=$this->db->insert('purc_pembelian_sm',$data);
		return $tambah;
	}
	public function edit_pembelian($id, $data)
	{
		$this->db->where('idPurcPembelian',$id);
		$update=$this->db->update('purc_pembelian_sm',$data);
		return $update;
	}
	public function tambah_pembelian_detail_manual($data)
	{
		$tambah=$this->db->insert('purc_pembelian_dt',$data);
		return $tambah;
	}
	public function tambah_det_work($data)
	{
		$tambah=$this->db->insert('purc_pembelian_dt',$data);
		return $tambah;
	}
	//== Approval 
	public function show_purchasing_apv()
	{
		$this->db->where_in('status', 'Send');
		$data = $this->db->get("purc_pembelian_sm a");
		return $data->result();
	}
	public function tambah_approve_beli($data)
	{
		$tambah=$this->db->insert('purc_pembelian_apv',$data);
		return $tambah;
	}
	public function show_purchasing_lap()
	{
		$data = $this->db->get("purc_pembelian_sm");
		return $data->result();
	}
	*/
	
}
