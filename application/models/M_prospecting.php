<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_prospecting extends CI_Model {
	public function show_admin_act()
	{
		$this->db->order_by('idProspecting','ASC');
		$this->db->where('isActive',0);
		$data = $this->db->get("mkt_pros_sm");
		return $data->result();
	}
	//=== Admin Activity
	public function tambah_admin_act($data)
	{
		$tambah=$this->db->insert('mkt_pros_sm',$data);
		return $tambah;
	}
	public function edit_admin_act($id, $data)
	{
		$this->db->where('idProspecting',$id);
		$update=$this->db->update('mkt_pros_sm',$data);
		return $update;
	}
	public function show_admin_det($id)
	{
		$this->db->where('idProspecting', $id);
		$data = $this->db->get("mkt_pros_dt");
		return $data->result();
	}
	public function show_admin_head($id)
	{
		$this->db->where('idProspecting', $id);
		$data = $this->db->get("mkt_pros_sm");
		return $data->result();
	}
	public function tambah_admin_act_det($data)
	{
		$tambah=$this->db->insert('mkt_pros_dt',$data);
		return $tambah;
	}
	//=== Design Request
	public function show_design_req()
	{
		$this->db->select('a.idProspecting, b.tanggal, b.refNo, b.idClient, b.keterangan, b.userId, b.keterangan');
		$this->db->order_by('a.idProspecting','ASC');
		$this->db->where('a.status', 'Design');
		$this->db->where('a.isActive', 1);
		$this->db->join('mkt_pros_sm b', 'b.idProspecting = a.idProspecting', 'inner');
		$this->db->group_by('a.idProspecting');
		$data = $this->db->get("mkt_pros_dt a");
		return $data->result();
	}
	public function show_design_req_det($id)
	{
		$this->db->where('idProspecting', $id);
		$data = $this->db->get("mkt_pros_desgn");
		return $data->result();
	}
	public function show_design_req_bahan($id, $idStyle)
	{
		$this->db->where('idProspecting', $id);
		$this->db->where('idStyle', $idStyle);
		$data = $this->db->get("mkt_pros_desgn_bhn");
		return $data->result();
	}
	public function show_nourut_design($id)
	{
		$this->db->select('max(nourut) as nom');
		$this->db->where('idProspecting', $id);
		$this->db->from('mkt_pros_desgn');
		return $this->db->get();
	}
	public function tambah_upload_design($data)
	{
		$tambah=$this->db->insert('mkt_pros_desgn',$data);
		return $tambah;
	}
	public function tambah_design_bahan($data)
	{
		$tambah=$this->db->insert('mkt_pros_desgn_bhn',$data);
		return $tambah;
	}
	//=== Design Approve
	public function show_design_approve()
	{
		$this->db->select('a.idProspecting, b.tanggal, b.refNo, b.idClient, b.keterangan, a.isActive, a.userId');
		$this->db->order_by('a.idProspecting','ASC');
		$this->db->join('mkt_pros_sm b','b.idProspecting=a.idProspecting', 'inner');
		$this->db->group_by('a.idProspecting');
		$this->db->where('b.isDesign', 0);
		$data = $this->db->get("mkt_pros_desgn a");
		return $data->result();
	}
	public function tambah_design_approve($data)
	{
		$tambah=$this->db->insert('mkt_pros_desgn_apv',$data);
		return $tambah;
	}
	//== approveal Cust Design
	public function show_apv_cust()
	{
		$this->db->select('b.idProspecting, a.tanggal, a.refNo, a.idClient, a.idInformasi, a.keterangan, a.isApvCust, b.tgl_proses, b.userId, b.status');
		$this->db->where('a.isDesign', 1);
		$this->db->where('a.isApvCust', 0);
		$this->db->where('b.status', 0);
		$this->db->join('mkt_pros_desgn b','b.idProspecting = a.idProspecting', 'inner');
		$this->db->group_by('b.idProspecting');
		$data = $this->db->get("mkt_pros_sm a");
		return $data->result();
	}
	function updateStatusSty($id, $data)
	{
		$this->db->where('idProspecting',$id);
		$update=$this->db->update('mkt_pros_desgn',$data);
		return $update;
	}
	public function tambah_cust_apv($data)
	{
		$tambah=$this->db->insert('log_cust_design',$data);
		return $tambah;
	}
	//=== Upload Cost Calculation
	public function show_upl_cost()
	{
		$this->db->where('a.isDesign', 1);
		$this->db->where('a.isCostCalc', 0);
		$data = $this->db->get("mkt_pros_sm a");
		return $data->result();
	}
	public function tambah_upl_cost($data)
	{
		$tambah=$this->db->insert('mkt_pros_cost',$data);
		return $tambah;
	}
	//=== Upload Cost Approve
	public function show_upl_cost_apv()
	{
		$this->db->group_by('a.idProspecting');
		$this->db->where('a.isActive', 99);
		$this->db->where('b.isCostCalc', 0);
		$this->db->join('mkt_pros_sm b','b.idProspecting = a.idProspecting', 'inner');
		$data = $this->db->get("mkt_pros_cost a");
		return $data->result();
	}
	public function show_upl_cost_lap()
	{
		$this->db->group_by('a.idProspecting');
		$this->db->join('mkt_pros_sm b','b.idProspecting = a.idProspecting', 'inner');
		$data = $this->db->get("mkt_pros_cost a");
		return $data->result();
	}
	public function tambah_cost_apv($data)
	{
		$tambah=$this->db->insert('mkt_pros_cost_apv',$data);
		return $tambah;
	}
	//=== Sample
	public function show_samp_req()
	{
		$this->db->where('a.status',0);
		$data = $this->db->get("mkt_pros_sample a");
		return $data->result();
	}
	public function tambah_sample($data)
	{
		$tambah=$this->db->insert('mkt_pros_sample',$data);
		return $tambah;
	}
	public function show_sample_show($id)
	{
		$this->db->where('idSample', $id);
		$data = $this->db->get("mkt_pros_sample");
		return $data->result();
	}
	public function show_sample_bahan($id)
	{
		$this->db->where('idSample', $id);
		$data = $this->db->get("mkt_pros_sample_bhn");
		return $data->result();
	}
	public function show_sample_style($id)
	{
		$this->db->where('idSample', $id);
		$data = $this->db->get("mkt_pros_sample_sty");
		return $data->result();
	}
	public function tambah_sample_bahan($data)
	{
		$tambah=$this->db->insert('mkt_pros_sample_bhn',$data);
		return $tambah;
	}
	public function show_samp_input()
	{
		$this->db->select('a.*');
		$this->db->where('a.isActive', 0);
		$this->db->where('a.status', 0);
		$this->db->join('mkt_pros_sm b','b.idProspecting = a.idProspecting', 'inner');
		$data = $this->db->get("mkt_pros_sample a");
		return $data->result();
	}
	public function show_samp_input_apv()
	{
		$this->db->select('a.*');
		//$this->db->where('b.isSample', 0);
		$this->db->where('a.status',99);
		$this->db->join('mkt_pros_sm b','b.idProspecting = a.idProspecting', 'inner');
		$data = $this->db->get("mkt_pros_sample a");
		return $data->result();
	}
	public function tambah_sample_style($data)
	{
		$tambah=$this->db->insert('mkt_pros_sample_sty',$data);
		return $tambah;
	}
	public function tambah_sample_approve($data)
	{
		$tambah=$this->db->insert('mkt_pros_sample_apv',$data);
		return $tambah;
	}
	//=== Laporan Design
	public function show_desg_lap01()
	{
		$this->db->select('a.idProspecting, b.tanggal, b.refNo, b.idClient, b.keterangan, b.userId, b.keterangan');
		$this->db->order_by('a.idProspecting','ASC');
		$this->db->where('a.status', 'Design');
		$this->db->join('mkt_pros_sm b', 'b.idProspecting = a.idProspecting', 'inner');
		$this->db->group_by('a.idProspecting');
		$data = $this->db->get("mkt_pros_dt a");
		return $data->result();
	}
	public function show_smp_lap02()
	{
		$this->db->select('a.*');
		$this->db->join('mkt_pros_sm b','b.idProspecting = a.idProspecting', 'inner');
		$data = $this->db->get("mkt_pros_sample a");
		return $data->result();
	}
}
