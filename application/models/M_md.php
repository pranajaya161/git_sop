<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_md extends CI_Model {
	public function show_materialist()
	{
		$status = array('Open','Reject');
		$this->db->where_in('status', $status);
		$data = $this->db->get("md_materialist");
		return $data->result();
	}
	public function show_materialist_lap()
	{
		$data = $this->db->get("md_materialist");
		return $data->result();
	}
	public function show_materialist_apv()
	{
		$this->db->where('status', 'Send');
		$data = $this->db->get("md_materialist");
		return $data->result();
	}
	public function show_materialist_id($id)
	{
		$this->db->where('idMaterialist', $id);
		$data = $this->db->get("md_materialist");
		return $data->result();
	}
	public function tambah($data)
	{
		$tambah=$this->db->insert('md_materialist',$data);
		return $tambah;
	}
	public function tambah_approve($data)
	{
		$tambah=$this->db->insert('md_materialist_apv',$data);
		return $tambah;
	}
	public function tambah_detail($data)
	{
		$tambah=$this->db->insert('md_materialist_dt',$data);
		return $tambah;
	}
	public function edit($id, $data)
	{
		$this->db->where('idMaterialist',$id);
		$update=$this->db->update('md_materialist',$data);
		return $update;
	}
	//==== Worksheet
	public function show_worksheet()
	{
		$status = array('Open','Reject');
		$this->db->where_in('status', $status);
		$data = $this->db->get("md_worksheet");
		return $data->result();
	}
	public function show_worksheet_id($id)
	{
		$this->db->where('idWorksheet', $id);
		$data = $this->db->get("md_worksheet");
		return $data->result();
	}
	public function tambah_worksheet($data)
	{
		$tambah=$this->db->insert('md_worksheet',$data);
		return $tambah;
	}
	public function show_worksheet_apv()
	{
		$this->db->where('status', 'Send');
		$data = $this->db->get("md_worksheet");
		return $data->result();
	}
	public function show_worksheet_lap()
	{
		$data = $this->db->get("md_worksheet");
		return $data->result();
	}
	public function tambah_worksheet_det($data)
	{
		$tambah=$this->db->insert('md_worksheet_dt',$data);
		return $tambah;
	}
	public function tambah_apv_worksheet($data)
	{
		$tambah=$this->db->insert('md_worksheet_apv',$data);
		return $tambah;
	}
	public function edit_worksheet($id, $data)
	{
		$this->db->where('idWorksheet',$id);
		$update=$this->db->update('md_worksheet',$data);
		return $update;
	}
	function cariStyle($id, $lot)
	{
		$this->db->select('a.import_id, a.idStyle, a.size, count(*) as qty, b.lot_number'); 
		$this->db->where('a.idPo',$id);
		$this->db->where('b.lot_number',$lot);
		$this->db->where('a.isActive',1);
		$this->db->join('edp_assortment b', 'b.import_id = a.import_id', 'inner');
		$this->db->group_by('a.import_id', 'ASC');
		$this->db->group_by('a.idStyle', 'ASC');
		$this->db->group_by('a.size', 'ASC');
		$data = $this->db->get("edp_assortment_pros a");
		return $data->result();
	}
	
}
