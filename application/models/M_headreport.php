<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_headreport extends CI_Model {
    public function show_head($idWorksheet)
    {
        $this->db->where('b.idWorksheet', $idWorksheet);    
        $this->db->join('md_worksheet_dt b', 'b.id = a.id', 'inner');
        return $this->db->get('edp_assortment_pros a');
    }
}
