<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_employee extends CI_Model {
	public function show()
	{
		$this->db->order_by('isActive','ASC');
		$data = $this->db->get("employee");
		return $data->result();
	}
	public function show_edit($id)
	{
		$this->db->where('idEmployee', $id);
		$this->db->from('employee');
		return $this->db->get()->result();
	}
	public function tambah($data)
	{
		$tambah=$this->db->insert('employee',$data);
		return $tambah;
	}
	
	/*
	function record_count()
	{
		$this->db->select('*'); 
		$this->db->from('tblbank');
		$query = $this->db->get();
		return $query->num_rows();
	}
	function show(){
		$this->db->select('*'); 
		$this->db->from('tblbank');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result();
		} else {
			return array();
		}
    }  
	function add_bank($data)
	{
		$tambah=$this->db->insert('tblbank',$data);
		return $tambah;
	}
	function update($data, $id)
	{
		$this->db->where('idBank',$id);
		$update=$this->db->update('tblbank',$data);
		return $update;
	}
	*/
}