<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_sewing extends CI_Model {
	//=== Cutting
	public function show_id($id)
	{
		return $this->db->get_where('sewing_stock01', ['id'=>$id])->row();
	}
	public function show_sewing_penerimaan()
	{
		$idDept = 8;
		$this->db->where('idLine', $this->session->userdata('idLine'));
		$this->db->where('isActive', 0);
		$data = $this->db->get("ppic_sewing");
		return $data->result();
	}
	public function show_ppic_penerimaan($idPpicSewing)
	{
		return $this->db->get_where('ppic_sewing', ['idPpicSewing'=>$idPpicSewing])->result();
	}
	public function tambah_penerimaan($data)
	{
		$tambah=$this->db->insert('sewing_stock01',$data);
		return $tambah;
	}
	// Stock Sewing
	public function show_sewing_stock()
	{
		return $this->db->get_where('sewing_stock02', ['isActive'=>0])->result();
	}
	//== Stock Sewing Penerimaan
	public function show_sewing_stock_penerimaan()
	{
		//$this->db->where('isActive', 0);
		$data = $this->db->get("sewing_stock01");
		return $data->result();
	}
	//=== Input Sewing
	public function show_sewing_input()
	{
		$isAcive = [0,99];
		$this->db->select('a.idPpicSewing, a.idWorksheet, min(a.tgl_proses) as tgl, a.idLine');
		$this->db->where('idLine', $this->session->userdata('idLine'));
		$this->db->where_in('a.isActive', $isAcive);
		$this->db->group_by('a.idPpicSewing');
		$data = $this->db->get("sewing_stock01 a");
		return $data->result();
	}
	public function show_sewing_input_id($id)
	{
		$this->db->select('idPpicSewing, nourut, idWorksheet, idLine, max(tgl_proses) as tgl');
		$this->db->where('idPpicSewing', $id);
		$this->db->group_by('idPpicSewing');
		$data = $this->db->get("sewing_stock01");
		return $data->result();
	}
	public function tambah_sewing_input($data)
	{
		$tambah=$this->db->insert('sewing_in',$data);
		return $tambah;
	}
	public function cek_input_sewing_add($idPpicSewing, $idWorksheet, $numbering)
	{
		$this->db->where('idPpicSewing', $idPpicSewing);
		$this->db->where('idWorksheet', $idWorksheet);
		$this->db->where('nourut', $numbering);
		return $this->db->get('sewing_stock01')->row();
	}
	public function cek_input_sewing_kirim($idPpicSewing)
	{
		$this->db->where('isActive', 0);
		$this->db->where('idPpicSewing', $idPpicSewing);
		return $this->db->get('sewing_in');
	}
	public function show_sewing_input_detail($idPpicSewing, $idWorksheet)
	{
		$this->db->where('a.isActive', 99);
		$this->db->where('a.idWorksheet',$idWorksheet);
		$this->db->where('a.idPpicSewing',$idPpicSewing);
		$this->db->join('edp_assortment_pros b', 'b.id = a.id', 'inner');
		return $this->db->get('sewing_stock01 a')->result();
	}
	public function show_sewing_stock_input()
	{
		$isAcive = [1];
		$this->db->where_in('isActive', $isAcive);
		return $this->db->get('sewing_in')->result();
	}

	
	//== Approval
	public function show_sewing_input_apv()
	{
		$this->db->where('a.isActive', 99);
		$this->db->join('barcode b', 'b.id = a.id', 'inner');
		$data = $this->db->get('sewing_in a');
		return $data->result();
	}
	public function tambah_stock02($data)
	{
		$tambah=$this->db->insert('sewing_stock02',$data);
		return $tambah;
	}
	//==== Stock
	public function show_sewing_input_stock()
	{
		$this->db->select('*, a.id');
		$this->db->where('a.isActive', 1);
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt c', 'c.idWorksheet = a.idWorksheet and c.nourut = a.nourut and a.id = c.id', 'inner');
		$data = $this->db->get('sewing_in a');
		return $data->result();
	}
	//=== Sewing Keluar
	public function show_sewing_keluar_input()
	{
		$this->db->where_in('isActive', 0);
		$data = $this->db->get('sewing_out_sm a');
		return $data->result();
	}
	public function cek_sewing_keluar_id($id)
	{
		return $this->db->get_where('sewing_stock02', ['id'=>$id])->row();
	}
	public function cek_sewing_keluar_idSewing($idSewing)
	{
		$this->db->where('idSewing', $idSewing);
		return $this->db->get_where('sewing_out')->result();
	}
	public function tambah_keluar($data)
	{
		$tambah=$this->db->insert('sewing_out_sm',$data);
		return $tambah;
	}
	public function show_sewing_keluar_input_id($id)
	{
		$this->db->where('idSewing', $id);
		$data = $this->db->get('sewing_out_sm a');
		return $data->result();
	}
	public function tambah_keluar_detail($data)
	{
		$tambah=$this->db->insert('sewing_out',$data);
		return $tambah;
	}
	// === Approve
	public function show_sewing_keluar_input_apv()
	{
		$this->db->where('a.isActive', 99);
		$this->db->join('barcode b', 'b.id = a.id', 'inner');
		$data = $this->db->get('sewing_out a');
		return $data->result();

	}
	public function tambah_stock03($data)
	{
		$tambah=$this->db->insert('sewing_stock03',$data);
		return $tambah;
	}
	public function tambah_sewing_keluar_apv($data)
	{
		$tambah=$this->db->insert('sewing_out_apv',$data);
		return $tambah;
	}
	public function show_sewing_stock_keluar()
	{
		return $this->db->get_where('sewing_stock03', ['isActive'=>0])->result();
	}
	//== Stock Keluar Sewing
	public function show_sewing_keluar_stock()
	{
		//$this->db->where('a.isActive', 1);
		$this->db->join('cutting_in b', 'b.id = a.id and b.nourut = a.nourut', 'inner');
		$this->db->join('md_worksheet c', 'c.idWorksheet = b.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt d', 'd.idWorksheet = b.idWorksheet and d.nourut = b.nourut and d.id = b.id', 'inner');
		$data = $this->db->get('sewing_out a');
		return $data->result();
	}
}
