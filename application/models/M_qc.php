<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_qc extends CI_Model {
	public function show_id($id)
	{
		return $this->db->get_where('qc_stock01', ['id'=>$id])->row();
	}
	//=== Cutting
	public function show_qc_penerimaan()
	{
		$idDept = 10;
		$date = date('Y-m-d');
		$this->db->where('startDate <=', $date);
		$this->db->where('isActive', 0);
		$data = $this->db->get("ppic_qc");
		return $data->result();
	}
	public function tambah_penerimaan($data)
	{
		$tambah=$this->db->insert('qc_stock01',$data);
		return $tambah;
	}
	//==== Stock
	public function show_qc_stock_penerimaan()
	{
		$data = $this->db->get("qc_stock01");
		return $data->result();
	}
	public function show_qc_stock_input()
	{
		$data = $this->db->get("qc_in");
		return $data->result();
	}
	//=== Input Qc
	public function show_qc_input()
	{
		$isAcive = [0];
		$this->db->select('a.idPpicQc, a.idWorksheet, min(a.tgl_proses) as tgl');
		$this->db->where_in('a.isActive', $isAcive);
		$this->db->group_by('a.idPpicQc');
		$data = $this->db->get("qc_stock01 a");
		return $data->result();
	}
	public function show_qc_input_id($id)
	{
		$this->db->select('idPpicQc, idWorksheet,  max(tgl_proses) as tgl');
		$this->db->where('isActive', 0);
		$this->db->where('idPpicQc', $id);
		$this->db->group_by('idPpicQc');
		$data = $this->db->get("qc_stock01");
		return $data->result();
	}
	public function tambah_qc_input($data)
	{
		$tambah=$this->db->insert('qc_in',$data);
		return $tambah;
	}
	public function tambah_qc_retur($data)
	{
		$tambah=$this->db->insert('qc_reture',$data);
		return $tambah;
	}
	//== Approval
	public function show_qc_input_apv()
	{
		$this->db->where('a.isActive', 0);
		$this->db->join('barcode b', 'b.id = a.id', 'inner');
		$data = $this->db->get('qc_in a');
		return $data->result();
	}
	public function tambah_stock02($data)
	{
		$tambah=$this->db->insert('qc_stock02',$data);
		return $tambah;
	}
	//==== Stock
	public function show_qc_input_stock()
	{
		$this->db->select('*, a.id');
		$this->db->where('a.isActive', 1);
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('md_worksheet_dt c', 'c.idWorksheet = a.idWorksheet and c.nourut = a.nourut and a.id = c.id', 'inner');
		$data = $this->db->get('qc_in a');
		return $data->result();
	}
	//=== Keluar
	public function show_qc_keluar_input()
	{
		$status = array('Open','Reject');
		$this->db->where_in('status', $status);
		$data = $this->db->get('qc_out_sm a');
		return $data->result();
	}
	public function cek_qc_keluar_id($id)
	{
		return $this->db->get_where('qc_stock02', ['id'=>$id])->row();
	}
	public function cek_qc_keluar_idQc($idQc)
	{
		$this->db->where('idQc', $idQc);
		return $this->db->get_where('qc_out')->result();
	}
	public function tambah_keluar($data)
	{
		$tambah=$this->db->insert('qc_out_sm',$data);
		return $tambah;
	}
	public function show_qc_keluar_input_id($id)
	{
		// $this->db->where('idQc', $id);
		// $data = $this->db->get('qc_out_sm a');
		// return $data->result();
		$this->db->where_in('isActive', 0);
		$data = $this->db->get('qc_out_sm a');
		return $data->result();
	}
	public function tambah_keluar_detail($data)
	{
		$tambah=$this->db->insert('qc_out',$data);
		return $tambah;
	}
	//== Stock Keluar Sewing
	public function show_qc_keluar_stock()
	{
		//$this->db->where('a.isActive', 1);
		// $this->db->join('triming_in b', 'b.id = a.id and b.nourut = a.nourut', 'inner');
		// $this->db->join('md_worksheet c', 'c.idWorksheet = b.idWorksheet', 'inner');
		// $this->db->join('md_worksheet_dt d', 'd.idWorksheet = b.idWorksheet and d.nourut = b.nourut and d.id = b.id', 'inner');
		// $data = $this->db->get('qc_out a');
		// return $data->result();
		//$this->db->join('barcode b', 'b.id = a.id', 'inner');
		$this->db->join('qc_stock01 b', 'b.id = a.id', 'inner');
		$data = $this->db->get("qc_stock03 a");
		return $data->result();

	}
	// === Approve
	public function show_qc_keluar_input_apv()
	{
		$this->db->where('a.isActive', 99);
		$this->db->join('barcode b', 'b.id = a.id', 'inner');
		$data = $this->db->get('qc_out a');
		return $data->result();
	}
	public function tambah_stock03($data)
	{
		$tambah=$this->db->insert('qc_stock03',$data);
		return $tambah;
	}
	public function tambah_qc_keluar_apv($data)
	{
		$tambah=$this->db->insert('qc_out_apv',$data);
		return $tambah;
	}
}
