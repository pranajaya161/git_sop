<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_modul extends CI_Model {
	function show(){
		$this->db->select('*'); 
		$this->db->from('tblmodul_adm');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result();
		} else {
			return array();
		}
    }
	public function fetch_data($limit, $offset, $cari)
	{
		if (!empty($cari)) {
			$this->db->like('keterangan', $cari);
		}
		$this->db->limit($limit, $offset);
		$data = $this->db->get("tblmodul_adm");
		return $data->result();
 
	}
	function record_count()
	{
		$this->db->select('*'); 
		$this->db->from('tblmodul_adm');
		$query = $this->db->get();
		return $query->num_rows();
	}	
	function add($data)
	{
		$tambah=$this->db->insert('tblmodul_adm',$data);
		return $tambah;
	}
	function update($data, $id)
	{
		$this->db->where('idModul',$id);
		$update=$this->db->update('tblmodul_adm',$data);
		return $update;
	}
	function hapusList($id)
	{
		$this->db->where('idModul',$id);
		$hapus=$this->db->delete('tblmodul_list_adm');
		return $hapus;
	}
	function add_list($data)
	{
		$tambah=$this->db->insert('tblmodul_list_adm',$data);
		return $tambah;
	}
}
