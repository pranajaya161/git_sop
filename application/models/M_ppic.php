<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_ppic extends CI_Model {
	//=== Cutting
	public function show_ppic_cutting()
	{
		$idDept = 6;
		$this->db->select('a.idWorksheet, b.*');
		$this->db->where('a.idDept', $idDept);
		$this->db->where('b.isActive', 1);
		$this->db->group_by('a.idWorksheet');
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		return $this->db->get('barcode a')->result();
	}
	public function show_ppic_lap01()
	{
		$this->db->select('a.idWorksheet, b.*, c.startDate, c.endDate');
		$this->db->group_by('a.idWorksheet');
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('ppic_cutting c', 'c.idWorksheet = a.idWorksheet', 'inner');
		return $this->db->get('barcode a')->result();
	}
	public function show_ppic_lap02()
	{
		$this->db->select('a.idWorksheet, b.*, c.startDate, c.endDate');
		$this->db->group_by('a.idWorksheet');
		$this->db->join('md_worksheet b', 'b.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('ppic_sewing c', 'c.idWorksheet = a.idWorksheet', 'inner');
		$this->db->join('cutting_out d', 'd.id = a.id', 'inner');
		return $this->db->get('barcode a')->result();
	}
	public function tambah($data)
	{
		$tambah=$this->db->insert('ppic_cutting',$data);
		return $tambah;
	}
	//=== Sewing
	public function show_cutting($idCutting)
	{
		return $this->db->get_where('cutting_out', ['idCutting'=>$idCutting])->result();
	}
	public function show_ppic_sewing()
	{
		$idDept = 6;
		$this->db->where('a.idDept', $idDept);
		$this->db->where('b.isActive', 1);
		$this->db->group_by('a.idWorksheet, b.idCutting');
		$this->db->join('cutting_out_sm b', 'b.idWorksheet = a.idWorksheet', 'inner');
		return $this->db->get('barcode a')->result();
	}
	public function tambah_sewing($data)
	{
		$tambah=$this->db->insert('ppic_sewing',$data);
		return $tambah;
	}
	//== triming
	public function show_sewing($idSewing)
	{
		return $this->db->get_where('sewing_out', ['idSewing'=>$idSewing])->result();
	}
	public function show_ppic_triming()
	{
		$idDept = 6;
		$this->db->where('a.idDept', $idDept);
		$this->db->where('b.isActive', 99);
		$this->db->group_by('a.idWorksheet, b.idSewing');
		$this->db->join('sewing_out_sm b', 'b.idWorksheet = a.idWorksheet', 'inner');
		return $this->db->get('barcode a')->result();
	}
	public function tambah_triming($data)
	{
		$tambah=$this->db->insert('ppic_triming',$data);
		return $tambah;
	}
	//=== QC
	public function show_triming($idTriming)
	{
		return $this->db->get_where('triming_out', ['idTriming'=>$idTriming])->result();
	}
	public function show_ppic_qc()
	{
		$idDept = 6;
		$this->db->where('a.idDept', $idDept);
		$this->db->where('b.isActive', 99);
		$this->db->group_by('a.idWorksheet, b.idTriming');
		$this->db->join('triming_out_sm b', 'b.idWorksheet = a.idWorksheet', 'inner');
		return $this->db->get('barcode a')->result();
	}
	public function tambah_qc($data)
	{
		$tambah=$this->db->insert('ppic_qc',$data);
		return $tambah;
	}
	//=== Packing
	public function show_ppic_packing()
	{
		$idDept = 6;
		$this->db->where('a.idDept', $idDept);
		$this->db->where('b.isActive', 99);
		$this->db->group_by('a.idWorksheet, b.idQc');
		$this->db->join('qc_out_sm b', 'b.idWorksheet = a.idWorksheet', 'inner');
		return $this->db->get('barcode a')->result();
	}
	public function tambah_packing($data)
	{
		$tambah=$this->db->insert('ppic_packing',$data);
		return $tambah;
	}
	public function show_qc($idQc)
	{
		return $this->db->get_where('qc_out', ['idQc'=>$idQc])->result();
	}
}
