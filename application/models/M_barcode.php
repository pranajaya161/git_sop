<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_barcode extends CI_Model {
    public function show_id($id)
    {
        return $this->db->get_where('barcode', ['id'=>$id])->row();
    }
    public function tambah_hist($data)
    {
        return $this->db->insert('barcode_hist', $data);
    }
    public function show_agremeent($id)
    {
        $this->db->select('a.*, b.idWorksheet, b.idSewing, b.idTriming, b.idQc');
        $this->db->where('a.id', $id);
        $this->db->join('barcode b', 'b.id = a.id', 'inner');
        return $this->db->get('edp_assortment_pros a')->row();
    }
}
